/*####################################################################################
File that contains the necessary functions to store/modify the plays into the database
####################################################################################*/

// Defining callback methods to save/remove/update data from the databases
function setup_data_savers(){

  // method for saving new team after substitutions
  teams_db.updateCourtPlayers = function(team) {

    var store =  teams_db.result.transaction("Teams", "readwrite").objectStore("Teams");
    var teamID = team.teamID;

    // removing old team data and adding updated one
    try {
      var request_delete = store.delete(teamID);
      request_delete.onsuccess = function(e) {
      };
      request_delete.onerror = function(e) {
        console.error("Error Removing Team: ", e);
      };
    } catch (e) {
    }

    var request_add = store.put(team);
    request_add.onsuccess = function(e) {
    };
    request_add.onerror = function(e) {
      console.error("Error Adding Team: ", e);
    };
  };


  // method for saving play in database
  plays_db.savePlayInDatabase = function(play){

    var store =  plays_db.result.transaction("Plays", "readwrite").objectStore("Plays");
    var playID = play.id;

    // removing old play data and adding updated one
    try{
      var request_delete = store.delete(playID);
      request_delete.onsuccess = function(e) {
      };
      request_delete.onerror = function(e) {
        console.error("Error Removing Play: ", e);
      };
    } catch (e) {
    }

    console.log(play)
    var request_add = store.put(play);
    request_add.onsuccess = function(e) {
      var snapshots = play.snapshots
      for( i=0; i<snapshots.length;i++ ){
        cur_snapshot = snapshots[i]
        snapshots_db.saveSnapshotsInDatabase(cur_snapshot)
      }
    };
    request_add.onerror = function(e) {
      console.error("Error Adding Play: ", e);
    };
  };


  // method for saving snapshots in database. Called in the OnSuccess of the savePlayInDatabase()
  snapshots_db.saveSnapshotsInDatabase = function(snapshot){

    var store =  snapshots_db.result.transaction("Snapshots", "readwrite").objectStore("Snapshots");
    var snapshotIdx = snapshot.snapshotIdx;

    // removing old play data and adding updated one
    try{
      var request_delete = store.delete(snapshotIdx);
      request_delete.onsuccess = function(e) {
      };
      request_delete.onerror = function(e) {
        console.error("Error Removing Snapshot: ", e);
      };
    } catch (e) {
    }

    var request_add = store.put(snapshot);
    request_add.onsuccess = function(e) {
      var moves = snapshot.moves
      var playName = snapshot.playName
      for( i=0; i<moves.length;i++ ){
        cur_move = moves[i]
        moves_db.saveMovesInDatabase(cur_move, playName)
      }
    };
    request_add.onerror = function(e) {
      console.error("Error Adding Snapshot: ", e);
    };
  };


  // method for saving moves in database. Called in the OnSuccess of the saveSnapshotsInDatabase()
  moves_db.saveMovesInDatabase = function(move, playName){

    var store =  moves_db.result.transaction("Moves", "readwrite").objectStore("Moves");
    var moveIdx = snapshot.moveIdx;

    move.playName = playName

    // removing old play data and adding updated one
    try {
      var request_delete = store.delete(moveIdx);
      request_delete.onsuccess = function(e) {
      };
      request_delete.onerror = function(e) {
        console.error("Error Removing Move: ", e);
      };
    } catch (e) {
    }

    var request_add = store.put(move);
    request_add.onsuccess = function(e) {
    };
    request_add.onerror = function(e) {
      console.error("Error Adding Move: ", e);
    };
  };


  // method for deleting a play from the database given the PlayName
  plays_db.deletePlayGivenName = function(playName){

    var store =  plays_db.result.transaction("Plays", "readwrite").objectStore("Plays");

    // searching for the play with the corresponding playName
    playID = -1
    idx = -1
    for(i=0; i<plays.length; i++){
      var cur_play = plays[i];
      if(cur_play.playName == playName){
        playID = cur_play.id
        idx = i
        break
      }
    }
    if(playID == -1){
      console.error("Error Removing Play: ");
      return
    }

    // removing snapshots and moves associated to the given play
    snapshots = plays[i].snapshots
    for(i=0; i<snapshots.length; i++){
      cur_snapshot = snapshots[i]
      snapshots_db.deleteSnapshot(cur_snapshot)
    }

    // removing play as requested
    var request_delete = store.delete(playID);
    request_delete.onsuccess = function(e) {
      console.log("Success Removing Play: ", e);
    };
    request_delete.onerror = function(e) {
      console.error("Error Removing Play: ", e);
    };

}


  // method for deleting a snapshot from the database
  snapshots_db.deleteSnapshot = function(snapshot){

    var store =  snapshots_db.result.transaction("Snapshots", "readwrite").objectStore("Snapshots");
    var snapshotIdx = snapshot.snapshotIdx;

    // remove moves associated to the current snapshot
    var moves= snapshot.moves
    for(i=0; i<moves.length; i++){
      cur_move = moves[i]
      moves_db.deleteMove(cur_move)
    }

    // removing requested snapshot
    var request_delete = store.delete(snapshotIdx);
    request_delete.onerror = function(e) {
      console.error("Error Removing Snapshot: ", e);
    };
  }


  // method for deleting a moves from the database
  moves_db.deleteMove = function(move){

    var store =  moves_db.result.transaction("Moves", "readwrite").objectStore("Moves");
    var moveIdx = move.moveIdx;

    // removing requested move
    var request_delete = store.delete(moveIdx);
    request_delete.onerror = function(e) {
      console.error("Error Removing Snapshot: ", e);
    };
  }

}


// DEPRECATED
/////////////////////////////////////////////
//method that saves a tactic in the database
function savePlayInDatabase( play ){

  writeConsoleLog("savePlayInDatabase")

  //first we save the general information of the tactic
  var db = openDatabase('timeOutDB', '1.0', 'Database with Game, Team and Player data', 5000 * 1024 * 1024);

  db.transaction(function (tx) {
    tx.executeSql("INSERT INTO Plays (id, playName, type, favorite, snapshots, offensiveStrategy, defensiveStrategy) VALUES ('"+play.id+"', '"+play.playName+"', '"+play.type+"','" + play.favorite + "', '', '"+play.offensiveStrategy+"', '"+play.defensiveStrategy+"') ")
  });


  //the we save the snapshots with its correspondent moves
  snapshots = play.snapshots

  db.transaction(function (tx) {

    for( i=0; i<snapshots.length;i++ ){
      //inserting snapshot
      actualSnapshot = snapshots[i]
      tx.executeSql("INSERT INTO Snapshots (snapshotNumber, playName, playerWithBall, moves) VALUES ("+actualSnapshot.snapshotNumber+", '"+actualSnapshot.playName+"', '"+actualSnapshot.playerWithBall+"', '') ")

      //inserting moves for the given snapshot
      moves = snapshots[i].moves

      playName = actualSnapshot.playName
      for( j=0; j<moves.length; j++ ){
        actualMove = moves[j]

        //converting the positions to a string
        pos2save = actualMove.positions
        pos2save = pos2save.join("%%")

        tx.executeSql("INSERT INTO Moves (position, playName, snapshotNumber, positions) VALUES ('"+actualMove.position+"', '"+playName+"', "+actualMove.snapshotNumber+", '"+pos2save+"') ")
      }
    }

  });

  writeConsoleLog("Play data has been saved in the database")

}


// DEPRECATED
/////////////////////////////////////////////////////////////////
//method that saves the new courtPlayers of a team after a change
function updateCourtPlayersOnDatabase(team){

  var teamID = team.teamID;
  var serializedCourtPlayers = serializeCourtPlayers( team.courtPlayers );

  var db = openDatabase('timeOutDB', '1.0', 'Database with Game, Team and Player data', 5000 * 1024 * 1024);

  //updating the given team
  writeConsoleLog(teamID)
  writeConsoleLog( team.courtPlayers )
  writeConsoleLog(serializedCourtPlayers)

  db.transaction(function (tx) {
    tx.executeSql("UPDATE Teams SET courtPlayers='" + serializedCourtPlayers + "' WHERE teamID='" + teamID + "' ")
  });

  writeConsoleLog("updated database")
  return;

}


// TODO
/////////////////////////////////////////////////////////////////
//method that updates a team on the database
function updateTeamDataOnDatabase( team ){

  var teamID = team.teamID;
  var db = openDatabase('timeOutDB', '1.0', 'Database with Game, Team and Player data', 5000 * 1024 * 1024);

  //updating the given team
  writeConsoleLog("Updating team in database")
  writeConsoleLog( team )

  /*db.transaction(function (tx) {
    tx.executeSql("UPDATE Teams SET teamName='" + team.teamName + "' WHERE teamID='" + teamID + "' ")
  });*/


  db.transaction(function (tx) {
    tx.executeSql("UPDATE Teams SET teamName='" + team.teamName + "', filledColorA='" + team.filledColorA + "', \
    filledColorB='" + team.filledColorB + "', lineColorA='" + team.lineColorA + "', lineColorB='" + team.lineColorB + "', \
    textColorA='" + team.textColorA + "' WHERE teamID='" + teamID + "' ")
  });

}


// TODO
/////////////////////////////////////////////////////////////////
//method that updates a player on the database
function updatePlayerDataOnDatabase( player ){

  var playerID = player.playerID;
  var db = openDatabase('timeOutDB', '1.0', 'Database with Game, Team and Player data', 5000 * 1024 * 1024);

  //updating the given team
  writeConsoleLog("Updating player in database")
  writeConsoleLog( player )

  db.transaction(function (tx) {
    tx.executeSql("UPDATE Players SET number='" + player.number + "', playerNameLong='" + player.playerNameLong + "', \
    playerNameShort='" + player.playerNameShort + "', validPositions='" + player.validPositions + "' \
    WHERE playerID='" + playerID + "' ")
  });


}


// TODO
/////////////////////////////////////////////////////////////////
//method that adds a new player to the database and to the team
function addPlayerDataOnDatabase( team, player ){

  var playerID = player.playerID;
  var db = openDatabase('timeOutDB', '1.0', 'Database with Game, Team and Player data', 5000 * 1024 * 1024);

  //updating the given team
  writeConsoleLog("Adding player in database")
  writeConsoleLog( player )

  db.transaction(function (tx) {

    tx.executeSql("INSERT INTO Players (playerID, team, number, playerNameLong, playerNameShort, validPositions, \
       fieldPosX, fieldPosY, displayPosX, displayPosY, starter) VALUES \
       ('"+player.playerID+"', '"+player.team+"', '"+player.number.toString()+"', '"+player.playerNameLong+"', \
       '"+player.playerNameShort+"', '"+player.validPositions+"', '0', '0', '0', '0', '0')")

  });


}


// TODO
/////////////////////////////////////////////////////////////////
//method that removes a player from the database and from the team
function removePlayerFromDatabase( team, player ){

  var playerID = player.playerID;
  var db = openDatabase('timeOutDB', '1.0', 'Database with Game, Team and Player data', 5000 * 1024 * 1024);

  //updating the given team
  writeConsoleLog("Removing player from database")
  writeConsoleLog( player )

  db.transaction(function (tx) {

    tx.executeSql("DELETE FROM Players WHERE playerID='" + playerID + "'")
    updateTeamDataOnDatabase( team )

  });

  return

}


// TODO
/////////////////////////////////////////////////////////////////
//method that updates the image of a player or team on the database
function save_image_on_database(table, image_name, id){

  var db = openDatabase('timeOutDB', '1.0', 'Database with Game, Team and Player data', 5000 * 1024 * 1024);

  // updating image given team or player
  db.transaction(function (tx) {
    if(table=="players"){
      console.log("updating player img on database")
      tx.executeSql("UPDATE Players SET image_path='" + image_name + "'\
      WHERE playerID='" + id + "' ")
    }else if(table=="teams"){
      console.log("updating team img on database")
      tx.executeSql("UPDATE Teams SET image_path='" + image_name + "'\
      WHERE teamID='" + id + "' ")
    }
  });

}


//
