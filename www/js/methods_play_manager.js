
/////////////////////////////////////////////////////////////////
//method that copies the progress of a tactic to allow editing
function copy_tactic(){

  //deep copying actual play into the auxiliar object
  auxPlay = JSON.parse(JSON.stringify(actualPlay))
  auxSnapshots = JSON.parse(JSON.stringify(actualPlay.snapshots))
  auxPlay.id = allPlays[allPlays.length-1].id+1

  //keeping only the snapshots up to the moment
  auxSnapshots = auxSnapshots.slice(0, actualSnapshotIndex);
  auxPlay.snapshots = auxSnapshots
  auxPlay.favorite = "false"
  auxPlay.name = "aux"

  //setting as actual play
  actualPlay = auxPlay

  var newData = initializeEmptySnapshot()
  actualSnapshot = newData.newSnapshot
  actualMoves = newData.newMoves


}



/////////////////////////////////////////////////////////////////
//method that initializes a semi-empty snapshot for after undoing
function initializeEmptySnapshot(){


  snapshotNumber = (actualPlay.snapshots).length
  moves = []
  playName = actualPlay.playName


  $(".players").each(function() {

    //we get the position and convert it to fieldPos
    var actualTop = parseFloat($(this).css('top'))
    var actualLeft = parseFloat($(this).css('left'))
    pos = convert2Field( actualLeft, actualTop )

    //creating a move for the player, which corresponds with its initial position
    player = $(this).attr("id")
    playerName = $(this).attr("id")

    positions = []
    positions.push(pos.left + "&&" +  pos.top)


    if( game.playerWithBall == getPositionOfPlayerPerId(player) ){
      playerWithBall = game.playerWithBall
    }


    position = ""
    classes = $(this).attr("class").split(' ');
    for( j=0; j<classes.length; j++ ){
      if( classes[j].includes("position") ){
        position = (classes[j]).substring(8, classes[j].length )
      }
    }
    if( position == "" ){
      //writeConsoleLog("New Initial Position:  " + pos.left + "   " + pos.top)
      position = "ball"
    }

    //writeConsoleLog("Snapshot:  " + (actualSnapshot.snapshotNumber) + "   Move corresponds to: " + snapshotNumber)
    lastMoveIdx = lastMoveIdx + 1
    move = new Move( lastMoveIdx, position, playerName, snapshotNumber, positions )
    moves.push( move )
    processSnapshotDisplay()

  });

  actualMoves = moves
  actualSnapshot.playerWithBall = playerWithBall
  lastSnapshotIdx = lastSnapshotIdx + 1
  var newSnapshot = new Snapshot( lastSnapshotIdx, snapshotNumber, playName, playerWithBall, moves )

  return {
    "newMoves":moves,
    "newSnapshot":newSnapshot
  };

}



///////////////////////////////////////////////////////////////////
//method that processes the model to let the user choose a fav play
function allowUserToChooseFavTactic(buttonNumber){

  $("#ButtonFavEmpty").show()
  $("#ButtonFavFull").hide()

  //restarting the lists
  $("#listOfPlaysToAdd").empty();
  $("#playsAlreadySaveToBeOpened").empty();

  //if there are no tactics, display message
  if( allPlays.length == 0 ){
    $("#listOfPlaysToAdd").append("<p class='list-group-item list-group-item-action' data-dismiss='modal'> There are no tactics saved on this device </p> ");
  }

  var possiblePlays = []

  //we iterate through all the saved plays and we display the tactics
  for( i=0 ; i<allPlays.length ; i++ ){
    name = allPlays[i].playName
    type = allPlays[i].type
    favorite = allPlays[i].favorite
    display = name
    var idForButton = display.split(' ').join('---');
    if(favorite!="true"){
      $("#listOfPlaysToAdd").append("<button type='button' id='" + idForButton + "' class='list-group-item list-group-item-action playButton' data-dismiss='modal'>" + display + "</button> ");
    }
    possiblePlays.push(idForButton)
  }

  //creating the event listener for a normal click to add to the button
  for(  i=0; i<possiblePlays.length; i++){

    var id = document.getElementById(possiblePlays[i]).id
    writeConsoleLog( "button " + buttonNumber + " was pressed" )
    writeConsoleLog("Creating binder for: %%" + id + "%%")

    //$("#"+id).bind("click", function(){
    $("#"+id).click( function(){

      writeConsoleLog("\n\n\nasdasdadasdadsasd\n\n\n")

      var name = $(this).attr("id")
      //var name = $(this).attr("id").split('---').join(' ')
      $("#favPlay"+buttonNumber).html(name)

      if( buttonNumber=="1" ){
        window.localStorage.favPlay1 = name
        $("#star1").attr("src","img/Icon_Star_Yellow.svg");
      }
      if( buttonNumber=="2" ){
        window.localStorage.favPlay2 = name
        $("#star2").attr("src","img/Icon_Star_Yellow.svg");
      }
      if( buttonNumber=="3" ){
        window.localStorage.favPlay3 = name
        $("#star3").attr("src","img/Icon_Star_Yellow.svg");
      }
      if( buttonNumber=="4" ){
        window.localStorage.favPlay4 = name
        $("#star4").attr("src","img/Icon_Star_Yellow.svg");
      }
      else if( buttonNumber=="5" ){
        window.localStorage.favPlay5 = name
        $("#star5").attr("src","img/Icon_Star_Yellow.svg");
      }
    });

  }

}



/////////////////////////////////////////////////////
//function that carries out the actual move backwards
function carryOutMoveBackwards( index ){

  snapshots = actualPlay.snapshots

  initialPositions = snapshots[ index ].moves
  ballMoved = false

  processSnapshotDisplay()

  //iterating through all moves in the snapshot
  for( j=0; j<initialPositions.length; j++ ){

    positions = initialPositions[j].positions
    playerPosition = initialPositions[j].position
    playerID = $(".position"+playerPosition).attr("id")

    arrowPositions = getBezierPoints( positions, playerID );

    //iterating through the movements of a player in the actual snapshot

    //creating the player if he comes from a part of the field which is not visible
    if( playerID == undefined ){
      playerID = createPlayerFromHiddenPlace( playerPosition, arrowPositions, 0, index )
    }else{
      hidePlayerWhenGoingOutOfTheField( playerPosition, arrowPositions, 0, index )
    }

    //getting the position and converting it to display
    // setPositionLeft = arrowPositions[0][0]
    // setPositionTop = arrowPositions[0][1]
    pos = []
    for( k=0; k<4;k++ ){
      pos[k] = fieldPos2displayPos( arrowPositions[k][0], arrowPositions[k][1], getCurrentLayout());
      writeConsoleLog("BackwardsF fieldPos   top: " + arrowPositions[k][0] + "   left:  " + arrowPositions[k][1] + "  layout: " + getCurrentLayout() )
      writeConsoleLog("BackwardsF diplayPos   top: " + pos[k].top + "   left:  " + pos[k].left)
    }

    // Updating position
    setPlayerValuePerID(playerID, "fieldPosX", arrowPositions[0][0]);
    setPlayerValuePerID(playerID, "fieldPosY", arrowPositions[0][1]);

    //creating the animators

     var bezier_params = {
      start: {
        x: pos[3].left,
        y: pos[3].top,
        x1: pos[2].left,
        y1: pos[2].top,
        x2: pos[1].left,
        y2: pos[1].top,
        angle: 0 //dummy value
      },
      end: {
        x:pos[0].left,
        y:pos[0].top,
        angle: 0, // dummy value
        length: 0.3 // dummy value
      }
    }

    if( game.playerWithBall != playerPosition ){
      $( ".position" + playerPosition ).animate({
        path: new $.path.bezier(bezier_params)
      }, parseInt(window.localStorage.playSpeed)/2 );
    }
    //for the player with the ball if it is not a pass snapshot
    else if( game.playerWithBall == playerPosition &&  isPassSnapshot(snapshots, index) == false ){
      writeConsoleLog( "playerWithTheBall:  " +  snapshots[ index ].playerWithBall )
      $( ".position" + playerPosition ).animate({
        path: new $.path.bezier(bezier_params)
      }, parseInt(window.localStorage.playSpeed)/2 );
      $( ".ball" ).animate({
        path: new $.path.bezier(bezier_params)
      }, parseInt(window.localStorage.playSpeed)/2 );
    }
    //for the player with the ball in a pass snapshot
    else{
      $( ".position" + playerPosition ).animate({
        path: new $.path.bezier(bezier_params)
      }, parseInt(window.localStorage.playSpeed)/2 );
    }

    //checking if player goes out of bounds
    var teamFromID = getTeam( playerID, playerPosition )
    teamFromID = reverseID(teamFromID)
    if( !playerFitsInScreen( arrowPositions[0][1], getCurrentLayout() )){
      $("#ArrowOutOfSight"+teamFromID+playerPosition).remove()
      createArrow( teamFromID , getCurrentLayout() , playerPosition, arrowPositions[0][0]  )
    }else{
      $("#ArrowOutOfSight"+teamFromID+playerPosition).remove()
    }

  }


  //for the ball in a pass snapshot
  if( isPassSnapshot(snapshots, index) ){
    writeConsoleLog("we are passing")
    offset=0

    var previousPlayerWithBall = snapshots[index-1].playerWithBall
    console.log(previousPlayerWithBall)
    pos = $(".position" + previousPlayerWithBall).position()

    // if($("#ball").attr("id")==undefined){
    //   $(".court").append("<h1 class='ball rotates' id='ball' </h1> ");
    //   $(".ball").css({'top':(pos.top), 'left': (pos.left)});
    // }
    try{
      $( ".ball" ).animate({
        top: pos.top,
        left: pos.left - offset
      }, parseInt(window.localStorage.playSpeed)/3 );
    } catch (e) {
    }

  }

  arrowsManager(snapshots, index);


}



///////////////////////////////////////////////////
//function that carries out the actual move forward
function carryOutMoveForward( index, continuous ){

  if( $("#playState").html()=="stopped" ){
    actualSnapshotIndex = 1
    return
  }

  snapshots = actualPlay.snapshots;

  initialPositions = snapshots[ index ].moves;
  ballMoved = false;
  ballTopPos = 0;
  ballLeftPos = 0;
  durationAnimation = 0;
  startPassAnimationLater = false;
  ballPositionsForPass = 0;

  processSnapshotDisplay()
  console.log("holaaaa")

  //iterating through all moves in the snapshot
  for( j=0; j<initialPositions.length; j++ ){

    positions = initialPositions[j].positions
    playerPosition = initialPositions[j].position
    playerID = $(".position"+playerPosition).attr("id")

    // Test create arrow when playing play
    if( isPassSnapshot(snapshots, index)==false || playerPosition != "ball"){
      arrowPoints = getBezierPoints( positions, playerID);
    }

    var pos = [0,0,0,0]
    for( var k=0; k<4;k++ ){
      pos[k] = fieldPos2displayPos( arrowPoints[k][0], arrowPoints[k][1], getCurrentLayout() );
      writeConsoleLog("carryOutMoveForward PlayerId: "+playerID+" index: "+index+" fieldPos   top["+k+"]: " + arrowPoints[k][0] + "   left["+k+"]:  " + arrowPoints[k][1] + "  layout: " + getCurrentLayout() )
      writeConsoleLog("carryOutMoveForward PlayerId: "+playerID+" index: "+index+" diplayPos   top["+k+"]: " + pos[k].top + "   left["+k+"]:  " + pos[k].left)
    }

    //creating the player if he comes from a part of the field which is not visible
    if( playerID == undefined && playerPosition!="ball"){
      playerID = createPlayerFromHiddenPlace( playerPosition, arrowPoints, 3, index)
    }else{
      console.log("__"+snapshots[ index ].playerWithBall)
      hidePlayerWhenGoingOutOfTheField( playerPosition, arrowPoints, 3, index )
    }

    //Updating position
    setPlayerValuePerID(playerID, "fieldPosX", arrowPoints[3][0]);
    setPlayerValuePerID(playerID, "fieldPosY", arrowPoints[3][1]);

    //creating the animators
    var bezier_params = {
      start: {
        x: pos[0].left,
        y: pos[0].top,
        x1: pos[1].left,
        y1: pos[1].top,
        x2: pos[2].left,
        y2: pos[2].top,
        angle: 100
      },
      end: {
        x:pos[3].left,
        y:pos[3].top,
        angle: -100,
        length: 0.3
      }
    }

    writeConsoleLog("BezierPath for PlayerId: "+playerID+"  set to start: {x:"+pos[0].left+", y:"+pos[0].top+"  end:   {x:"+pos[3].left+", y:"+pos[3].top);

    if( playerPosition != "ball"){
      //for player other than the player with the ball
      if( game.playerWithBall != playerPosition ){
        $( ".position" + playerPosition ).animate({
          path: new $.path.bezier(bezier_params)
        }, parseInt(window.localStorage.playSpeed));
        writeConsoleLog("BezierPath Animation for PlayerId: "+playerID);
      }
      //for the player with the ball if it is not a pass snapshot
      else if( game.playerWithBall == playerPosition &&  isPassSnapshot(snapshots, index) == false ){
        writeConsoleLog( "playerWithTheBall:  " +  snapshots[ index ].playerWithBall )
        $( ".position" + playerPosition ).animate({
          path: new $.path.bezier(bezier_params)
        }, parseInt(window.localStorage.playSpeed));
        $( ".ball" ).animate({
          path: new $.path.bezier(bezier_params)
        }, parseInt(window.localStorage.playSpeed));
        writeConsoleLog("BezierPath Animation player with ball for not a pass snapshot for PlayerId: "+playerID +" With speed: "+parseInt(window.localStorage.playSpeed));
      }
      //for the player with the ball in a pass snapshot
      else{
        durationAnimation += (parseInt(window.localStorage.playSpeed));
        $( ".position" + playerPosition ).animate({
          path: new $.path.bezier(bezier_params)
        }, parseInt(window.localStorage.playSpeed));
        writeConsoleLog("BezierPath Animation player with ball in a pass snapshot for PlayerId: "+playerID);
      }

      //checking if player goes out of bounds
      var teamFromID = getTeam( playerID, playerPosition )
      teamFromID = reverseID(teamFromID)
      if( !playerFitsInScreen( arrowPoints[3][1], getCurrentLayout() )){
        $("#ArrowOutOfSight"+teamFromID+playerPosition).remove()
        createArrow( teamFromID , getCurrentLayout() , playerPosition, arrowPoints[3][0]  )
      }else{
        $("#ArrowOutOfSight"+teamFromID+playerPosition).remove()
      }

    }
  }

  // //for the ball in a pass snapshot
  if( isPassSnapshot(snapshots, index) ){
    id = game.playerWithBall
    offset = 0

    var playerWithBall_old = snapshots[index-1].playerWithBall
    var playerWithBall_aux = snapshots[index].playerWithBall

    // obtaining the id of the passer player
    var id = $(".position"+playerWithBall_old).attr("id")
    ballPositionsForPass = []

    //try catch is necessary for cases in which players are out of bounds and we cannot access the positions nicely
    posLeftSender = getPlayerValuePerID(id, "fieldPosX")
    posTopSender = getPlayerValuePerID(id, "fieldPosY")
    ballTopPos = posTopSender;
    ballLeftPos = posLeftSender;
    ballPositionsForPass.push( ballLeftPos + "&&" + ballTopPos )

    //finding the target and saving the ball target positions in variables
    for(i=0;i<snapshots[index].moves.length;i++){

      if( snapshots[index].moves[i].position != playerWithBall_aux ){
        continue
      }
      //pos_aux = $(".position"+playerWithBall_aux).position()
      //pos_aux = displayPos2fieldPos( ballLeftPos, ballTopPos, getCurrentLayout());
      //ballPositionsForPass.push( pos_aux.left + "&&" + pos_aux.top )
      pos_aux = snapshots[index].moves[i].positions[ snapshots[index].moves[i].positions.length-1 ]
      ballPositionsForPass.push( pos_aux )
      pos_aux = fieldPos2displayPos( pos_aux.split("&&")[0], pos_aux.split("&&")[1], getCurrentLayout());
      ballTopPos = pos_aux.top;
      ballLeftPos = pos_aux.left;
      break
    }

    writeConsoleLog("\n\n\n")
    writeConsoleLog(ballPositionsForPass)

    //changing the value of the variables to later trigger the animation and the arrow
    startPassAnimationLater = true;

  }

  //creating the arrows up to this point
  if( startPassAnimationLater==false){
    arrowsManager( snapshots, index+1)
  }

  //calling the ball passing animation if necessary
  if( startPassAnimationLater==true){
    setTimeout (function (){
      $( ".ball" ).animate({
        top: ballTopPos,
        left: ballLeftPos - offset
      }, parseInt(window.localStorage.playSpeed)/2)
      arrowsManager( snapshots, index+1)
    }, parseInt(durationAnimation/2));
  }


  ///////////////////////////////////////////////////////////////////////////
  // CODE THAT BELONGS TO THE CONTINUOUS PLAYING MODE
  ////////////////////////////////////////////////////////////////////////////
  //if we are in continuous mode, we call the next spashot after some delay
  if( continuous!=null && continuous==true){

    //if pause button is pressed, continuous mode is paused
    if( $("#playState").html() == "paused"){
      actualSnapshotIndex = index+1
      return;
    }else if( $("#playState").html() == "stopped" ){
      actualSnapshotIndex = 1
      return;
    }

    //counting the number of positions taking place in this snapshot
    var numberOfMoves = 0;
    currentMoves = snapshots[ index ].moves

    for( k=0; k<currentMoves.length; k++ ){
      positions = currentMoves[k].positions
      //adding 4 moves for every player that performs an actual move
      if(positions.length>=2){
        numberOfMoves = numberOfMoves + 4
      }
    }
    //adding two more beacause of the ball
    numberOfMoves+=2

    //setting a minimum number of moves
    if(numberOfMoves<6){
      numberOfMoves=6
    }

    //setting the right parameters and calling the next snapshot after the delay
    setTimeout (function (){

      actualSnapshotIndex = index+1

      if( $("#playState").html()=="paused" || $("#playState").html()=="stopped" ){
        return
      }

      if( actualSnapshotIndex < snapshots.length-1 ){
        playerWithBall = game.playerWithBall
        playerWithBall = snapshots[ index ].playerWithBall
        game.playerWithBall = playerWithBall
        carryOutMoveForward( actualSnapshotIndex, true )
      }
      //changing the state back to openend when the play is finished
      if( actualSnapshotIndex == snapshots.length-1  ){
        $("#playState").html("opened")
        $("#buttonPause").hide();
        $("#buttonPlay").show();
      }
    }, (1.2)*parseInt(window.localStorage.playSpeed));
    //}, numberOfMoves*parseInt(window.localStorage.playSpeed)+10);
    //}, parseInt(numberOfMoves/2)*parseInt(0.5*window.localStorage.playSpeed));

  }

}



//////////////////////////////////////////////////////////
//method that returns if the actual one is a pass snapshot
function isPassSnapshot( snapshots , actualSnapshotIndex){

  actualPlayerWithBall = snapshots[actualSnapshotIndex-1].playerWithBall
  nextPlayerWithBall = snapshots[actualSnapshotIndex].playerWithBall

  if(nextPlayerWithBall!=actualPlayerWithBall){
    return true
  }else{
    return false
  }

}



/////////////////////////////////////////////////////
//function that waits for a given time of miliseconds
function wait(ms){
  var d = new Date();
  var d2 = null;
  do { d2 = new Date(); }
  while(d2-d < ms);
}



/////////////////////////////////////////////////////
function loadTactic( tacticId ){

  //removing arrows and changing the state for the app and for the buttons
  removePlayerArrows()
  $("#playState").html("opened")
  $("#buttonPause").hide();
  $("#buttonPlay").show();
  actualSnapshotIndex = 1
  setState("Playing");

  tacticName = tacticId.split('---').join(' ');
  tactic = ""

  //getting the tactic from the array with all tactics
  for( i=0; i < allPlays.length; i++ ){
    if( allPlays[i].playName == tacticName ){
      tactic = allPlays[i]
      actualPlay = allPlays[i]
      writeConsoleLog("loadTactic -->allPlays[i].offensiveStrategy:  "+allPlays[i].offensiveStrategy);
    }
  }

  //in case the tactic was not found, we give an error
  if( tactic == "" ){
    console.error("An error occured while opening the tactic")
    return
  }

  snapshots = tactic.snapshots
  numberSnapshots = snapshots.length
  writeConsoleLog( "loadTactic -->"+tactic.playName )
  writeConsoleLog( "loadTactic -->"+snapshots[0].playName )
  writeConsoleLog("###### ")

  //setting the min and max to the snapshot display
  $('#snapshotDisplay').attr('min', 1);
  $('#snapshotDisplay').attr('max', numberSnapshots-1);
  $('#snapshotDisplay').val(1);

  var c = document.getElementById("courtCanvas");
  var ctx = c.getContext("2d");
  ctx.canvas.width = ctx.canvas.width;

  //setting the first position
  initialPositions = snapshots[0].moves
  writeConsoleLog( initialPositions )
  writeConsoleLog( initialPositions.length + " moves on the first snapshot" )
  writeConsoleLog(snapshots)

  var ballPosLeft, ballPosTop

  //load every player in the position
  for( k=0 ; k<initialPositions.length ; k++ ){
    //getting the data about the current element
    //number = initialPositions[k].number
    //team = initialPositions[k].team
    // writeConsoleLog(number)
    position = initialPositions[k].position
    setPosition = initialPositions[k].positions[0]
    setPositionLeft = setPosition.split("&&")[0]
    setPositionTop = setPosition.split("&&")[1]
    writeConsoleLog("loadTactic --> iteration: " + k + "  "  + position + "  " + setPositionLeft + "  " + setPositionTop )

    //checking if it is the ball or the player
    if( position[0]=="O" ){
      team = game.offensiveTeam
    }else{
      team = getDefensiveTeam()
    }
    setPlayerValuePerID(game[team].courtPlayers[position], "fieldPosX", setPositionLeft);
    setPlayerValuePerID(game[team].courtPlayers[position], "fieldPosY", setPositionTop);
    writeConsoleLog("loadTactic --> game[team].courtPlayers[position]: "+game[team].courtPlayers[position])
    // setPlayer(team, position, [setPositionTop, setPositionLeft], getCurrentLayout());

    // setPlayerValuePerPosition(undefined, position, "fieldPosY", setPositionTop);
    // setPlayerValuePerPosition(undefined, position, "fieldPosX", setPositionLeft);

    //asign the ball to the corresponding player
    // if( snapshots[0].playerWithBall == position ){
    //   game.playerWithBall = position
    //   ballPosTop = setPositionTop
    //   ballPosLeft = setPositionLeft
    // }

  }

  writeConsoleLog("loadTactic -->tactic.offensiveStrategy:  "+tactic.offensiveStrategy);
  writeConsoleLog("loadTactic -->tactic.playName:  "+tactic.playName);
  game[game.offensiveTeam].offensiveTeam = tactic.offensiveStrategy;
  game[getDefensiveTeam()].defensiveTeam = tactic.defensiveStrategy;
  setTeam(game.offensiveTeam, tactic.offensiveStrategy, getCurrentLayout(), false); // Temporary solution
  setTeam(getDefensiveTeam(), tactic.defensiveStrategy, getCurrentLayout(), false); // Temporary solution

  game.playerWithBall = snapshots[0].playerWithBall;
  SetBall(false, getCurrentLayout());

  actualSnapshotIndex = 1
  $("#modeHidden").html("normal")
  $("#playState").html("opened")
  processSnapshotDisplay()
  // setOrientation();

}



////////////////////////////////////////
//method that initializes a new snapshot
function newSnapshot(){

  lastSnapshotUndone = false
  writeConsoleLog("Creating a new snapshot")

  //appending the old moves to the snapshot, and the snapshot to the play
  actualSnapshot.moves = actualMoves;
  snapshotNumber = (actualPlay.snapshots).length

  writeConsoleLog(actualPlay.snapshots)

  moves = []
  playName = actualPlay.playName


  //we iterate for all players and ball
  $(".players").each(function() {  //CHANGE

    //we get the position and convert it to fieldPos
    //var actualTop = parseFloat($(this).css('top'))
    //var actualLeft = parseFloat($(this).css('left'))
    //pos = convert2Field( actualLeft, actualTop )
    var pos = {}
    playerId = $(this).attr("id")
    pos.left = getPlayerValuePerID( playerId, "fieldPosX" )
    pos.top = getPlayerValuePerID( playerId, "fieldPosY")

    //creating a move for the player, which corresponds with its initial position
    player = $(this).attr("id")
    playerName = $(this).attr("id")

    positions = []
    positions.push(pos.left + "&&" +  pos.top)


    if( game.playerWithBall == getPositionOfPlayerPerId(player) ){
      playerWithBall = game.playerWithBall
    }


    position = ""
    classes = $(this).attr("class").split(' ');
    for( j=0; j<classes.length; j++ ){
      if( classes[j].includes("position") ){
        position = (classes[j]).substring(8, classes[j].length )
      }
    }
    if( position == "" ){
      //writeConsoleLog("New Initial Position:  " + pos.left + "   " + pos.top)
      position = "ball"
    }


    writeConsoleLog("Snapshot:  " + (actualSnapshot.snapshotNumber) + "   Move corresponds to: " + snapshotNumber)
    lastMoveIdx = lastMoveIdx + 1
    move = new Move( lastMoveIdx, position, playerName, snapshotNumber + 1, positions )
    moves.push( move )

    processSnapshotDisplay()

  });


  // we add the players out of the field which have been removed
  // for example, the OGK
  if( moves.length<actualPlay.snapshots[snapshotNumber-1].moves.length ){

    var prev_snapshot_moves = actualPlay.snapshots[0].moves;
    var already_found;

    for(var i=0;i<prev_snapshot_moves.length;i++){
      already_found = false
      for(var j=0;j<moves.length;j++){
        if( moves[j].position==prev_snapshot_moves[i].position ){
          already_found = true
          break
        }
      }

      // adding the missong player
      if( already_found==false ){
        moves.push( prev_snapshot_moves[i] )
      }
    }
  }


  //initializing the next snapshot
  actualMoves = moves
  actualSnapshot.playerWithBall = playerWithBall

  actualPlay.snapshots.push(actualSnapshot)
  writeConsoleLog( "pushed snapshot #" + snapshotNumber )

  snapshotNumber = (actualPlay.snapshots).length

  writeConsoleLog( "now working on snapshot #" + snapshotNumber )

  lastSnapshotIdx = lastSnapshotIdx + 1
  actualSnapshot = new Snapshot( lastSnapshotIdx, snapshotNumber, playName, playerWithBall, moves )

}



//////////////////////////////////////////////
//method that returns the Bezier anchor points
function getBezierPoints( arrowPositions, playerId ){

  initTop = arrowPositions[0].split("&&")[1]
  initLeft = arrowPositions[0].split("&&")[0]
  secondPointTop = arrowPositions[parseInt(0.33*(arrowPositions.length-1))].split("&&")[1]
  secondPointLeft = arrowPositions[parseInt(0.33*(arrowPositions.length-1))].split("&&")[0]
  thirdPointTop = arrowPositions[parseInt(0.66*(arrowPositions.length-1))].split("&&")[1]
  thirdPointLeft = arrowPositions[parseInt(0.66*(arrowPositions.length-1))].split("&&")[0]
  targetTop = arrowPositions[arrowPositions.length-1].split("&&")[1]
  targetLeft = arrowPositions[arrowPositions.length-1].split("&&")[0]

  return [ [initLeft, initTop], [secondPointLeft, secondPointTop], [thirdPointLeft, thirdPointTop], [targetLeft, targetTop] ]


}



///////////////////////////////////////////////////
//method that creates the arrow for player movement
function createArrowMove( arrowPositions, playerId, playerPosition){

    //if the checkbox "show traces" is deactivates, we do not draw the lines
    if( window.localStorage.showTraces == "false" ){
      return
    }

    ////////////////////////////////////
    //// Test with bezier lines
    initTop = arrowPositions[0].split("&&")[1]
    initLeft = arrowPositions[0].split("&&")[0]
    secondPointTop = arrowPositions[parseInt(0.33*(arrowPositions.length-1))].split("&&")[1]
    secondPointLeft = arrowPositions[parseInt(0.33*(arrowPositions.length-1))].split("&&")[0]
    thirdPointTop = arrowPositions[parseInt(0.66*(arrowPositions.length-1))].split("&&")[1]
    thirdPointLeft = arrowPositions[parseInt(0.66*(arrowPositions.length-1))].split("&&")[0]
    targetTop = arrowPositions[arrowPositions.length-1].split("&&")[1]
    targetLeft = arrowPositions[arrowPositions.length-1].split("&&")[0]

    // initPos = convert2Display( initLeft, initTop )
    // secondPos = convert2Display(secondPointLeft, secondPointTop)
    // thirdPos = convert2Display(thirdPointLeft, thirdPointTop)
    // targetPos = convert2Display( targetLeft, targetTop )

    initPos = fieldPos2displayPos( initLeft, initTop, getCurrentLayout());
    secondPos = fieldPos2displayPos(secondPointLeft, secondPointTop, getCurrentLayout());
    thirdPos = fieldPos2displayPos(thirdPointLeft, thirdPointTop, getCurrentLayout());
    targetPos = fieldPos2displayPos( targetLeft, targetTop, getCurrentLayout());

    initArrowPosLeft = initPos.left + $(".players").height()/2;
    secondArrowPosLeft = secondPos.left + $(".players").height()/2;
    thirdArrowPosLeft = thirdPos.left + $(".players").height()/2;
    targetArrowPosLeft = targetPos.left + $(".players").height()/2;

    initArrowPosTop = initPos.top + $(".players").height()/2;
    secondArrowPosTop = secondPos.top + $(".players").height()/2;
    thirdArrowPosTop = thirdPos.top + $(".players").height()/2;
    targetArrowPosTop = targetPos.top + $(".players").height()/2;

    var c = document.getElementById("courtCanvas");
    var ctx = c.getContext("2d");

    //if( initTop>60 && secondPointTop>60 && thirdPointTop>60 && targetTop>60  ){
    //  return [ [initLeft, initTop], [secondPointLeft, secondPointTop], [thirdPointLeft, thirdPointTop], [targetLeft, targetTop] ]
    //}

    ctx.beginPath()
    ctx.moveTo(parseInt(initArrowPosLeft), parseInt(initArrowPosTop));
    ctx.bezierCurveTo(parseInt(secondArrowPosLeft), parseInt(secondArrowPosTop), parseInt(thirdArrowPosLeft), parseInt(thirdArrowPosTop), parseInt(targetArrowPosLeft), parseInt(targetArrowPosTop));
    ctx.lineTo(parseInt(targetArrowPosLeft), parseInt(targetArrowPosTop));

    writeConsoleLog("\n\n\n")
    writeConsoleLog( playerId )
    writeConsoleLog( [ [initLeft, initTop], [secondPointLeft, secondPointTop], [thirdPointLeft, thirdPointTop], [targetLeft, targetTop] ] )
    writeConsoleLog("\n\n\n")


    var team = getTeam( playerId )
    var lineColor


    if(playerId == undefined){
      if(playerPosition.substring(0,1)=="O"){
        team = "teamA"
        team = reverseID(team)
      }else if(playerPosition.substring(0,1)=="D"){
        team = "teamB"
        team = reverseID(team)
      }
    }

    // screen factor that adjusts for the responsivenes of the lines
    var screen_factor = 0
    var init_arrow_factor = 20
    if(screen.width > 800){
      screen_factor = Math.ceil(window.localStorage.lineThickness/2)
      init_arrow_factor = 30
    }else if(screen.width < 500){
      screen_factor = -Math.ceil(window.localStorage.lineThickness/2)
      init_arrow_factor = 10
    }

    lineThickness = parseInt(window.localStorage.lineThickness) + parseInt(screen_factor)
    ballLineThickness = Math.ceil( parseInt(lineThickness)/2 )

    if( team == "teamA"){
      ctx.setLineDash([]);
      ctx.lineWidth = lineThickness;
      lineColor = game.teamA.lineColorA;
    }
    else if( team == "teamB" ){
      ctx.setLineDash([]);
      ctx.lineWidth = lineThickness;
      lineColor = game.teamB.lineColorA;
    }
    else{
      ctx.setLineDash([15, 15]);
      ctx.lineWidth = ballLineThickness;
      lineColor = "black";
      //lineColor = game.teamA.lineColorA;
    }
    ctx.strokeStyle = lineColor
    ctx.stroke();

    //small cheat not to draw extra arrows on the ball from the arrowsManager function
    if(arrowPositions.length < 2 || window.localStorage.showTraces == "false"){
      return [ [initLeft, initTop], [secondPointLeft, secondPointTop], [thirdPointLeft, thirdPointTop], [targetLeft, targetTop] ]
    }

    //adding the arrow head to the arrow
    var triangleWidth = ( targetArrowPosLeft - thirdArrowPosLeft )
    var triangleHeight = ( targetArrowPosTop - thirdArrowPosTop )
    var angle = Math.atan2( triangleHeight, triangleWidth )

    var arrow_factor = init_arrow_factor + lineThickness

		ctx.beginPath()
		ctx.moveTo( targetArrowPosLeft + arrow_factor*Math.cos(angle)*0.5, targetArrowPosTop + arrow_factor*Math.sin(angle)*0.5)
		ctx.lineTo( targetArrowPosLeft - arrow_factor*Math.cos(angle - Math.PI/4) , targetArrowPosTop - arrow_factor*Math.sin(angle - Math.PI/4))
    ctx.lineTo( targetArrowPosLeft - arrow_factor*Math.cos(angle + Math.PI/4) , targetArrowPosTop - arrow_factor*Math.sin(angle + Math.PI/4))
		ctx.closePath();
		ctx.fillStyle  = lineColor
		ctx.fill();

    return [ [initLeft, initTop], [secondPointLeft, secondPointTop], [thirdPointLeft, thirdPointTop], [targetLeft, targetTop] ]

}



/////////////////////////////////////
//function that converts to field pos
function convert2Field( actualLeft, actualTop ){

  console.log("Initial log Convert2Field")

  if( $("#rotationHidden").html() == "HalfVert" && $("#orientationHidden").html() == "0" && $("#fieldTypeHidden").html() == "Half"){
    pos = displayPos2fieldPos( actualLeft, actualTop, "HalfVertOffense" )
  }
  else if(  $("#rotationHidden").html() == "HalfVert" && $("#orientationHidden").html() == "180" && $("#fieldTypeHidden").html() == "Half"){
    pos = displayPos2fieldPos( actualLeft, actualTop, "HalfVertDefense" )
  }
  else if(  $("#rotationHidden").html() == "HalfVert" && $("#orientationHidden").html() == "0" && $("#fieldTypeHidden").html() == "Full"){
    pos = displayPos2fieldPos( actualLeft, actualTop, "FullOffense" )
  }
  else if(  $("#rotationHidden").html() == "HalfVert" && $("#orientationHidden").html() == "180" && $("#fieldTypeHidden").html() == "Full"){
    pos = displayPos2fieldPos( actualLeft, actualTop, "FullDefense" )
  }
  else if(  $("#rotationHidden").html() == "HalfHoriz" && $("#orientationHidden").html() == "0" && $("#fieldTypeHidden").html() == "Half"){
    pos = displayPos2fieldPos( actualLeft, actualTop, "HorizOffense" )
  }
  else if(  $("#rotationHidden").html() == "HalfHoriz" && $("#orientationHidden").html() == "180" && $("#fieldTypeHidden").html() == "Half"){
    pos = displayPos2fieldPos( actualLeft, actualTop, "HorizDefense" )
  }

  console.log("Log log")

  return {
          top: pos.top,
          left: pos.left
      };

}



/////////////////////////////////////
//function that converts to dispaly pos
function convert2Display( actualLeft, actualTop ){

  if( $("#rotationHidden").html() == "HalfVert" && $("#orientationHidden").html() == "0" && $("#fieldTypeHidden").html() == "Half"){
    pos = fieldPos2displayPos( actualLeft, actualTop, "halfVertOffense" )
  }
  else if(  $("#rotationHidden").html() == "HalfVert" && $("#orientationHidden").html() == "180" && $("#fieldTypeHidden").html() == "Half"){
    pos = fieldPos2displayPos( actualLeft, actualTop, "halfVertDefense" )
  }
  else if(  $("#rotationHidden").html() == "HalfVert" && $("#orientationHidden").html() == "0" && $("#fieldTypeHidden").html() == "Full"){
    pos = fieldPos2displayPos( actualLeft, actualTop, "fullOffense" )
  }
  else if(  $("#rotationHidden").html() == "HalfVert" && $("#orientationHidden").html() == "180" && $("#fieldTypeHidden").html() == "Full"){
    pos = fieldPos2displayPos( actualLeft, actualTop, "fullDefense" )
  }
  else if(  $("#rotationHidden").html() == "HalfHoriz" && $("#orientationHidden").html() == "0" && $("#fieldTypeHidden").html() == "Half"){
    pos = fieldPos2displayPos( actualLeft, actualTop, "horizOffense" )
  }
  else if(  $("#rotationHidden").html() == "HalfHoriz" && $("#orientationHidden").html() == "180" && $("#fieldTypeHidden").html() == "Half"){
    pos = fieldPos2displayPos( actualLeft, actualTop, "horizDefense" )
  }

  return {
          top: pos.top,
          left: pos.left
      };

}



/////////////////////////////////////
//function that asigns the ball
function asignBall( actualLeft, actualTop ){

  pos = fieldPos2displayPos( actualLeft, actualTop, getCurrentLayout() );

  $(".court").append("<h1 class='ball rotates' id='ball' </h1> ");
  $(".ball").css("top", pos.top);
  $(".ball").css("left", pos.left);


}



/////////////////////////////////////////////
//method that erases all arrows on the screen
function resetCanvas(){

  //  Erase current canvas
  // writeConsoleLog("Removing arrows!!!")
  console.log("Removing arrows!!!")
  var c = document.getElementById("courtCanvas");
  var ctx = c.getContext("2d");
  ctx.canvas.width = ctx.canvas.width;

}



/////////////////////////////////////////////////////////////////
// function that draws all arrows till the given screenshot index
function arrowsManager(snapshots, positionIndex){

  // Erase current canvas
  resetCanvas()

  //if( $("#playState").html() == "edit" ){
  //  return
  //}

  //obtaining the starting point to draw arrows based on the memoryArrows parameter
  var memory = window.localStorage.memoryArrows;
  var starting_point;
  if(parseInt(memory)==9 || memory>=positionIndex ){
    starting_point = 1
  }else{
    starting_point = positionIndex - window.localStorage.memoryArrows
  }

  console.log("start: " + starting_point)
  console.log("end: " + positionIndex)

  //iterating snapshots and moves to create the arrows up to the point
  for ( i=starting_point; i < positionIndex; i++) {

    var previousPlayerWithBallPos = ""
    var actualPlayerWithBallPos = ""

    for( j=0; j<snapshots[i].moves.length; j++ ){

      var playerId = $(".position"+snapshots[i].moves[j].position).attr("id")
      var playerPosition = snapshots[i].moves[j].position
      var team = getTeam( playerId )

      //getting the postions of sender and receiver to make the arrow for the pass
      if( i>0 && snapshots[ i-1 ].playerWithBall == snapshots[i].moves[j].position ){
        previousPlayerWithBallPos = snapshots[i].moves[j].positions[0]
      }else if(i==0){
        var posLeftSender = $("#ball").position().left;
        var posTopSender = $("#ball").position().top;
        var pos_aux = displayPos2fieldPos( posLeftSender, posTopSender, getCurrentLayout());
        var ballTopPos = pos_aux.top;
        var ballLeftPos = pos_aux.left;
        previousPlayerWithBallPos = ballLeftPos + "&&" + ballTopPos
      }
      if( snapshots[ i ].playerWithBall == snapshots[i].moves[j].position ){
        actualPlayerWithBallPos = snapshots[i].moves[j].positions[ snapshots[i].moves[j].positions.length-1 ]
      }

      //skipping cases where players do not move
      if( snapshots[i].moves[j].positions.length <= 2 && snapshots[i].moves[j].position != "ball" ){
        continue
      }
      //creating the arrow otherwise
      else{
        console.log("__playerID: " + playerId)
        createArrowMove(snapshots[i].moves[j].positions, playerId, playerPosition);
      }
    }

    //arrow for the pass
    if( isPassSnapshot(snapshots, i) ){

      var ballPositionsForPass = ["",""]
      ballPositionsForPass[0] = previousPlayerWithBallPos
      ballPositionsForPass[1] = actualPlayerWithBallPos

      createArrowMove(ballPositionsForPass, "ball", "-");

    }

  }
}



///////////////////////////////////////////////////////77
//function that shows the modal that allows the user to modify name, type or delete of tactic
function editPlayMenu( event ){

  // obtaining the name of the tactic to edit given the pressed button. Depending on when the user
  // presses, event.target corresponds to a different div
  playName = event.target.id
  if(playName.length==0){
    playName = event.target.parentNode.parentNode.id
  }
  playName = playName.split('---').join(' ');


  //finding the type of the play
  for( i=0;i<allPlays.length;i++ ){
    //writeConsoleLog( allPlays[i].playName + "   " + playName )
    if( allPlays[i].playName == playName ){
      playType = allPlays[i].playType
      favorite = allPlays[i].favorite
    }
  }

  writeConsoleLog(playType)
  writeConsoleLog(playName)

  //hiding the open play popup from the screen
  $('#openPlayPopup').modal('hide');

  //giving the correct information to the edit play popup
  $("#editPlayModalTitle").text( "Edit play: " +  playName );
  $("#inputEditPlayName").val( playName )

  if( playType == "all" ){
    $('#inlineRadio3Edit').prop('checked', true)
  }else if( playType == "defense"  ){
    $('inlineRadio2Edit').prop('checked', true)
  }else{
    $('#inlineRadio1Edit').prop('checked', true)
  }

  //setting the text of the favorite button
  if( favorite == "true" ){
    $("#addTacticToFavorites").html("Remove from Favorites")
  }else{
    $("#addTacticToFavorites").html("Add to Favorites")
  }

  //finally showing the edit play popup on the screen
  $("#infoEditPlay").html("")
  $('#editPlayPopup').modal('show');


}



//////////////////////////////////////////////////////////////////////////
// method that creates the player if he does not previously exist
function createPlayerFromHiddenPlace( position, coordinates, idx, snapshot_index){

  var playerID = undefined
  var targetLayout = getCurrentLayout()

  //esto es horrible pero no se me ocurrio otra cosa
  //getting the team of the player to set
  var team
  if( position[0]=="O" ){
    team = "teamA"
    team = reverseID(team)
  }else{
    team = "teamB"
    team = reverseID(team)
  }

  //check if the player should be added or if he remains hidden
  var coordinateY = coordinates[idx][1]
  var playerFits = playerFitsInScreen(coordinateY, targetLayout)
  //creating the player
  if( playerFits==true){

    if( targetLayout.includes("HalfHoriz") ){
      maxY = "42"
    }else if( targetLayout.includes("HalfVert") ){
      maxY = "58"
    }

    //adjusting the Y coordinate to be inside the field
    for( var k=0;k<coordinates.length;k++){
      if(coordinates[k][1]>maxY){
        coordinates[k][1] = maxY
      }
    }

    setPlayer( team, position, [maxY,coordinates[0][0]], targetLayout )
    //writeConsoleLog(coordinates)
    playerID = $(".position"+position).attr("id")
    //writeConsoleLog(playerID)

    if(snapshots[snapshot_index].playerWithBall==position){
      writeConsoleLog( "__deleting ball as well")
      SetBall(false, getCurrentLayout());
    }

  }

  return playerID

}



//////////////////////////////////////////////////////////////////////////
// method that deletes a player if he leaves the visible field
function hidePlayerWhenGoingOutOfTheField( position, coordinates, idx, snapshot_index){

  var targetLayout = getCurrentLayout()

  //check if the player should be added or if he remains hidden
  var coordinateY = coordinates[idx][1]
  var playerFits = playerFitsInScreen(coordinateY, targetLayout)

  writeConsoleLog("position:  " + position)
  writeConsoleLog("coordinates:  " + coordinates[idx] )
  writeConsoleLog("playerFits:  " + playerFits)

  //we only delelte if idx=3 so the player makes the animation before being removed
  if( playerFits==false){
    writeConsoleLog( "__deleting player:  " + position )
    if(snapshots[snapshot_index].playerWithBall==position){
      writeConsoleLog( "__deleting ball as well")
      RemoveBall()
    }
    deletePlayer(position)
  }

  return
}


//////////////////////////////////////////////////////////////////////////
// method that parses tghe play name so there are only alphanumeric characters, _ and spaces
function parse_play_name(playName, element){

  // accepting only alphanumeric, spaces and underscores
  var regex = new RegExp("^[0-9a-zA-Z_\. ]+$");
  if( regex.test(playName)==false ){
    $(element).html("Only alphanumeric charactest, spaces and underscores are allowed")
    return false;
  }else{
    $(element).html("")
    return true
  }

}


//
