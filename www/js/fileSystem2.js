// https://github.com/cfjedimaster/Cordova-Examples/tree/master/writelog/www/js

// document.addEventListener('deviceready', onDeviceReady, false);

// var logOb;

// function fail(e) {
// 	console.log("FileSystem Error");
// 	console.dir(e);
// }

// function onDeviceReady() {

// 	// window.resolveLocalFileSystemURL(cordova.file.externalDataDirectory, function(dir) {
	 	
// 	window.resolveLocalFileSystemURL(cordova.file.cacheDirectory, function(dir) {
// 	//window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function(dir) {
// 		console.log("got main dir",dir);
// 		dir.getFile("logTACBrd.txt", {create:true}, function(file) {
// 			console.log("got the file", file);
// 			logOb = file;
// 			writeLog("App started");			
// 		});
// 	});
	
// 	document.querySelector("#actionOne").addEventListener("touchend", function(e) {
// 		//Ok, normal stuff for actionOne here
// 		//
// 		//Now log it
// 		writeLog("actionOne fired");
// 	}, false);

// 	document.querySelector("#actionTwo").addEventListener("touchend", function(e) {
// 		//Ok, normal stuff for actionTwo here
// 		//
// 		//Now log it
// 		writeLog("actionTwo fired");
// 	}, false);

// }

// function writeLog(str) {
// 	if(!logOb) return;
// 	var log = "[" + (new Date()) + "] "+ str +"\n";
// 	//console.log("going to log "+log);
// 	logOb.createWriter(function(fileWriter) {
		
// 		fileWriter.seek(fileWriter.length);
		
// 		var blob = new Blob([log], {type:'text/plain'});
// 		fileWriter.write(blob);
// 		//console.log("ok, in theory i worked");
// 	}, fail);
// }

// function justForTesting() {
// 	logOb.file(function(file) {
// 		var reader = new FileReader();

// 		reader.onloadend = function(e) {
// 			console.log(this.result);
// 		};

// 		reader.readAsText(file);
// 	}, fail);

// }

function writeConsoleLog(str){
	console.log(str);
	//writeLog(str);
}

function getTimeMilliseconds(){
	var d = new Date();
	var ret = d.getHours()+":"+d.getMinutes()+":"+d.getSeconds()+":"+d.getMilliseconds()+" ";
	return(ret);
}