////////////////////////////////////////////////////////////////////////////////////////
/////  Library that contains functions to process the actions related to the ball  /////
////////////////////////////////////////////////////////////////////////////////////////

var removed_ball_pos_left = 0;
var removed_ball_pos_top = 0;

$(document).ready(function() {
// $(document).ready(function($) {

  /////////////////////////////////////////////////////
  // removing all attackers
  $("#offensiveBall").click(function(){

    if (!($(".ball")[0])){
      // Do something if ball exists
      SetBall(true, getCurrentLayout());
    } else {
       // Do something if ball does not exist
       RemoveBall();
    }
  });

});


function RemoveBall(){
  $(".ball").remove();
}


/////////////////////////////////////////////////////7
// fucntion that passes the ball
function PassTheBall( idReceiver, idPasser ){

  //we change the hasTheBall class
  //$(".hasTheBall").removeClass("hasTheBall");
  //$("#" + idReceiver ).addClass("hasTheBall");

  // creating ball when passing from out of bounds
  if($("#ball").attr("id")==undefined){

    var targetLayout = getCurrentLayout()
    if(targetLayout.includes("Full")){
      offset = 0
    }else{
      offset = 2.5
    }

    $(".court").append("<h1 class='ball rotates' id='ball' </h1> ");
    pos = fieldPos2displayPos( removed_ball_pos_left-offset, removed_ball_pos_top, targetLayout);
    writeConsoleLog("SetBall: pos.top:   "+pos.top+"pos.left:   "+pos.left);
    $(".ball").css({'top':(pos.top), 'left': (pos.left)});
  }

  game.playerWithBall = getPositionOfPlayerPerId(idReceiver);

  writeConsoleLog("idReceiver:  " + idReceiver)
  writeConsoleLog("idPasser:  " + idPasser)
  writeConsoleLog("PassTheBall: game.playerWithBall: "+game.playerWithBall);

  //the position of the receiver is obtained
  var top = parseFloat( parseFloat($('#'+idReceiver).css("top")) / $('#'+idReceiver).parent().height() * 100);
  var left = parseFloat( parseFloat($('#'+idReceiver).css("left"))  / $('#'+idReceiver).parent().width() * 100);
  if( $("#fieldTypeHidden").html() == "Full" ){
    offset = 2.5
  }else{
    offset = 2.5
  }

  //updating the defense positions in the automatic defense functionality
  var automaticDefenseMovement = window.localStorage.automaticDefenseMovement;
  writeConsoleLog("ASSIGNPAIRS window.localStorage.automaticDefenseMovement: "+window.localStorage.automaticDefenseMovement);

  //updating the defense automatically
  if(automaticDefenseMovement != "false"){
    updateAutomaticDefense()
  }


  //passing during playing mode goes to FreeDrawWithBackup mode (edit mode)
  if( $("#playState").html() == "playing" || $("#playState").html() == "opened" || $("#playState").html() == "paused" || $("#playState").html() == "stopped" ){
    copy_tactic()
    $("#playState").html("edit")
    setState("FreeDrawWithBackup");
    processSnapshotDisplay()
  }

  //logic for recording the ball movement
  if( $("#modeHidden").html() == "recording"){

    writeConsoleLog(game)
    writeConsoleLog(actualPlay.snapshots)
    writeConsoleLog(actualMoves)

    //we save the position of the player in its Move element of the corresponding Snapshot
    var initTop = parseFloat($("#" + idPasser).css('top'))
    var initLeft = parseFloat($("#" + idPasser).css('left'))
    var targetTop = parseFloat($("#" + idReceiver).css('top'))
    var targetLeft = parseFloat($("#" + idReceiver).css('left'))
    initPos = convert2Field( initLeft, initTop )
    targetPos = convert2Field( targetLeft, targetTop )

    posVector = []
    posVector.push( initPos.left + "&&" +  initPos.top )
    posVector.push( targetPos.left + "&&" +  targetPos.top)

    writeConsoleLog( posVector )

    //creating the arrow for the pass so it shows when recording
    //creating the arrow for the move
    createArrowMove( posVector, "ball" )

    //after moving the ball, we start a new snapshot
    newSnapshot()
    PLAYERS_MOVED_IN_SNAPSHOT = []
  }

  else{
    console.log("Initial position0: " + $( ".ball" ).top + "  " + $( ".ball" ).left)
    $("#playState").html("noPlay")
    $("#buttonPause").hide();
    $("#buttonPlay").show();
    setState("FreeDraw");
    processSnapshotDisplay()
    console.log("Before reset")
    resetCanvas()
    console.log("After reset")
  }

  //we animate the movement
  $( ".ball" ).animate({
    top: top + "%",
    left: (left-offset) + "%"
  }, 200, "swing");

}


/////////////////////////////////////////////////////7
// function that sets the ball
// if resetPosition == true, then the ball will be set to the OCB
// else if resetPosition == false, the ball will be set to the player who has the ball currently
function SetBall(resetPosition, targetLayout){

  RemoveBall();

  var pos = [];
  var offset = 0;

  writeConsoleLog("SetBall: getPositionsForSystem(game[game.offensiveTeam].offensiveTeam):   "+getPositionsForSystem(game[game.offensiveTeam].offensiveTeam));
  writeConsoleLog("SetBall: game.playerWithBall: "+game.playerWithBall);

  if(resetPosition){
      if(getPositionsForSystem(game[game.offensiveTeam].offensiveTeam).includes("OCB")){
         game.playerWithBall = "OCB";
      }
      else
      {
         game.playerWithBall = "OLB"; // Open Point: Check if all teams that doesn't have an OCB, have an OLB.
      }
  }

  if(targetLayout.includes("Full")){
    offset = 0
  }else{
    offset = 2.5
  }


  writeConsoleLog("SetBall: game.playerWithBall: "+game.playerWithBall);

  if((getPositionsForSystem(game[game.offensiveTeam].offensiveTeam)).includes(game.playerWithBall)){ //If player with Ball is from the attacking team
    pos.top = getPlayerValuePerID(game[game.offensiveTeam].courtPlayers[game.playerWithBall], "fieldPosY");
    pos.left = getPlayerValuePerID(game[game.offensiveTeam].courtPlayers[game.playerWithBall], "fieldPosX");
  }
  else{ //If player with Ball is from the defending team
    pos.top = getPlayerValuePerID(game[getDefensiveTeam()].courtPlayers[game.playerWithBall], "fieldPosY");
    pos.left = getPlayerValuePerID(game[getDefensiveTeam()].courtPlayers[game.playerWithBall], "fieldPosX");
  }

  writeConsoleLog("SetBall to player: "+getPlayerValuePerID(game[game.offensiveTeam].courtPlayers[game.playerWithBall], "playerNameShort"));
  writeConsoleLog("SetBall: pos.top:   "+pos.top+"pos.left:   "+pos.left);

  if( pos.top == undefined || pos.left == undefined){
    alert("There are no attackers, ball cannot be placed");
  }else if(playerFitsInScreen(pos.top, targetLayout)){
    $(".court").append("<h1 class='ball rotates' id='ball' </h1> ");

    pos = fieldPos2displayPos( pos.left-offset, pos.top, targetLayout);
    writeConsoleLog("SetBall: pos.top:   "+pos.top+"pos.left:   "+pos.left);

    $(".ball").css({'top':(pos.top), 'left': (pos.left)});
    //$("#" + game[game.offensiveTeam].courtPlayers[game.playerWithBall].playerId).addClass( "hasTheBall" );
  }else{
    removed_ball_pos_left = pos.left
    removed_ball_pos_top = pos.top
  }


}


/////////////////////////////////////////////////////7
// fucntion that returns coordinates of the center player
function FindCenterPlayer(){

  var offensiveTeam = ""
  //first we check which is the offensive team
  offensiveTeam = "."+game.offensiveTeam;

  var attackers=0;
  //we check if there are attackers
  $(offensiveTeam).each(function() {
    attackers++;
  });

  if(attackers==0){
    return{
      attackers: 0
    }
  }

  var perfectTop = 35;
  var perfectLeft = 50;
  var top = 100;
  var left = 100;
  var playerId = "";

  //we iterate the players looking for the center player
  // $(offensiveTeam).each(function() {
  //   var actualTop =  parseFloat($(this).css("top"))
  //   var actualLeft =  parseFloat($(this).css("left"))
  //   pos = displayPos2fieldPos( actualLeft, actualTop, "halfVertOffense" )
  //   actualTop = pos.top
  //   actualLeft = pos.left

  //   if ( Math.sqrt( (top-perfectTop)*(top-perfectTop) + (left-perfectLeft)*(left-perfectLeft) ) > Math.sqrt( (actualTop-perfectTop)*(actualTop-perfectTop) + (actualLeft-perfectLeft)*(actualLeft-perfectLeft) ) ){
  //     top = actualTop;
  //     left = actualLeft;
  //     playerId = $(this).attr('id');
  //   }
  // });

  $(".positionOCB").each(function(){
      var actualTop =  parseFloat($(this).css("top"))
      var actualLeft =  parseFloat($(this).css("left"))
      pos = displayPos2fieldPos( actualLeft, actualTop, "halfVertOffense" )
      actualTop = pos.top
      actualLeft = pos.left
      top = actualTop;
      left = actualLeft;
      playerId = $(this).attr('id');
  })


  //getting the position of the right Back in case the is no centerback
  if( playerId=="" ){
    $(".positionORB").each(function(){
        var actualTop =  parseFloat($(this).css("top"))
        var actualLeft =  parseFloat($(this).css("left"))
        pos = displayPos2fieldPos( actualLeft, actualTop, "halfVertOffense" )
        actualTop = pos.top
        actualLeft = pos.left
        top = actualTop;
        left = actualLeft;
        playerId = $(this).attr('id');
    })
  }

  //getting the position of the right Back in case the is no centerback
  if( playerId=="" ){
    $(".positionOLB").each(function(){
        var actualTop =  parseFloat($(this).css("top"))
        var actualLeft =  parseFloat($(this).css("left"))
        pos = displayPos2fieldPos( actualLeft, actualTop, "halfVertOffense" )
        actualTop = pos.top
        actualLeft = pos.left
        top = actualTop;
        left = actualLeft;
        playerId = $(this).attr('id');
    })
  }


  pos = fieldPos2displayPos( left-2, top, "halfVertOffense" )

  //returning the top and left values
  return {
          top: pos.top,
          left: pos.left,
          playerId: playerId,
          attackers: attackers
      };
}


//
