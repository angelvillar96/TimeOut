class Player{
     // team, id,   number, name, nameShort, validPositions, fieldPosX, fieldPosY, displayPosX, displayPosY, starter
    constructor(playerID, team, image_path, number, playerNameLong, playerNameShort, validPositions, fieldPosX, fieldPosY, displayPosX, displayPosY, starter) {

        this.playerID = playerID;
        this.team = team;
        this.number = number;
        this.playerNameLong = playerNameLong;
        this.playerNameShort = playerNameShort;
        this.validPositions = validPositions;
        this.fieldPosX = fieldPosX;
        this.fieldPosY = fieldPosY;
        this.displayPosX = displayPosX;
        this.displayPosY = displayPosY;
        this.starter = starter;

        var new_image_path

        if( image_path==undefined || image_path.length==0 ){
          var id_reshaped = ("0000" + playerID).slice(-5)
          new_image_path = "img/players/"+ id_reshaped + ".png"
        }else{
          new_image_path = image_path
        }

        this.image_path = new_image_path

    }

}


function createPlayers(game){

	// Create players Team A
	game.teamA.players = [];

	for (var i = 7; i >= 0; i--) {
		game.teamA.players.push(new Player(i, i, "Name example"));
	}

	// Create players Team A
	game.teamB.players = [];

	for (var i = 7; i >= 0; i--) {
		game.teamB.players.push(new Player(20+i, i, "Name example"));
	}

}



function getPlayerValuePerID(id, attribute){

    var ret = undefined;

    for(var i=0; i<game.teamA.players.length; i++){
        if(id == game.teamA.players[i].playerID){
            ret = game.teamA.players[i][attribute];
            return ret;
        }
    }

    for(var i=0; i<game.teamB.players.length; i++){
        if(id == game.teamB.players[i].playerID){
            ret = game.teamB.players[i][attribute];
            return ret;
        }
    }

    return ret;

}



function setPlayerValuePerID(id, attribute, value){

    var ret = undefined;
    for(var i=0; i<game.teamA.players.length; i++){
        if(id == game.teamA.players[i].playerID){
            game.teamA.players[i][attribute] = value;
            ret = true;
            return ret;
        }
    }
    for(var i=0; i<game.teamB.players.length; i++){
        if(id == game.teamB.players[i].playerID){
            game.teamB.players[i][attribute] = value;
            ret = true;
            return ret;
        }
    }

    // return ret;

}



function getPlayerValuePerPosition(team, position, attribute){

    var playerTeam = undefined;
    if(team == undefined){
        if(position == undefined){
            return undefined;
        }
        var offDef = position.charAt(0);
        if(offDef == "O"){
            playerTeam = game.offensiveTeam;
        }
        else if(offDef == "D"){
            playerTeam = getDefensiveTeam();
        }
        else{
            alert("Error: Position not recognised");
        }

    }
    else{
        playerTeam = team;
    }

    var ret = getPlayerValuePerID(game[playerTeam].courtPlayers[position], attribute);

    writeConsoleLog("VALUE team "+team+" position "+position+" attribute "+attribute+" value "+ret);

    return ret;
}



function setPlayerValuePerPosition(team, position, attribute, value){

    var playerTeam = undefined;
    if(team == undefined){
        var offDef = position.charAt(0);
        if(offDef == "O"){
            playerTeam = game.offensiveTeam;
        }
        else if(offDef == "D"){
            playerTeam = getDefensiveTeam();
        }
        else{
            alert("Error: Position not recognised");
        }

    }
    else{
        playerTeam = team;
    }

    var ret = setPlayerValuePerID(game[playerTeam].courtPlayers[position], attribute, value);

    writeConsoleLog("VALUE team "+team+" position "+position+" attribute "+attribute+" value "+ret);

    return ret;
}


function editPlayerValidPosition(id, deleteAll, value){

    var ret = undefined;
    for(var i=0; i<game.teamA.players.length; i++){
        if(id == game.teamA.players[i].playerID){
            if(deleteAll){
                game.teamA.players[i]["validPositions"] = value;
            }
            else{
                game.teamA.players[i]["validPositions"] = game.teamA.players[i]["validPositions"].concat(" "+value);
            }
            updatePlayerDataOnDatabase(game.teamA.players[i]);
            ret = true;
            return ret;
        }
    }
    for(var i=0; i<game.teamB.players.length; i++){
        if(id == game.teamB.players[i].playerID){
            if(deleteAll){
                game.teamB.players[i]["validPositions"] = value;
            }
            else{
                game.teamB.players[i]["validPositions"] = game.teamB.players[i]["validPositions"].concat(" "+value);
            }
            updatePlayerDataOnDatabase(game.teamB.players[i]);
            ret = true;
            return ret;
        }
    }

    // return ret;
}


//
