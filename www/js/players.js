////////////////////////////////////////////////////////////////////////////
// SCRIPT CONTAING THE NECESSARY FUNCTIONS FOR PLAYER POSITIONING AND MOVING
////////////////////////////////////////////////////////////////////////////

var PLAYERS_MOVED_IN_SNAPSHOT = [];

$(document).ready(function() {
// $(document).ready(function($) {

  ////////////////////////////////////////////
  //Deleting all defenders
  $("#defenseNone").click(function(){

    resetCanvas()
    actualSnapshotIndex = 0
    $("#playState").html("noPlay")
    setState("FreeDraw");
    processSnapshotDisplay()

    offensiveTeam = game.offensiveTeam
    if( offensiveTeam == "teamA" ){
      defensiveTeam = "teamB"
    }else{
      defensiveTeam = "teamA"
    }
    $("."+defensiveTeam).remove();
    $("."+defensiveTeam+"Arrow").remove();
  });


  ////////////////////////////////////////////
  //positioning of 5:1 defense
  $("#defense51").click(function(){

    resetCanvas()
    actualSnapshotIndex = 0
    $("#playState").html("noPlay")
    setState("FreeDraw");
    processSnapshotDisplay()

    offensiveTeam = game.offensiveTeam
    if( offensiveTeam == "teamA" ){
      defensiveTeam = "teamB"
    }else{
      defensiveTeam = "teamA"
    }
    $("."+defensiveTeam).remove();
    $("."+defensiveTeam+"Arrow").remove();

    // Set current offensive position
    game[defensiveTeam].defensiveTeam = "defensive51";
    setTeam(defensiveTeam, game[defensiveTeam].defensiveTeam, getCurrentLayout(), true);
  });


  ////////////////////////////////////////////
  //positioning of 6:0 defense
  $("#defense60").click(function(){

    resetCanvas()
    actualSnapshotIndex = 0
    $("#playState").html("noPlay")
    setState("FreeDraw");
    processSnapshotDisplay()

    offensiveTeam = game.offensiveTeam
    if( offensiveTeam == "teamA" ){
      defensiveTeam = "teamB"
    }else{
      defensiveTeam = "teamA"
    }
    $("."+defensiveTeam).remove();
    $("."+defensiveTeam+"Arrow").remove();

    // Set current offensive position
    game[defensiveTeam].defensiveTeam = "defensive60";
    setTeam(defensiveTeam, game[defensiveTeam].defensiveTeam, getCurrentLayout(), true);
  });


  ////////////////////////////////////////////
  //positioning of 3:2:1 defense
  $("#defense321").click(function(){

    resetCanvas()
    actualSnapshotIndex = 0
    $("#playState").html("noPlay")
    setState("FreeDraw");
    processSnapshotDisplay()

    offensiveTeam = game.offensiveTeam
    if( offensiveTeam == "teamA" ){
      defensiveTeam = "teamB"
    }else{
      defensiveTeam = "teamA"
    }
    $("."+defensiveTeam).remove();
    $("."+defensiveTeam+"Arrow").remove();

    // Set current offensive position
    game[defensiveTeam].defensiveTeam = "defensive321";
    setTeam(defensiveTeam, game[defensiveTeam].defensiveTeam, getCurrentLayout(), true);
  });


  ////////////////////////////////////////////
  //positioning of 4:2 defense
  $("#defense42").click(function(){

    resetCanvas()
    actualSnapshotIndex = 0
    $("#playState").html("noPlay")
    setState("FreeDraw");
    processSnapshotDisplay()

    offensiveTeam = game.offensiveTeam
    if( offensiveTeam == "teamA" ){
      defensiveTeam = "teamB"
    }else{
      defensiveTeam = "teamA"
    }
    $("."+defensiveTeam).remove();
    $("."+defensiveTeam+"Arrow").remove();

    // Set current offensive position
    game[defensiveTeam].defensiveTeam = "defensive42";
    setTeam(defensiveTeam, game[defensiveTeam].defensiveTeam, getCurrentLayout(), true);
  });


  ////////////////////////////////////////////
  //positioning of 3:3 defense
  $("#defense33").click(function(){

    resetCanvas()
    actualSnapshotIndex = 0
    $("#playState").html("noPlay")
    setState("FreeDraw");
    processSnapshotDisplay()

    offensiveTeam = game.offensiveTeam
    if( offensiveTeam == "teamA" ){
      defensiveTeam = "teamB"
    }else{
      defensiveTeam = "teamA"
    }
    $("."+defensiveTeam).remove();
    $("."+defensiveTeam+"Arrow").remove();

    // Set current offensive position
    game[defensiveTeam].defensiveTeam = "defensive33";
    setTeam(defensiveTeam, game[defensiveTeam].defensiveTeam, getCurrentLayout(), true);
  });


  ////////////////////////////////////////////
  //positioning of 5:0 defense
  $("#defense50").click(function(){

    resetCanvas()
    actualSnapshotIndex = 0
    $("#playState").html("noPlay")
    setState("FreeDraw");
  processSnapshotDisplay()

    offensiveTeam = game.offensiveTeam
    if( offensiveTeam == "teamA" ){
      defensiveTeam = "teamB"
    }else{
      defensiveTeam = "teamA"
    }
    $("."+defensiveTeam).remove();
    $("."+defensiveTeam+"Arrow").remove();

    // Set current offensive position
    game[defensiveTeam].defensiveTeam = "defensive50";
    setTeam(defensiveTeam, game[defensiveTeam].defensiveTeam, getCurrentLayout(), true);
  });


  ////////////////////////////////////////////
  //positioning of 4:1 defense
  $("#defense41").click(function(){

    resetCanvas()
    actualSnapshotIndex = 0
    $("#playState").html("noPlay")
    setState("FreeDraw");
    processSnapshotDisplay()

    offensiveTeam = game.offensiveTeam
    if( offensiveTeam == "teamA" ){
      defensiveTeam = "teamB"
    }else{
      defensiveTeam = "teamA"
    }
    $("."+defensiveTeam).remove();
    $("."+defensiveTeam+"Arrow").remove();

    // Set current offensive position
    game[defensiveTeam].defensiveTeam = "defensive41";
    setTeam(defensiveTeam, game[defensiveTeam].defensiveTeam, getCurrentLayout(), true);
  });


  ////////////////////////////////////////////
  //positioning of 4:0 defense
  $("#defense40").click(function(){

    resetCanvas()
    actualSnapshotIndex = 0
    $("#playState").html("noPlay")
    setState("FreeDraw");
    processSnapshotDisplay()

    offensiveTeam = game.offensiveTeam
    if( offensiveTeam == "teamA" ){
      defensiveTeam = "teamB"
    }else{
      defensiveTeam = "teamA"
    }
    $("."+defensiveTeam).remove();
    $("."+defensiveTeam+"Arrow").remove();

    // Set current offensive position
    game[defensiveTeam].defensiveTeam = "defensive40";
    setTeam(defensiveTeam, game[defensiveTeam].defensiveTeam, getCurrentLayout(), true);
  });


  /////////////////////////////////////////////////////
  // removing all attackers
  $("#offensiveNone").click(function(){

    resetCanvas()
    actualSnapshotIndex = 0
    $("#playState").html("noPlay")
    setState("FreeDraw");
    processSnapshotDisplay()

    offensiveTeam = game.offensiveTeam
    $("."+offensiveTeam).remove();
    $("."+offensiveTeam+"Arrow").remove();
    RemoveBall();
  });


  /////////////////////////////////////////////////////
  // positioning Offensive 3:3
  $("#offensive33").click(function(){

    resetCanvas()
    actualSnapshotIndex = 0
    $("#playState").html("noPlay")
    setState("FreeDraw");
    processSnapshotDisplay()

    offensiveTeam = game.offensiveTeam
    $("."+offensiveTeam).remove();
    $("."+offensiveTeam+"Arrow").remove();

    // Set current offensive position
    game[offensiveTeam].offensiveTeam = "offensive33";
    setTeam(offensiveTeam, game[offensiveTeam].offensiveTeam, getCurrentLayout(), true);
    writeConsoleLog("game.offensiveTeam: "+ game.offensiveTeam+"    game.teamA.offensiveTeam: "+game.teamA.offensiveTeam+"   game.teamB.offensiveTeam: "+game.teamB.offensiveTeam);

  });


  /////////////////////////////////////////////////////
  // positioning Offensive 3:4
  $("#offensive34").click(function(){

    resetCanvas()
    actualSnapshotIndex = 0
    $("#playState").html("noPlay")
    setState("FreeDraw");
    processSnapshotDisplay()

    offensiveTeam = game.offensiveTeam
    $("."+offensiveTeam).remove();
    $("."+offensiveTeam+"Arrow").remove();

    // Set current offensive position
    game[offensiveTeam].offensiveTeam = "offensive34";
    setTeam(offensiveTeam, game[offensiveTeam].offensiveTeam, getCurrentLayout(), true);
    writeConsoleLog("game.offensiveTeam: "+ game.offensiveTeam+"    game.teamA.offensiveTeam: "+game.teamA.offensiveTeam+"   game.teamB.offensiveTeam: "+game.teamB.offensiveTeam);

  });


  /////////////////////////////////////////////////////
  // positioning Offensive 2:4
  $("#offensive24").click(function(){

    resetCanvas()
    actualSnapshotIndex = 0
    $("#playState").html("noPlay")
    setState("FreeDraw");
    processSnapshotDisplay()

    offensiveTeam = game.offensiveTeam
    $("."+offensiveTeam).remove();
    $("."+offensiveTeam+"Arrow").remove();

    // Set current offensive position
    game[offensiveTeam].offensiveTeam = "offensive24";
    setTeam(offensiveTeam, game[offensiveTeam].offensiveTeam, getCurrentLayout(), true);
    writeConsoleLog("game.offensiveTeam: "+ game.offensiveTeam+"    game.teamA.offensiveTeam: "+game.teamA.offensiveTeam+"   game.teamB.offensiveTeam: "+game.teamB.offensiveTeam);

  });


  /////////////////////////////////////////////////////
  // positioning Offensive 3:2
  $("#offensive32").click(function(){

    resetCanvas()
    actualSnapshotIndex = 0
    $("#playState").html("noPlay")
    setState("FreeDraw");
    processSnapshotDisplay()

    offensiveTeam = game.offensiveTeam
    $("."+offensiveTeam).remove();
    $("."+offensiveTeam+"Arrow").remove();

    // Set current offensive position
    game[offensiveTeam].offensiveTeam = "offensive32";
    setTeam(offensiveTeam, game[offensiveTeam].offensiveTeam, getCurrentLayout(), true);
    writeConsoleLog("game.offensiveTeam: "+ game.offensiveTeam+"    game.teamA.offensiveTeam: "+game.teamA.offensiveTeam+"   game.teamB.offensiveTeam: "+game.teamB.offensiveTeam);

  });


  // To be triggered when the modal openSubstitutionPopup is being closed
  $('#openSubstitutionPopup').on('hidden.bs.modal', function (e) {
      $('#substitutionModalTitle').text("");
      $('#playersReadyToBeSubstituted').text("");
  });


});


///////////////////////////////////////////////////
// Function to delete player
function deletePlayer(playerPosition){
  writeConsoleLog("Player to be removed: "+playerPosition);
  $(".position"+playerPosition).remove();
}


/////////////////////////////////////////////////////
// function that set a player given a team ("teamA" or "teamB"), a position ("DGK","OLW",...), a playing position (playerPos[1], playerPos[0] --> fieldPosX, fieldPosY) and a Layout.
function setPlayer(playerTeam, position, playerPos, targetLayout){

  var recordThisPlayer = false;
  var recordingIndex = 0;
  var playerId = undefined;

  // if(playerFitsInScreen(playerPos[0], targetLayout)){
    // Choose and create Player
    if( playerTeam == "teamA"){
        for( i=0; i<game.teamA.players.length; i++  ){

            if( ($("#"+game.teamA.players[i].playerID).length == 0) && (game.teamA.players[i].playerID == game.teamA.courtPlayers[position]) ){

               // writeConsoleLog("IN game.teamA.players[i].playerID: "+ game.teamA.players[i].playerID + "  game.teamA.courtPlayers[position]: "+ game.teamA.courtPlayers[position]);
                var img = "url('"+game.teamA.players[i].image_path+"')";
                $(".court").append("<div class='players teamA position" + position + " rotates' id='"+game.teamA.players[i].playerID+"'><div class='players-content'>"+game.teamA.players[i].number+"</div></div>");
                $("#"+game.teamA.players[i].playerID).css({"border-color":game.teamA.lineColorA, "color": game.teamA.textColorA});
                if(game.teamA.players[i].playerNameShort.includes("player")){ // for dummy players
                  $("#"+game.teamA.players[i].playerID).css({"background-image":"url('./img/players/dummyPlayer"+game.teamA.textColorA+".png')"});
                }
                else{
                  $("#"+game.teamA.players[i].playerID).css({"background-image":img});
                }
                playerId = game.teamA.players[i].playerID;
                break;
            }
        }
    }

    else if(playerTeam == "teamB"){
        for( i=0; i<game.teamB.players.length; i++  ){
            if( $("#"+game.teamB.players[i].playerID).length == 0 && + (game.teamB.players[i].playerID === game.teamB.courtPlayers[position]) ){
                var img = "url('"+game.teamB.players[i].image_path+"')";
                $(".court").append("<div class='players teamB position" + position + " rotates' id='"+game.teamB.players[i].playerID+"'><div class='players-content'>"+game.teamB.players[i].number+"</div></div>");
                $("#"+game.teamB.players[i].playerID).css({"border-color":game.teamB.lineColorA, "color": game.teamB.textColorA});
                if(game.teamB.players[i].playerNameShort.includes("player")){ // for dummy players
                  $("#"+game.teamB.players[i].playerID).css({"background-image":"url('./img/players/dummyPlayer"+game.teamB.textColorA+".png')"});
                }
                else{
                  $("#"+game.teamB.players[i].playerID).css({"background-image":img});
                }
                playerId = game.teamB.players[i].playerID;
                break;
            }
        }
    }


  //  if(!idExist){ // If element hasn't yet been created

      // Make elements draggable
      $("#"+playerId).draggable({

        //delay to avoid confusing dragging with clicking
        delay: 100,
        //refreshPositions: true,
        containment: "#courtImage",

        // Elements look bigger and draw a shadow when lifting
        start: function(){

          //$(this).css({'height':'6vh', 'width':'6vh', 'box-shadow' : '7px 7px 2px 2px grey'});
          setPlayerAttributes(playerId, targetLayout, "drag");

          //passing during playing mode goes to FreeDrawWithBackup mode (edit mode)
          if( $("#playState").html() == "playing" || $("#playState").html() == "opened" || $("#playState").html() == "paused" || $("#playState").html() == "stopped" ){
              copy_tactic()
              $("#playState").html("edit")
              setState("FreeDrawWithBackup");
              processSnapshotDisplay()
          }

          //if we are on record mode, we start saving the movement in the moves vector of the player
          if( $("#modeHidden").html() == "recording"){

            if( PLAYERS_MOVED_IN_SNAPSHOT.includes( $(this).attr("id") ) ){
              PLAYERS_MOVED_IN_SNAPSHOT = [];
              PLAYERS_MOVED_IN_SNAPSHOT.push( $(this).attr("id") )
              newSnapshot( false )
            }else{
              PLAYERS_MOVED_IN_SNAPSHOT.push( $(this).attr("id") );
            }

            //iterating the moves to find the move correspondig to this player
            for( i=0; i<actualMoves.length; i++ ){

              if( actualMoves[i].position == getPositionOfPlayerPerId($(this).attr("id")) ){

                writeConsoleLog("recording: " + $(this).attr("id") )
                //we save the position of the player in its Move element of the corresponding Snapshot
                recordThisPlayer = true
                recordingIndex = i
                var actualTop = parseFloat($(this).css('top'))
                var actualLeft = parseFloat($(this).css('left'))
                pos = convert2Field( actualLeft, actualTop )
                actualMoves[recordingIndex].positions.push( pos.left + "&&" +  pos.top )
                break
              }
            }
          }

          else{
            $("#playState").html("noPlay")
            setState("FreeDraw");
            processSnapshotDisplay()
            resetCanvas()
          }

            // Test with offDefPairs
            var offDefPairs = window.localStorage.drawDefensivePairs;
            writeConsoleLog("ASSIGNPAIRS window.localStorage.drawDefensivePairs: "+window.localStorage.drawDefensivePairs);
            if(offDefPairs != "false"){
              writeConsoleLog("ASSIGNPAIRS Called");
              assignPairs(drawOffDefLines);
            }

        },


        // Elements go back to original size when dropping.
        stop: function(){

          setPlayerAttributes(playerId, targetLayout, "stop");
          console.log("first log")

          if( game.playerWithBall == getPositionOfPlayerPerId($(this).attr("id")) ){
            $(".ball").css({'top': actualTop+'%', 'left' : (actualLeft-2)+'%'});
          }

          console.log("second log")

          //storing last position
          var actualTop = parseFloat($(this).css('top'))
          var actualLeft = parseFloat($(this).css('left'))
          console.log("second point five log")
          console.log(convert2Field_playersJS)
          console.log("second point seventy five log")
          pos = convert2Field_playersJS( actualLeft, actualTop )
          // pos = convert2Field( actualLeft, actualTop )

          console.log("third log")

          // Store last position in the player object
          setPlayerValuePerID(playerId, "fieldPosX", pos.left);
          setPlayerValuePerID(playerId, "fieldPosY", pos.top);

          // Test with offDefPairs
          var offDefPairs = window.localStorage.drawDefensivePairs;
          writeConsoleLog("ASSIGNPAIRS window.localStorage.drawDefensivePairs: "+window.localStorage.drawDefensivePairs);
          if(offDefPairs != "false"){
            writeConsoleLog("ASSIGNPAIRS Called");
            assignPairs(drawOffDefLines);
          }

          //updating the defense positions in the automatic defense functionality
          var automaticDefenseMovement = window.localStorage.automaticDefenseMovement;
          writeConsoleLog("ASSIGNPAIRS window.localStorage.automaticDefenseMovement: "+window.localStorage.automaticDefenseMovement);
          if(automaticDefenseMovement == "true"){
            updateAutomaticDefense()
          }

          //logic necessary when the player is being recorded
          if( recordThisPlayer == true ){
            //writeConsoleLog( act )
            actualMoves[recordingIndex].positions.push( pos.left + "&&" +  pos.top )
            //writeConsoleLog("\n")
            //writeConsoleLog("Positions for player: " + $(this).attr("id"))
            //writeConsoleLog( actualMoves[recordingIndex].player + "  " + actualMoves[recordingIndex].number + "  " + actualMoves[recordingIndex].position)
            //writeConsoleLog(actualMoves[recordingIndex].positions)
            //writeConsoleLog("\n")

            //creating the arrow for the move
            createArrowMove( actualMoves[recordingIndex].positions, $(this).attr("id"), "-" )
            recordThisPlayer = false

            //if the moved player had the ball, we start a new snapshot
            writeConsoleLog( game.playerWithBall )
            writeConsoleLog( getPositionOfPlayerPerId( $(this).attr("id") ) )
            if( game.playerWithBall == getPositionOfPlayerPerId( $(this).attr("id") ) ){
              PLAYERS_MOVED_IN_SNAPSHOT = [];
              newSnapshot( false )
            }
          }


        },


        //if the player has the ball, we move the player and the ball together
        drag: function( event, ui) {

          ballOffset = getBallOffsetsForLayout(getCurrentLayout());

          if( game.playerWithBall == getPositionOfPlayerPerId( $(this).attr("id") ) ){
            var actualTop = ui.position.top / $(this).parent().height() * 100
            var actualLeft = ui.position.left / $(this).parent().width() * 100
            $(".ball").css({'top': (actualTop+ballOffset.top)+'%', 'left' : (actualLeft+ballOffset.left)+'%'});
          }

          //logic necessary when the player is being recorded
          if( recordThisPlayer == true ){

            //we check the actual position of the player
            //var actualTop = ui.position.top
            //var actualLeft = ui.position.left
            //var actualTop = ui.offset.top
            //var actualLeft = ui.offset.left
            var actualTop = parseFloat($(this).css('top'))
            var actualLeft = parseFloat($(this).css('left'))
            pos = convert2Field( actualLeft, actualTop )

            //we get the previous saved position
            initialPosition = actualMoves[recordingIndex].positions[ (actualMoves[recordingIndex].positions).length - 1 ]
            iniPosTop = initialPosition.split("&&")[1]
            iniPosLeft = initialPosition.split("&&")[0]

            //we calculate the distance in between points
            distance = Math.sqrt( (pos.top - iniPosTop)*(pos.top - iniPosTop) + (pos.left - iniPosLeft)*(pos.left - iniPosLeft)  );

            //if the distance is greater than 1%, we take a sample
            if( distance >= 0.5 ){
              actualMoves[recordingIndex].positions.push( pos.left  + "&&" + pos.top )
              //writeConsoleLog("left: " + pos.left + "  top: " + pos.top + "  distance:  " + distance)
            }
          }

            var offDefPairs2 = window.localStorage.drawDefensivePairs;
            writeConsoleLog("ASSIGNPAIRS window.localStorage.drawDefensivePairs: "+window.localStorage.drawDefensivePairs);
            if(offDefPairs2 != "false"){
              var actualTop2 = parseFloat($(this).css('top'))
              var actualLeft2 = parseFloat($(this).css('left'))
              updateLine(playerId, actualLeft2, actualTop2);
            }


        }


      });


      // Make elements double-clickable
      $("#"+playerId).on( "tap",  {receiver: playerId}, tap )
      //$("#"+playerId).on( "taphold", {activePlayer: playerId, activePlayerPosition: position}, substitutionPopup);

      // Set position of player and ball if necessary
      pos = fieldPos2displayPos( playerPos[1], playerPos[0], targetLayout);
      writeConsoleLog("SetPlayer PlayerId: "+playerId+" fieldPos   top: " + playerPos[1] + "   left:  " + playerPos[0] + "  layout: " + targetLayout)
      writeConsoleLog("SetPlayer PlayerId: "+playerId+" diplayPos   top: " + pos.top + "   left:  " + pos.left)
      $( "#"+playerId).css({'top': pos.top, 'left' : pos.left});
      //$( "#"+playerId).animate({
      //  top: pos.top,
      //  left: pos.left
      //}, 0 );


      //storing starting position
      setPlayerValuePerID(playerId, "fieldPosX", playerPos[1]);
      setPlayerValuePerID(playerId, "fieldPosY", playerPos[0]);

      // else{ //remove player and createArrow
      if(!playerFitsInScreen(playerPos[0], targetLayout)){
        $("#"+playerId).remove();
        writeConsoleLog("Set Arrow! Player "+playerId+" doesn't fit in the screen");
        writeConsoleLog( playerPos + "   " + position )
        writeConsoleLog(targetLayout)
        createArrow(playerTeam, targetLayout, position, playerPos[1]);
      }

      // Set Players attributes
      setPlayerAttributes(playerId, targetLayout, "stop");
}


/////////////////////////////////////////7///
//event handler for the taps to pass the ball
function tap( event ){

  var sender = ""

  $(".rotates").each(function() {
    writeConsoleLog(game.playerWithBall)
    if( game.playerWithBall == getPositionOfPlayerPerId($(this).attr("id")) ){
      sender = $(this).attr("id");
    }
  });

  var receiver = event.data.receiver;

  PassTheBall(receiver, sender);
  // console.log(getCurrentLayout())
  // console.log($(".ball"))
  // console.log(getCurrentLayout())
  // SetBall(false, getCurrentLayout())

}


/////////////////////////////////////////7/////////////////////////////
// To be triggered when the modal openSubstitutionPopup is being opened
function substitutionPopup(event){

  var playerIndex = undefined;
  var playerToBeSubstituted = event.data.activePlayer;
  var currentTeam = undefined; // playerToBeSubstituted.slice(0,5);
  var positionToBeSubstituted = event.data.activePlayerPosition;
  var positionLongName = getPositionLongName(positionToBeSubstituted);

  $('#substitutionModalTitle').text(" "+positionLongName);

  // Find Team and index of current player
  for(var i=0; i< game.teamA.players.length; i++){
    if(game.teamA.players[i].playerID == playerToBeSubstituted){
      playerIndex = i;
      currentTeam = "teamA";
    }
  }
  if(currentTeam == undefined){
    for(var i=0; i< game.teamB.players.length; i++){
      if(game.teamB.players[i].playerID == playerToBeSubstituted){
        playerIndex = i;
        currentTeam = "teamB";
      }
    }
  }

  if(currentTeam == "teamA"){
    for (var i = 0; i < game.teamA.players.length; i++) {
      if(game.teamA.players[i].playerID == playerToBeSubstituted){
          $('#playerToBeSubstitutedName').text(" "+game.teamA.players[i].number+" - "+game.teamA.players[i].playerNameLong);
          // $('#playerToBeSubstitutedPic').attr('src','./img/players/'+playerToBeSubstituted+'.png')
          if(game.teamA.players[i].playerNameShort.includes("player")){ // for dummy players
            $('#playerToBeSubstitutedPic').attr('src','./img/players/dummyPlayer'+game.teamA.textColorA+'.png')
          }
          else{
            $('#playerToBeSubstitutedPic').attr('src', game.teamA.players[i].image_path)
          }
      }
      else{
        var a = "";
        if(game.teamA.players[i].validPositions.includes(positionToBeSubstituted)){
          a = "same-position";
        }
        $("#playersReadyToBeSubstituted").append("<button type='button' class='substitution-players-list list-group-item-action col-5 m-1 "+a+"' id='substitutionPopupPlayer"+game.teamA.players[i].playerID+"' data-dismiss='modal'>"+game.teamA.players[i].number+" - "+game.teamA.players[i].playerNameShort+"</button><br>");
        $("#substitutionPopupPlayer"+game.teamA.players[i].playerID).on( "tap", {team: "teamA", position: positionToBeSubstituted, playerOut: playerToBeSubstituted, playerIn: game.teamA.players[i].playerID}, substitutionAction);
      }
    }
  }
  else if(currentTeam == "teamB"){
    for (var i = 0; i < game.teamB.players.length; i++) {
      if(game.teamB.players[i].playerID == playerToBeSubstituted){
          $('#playerToBeSubstitutedName').text("  " +game.teamB.players[i].number+" - "+game.teamB.players[i].playerNameLong);
          //$('#playerToBeSubstitutedPic').attr('src','./img/players/'+playerToBeSubstituted+'.png')
          if(game.teamB.players[i].playerNameShort.includes("player")){ // for dummy players
            $('#playerToBeSubstitutedPic').attr('src','./img/players/dummyPlayer'+game.teamB.textColorA+'.png')
          }
          else{
            $('#playerToBeSubstitutedPic').attr('src', game.teamB.players[i].image_path)
          }
      }
      else{
        var a = "";
        if(game.teamB.players[i].validPositions.includes(positionToBeSubstituted)){
          a = "same-position";
        }
        $("#playersReadyToBeSubstituted").append("<button type='button' class='substitution-players-list list-group-item-action col-5 m-1 "+a+"' id='substitutionPopupPlayer"+game.teamB.players[i].playerID+"' data-dismiss='modal'>"+game.teamB.players[i].number+" - "+game.teamB.players[i].playerNameShort+"</button><br>");

        $("#substitutionPopupPlayer"+game.teamB.players[i].playerID).on( "tap", {team: "teamB", position: positionToBeSubstituted, playerOut: playerToBeSubstituted, playerIn: game.teamB.players[i].playerID}, substitutionAction);
      }
    }
  }

  $('#openSubstitutionPopup').modal('show');

}


/////////////////////////////////////////7/////////////////////////////
function substitutionAction(event){
  //

  var substitutionTeam = event.data.team;
  var substitutionPositionPlayerOut = event.data.position;
  var playerIDOut = event.data.playerOut;
  var playerIDIn = event.data.playerIn;

  fieldPosX_playerIDIn = getPlayerValuePerID(playerIDIn, "fieldPosX");
  fieldPosY_playerIDIn = getPlayerValuePerID(playerIDIn, "fieldPosY");
  fieldPosX_playerIDOut = getPlayerValuePerID(playerIDOut, "fieldPosX");
  fieldPosY_playerIDOut = getPlayerValuePerID(playerIDOut, "fieldPosY");

  setPlayerValuePerID(playerIDIn, "fieldPosX", fieldPosX_playerIDOut);
  setPlayerValuePerID(playerIDIn, "fieldPosY", fieldPosY_playerIDOut);

  setPlayerValuePerID(playerIDOut, "fieldPosX", fieldPosX_playerIDIn);
  setPlayerValuePerID(playerIDOut, "fieldPosY", fieldPosY_playerIDIn);

  writeConsoleLog("SUB Starts substitutionAction for team: "+substitutionTeam);
  writeConsoleLog("SUB Player IN will be in: "+getPlayerValuePerID(playerIDIn, "fieldPosX")+" : "+getPlayerValuePerID(playerIDIn, "fieldPosY"));

  //Change Player
  var offDef = undefined; // Temporal variable
  if(substitutionTeam == game.offensiveTeam){
    offDef = "offensiveTeam";
  }
  else{
    offDef = "defensiveTeam";
  }

  var currentPosition = game[substitutionTeam][offDef]; // e.g. currentPosition = "offensive33"
  var positionsArray = getPositionsForSystem(currentPosition);

  // Check if playerIDIn is currently playing
  var isPlayerInPlaying = false;
  for(var i=0;  i<positionsArray.length; i++){
      if(game[substitutionTeam].courtPlayers[positionsArray[i]] == playerIDIn){
        isPlayerInPlaying = true;
        substitutionPositionPlayerIn = positionsArray[i];
      }
  }
  game[substitutionTeam].courtPlayers[substitutionPositionPlayerOut] = playerIDIn;

  if(isPlayerInPlaying){ // If playerIn is already playing then switch positions of players
    game[substitutionTeam].courtPlayers[substitutionPositionPlayerIn] = playerIDOut;
  }

  if(offDef == "offensiveTeam"){
    setTeam(substitutionTeam, game[substitutionTeam].offensiveTeam, getCurrentLayout(), false);
  }
  else{
    setTeam(substitutionTeam, game[substitutionTeam].defensiveTeam, getCurrentLayout(), false);
  }

  //updating the table on the database so the changes are permanent
  // updateCourtPlayersOnDatabase( game[substitutionTeam] )
  teams_db.indexedDB.updateCourtPlayers(game[substitutionTeam]);


}


//////////////////////////////////////////////////////////////
//function that sets a team given its predetermined system
function setTeam(teamToSet, playingSystem, targetLayout, resetPosition){

  $("."+teamToSet).remove();
  $(".arrowsOutOfSight"+teamToSet).remove();

  writeConsoleLog( "teamToSet, playingSystem "+teamToSet+" "+ playingSystem );

  var playingPositions = getPositionsForSystem(playingSystem);


  for(var i=0; i<playingPositions.length; i++){
    var settingPosition = [];

    if(resetPosition){
      settingPosition = getPositionFieldForPosition(playingPositions[i]);
    }
    else
    {
      settingPosition[0] = getPlayerValuePerID(game[teamToSet].courtPlayers[playingPositions[i]], "fieldPosY");
      settingPosition[1] = getPlayerValuePerID(game[teamToSet].courtPlayers[playingPositions[i]], "fieldPosX");
    }

    var currentPlayersIDs = [];
    for(var j=0; j<i; j++){
      currentPlayersIDs.push(game[teamToSet].courtPlayers[playingPositions[j]])

    }
    writeConsoleLog("SETTEAM CurrentPlayersIDs: "+currentPlayersIDs)

    var k = 1;
    while(currentPlayersIDs.includes(game[teamToSet].courtPlayers[playingPositions[i]])){
      writeConsoleLog("SETTEAM Player was included already: "+game[teamToSet].courtPlayers[playingPositions[i]])
      game[teamToSet].courtPlayers[playingPositions[i]] = game[teamToSet].players[k].playerID;
      k++
      writeConsoleLog("SETTEAM New Player: "+game[teamToSet].courtPlayers[playingPositions[i]])
    }


    writeConsoleLog("teamToSet: "+teamToSet+ "   playingPositions[i]: "+playingPositions[i]+ "   settingPosition: "+settingPosition);
    setPlayer(teamToSet, playingPositions[i], settingPosition, targetLayout);

  }

  if(teamToSet == game.offensiveTeam){
    RemoveBall();
    if(resetPosition){
      SetBall(true, targetLayout);
    }
    else{
      SetBall(false, targetLayout);
    }

  }
  else{ // Things to do only for defenses
    var automaticDefenseMovement = window.localStorage.automaticDefenseMovement;
    if(automaticDefenseMovement == "true"){
      initAutomaticDefenseValues();
    }
  }

  if($(".line-offDef")){
    deleteOffDefPairsLines();
  }

//  initializeAnchorPoints()


  // var offDefPairs = window.localStorage.drawDefensivePairs;
  // writeConsoleLog("ASSIGNPAIRS window.localStorage.drawDefensivePairs: "+window.localStorage.drawDefensivePairs);
  // if(offDefPairs != "false"){
  //   writeConsoleLog("ASSIGNPAIRS Called");
  //   assignPairs(drawOffDefLines);
  // }
}



function getPositionsForSystem(playingSystem){

  var playingPositions = [];

  switch(playingSystem){
    case "offensive33":
      playingPositions = ["OGK", "OLW", "OLB", "OCB", "ORB", "ORW", "OPV1"];
      break;
    case "offensive033":
      playingPositions = ["OLW", "OLB", "OCB", "ORB", "ORW", "OPV1"];
      break;
    case "offensive34":
      playingPositions = ["OLW", "OLB", "OCB", "ORB", "ORW", "OPV1", "OPV2"];
      break;
    case "offensive24":
      playingPositions = ["OGK", "OLW", "OLB", "ORB", "ORW", "OPV1", "OPV2"];
      break;
    case "offensive32":
      playingPositions = ["OGK", "OLW", "OLB", "OCB", "ORB", "ORW"];
      break;
    case "defensive60":
      playingPositions = ["DGK", "DLO", "DLL", "DLCL", "DLCR", "DLR", "DRO"];
      break;
    case "defensive51":
      playingPositions = ["DGK", "DLO", "DLL", "DLC", "DLR", "DRO", "DFC"];
      break;
    case "defensive321":
      playingPositions = ["DGK", "DLO", "DHL", "DLC", "DHR", "DRO", "DFC"];
      break;
    case "defensive42":
      playingPositions = ["DGK", "DLO", "DHL", "DLCL", "DLCR", "DHR", "DRO"];
      break;
    case "defensive33":
      playingPositions = ["DGK", "DLO", "DFL", "DLC", "DFR", "DRO", "DFC"];
      break;
    case "defensive50":
      playingPositions = ["DGK", "DLO", "DLL", "DLC", "DLR", "DRO"];
      break;
    case "defensive41":
      playingPositions = ["DGK", "DLO", "DLCL", "DLCR", "DRO", "DFC"];
      break;
    case "defensive40":
      playingPositions = ["DGK", "DLO", "DLCL", "DLCR", "DRO"];
      break;
  }

  return playingPositions;


}



function getPositionFieldForPosition(playingPositions){

    switch(playingPositions){
      case "DGK": // Goalkeeper
        settingPosition = [2, 49.5];
        break;
      case "DLO": // Defense Left Outside
        settingPosition = [10, 84.5];
        break;
      case "DLL": // Defense Line Left
        settingPosition = [16, 71.5];
        break;
      case "DLCL": // Defense Line Center Left
        settingPosition = [17, 58.5];
        break;
      case "DLC": // Defense Line Center
        settingPosition = [17, 49.5];
        break;
      case "DLCR": // Defense Line Center Right
        settingPosition = [17, 40.5];
        break;
      case "DLR": // Defense Line Right
        settingPosition = [16, 27.5];
        break;
      case "DRO": // Defense Right Outside
        settingPosition = [10, 13.5];
        break;
      case "DFC": // Defense Forward Center
        settingPosition = [25, 49.5];
        break;
      case "DFL": // Defense Forward Left
        settingPosition = [25, 74.5];
        break;
      case "DFR": // Defense Forward Right
        settingPosition = [25, 25.5];
        break;
      case "DHL": // Defense Half Left
        settingPosition = [20, 74.5];
        break;
      case "DHR": // Defense Half Right
        settingPosition = [20, 25.5];
        break;

      case "OGK": // Goalkeeper
        settingPosition = [75, 49.5];
        break;
      case "OLW": // Offensive Left Wing
        settingPosition = [2, 4.25];
        break;
      case "OLB": // Offensive Left Back
        settingPosition = [30, 11.5];
        break;
      case "OCB": // Offensive Center Back
        settingPosition = [35, 49.5];
        break;
      case "ORB": // Offensive Right Back
        settingPosition = [30, 87.7];
        break;
      case "ORW": // Offensive Right Wing
        settingPosition = [2, 95.25];
        break;
      case "OPV1": // Offensive Pivot
        settingPosition = [17, 63.5];
        break;
      case "OPV2": // Offensive 2nd Pivot
        settingPosition = [17, 36.5];
        break;
    }

    return settingPosition;
}



function playerFitsInScreen(fieldPosY, targetLayout){

    var ret = true;

    if((targetLayout.includes("HalfHoriz") && fieldPosY > 42) || (targetLayout.includes("HalfVert") && fieldPosY > 58)){
      writeConsoleLog("\n\n\n")
      writeConsoleLog("fieldPosY: "+fieldPosY+"   targetLayout: "+targetLayout);
      writeConsoleLog("\n\n\n")
      ret = false;
    }

    return ret;

}



function getPositionOfPlayerPerId(playerID){

  var ret = undefined;

  var playingPositions = getPositionsForSystem(game[game.offensiveTeam].offensiveTeam);

  for(var i=0; i<playingPositions.length; i++){
    if(game[game.offensiveTeam].courtPlayers[playingPositions[i]] == playerID){
      ret = playingPositions[i];
    }
  }

  if(ret == undefined){ // in case it was not an offensive Player
    playingPositions = getPositionsForSystem(game[getDefensiveTeam()].defensiveTeam);

    for(var i=0; i<playingPositions.length; i++){
      if(game[getDefensiveTeam()].courtPlayers[playingPositions[i]] == playerID){
        ret = playingPositions[i];
      }
    }
  }

  return ret;
}



function getDefensiveTeam(){

  var defTeam = undefined;

  if(game.offensiveTeam == "teamA"){
    defTeam = "teamB";
  }
  else{
    defTeam = "teamA";
  }

  return defTeam

}



function getOffensiveTeam(){
  var defTeam = getDefensiveTeam();
  var offTeam = undefined;

  if(defTeam == "teamA"){
    offTeam = "teamB";
  }
  else{
    offTeam = "teamA";
  }

  return offTeam;
}



function createArrow(playerTeam, targetLayout, position, playerPosX){


  var ArrowColor = game[playerTeam].filledColorA;
  var ArrowFieldPosX = playerPosX;
  var ArrowFieldPosY = undefined;
  var ArrowAngle = undefined;

  if(!targetLayout.includes("Full")){ // There're no arrows for Full field
    // Create Arrow
    if(playerTeam == "teamA"){
      // Style to change SVG colors: https://codepen.io/sosuke/pen/Pjoqqp
      $("#courtCanvasOutOfSight").append("<div class='arrowsOutOfSight"+playerTeam+"' id='ArrowOutOfSight"+playerTeam+position+"'><img class='ArrowOutOfSightImg' src='img/arrow-black.svg' alt='Arrow' style='filter: invert(15%) sepia(64%) saturate(4467%) hue-rotate(347deg) brightness(104%) contrast(97%);;'></div>");
    }
    else{
      $("#courtCanvasOutOfSight").append("<div class='arrowsOutOfSight"+playerTeam+"' id='ArrowOutOfSight"+playerTeam+position+"'><img class='ArrowOutOfSightImg' src='img/arrow-black.svg' alt='Arrow' style='filter: invert(10%) sepia(100%) saturate(6619%) hue-rotate(246deg) brightness(71%) contrast(141%);'></div>");
    }
    // Check target Layout and define position and angle
    if(targetLayout == "HalfHorizOffense"){
      ArrowFieldPosY = 41;
      ArrowAngle = 90;
    }
    else if(targetLayout == "HalfHorizDefense"){
      ArrowFieldPosY = 39;
      ArrowAngle = 270;
    }
    else if(targetLayout == "HalfVertOffense"){
      ArrowFieldPosY = 56;
      ArrowAngle = 0;
    }
    else if(targetLayout == "HalfVertDefense"){
      ArrowFieldPosY = 56;
      ArrowAngle = 180;
    }

    // Transfor to display coordinates
    writeConsoleLog( ArrowFieldPosX + "  " + ArrowFieldPosY )
    //pos = convert2Display( ArrowFieldPosX, ArrowFieldPosY )
    var pos = fieldPos2displayPos( parseFloat(ArrowFieldPosX), parseFloat(ArrowFieldPosY), targetLayout);
    writeConsoleLog("Set position of '#ArrowOutOfSight"+playerTeam+position+"' in: "+pos.top+" "+pos.left+" with angle: "+ArrowAngle+"deg");

    // Define color and size
    $("#ArrowOutOfSight"+playerTeam+position).css({'top': pos.top+"px", 'left': pos.left+"px"});
    $("#ArrowOutOfSight"+playerTeam+position).css("transform", "rotate("+ArrowAngle+"deg) scale(0.4)");

    //var actualArrow =  $("#arrowSVG"+playerTeam+position).children()
    //writeConsoleLog("\n\n\n\n")
    //var actualArrow =  $("#arrowSVG"+playerTeam+position)
    //writeConsoleLog(actualArrow)
    //var arrow = actualArrow[0]
    //writeConsoleLog(arrow)
    //var svg = arrow.contentDocument
    //writeConsoleLog(svg)
    //var a = svg.getElementById("svg8")
    //writeConsoleLog(a)
    //writeConsoleLog("\n\n\n\n")

  }

}



function removePlayerArrows(){

  $(".arrowsOutOfSightteamA").remove()
  $(".arrowsOutOfSightteamB").remove()

}



function setPlayerAttributes(playerId, targetLayout, mode){

  var modeToBeRemoved;
  if(mode == "drag"){
    modeToBeRemoved = "stop";
  }
  else if(mode == "stop"){
    modeToBeRemoved = "drag";
  }

  if(targetLayout.includes("Full")){
    $("#"+playerId).removeClass("player-HalfVert-"+mode);
    $("#"+playerId).removeClass("player-HalfHoriz-"+mode);
    $("#"+playerId).removeClass("player-Full-"+modeToBeRemoved);
    $("#"+playerId).addClass("player-Full-"+mode);
    if( game.playerWithBall == $("#"+playerId) ){
      $(".ball").css({'transform':'scale(0.6)'});
    }
  }
  else if(targetLayout.includes("HalfVert")){
    $("#"+playerId).removeClass("player-Full-"+mode);
    $("#"+playerId).removeClass("player-HalfHoriz-"+mode);
    $("#"+playerId).removeClass("player-HalfVert-"+modeToBeRemoved);
    $("#"+playerId).addClass("player-HalfVert-"+mode);
    if( game.playerWithBall == $("#"+playerId) ){
      $(".ball").css({'transform':'scale(0.8)'});
    }
  }
  else if(targetLayout.includes("HalfHoriz")){
    $("#"+playerId).removeClass("player-HalfVert-"+mode);
    $("#"+playerId).removeClass("player-Full-"+mode);
    $("#"+playerId).removeClass("player-HalfHoriz-"+modeToBeRemoved);
    $("#"+playerId).addClass("player-HalfHoriz-"+mode);
    if( game.playerWithBall == $("#"+playerId) ){
      $(".ball").css({'transform':'scale(0.85)'});
    }
  }

  writeConsoleLog("setPlayerAttributes for: "+targetLayout+" "+mode);
}



function getTeam( playerId, position ){

  position = position || ""
  var cur_pos

  if( $("#"+playerId).hasClass("teamA") ){
    cur_pos = "teamA"
  }
  else if( $("#"+playerId).hasClass("teamB") ){
    cur_pos = "teamB"
  }else{
    if(position[0]=="O"){
      cur_pos = "teamA"
    }else if(position[0]=="D"){
      cur_pos = "teamB"
    }else{
      cur_pos = "ball"
    }
  }

  return cur_pos

}



function reverseID(cur_pos){

  if(getCurrentLayout().includes("Defense")){
    if(cur_pos=="teamA"){
      cur_pos="teamB"
    }else if(cur_pos=="teamB"){
      cur_pos="teamA"
    }
  }

  return cur_pos
}



function serializeCourtPlayers( courtPlayersObject ){

  keys = Object.keys(courtPlayersObject)
  serializedCourtPlayers = ""

  for( i=0; i<keys.length; i++ ){
    serializedCourtPlayers += (courtPlayersObject[keys[i]])
    if( i!=keys.length-1 ){
      serializedCourtPlayers+=","
    }
  }

  return serializedCourtPlayers
}



function deserializeCourtPlayers( courtPlayersArray ){

  /* CHANGELOG:
    - 16/06/2020: REMOVED FOLLOWING LINE FOR MIGRATION TO INDEX-DB
        var stringCourtPlayers = courtPlayersArray.split(',');
  */
  var stringCourtPlayers = courtPlayersArray
  var actualCourtPlayers = new CourtPlayers();
  var j=0;
  for(var key in actualCourtPlayers){
       if (actualCourtPlayers.hasOwnProperty(key)) {
           actualCourtPlayers[key] = stringCourtPlayers[j];
           j++;
       }
  }
  return actualCourtPlayers

}



function getPositionLongName(positionToBeSubstituted){

  var ret = positionToBeSubstituted;

  switch(positionToBeSubstituted){

    case "OGK":
      ret = "Offensive Goalkeeper";
      break;

    case "DGK":
      ret = "Defensive Goalkeeper";
      break;

    case "DLO":
      ret = "Defense Left Outside";
      break;

    case "DLL":
      ret = "Defense Line Left";
      break;

    case "DLCL":
      ret = "Defense Line Center Left";
      break;

    case "DLC":
      ret = "Defense Line Center";
      break;

    case "DLCR":
      ret = "Defense Line Center Right";
      break;

    case "DLR":
      ret = "Defense Line Right";
      break;

    case "DRO":
      ret = "Defense Right Outside";
      break;

    case "DFC":
      ret = "Defense Forward Center";
      break;

    case "DFL":
      ret = "Defense Forward Left";
      break;

    case "DFR":
      ret = "Defense Forward Right";
      break;

    case "DHL":
      ret = "Defense Half Left";
      break;

    case "DHR":
      ret = "Defense Half Right";
      break;

    case "OLW":
      ret = "Offensive Left Wing";
      break;

    case "OLB":
      ret = "Offensive Left Back";
      break;

    case "OCB":
      ret = "Offensive Center Back";
      break;

    case "ORB":
      ret = "Offensive Right Back";
      break;

    case "ORW":
      ret = "Offensive Right Wing";
      break;

    case "OPV1":
      ret = "Offensive Line Player";
      break;

    case "OPV2":
      ret = "Offensive 2nd Line Player";
      break;

  }

  return ret;
}



function getTacticReadableName(tactic){

  var ret = tactic;

  switch(tactic){

    case "offensive33":
      ret = "(1)3:3";
      break;
    case "offensive033":
      ret = "(0)3:3";
      break;
    case "offensive24":
      ret = "(1)2:4";
      break;
    case "offensive32":
      ret = "(1)3:2";
      break;
    case "offensive34":
      ret = "(1)3:4";
      break;
    case "defensive60":
      ret = "6:0";
      break;
    case "defensive51":
      ret = "5:1";
      break;
    case "defensive40":
      ret = "4:0";
      break;
    case "defensive41":
      ret = "4:1";
      break;
    case "defensive50":
      ret = "5:0";
      break;
    case "defensive33":
      ret = "3:3";
      break;
    case "defensive42":
      ret = "4:2";
      break;
    case "defensive321":
      ret = "3:2:1";
      break;
  }

  return ret;
}


function convert2Field_playersJS( actualLeft, actualTop ){

  console.log("Initial log Convert2Field")

  if( $("#rotationHidden").html() == "HalfVert" && $("#orientationHidden").html() == "0" && $("#fieldTypeHidden").html() == "Half"){
    pos = displayPos2fieldPos( actualLeft, actualTop, "HalfVertOffense" )
  }
  else if(  $("#rotationHidden").html() == "HalfVert" && $("#orientationHidden").html() == "180" && $("#fieldTypeHidden").html() == "Half"){
    pos = displayPos2fieldPos( actualLeft, actualTop, "HalfVertDefense" )
  }
  else if(  $("#rotationHidden").html() == "HalfVert" && $("#orientationHidden").html() == "0" && $("#fieldTypeHidden").html() == "Full"){
    pos = displayPos2fieldPos( actualLeft, actualTop, "FullOffense" )
  }
  else if(  $("#rotationHidden").html() == "HalfVert" && $("#orientationHidden").html() == "180" && $("#fieldTypeHidden").html() == "Full"){
    pos = displayPos2fieldPos( actualLeft, actualTop, "FullDefense" )
  }
  else if(  $("#rotationHidden").html() == "HalfHoriz" && $("#orientationHidden").html() == "0" && $("#fieldTypeHidden").html() == "Half"){
    pos = displayPos2fieldPos( actualLeft, actualTop, "HorizOffense" )
  }
  else if(  $("#rotationHidden").html() == "HalfHoriz" && $("#orientationHidden").html() == "180" && $("#fieldTypeHidden").html() == "Half"){
    pos = displayPos2fieldPos( actualLeft, actualTop, "HorizDefense" )
  }

  console.log("Log log")

  return {
          top: pos.top,
          left: pos.left
      };

}



//
