/*##############################################################
// File that contains the methods to change Team and Player data
// ##############################################################*/

// var teamToEdit;
// var playerToEdit;
// var newPlayer;

// var data_directory
// var players_directory
// var teams_directory

// var globalFileEntry


// // $(document).ready(function($) {
// $(document).ready(function() {


//   //////////////////////////////////////////////////////////
//   // starting the file system to be able to save images and stuff
//   document.addEventListener("deviceready", function() {
//     window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {
//         console.log(fs)
//         data_directory = cordova.file.dataDirectory;
//         players_directory = cordova.file.applicationDirectory + "img/" + "players/"
//         teams_directory = cordova.file.applicationDirectory + "img/" + "teams/"
//         console.log(cordova.file.dataDirectory)
//     }, function(err) {});
//   }, false);


//   //////////////////////////////////////////////////////////
//   //method that prepares the EDIT TEAM modal with TeamA data
//   $("#editTeamA").click(function(){

//     $("#gameSettingsPopup").modal("hide");
//     teamToEdit = game.teamA
//     loadDataToEditTeamModal()

//   });


//   //////////////////////////////////////////////////////////
//   //method that prepares the EDIT TEAM modal with TeamB data
//   $("#editTeamB").click(function(){

//     $("#gameSettingsPopup").modal("hide");
//     teamToEdit = game.teamB
//     loadDataToEditTeamModal()

//   });


//   //////////////////////////////////////////////////////////
//   //method that saves the new data after the EDIT TEAM
//   $("#editTeamApply").click(function(){

//     var newImg = $('#edit-team-logo').attr('src') //this might be a little more complicated
//     var newName = $("#edit-team-name-input").val()
//     var newColor1Border = $("#edit-color-1-border").spectrum('get').toHexString()
//     var newColor1Background = $("#edit-color-1-background").spectrum('get').toHexString()
//     var newColor2Border = $("#edit-color-2-border").spectrum('get').toHexString()
//     var newColor2Background = $("#edit-color-2-background").spectrum('get').toHexString()
//     var newColorText = $("#edit-color-text").spectrum('get').toHexString()

//     // saving the data on the current team object
//     teamToEdit.teamName = newName
//     teamToEdit.lineColorA = newColor1Border
//     teamToEdit.filledColorA = newColor1Background
//     teamToEdit.lineColorB = newColor2Border
//     teamToEdit.filledColorB = newColor2Background
//     teamToEdit.textColorA = newColorText

//     // updating Database
//     updateTeamDataOnDatabase(teamToEdit)

//     // changing the displayed color of the players
//     if( teamToEdit==game.teamA ){
//       $(".teamA").css('border-color', teamToEdit.lineColorA);
//       $(".teamA").css('color', teamToEdit.textColorA);
//     }
//     if( teamToEdit==game.teamB ){
//       $(".teamB").css('border-color', teamToEdit.lineColorA);
//       $(".teamB").css('color', teamToEdit.textColorA);
//     }


//   });


//   //////////////////////////////////////////////////////////
//   //method that processes and saves the new data after the EDIT PLAYER
//   $("#editPlayerApply").click(function(){

//     var new_img_src = $("#edit-player-img").attr('src');
//     var new_name = $("#edit-player-name-input").val()
//     var new_short_name = $("#edit-player-short-name-input").val()
//     var new_number = $("#edit-player-number-input").val()
//     var error_message;

//     // if the image is undefined, we create an image with the correct name
//     if( new_img_src == "./img/players/dummyPlayerblack.png" ){

//       var new_img_name = "player_" + playerToEdit.playerID + ".png"

//       // putting new image on server
//       fetch(new_img_src)
//       .then(function(response) {
//         console.log("converting to blob")
//         return response.blob()
//       })
//       .then(function(blob) {
//         // here the image is a blob
//         // creating and loading the new image
//         window.requestFileSystem(LocalFileSystem.TEMPORARY, 0, function (fs) {

//             // saving the image
//             fs.root.getFile( new_img_name, { create: true, exclusive: false }, function (fileEntry) {
//                 writeFile(fileEntry, blob, "players");
//                 readFile(fileEntry, "players");
//             }, function er(e){
//               console.error("error in saving");
//               console.error(e)
//             });
//         }, function(err) {
//           console.error("upsi")
//           console.log(err)
//         });
//       });

//     }


//     // Name field cannot be empty
//     if( new_name.length<1 ){
//       error_message = "Player Name field cannot be empty"
//       $("#error-edit-player").html(error_message)
//       return
//     }

//     // Short name field cannot be empty
//     if( new_short_name.length<1 ){
//       error_message = "Short Name field cannot be empty"
//       $("#error-edit-player").html(error_message)
//       return
//     }

//     // Number cannot be empty
//     if( new_number.length<1 ){
//       error_message = "Number Name field cannot be empty"
//       $("#error-edit-player").html(error_message)
//       return
//     }

//     // Number cannot be already taken by other player
//     for(var i=0; i<teamToEdit.players.length;i++){
//       if( teamToEdit.players[i]==playerToEdit ){
//         continue
//       }
//       if( teamToEdit.players[i].number == new_number.toString() ){
//         error_message = "Number " + new_number + " is already taken"
//         $("#error-edit-player").html(error_message)
//         return
//       }
//     }

//     // updating the positions of the player
//     var highlighted_buttons = $(".btn-highlight")
//     var num_off_positions = 0
//     var num_def_positions = 0
//     var positions = []
//     for(var i=0;i<highlighted_buttons.length;i++){
//       id = highlighted_buttons[i].id.split("_")[0]
//       positions.push(id)
//       if( id[0]=="O" ){
//         num_off_positions++
//       }else{
//         num_def_positions++
//       }
//     }
//     positions = positions.join(" ")

//     // at least one offesive/defensive position must be selected
//     if( num_off_positions==0 ){
//       error_message = "At least one offensive position must be selected"
//       $("#error-edit-player").html(error_message)
//       return
//     }else if( num_def_positions==0 ){
//       error_message = "At least one defensive position must be selected"
//       $("#error-edit-player").html(error_message)
//       return
//     }


//     // if no errors were found, saving the data on the player object
//     var player_before_edit = Object.assign({}, playerToEdit);
//     playerToEdit.playerNameLong = new_name
//     playerToEdit.playerNameShort = new_short_name
//     playerToEdit.number = new_number.toString()
//     playerToEdit.validPositions = positions

//     //updating the team with the new changes
//     if(newPlayer==false){
//       for(var i=0; i<teamToEdit.players.length;i++){
//         if( teamToEdit.players.playerID == playerToEdit.playerID ){
//           teamToEdit.players = playerToEdit
//         }
//       }
//       // updating player on the database
//       updatePlayerDataOnDatabase(playerToEdit)
//     }
//     // adding the new player to the team
//     else{
//       teamToEdit.players.push(playerToEdit)
//       addPlayerDataOnDatabase(teamToEdit, playerToEdit)
//     }

//     // closing modal
//     $("#editPlayerPopup").modal("hide")

//     // updating the player on the field if necessary
//     var number_div = $("#"+playerToEdit.playerID).children()[0]
//     $(number_div).html(playerToEdit.number)

//   });


//   //////////////////////////////////////////////////////////
//   //method that opens the NEW-PLAYER modal
//   $("#new-player-button").click(function(){

//     //teams can have max. 20 players
//     if( teamToEdit.players.length >= 20 ){
//       var error_message = "Team can have max. 20 players!"
//       $("#error-edit-team").html(error_message)
//       return
//     }

//     $("#editTeamPopup").modal("hide")
//     $("#editPlayerPopup").modal("show")

//   });


//   //////////////////////////////////////////////////////////
//   // method that obtains the player to edit once the button
//   // EDIT PLAYER has been pressed
//   $('#editPlayerPopup').on('shown.bs.modal', function (e) {

//     // obtaining the ID of the player from the pressed button
//     var button_id
//     var playerID
//     newPlayer = false

//     try {
//       button_id = e.relatedTarget.id.split("-")
//       playerID = e.relatedTarget.id.split("-")[ e.relatedTarget.id.split("-").length-1 ]
//     } catch (e) {
//       playerID = undefined
//     }


//     // obtaining the object of the player given the ID
//     playerToEdit = undefined
//     for(var i=0;i<teamToEdit.players.length; i++){
//       if( teamToEdit.players[i].playerID==playerID ){
//         playerToEdit = teamToEdit.players[i]
//         break
//       }
//     }

//     // case for creating a new player
//     if( playerToEdit==undefined ){
//       playerID = getFreeID(teamToEdit)
//       playerToEdit = new Player(playerID ,teamToEdit.teamID,"","","","","","","","","")
//       newPlayer = true
//     }

//     loadDataToEditPlayerModal()


//   })


//   //////////////////////////////////////////////////////////
//   // method that obtains the player to edit once the button
//   // DELETE PLAYER has been pressed
//   $('#confirmDeletePlayerModal').on('shown.bs.modal', function(e){

//     var message = "Do you want to delete player #" + playerToEdit.number + ": " + playerToEdit.playerNameShort + "?"
//     $("#confirmDeletePlayerMessage").html(message)

//   });


//   //////////////////////////////////////////////////////////
//   // performing some operations once the modal is closed
//   $("#editTeamPopup").on("hidden.bs.modal", function () {

//     //erasing error message once the modal is closed
//     $("#error-edit-team").html("")

//   });


//   //////////////////////////////////////////////////////////
//   // performing some operations once the modal is closed
//   $("#editPlayerPopup").on("hidden.bs.modal", function () {

//     //erasing error message once the modal is closed
//     $("#error-edit-player").html("")

//   });


//   //////////////////////////////////////////////////////////
//   //processing state of position buttons
//   $(".button-edit-player-position").click(function(){

//     if( $(this).hasClass("btn-highlight") ){
//         $(this).removeClass('btn-highlight');
//     }else{
//       $(this).addClass('btn-highlight');
//     }
//   });


//   //////////////////////////////////////////////////////////
//   //processing the click on the load team image button
//   $("#load-new-team-logo").click(function(){
//     $("#load-new-team-logo-hidden").click()
//   });


//   //////////////////////////////////////////////////////////
//   //processing the click on the load player image button
//   $("#load-new-player-img").click(function(){
//     $("#load-new-player-img-hidden").val(undefined)
//     $("#load-new-player-img-hidden").click()
//   });


//   //////////////////////////////////////////////////////////
//   //processing the click on the delete player button
//   $("#deletePlayerButton").click(function(){
//     $("#confirmDeletePlayerModal").modal("show")
//   })


//   //////////////////////////////////////////////////////////
//   //processing the click on the confirmation button to delete a player
//   $("#confirmDeletePlayerButton").click(function(){
//     deletePlayerCompletely(playerToEdit)
//   });


//   //////////////////////////////////////////////////////////
//   //loading the new image onto the image display
//   $("#load-new-team-logo-hidden").change(function(){

//     var new_file = this.files[0]
//     var name = teamToEdit.teamID

//     // in case we close the image selector without choosing an image
//     if(new_file==undefined){
//       return
//     }

//     // creating and loading the new image
//     window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fs) {

//           var blob = new Blob([new_file], { type: 'image/png' });
//           var img_name = "team_" + name + ".png"

//           // saving the image
//           fs.root.getFile( img_name, { create: true, exclusive: false }, function (fileEntry) {
//               writeFile(fileEntry, blob);
//           }, function er(e){
//             console.error("error in saving");
//             console.error(e)
//           });

//           // reading the image
//           fs.root.getFile( img_name, { create: false, exclusive: false }, function (fileEntry) {
//               readFile(fileEntry, "teams");
//           }, function er(e){
//             console.error("error in read");
//             console.error(e)
//           });

//     }, function(err) {
//       console.error("upsi")
//     });

//   });


//   //////////////////////////////////////////////////////////
//   //loading the new image onto the image display
//   $("#load-new-player-img-hidden").change(function(){

//     console.log("Inside load hiden...")
//     var new_file = this.files[0]
//     var name = playerToEdit.playerID

//     // in case we close the image selector without choosing an image
//     if(new_file==undefined){
//       return
//     }

//     // creating and loading the new image
//     window.requestFileSystem(LocalFileSystem.TEMPORARY, 0, function (fs) {

//         var blob = new Blob([new_file], { type: 'image/png' });
//         var img_name = "player_" + name + ".png"

//         // saving the image
//         fs.root.getFile( img_name, { create: true, exclusive: false }, function (fileEntry) {
//             writeFile(fileEntry, blob, "players");
//             readFile(fileEntry, "players");
//         }, function er(e){
//           console.error("error in saving");
//           console.error(e)
//         });

//         // // reading the image
//         // fs.root.getFile( img_name, { create: false, exclusive: false }, function (fileEntry) {
//         //     readFile(fileEntry, "players");
//         // }, function er(e){
//         //   console.error("error in read");
//         //   console.error(e)
//         // });


//     }, function(err) {
//       console.error("upsi")
//       console.log(err)
//     });

//   });

// });




// ///////////////////////////////////////////////////////
// //method that loads the data onto the EDIT TEAM modal
// function loadDataToEditTeamModal(){

//   $("#editTeamPopup").modal("show")

//   // updating modal title
//   var newModalTitle = "Editing Team: " + teamToEdit.teamName
//   $("#editTeamModalTitle").html(newModalTitle)

//   // setting team logo, name, colors
//   $('#edit-team-logo').attr('src',teamToEdit.image_path);
//   $("#edit-team-name-input").val( teamToEdit.teamName )
//   $("#edit-color-1-border").spectrum({
//     color: teamToEdit.lineColorA
//   });
//   $("#edit-color-1-background").spectrum({
//     color: teamToEdit.filledColorA
//   });
//   $("#edit-color-2-border").spectrum({
//     color: teamToEdit.lineColorB
//   });
//   $("#edit-color-2-background").spectrum({
//     color: teamToEdit.filledColorB
//   });
//   $("#edit-color-text").spectrum({
//     color: teamToEdit.textColorA
//   });

//   // setting the list of players that can be edited
//   $('#edit-players-in-team').text("");
//   for(var i=0;i<teamToEdit.players.length;i++){
//     $("#edit-players-in-team").append("<button type='button' class='edit-players-list list-group-item-action col-5 m-1 "+a+"' id='edit-popup-player-"+teamToEdit.players[i].playerID+"' data-dismiss='modal' data-toggle='modal' data-target='#editPlayerPopup'>"+teamToEdit.players[i].number+" - "+teamToEdit.players[i].playerNameShort+"</button><br>");
//   }

// }



// ///////////////////////////////////////////////////////
// //method that loads the data onto the EDIT PLAYER modal
// function loadDataToEditPlayerModal(){

//   // updating modal title
//   if(newPlayer==false){
//     var newModalTitle = "Editing Player: " + playerToEdit.playerNameShort
//     $("#editPlayerModalTitle").html(newModalTitle)
//     $('#edit-player-img').attr('src',playerToEdit.image_path);
//   }else{
//     var newModalTitle = "Creating new player"
//     $("#editPlayerModalTitle").html(newModalTitle)
//     $('#edit-player-img').attr('src','./img/players/dummyPlayerblack.png');
//   }


//   // setting team logo, name, number, ...
//   $("#edit-player-name-input").val(playerToEdit.playerNameLong)
//   $("#edit-player-short-name-input").val(playerToEdit.playerNameShort)
//   $("#edit-player-number-input").val(playerToEdit.number)

//   // setting position selectors
//   $(".button-edit-player-position").removeClass("btn-highlight")
//   var player_positions = playerToEdit.validPositions
//   player_positions = player_positions.split(" ")

//   for(var i=0; i<player_positions.length; i++){

//     if(player_positions[i]=="OPV1" || player_positions[i]=="OPV2"){
//       player_positions[i] = "OPV"
//     }
//     var button_to_change = "#"+player_positions[i]+"_button"
//     $(button_to_change).addClass("btn-highlight")
//   }

// }



// ///////////////////////////////////////////////////////
// //method that deletes a player completely
// function deletePlayerCompletely(player){

//   console.log("deleting player: " + player.playerNameShort)

//   // removing the player from the team
//   teamToEdit.players = teamToEdit.players.filter(function(elem){
//    return elem != player;
//   })

//   // removing player from database
//   removePlayerFromDatabase(teamToEdit, player)

//   // removing player from field
//   $("#"+player.playerID).remove()


//   // hiding modals
//   $("#editPlayerPopup").modal("hide")
//   $("#confirmDeletePlayerModal").modal("hide")

// }




// ////////////////////////////////////////
// /// methods to read and write images ///
// ////////////////////////////////////////
// function writeFile(fileEntry, dataObj, table) {

//     // Create a FileWriter object for our FileEntry
//     fileEntry.createWriter(function (fileWriter) {

//         fileWriter.onwriteend = function() {
//             console.log("fileEntry")
//             console.log(fileEntry)
//             console.log("Successful file write...");
//             readFile(fileEntry, table);
//         };

//         fileWriter.onerror = function (e) {
//             console.log("Failed file write: " + e.toString());
//         };

//         // If data object is not passed in,
//         // create a new Blob instead.
//         if (!dataObj) {
//             console.log("Creating blob")
//             dataObj = new Blob(['some file data'], { type: 'text/plain' });
//         }

//         fileWriter.write(dataObj);
//     });
// }


// function readFile(fileEntry, table) {

//     fileEntry.file(function (file) {
//         var reader = new FileReader();

//         reader.onloadend = function() {
//             console.log("Successful file read")
//             console.log("URL: " + fileEntry.toURL() )

//             // loading image into the logo
//             file_path = fileEntry.toURL()
//             globalFileEntry = fileEntry

//             // saving image on database and object
//             if(table=="teams"){
//               $("#edit-team-logo").attr("src", file_path+"#" + new Date().getTime());
//               teamToEdit.image_path = file_path+"#" + new Date().getTime()
//               save_image_on_database("teams", file_path, teamToEdit.teamID)
//             }
//             else if(table=="players"){
//               $("#edit-player-img").attr("src", file_path+"#" + new Date().getTime());
//               playerToEdit.image_path = file_path+"#" + new Date().getTime()
//               save_image_on_database("players", file_path, playerToEdit.playerID)

//               // updating player on field
//               $("#"+playerToEdit.playerID).css("background-image", "url("+file_path+"#" + new Date().getTime()+")");

//             }

//         };

//         reader.readAsText(file);

//     }, function er(){});
// }




// //
