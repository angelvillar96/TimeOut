///////////////////////////////////////////////////////////////////////////
// GAME SETTINGS
///////////////////////////////////////////////////////////////////////////

var BASELINE_SPEED = 1000;

//game settings with default values
var GAME_SETTINGS = {      //all of these need to be strings
  display_tutorial: "true",
  display_terms: "true",  // showing terms and conditions only once
  playSpeed: "1000",
  lineThickness: "4",
  memoryArrows: "6",
  showTraces: "true",
  drawDefensivePairs: "false",
  automaticDefenseMovement: "false",
  beta_version_finished: "false",

  teamA: "0000",           //code of team A
  teamB: "0001",           //code of team B

  fieldColor: "0001",
  layout: "HalfVertOffense", //halfVertOffense
  orientation: "0",          //0 or 180
  halfFull: "HalfHoriz",      //can be HalfVert, HalfHoriz, Full
  bench: "Left",             //can be Left or Right
  offensiveSystem: "offensive33", //offensive33
  defensiveSystem: "defense60",   //defense60

  favPlay1: "",
  favPlay2: "",
  favPlay3: "",
  favPlay4: "",
  favPlay5: "",
};


var confirmation_required = ""


//////////////////////////////////////////////////////////////////////////
//method that removes all the local window.localStorage user settings
function deleteLocalStorage(){

  window.localStorage.playSpeed = "";
  window.localStorage.memoryArrows = "";
  window.localStorage.lineThickness= "";
  window.localStorage.display_tutorial = "";
  window.localStorage.display_terms = "";
  window.localStorage.showTraces = "";
  window.localStorage.drawDefensivePairs = "";
  window.localStorage.automaticDefenseMovement = "";
  window.localStorage.beta_version_finished = "";

  window.localStorage.teamA = "";
  window.localStorage.teamB = "";
  window.localStorage.fieldColor = "";
  window.localStorage.orientation = "";
  window.localStorage.halfFull = "";
  window.localStorage.bench = "";

  window.localStorage.favPlay1 = "";
  window.localStorage.favPlay2 = "";
  window.localStorage.favPlay3 = "";
  window.localStorage.favPlay4 = "";
  window.localStorage.favPlay5 = "";

  window.localStorage.offensiveSystem = "";
  window.localStorage.defensiveSystem = "";
  window.localStorage.layout = "";

}



///////////////////////////////////////////////////////////////////////////////////////////
//method that checks if user settins exist, and if not fills them with the default settings
function checkAndLoadData(){

  if( window.localStorage.beta_version_finished=="" ){
    window.localStorage.beta_version_finished = GAME_SETTINGS.beta_version_finished;
  }
  if( window.localStorage.memoryArrows=="" ){
    window.localStorage.memoryArrows = GAME_SETTINGS.memoryArrows;
  }
  if( window.localStorage.lineThickness=="" ){
    window.localStorage.lineThickness = GAME_SETTINGS.lineThickness;
  }
  if( window.localStorage.playSpeed=="" ){
    window.localStorage.playSpeed = GAME_SETTINGS.playSpeed;
  }
  if( window.localStorage.display_tutorial == "" ){
    window.localStorage.display_tutorial = GAME_SETTINGS.display_tutorial;
  }
  if( window.localStorage.display_terms == "" ){
    window.localStorage.display_terms = GAME_SETTINGS.display_terms;
  }
  if( window.localStorage.drawDefensivePairs == "" ){
    window.localStorage.drawDefensivePairs = GAME_SETTINGS.drawDefensivePairs;
  }
  if( window.localStorage.showTraces == "" ){
    window.localStorage.showTraces = GAME_SETTINGS.showTraces;
  }
  if( window.localStorage.automaticDefenseMovement == "" ){
    window.localStorage.automaticDefenseMovement = GAME_SETTINGS.automaticDefenseMovement;
  }
  if( window.localStorage.teamA == "" ){
    window.localStorage.teamA = GAME_SETTINGS.teamA;
  }
  if( window.localStorage.teamB == "" ){
    window.localStorage.teamB = GAME_SETTINGS.teamB;
  }
  if( window.localStorage.fieldColor == "" ){
    window.localStorage.fieldColor = GAME_SETTINGS.fieldColor;
  }
  if( window.localStorage.orientation == "" ){
    window.localStorage.orientation = GAME_SETTINGS.orientation;
  }
  if( window.localStorage.bench == "" ){
    window.localStorage.bench = GAME_SETTINGS.bench;
  }
  if( window.localStorage.halfFull == "" ){
    window.localStorage.halfFull = GAME_SETTINGS.halfFull;
  }
  if( window.localStorage.favPlay1 == "" ){
    window.localStorage.favPlay1 = GAME_SETTINGS.favPlay1;
  }
  if( window.localStorage.favPlay2 == "" ){
    window.localStorage.favPlay2 = GAME_SETTINGS.favPlay2;
  }
  if( window.localStorage.favPlay3 == "" ){
    window.localStorage.favPlay3 = GAME_SETTINGS.favPlay3;
  }
  if( window.localStorage.favPlay4 == "" ){
    window.localStorage.favPlay4 = GAME_SETTINGS.favPlay4;
  }
  if( window.localStorage.favPlay5 == "" ){
    window.localStorage.favPlay5 = GAME_SETTINGS.favPlay5;
  }
  if( window.localStorage.offensiveSystem == "" ){
    window.localStorage.offensiveSystem = GAME_SETTINGS.offensiveSystem;
  }
  if( window.localStorage.defensiveSystem == "" ){
    window.localStorage.defensiveSystem = GAME_SETTINGS.defensiveSystem;
  }
  if( window.localStorage.layout == "" ){
    window.localStorage.layout = GAME_SETTINGS.layout;
  }


}



////////////////////////////////////////////////////////////////////////////////////////////
//method that loads the favorite plays in the buttons and sets the correct color to the star
function loadFavoritePlays(){

    $("#favPlay1").html(window.localStorage.favPlay1)
    $("#favPlay2").html(window.localStorage.favPlay2)
    $("#favPlay3").html(window.localStorage.favPlay3)
    $("#favPlay4").html(window.localStorage.favPlay4)
    $("#favPlay5").html(window.localStorage.favPlay5)

    if( window.localStorage.favPlay1.length == 0 ){
      $("#star1").attr("src","img/Icon_Star_Gray.svg");
    }
    if( window.localStorage.favPlay2.length == 0 ){
      $("#star2").attr("src","img/Icon_Star_Gray.svg");
    }
    if( window.localStorage.favPlay3.length == 0 ){
      $("#star3").attr("src","img/Icon_Star_Gray.svg");
    }
    if( window.localStorage.favPlay4.length == 0 ){
      $("#star4").attr("src","img/Icon_Star_Gray.svg");
    }
    if( window.localStorage.favPlay5.length == 0 ){
      $("#star5").attr("src","img/Icon_Star_Gray.svg");
    }

}



////////////////////////////////////////////////////////////////////////////
// method that converts a percentage (x0.75%,..) into a usable speed (1000 ms)
function percentToSpeed( percentage ){

  var speed = BASELINE_SPEED * ( 2 - parseFloat(percentage) );
  return speed

}



////////////////////////////////////////////////////////////////////////////
// method that converts a usable speed (1000 ms) into a percentage (x0.75%,..)
function speedToPercent( speed ){

  var percentage = 2 - speed/BASELINE_SPEED
  return percentage

}






///////////////////////////////////////////////////////////////
// MAIN ///////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
$(document).ready(function() {

  //auxiliar method to delete everything (should be commented)
  // window.localStorage.clear()
  writeConsoleLog( window.localStorage );

  //empty initializing the variables
  if( window.localStorage == null || window.localStorage.length < 1 ){
    deleteLocalStorage()
  }
  checkAndLoadData();
  check_beta_finished()

  //if the app is opened for the first time, an alert is shown
//  console.log(window.localStorage)
  if( window.localStorage.display_tutorial == "true"  ){
    writeConsoleLog("\n\nWelcome to TACbrd\n\n");
    start_tacbrd_tutorial()
  }

  //loading the favorite plays in the buttons
  loadFavoritePlays()


  /////////////////////////////////////////////////////
  //method that processes clicks on the settings button
  $("#settingsButton").click(function(){
    //play speed selector
    var speed = window.localStorage.playSpeed
    var percentage = speedToPercent( speed )
    $("#PlaySpeedSelectorSlider").val(percentage);
    $("#PlaySpeedDisplay").html( "Play Speed: x" + parseFloat(Math.round(percentage * 100)) + "%");


    //arrow memory selector
    var memoryArrows = window.localStorage.memoryArrows
    if( parseInt(memoryArrows) == 9 ){
      $("#MemoryArrowsSelector").hide()
      $("#deleteArrowsCheckbox").prop('checked', false);
    }else{
      $("#MemoryArrowsSelectorSlider").val( parseInt(memoryArrows) );
      $("#MemoryArrowsDisplay").html( "Arrow Life: " + memoryArrows);
      $("#deleteArrowsCheckbox").prop('checked', true);
    }


    // Load values
    if(window.localStorage.drawDefensivePairs == "true"){
      $("#checkboxDefensivePairs").prop('checked', true);
    }
    else{
      $("#checkboxDefensivePairs").prop('checked', false);
    }


    if(window.localStorage.showTraces == "true"){
      $("#checkboxTraceLines").prop('checked', true);

      //line Thickness selector
      var lineThickness = window.localStorage.lineThickness;
      $("#LineThicknessSelectorSlider").val(lineThickness);
      $("#LineThicknessDisplay").html( "Line Thickness: " + lineThickness/2);
//      $("#deleteArrowsCheckbox").prop('checked', true);
    }
    else{
      $("#checkboxDefensivePairs").prop('checked', false);
      $("#LineThicknessSelector").hide()
    }


    if(window.localStorage.automaticDefenseMovement == "true"){
      $("#checkboxAutomaticDefenseMovement").prop('checked', true);
    }
    else{
      $("#checkboxAutomaticDefenseMovement").prop('checked', false);
    }

    $('#openSettingsPopup').modal('show');

  });


  /////////////////////////////////////////////////////
  //method that updates the speed indicator while movign the speed slider
  $('#PlaySpeedSelectorSlider').on("input change", function() {

    //updating the slider display
    var value = $(this).val()
    $("#PlaySpeedDisplay").html( "Play Speed: x" + parseFloat(Math.round(value * 100)) + "%");

  });


  ///////////////////////////////////////////////////////
  // method that processes the memory arrow checkbox
  $("#deleteArrowsCheckbox").change(function() {
    if(this.checked) {
      $("#MemoryArrowsSelector").show("fast")
      var memoryArrows = $("#MemoryArrowsSelectorSlider").val();
      $("#MemoryArrowsDisplay").html( "Arrow Life: " + memoryArrows);
    }else{
      $("#MemoryArrowsSelector").hide("fast")
    }
  });


  ///////////////////////////////////////////////////////
  // method that processes the Trace Lines checkbox
  $("#checkboxTraceLines").change(function() {
    if(this.checked) {
      $("#LineThicknessSelector").show("fast")
      var lineThickness = $("#LineThicknessSelectorSlider").val();
      $("#LineThicknessDisplay").html( "Line Thickness: " + lineThickness/2);
    }else{
      $("#LineThicknessSelector").hide("fast")
    }
  });


  /////////////////////////////////////////////////////
  //method that updates the arrow life indicator while moving the slider
  $('#MemoryArrowsSelectorSlider').on("input change", function() {

    //updating the slider display
    var value = $(this).val()
    $("#MemoryArrowsDisplay").html( "Arrow Life: " +value);

  });


  /////////////////////////////////////////////////////
  //method that updates the arrow life indicator while moving the slider
  $('#LineThicknessSelectorSlider').on("input change", function() {

    //updating the slider display
    var value = $(this).val()
    $("#LineThicknessDisplay").html( "Line Thickness: " +value/2);

  });


  /////////////////////////////////////////////////////
  //method that applies the selected settings once the "Apply changes" button has been clicked
  $("#applyAppSettingsChanges").click(function(){

    //saving the play speed
    var speedPercentage = $("#PlaySpeedSelectorSlider").val()
    var actualPlaySpeed = percentToSpeed( speedPercentage )
    writeConsoleLog(actualPlaySpeed)
    window.localStorage.playSpeed = actualPlaySpeed

    //saving the arrows memory
    if($("#deleteArrowsCheckbox").is(":checked")){
      var memoryArrows = $("#MemoryArrowsSelectorSlider").val()
    }else{
      memoryArrows = 9  //9 is equivalent to infinity
    }
    window.localStorage.memoryArrows = memoryArrows


    //line thickness
    var lineThickness  = $("#LineThicknessSelectorSlider").val()
    window.localStorage.lineThickness = lineThickness


    // Checkbox defensive pairs
    writeConsoleLog("Settings for the checkbox: "+$("#checkboxDefensivePairs").is(":checked"));
    if($("#checkboxDefensivePairs").is(":checked")){
      window.localStorage.drawDefensivePairs = "true";
      // Init lines
      assignPairs(drawOffDefLines);
    }
    else{
      window.localStorage.drawDefensivePairs = "false";
      // If checkbox has been un-selected the lines have to be deleted
      deleteOffDefPairsLines();
    }

    // Checkbox for trace lines
    if($("#checkboxTraceLines").is(":checked")){
      window.localStorage.showTraces = "true";
      if($("#modeHidden").html() == "recording"){
        arrowsManager( actualPlay.snapshots, actualPlay.snapshots.length)
      }else{
        arrowsManager( actualPlay.snapshots, actualSnapshotIndex)
      }
    }
    else{
      window.localStorage.showTraces = "false";
      resetCanvas()
    }

    // Checkbox for automatic defense
    if($("#checkboxAutomaticDefenseMovement").is(":checked")){
      window.localStorage.automaticDefenseMovement = "true";
    }
    else{
      window.localStorage.automaticDefenseMovement = "false";
    }

  });


  ///////////////////////////////////////////////////
  // method that resets the players db to default
  $("#reset-players").click(function(){
    confirmation_required = "players"
    message = "Are you sure you want to reset the Tactics Database?<br><br>\
    You will lose all created players"
    $("#confirm_reset_database_message").html(message)
    $("#confirm_reset_database").modal("show")
  });


  ///////////////////////////////////////////////////
  // method that resets the teams db to default
  $("#reset-teams").click(function(){
    confirmation_required = "teams"
    message = "Are you sure you want to reset the Tactics Database?<br><br>\
    You will lose all created teams"
    $("#confirm_reset_database_message").html(message)
    $("#confirm_reset_database").modal("show")
  });


  ///////////////////////////////////////////////////
  // method that resets the players db to default
  $("#reset-tactics").click(function(){
    confirmation_required = "tactics"
    message = "Are you sure you want to reset the Tactics Database?<br><br>\
    You will lose all created tactics"
    $("#confirm_reset_database_message").html(message)
    $("#confirm_reset_database").modal("show")
  });


  ///////////////////////////////////////////////////
  // method that resets the given database after confirmation
  $("#confirmation-pressed").click(function(){

    for(var i=0; i<plays.length; i++){
      playName = plays[i].playName
      console.log("Removing play: " + playName)
      plays_db.deletePlayGivenName(playName)
    }
    allPlays = []
    plays = [];
    snapshots = [];
    moves = [];
    confirmation_required = ""

    // removing favorite plays from local-memory and removing star colors
    $("#favPlay1").html("")
    $("#favPlay2").html("")
    $("#favPlay3").html("")
    $("#favPlay4").html("")
    $("#favPlay5").html("")
    $("#star1").attr("src","img/Icon_Star_Gray.svg");
    $("#star2").attr("src","img/Icon_Star_Gray.svg");
    $("#star3").attr("src","img/Icon_Star_Gray.svg");
    $("#star4").attr("src","img/Icon_Star_Gray.svg");
    $("#star5").attr("src","img/Icon_Star_Gray.svg");
    window.localStorage.favPlay1 = "";
    window.localStorage.favPlay2 = "";
    window.localStorage.favPlay3 = "";
    window.localStorage.favPlay4 = "";
    window.localStorage.favPlay5 = "";

    // reseting canvas and removing tactic/snapshot if loaded
    processSnapshotDisplay()
    $("#playState").html("")
    setState("FreeDraw");
    resetCanvas()
    $("#homeLogo").click()
  });


  ///////////////////////////////////////////////////
  // displaying tutorial once again when button is pressed
  $("#start_tutorial").click(function(){
    $("#openSettingsPopup").modal("toggle")
    start_tacbrd_tutorial()
  });


});



//
