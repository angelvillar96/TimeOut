class CourtPlayers{

    // GK  // Goalkeeper

    // DLO  // Defense Left Outside
    // DLL  // Defense Line Left
    // DLCL  // Defense Line Center Left
    // DLC  // Defense Line Center
    // DLCR  // Defense Line Center Right
    // DLR  // Defense Line Right
    // DRO  // Defense Right Outside
    // DFC  // Defense Forward Center
    // DFL  // Defense Forward Left
    // DFR  // Defense Forward Right
    // DHL  // Defense Half Left
    // DHR  // Defense Half Right

    // OLW  // Offensive Left Wing
    // OLB  // Offensive Left Back
    // OCB  // Offensive Center Back
    // ORB  // Offensive Right Back
    // ORW  // Offensive Right Wing
    // OPV1  // Offensive Pivot
    // OPV2  // Offensive 2nd Pivot


    // constructor(DGK, DLO, DLL, DLCL, DLC, DLCR, DLR, DRO, DFC, DFL, DFR, DHL, DHR, OGK, OLW, OLB, OCB, ORB, ORW, OPV1, OPV2){
    //     this.DGK = DGK;
    //     this.DLO = DLO;
    //     this.DLL = DLL;
    //     this.DLCL = DLCL;
    //     this.DLC = DLC;
    //     this.DLCR = DLCR;
    //     this.DLR = DLR;
    //     this.DRO = DRO;
    //     this.DFC = DFC;
    //     this.DFL = DFL;
    //     this.DFR = DFR;
    //     this.DHL = DHL;
    //     this.DHR = DHR;
    //     this.OGK = OGK;
    //     this.OLW = OLW;
    //     this.OLB = OLB;
    //     this.OCB = OCB;
    //     this.ORB = ORB;
    //     this.ORW = ORW;
    //     this.OPV1 = OPV1;
    //     this.OPV2 = OPV2;
    // }

    constructor(playersString){
        if(playersString != undefined){
            var playersArray = playersString.split(",");
            this.DGK = playersArray[0];
            this.DLO = playersArray[1];
            this.DLL = playersArray[2];
            this.DLCL = playersArray[3];
            this.DLC = playersArray[4];
            this.DLCR = playersArray[5];
            this.DLR = playersArray[6];
            this.DRO = playersArray[7];
            this.DFC = playersArray[8];
            this.DFL = playersArray[9];
            this.DFR = playersArray[10];
            this.DHL = playersArray[11];
            this.DHR = playersArray[12];
            this.OGK = playersArray[13];
            this.OLW = playersArray[14];
            this.OLB = playersArray[15];
            this.OCB = playersArray[16];
            this.ORB = playersArray[17];
            this.ORW = playersArray[18];
            this.OPV1 = playersArray[19];
            this.OPV2 = playersArray[20];
        }
    }



}
