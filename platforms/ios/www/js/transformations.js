////////////////////////////////////////////////////////////////////////////////////////
////  Library that contains functions to transform the coordindates of the players  ////
////////////////////////////////////////////////////////////////////////////////////////

var fieldHeigth;
var fieldHeigthPercentage;

///////////////////////////////////7
//function that changes from HalfField to FullField
function HalfField2FullField(res){

  //eliminating preexisting arrows
  $(".arrow").remove();

  //we iterate for all players to check postion
  //if players will fall out of bounds after the transformation, an arrow will be placed
  $(".rotates").each(function() {
    if( $(this).attr("id")!="teamB1" && $(this).hasClass("arrow")==false){

      var actualTop =  parseFloat($(this).css("top"))
      var actualLeft =  parseFloat($(this).css("left"))

      //case when rotating to full attack
      if( $("#fieldTypeHidden").html() == "Half" ){
        if ($("#orientationHidden").html()=="0"){
          pos = displayPos2fieldPos( actualLeft, actualTop, "FullOffense")

          //setting the arrow in case the player will fall out of bounds
          if( pos.top > 58 ){
            if( $(this).hasClass("positionOGK") ){
              return true
            }
            setArrow( $(this).attr("id"), 3 )

          }
        }
        //case when rotating from full defense
        else if ($("#orientationHidden").html()=="180"){
          pos = displayPos2fieldPos( actualLeft, actualTop, "FullDefense")
          if( pos.top > 58 ){
            if( $(this).hasClass("positionOGK") ){
              return true
            }
            setArrow( $(this).attr("id"), 4 )
          }
        }
      }
    }
  });


  //we iterate for all players
  $(".rotates").each(function() {

    //we get the original top and left values
    var actualTop = parseFloat($(this).css('top'));
    var actualLeft = parseFloat($(this).css('left'));

    //converting to full fields (either offense or defense)
    if( $("#fieldTypeHidden").html() == "Full" ){
      //making the outofbounds players visible
      if( $(this).css('visibility') =="hidden" ){
        $(this).css('visibility', "visible")
        $(this).removeClass("hiddenPlayer")
      }
      if ($("#orientationHidden").html()=="0"){
        pos = displayPos2fieldPos( actualLeft, actualTop, "HalfVertOffense" )
        pos = fieldPos2displayPos( pos.left, pos.top, "FullOffense" )
      }else{
        pos = displayPos2fieldPos( actualLeft, actualTop, "HalfVertDefense" )
        pos = fieldPos2displayPos( pos.left, pos.top, "FullDefense" )
      }
      $("#courtImage").attr("src", res);
      $("#courtImage").attr("height", "100%");
      $("#courtImage").attr("width", "auto");
      if( $(this).hasClass("ball") ){
        $(this).css({'transform':'scale(0.5)', 'box-shadow' : '0px 0px'});
      }
      else{
        $(this).css({'transform':'scale(0.5)', 'box-shadow' : '0px 0px'});
        $(this).css("font-size","5vw");
      }
    }
    //converting back to the half fields
    else{
      if ($("#orientationHidden").html()=="0"){
        pos = displayPos2fieldPos( actualLeft, actualTop, "FullOffense" )
        pos = fieldPos2displayPos( pos.left, pos.top, "HalfVertOffense" )
      }else{
        pos = displayPos2fieldPos( actualLeft, actualTop, "FullDefense" )
        pos = fieldPos2displayPos( pos.left, pos.top, "HalfVertDefense" )
      }
      $("#courtImage").attr("src", res);
      $("#courtImage").attr("height", "auto");
      $("#courtImage").attr("width", "100%");
      if( $(this).hasClass("ball") ){
        $(this).css({'transform':'scale(0.8)', 'box-shadow' : '0px 0px'});
      }else{
        $(this).css({'transform':'scale(0.8)', 'box-shadow' : '0px 0px'});
        $(this).css("font-size","5vw");
      }
    }

    //we asign the new values
    $(this).css("top", pos.top);
    $(this).css("left", pos.left);

  });

  //setGoalkeeper();

  if ($("#orientationHidden").html()=="0"){
    $(".arrow").css("transform", "rotate(0deg)");
  }else{
    $(".arrow").css("transform", "rotate(180deg)");
  }

  return true;
}


///////////////////////////////////7
//function that changes from HorizOffense2HorizDefense
function HorizOffense2HorizDefense(){

  //we iterate for all players
  $(".rotates").each(function() {

    //we get the original top and left values
    var actualTop = parseFloat($(this).css("top"));
    var actualLeft = parseFloat($(this).css('left'));

    //we scalate the top and left values
    if ($("#orientationHidden").html()=="0"){
      pos = displayPos2fieldPos( actualLeft, actualTop, "HorizDefense" )
      if( ($(this).attr("id")).includes("arrow") ){
        $(this).css("transform", "rotate(90deg)");
      }
      pos = fieldPos2displayPos( pos.left, pos.top, "HorizOffense" )
    }else{
      pos = displayPos2fieldPos( actualLeft, actualTop, "HorizOffense" )
      if( ($(this).attr("id")).includes("arrow") ){
        $(this).css("transform", "rotate(270deg)");
      }
      pos = fieldPos2displayPos( pos.left, pos.top, "HorizDefense" )
    }

    //we asign the new values
    $(this).css("top", pos.top);
    $(this).css("left", pos.left);

  });

}


////////////////////////////////////7
//function that changes from HalfVert to HalfHoriz
function HalfVert2HalfHorizDefense(res, turn){


  //we iterate for all players
  $(".rotates").each(function() {

    //we get the original top and left values
    var actualTop = parseFloat($(this).css('top'));
    var actualLeft = parseFloat($(this).css('left'));

    //we make the rotations for each case
    if( $("#rotationHidden").html() == "HalfHoriz" ){
      pos = displayPos2fieldPos( actualLeft, actualTop, "HalfVertDefense" )

      if( ($(this).attr("id")).includes("arrow") ){
        pos.top = pos.top - 15
        $(this).css("transform", "rotate(270deg)");
      }else if( pos.top > 42 && $(this).hasClass("hiddenPlayer")==false ){
        setArrow( $(this).attr("id"), 2 )
        $("#arrow"+$(this).attr("id")).css("transform", "rotate(270deg)");
      }

      pos = fieldPos2displayPos( pos.left, pos.top, "HorizDefense" )
      $("#courtImage").attr("src", res);
      if(turn==true){
        $("#courtImage").css("transform", "rotate(180deg)");
      }

    }else{
      pos = displayPos2fieldPos( actualLeft, actualTop, "HorizDefense" )

      if( ($(this).attr("id")).includes("arrow") ){
        if( $(this).hasClass("horizArrow") ){
          $("#" + ($(this).attr("id")).substring(5)).css('visibility', "visible")
          $("#" + ($(this).attr("id")).substring(5)).removeClass("hiddenPlayer")
          $(this).remove()
        }else{
          pos.top = pos.top + 15
          $(this).css("transform", "rotate(180deg)");
        }
      }

      pos = fieldPos2displayPos( pos.left, pos.top, "HalfVertDefense" )
      $("#courtImage").attr("src", res);
      if(turn==true){
        $("#courtImage").css("transform", "rotate(180deg)");
      }
    }

    $(this).css("left", pos.left);
    $(this).css("top", pos.top);
  });
  return true;
}


////////////////////////////////////7
//function that changes from HalfVert to HalfHoriz
function HalfVert2HalfHorizOffense(res, turn){

  //we iterate for all players
  $(".rotates").each(function() {

    //we get the original top and left values
    var actualTop = parseFloat($(this).css('top'));
    var actualLeft = parseFloat($(this).css('left'));

    //we make the rotations for each case
    if( $("#rotationHidden").html() == "HalfHoriz" ){
      pos = displayPos2fieldPos( actualLeft, actualTop, "HalfVertOffense" )

      if( ($(this).attr("id")).includes("arrow") ){
        pos.top = pos.top - 15
        $(this).css("transform", "rotate(90deg)");
      }else if( pos.top > 42 && $(this).hasClass("hiddenPlayer")==false){
        setArrow( $(this).attr("id"), 1 )
        $("#arrow"+$(this).attr("id")).css("transform", "rotate(90deg)");
      }

      pos = fieldPos2displayPos( pos.left, pos.top, "HorizOffense" )
      $("#courtImage").attr("src", res);
    }else{
      pos = displayPos2fieldPos( actualLeft, actualTop, "HorizOffense" )

      if( ($(this).attr("id")).includes("arrow") ){
        if( $(this).hasClass("horizArrow") ){
          $("#" + ($(this).attr("id")).substring(5)).css('visibility', "visible")
          $("#" + ($(this).attr("id")).substring(5)).removeClass("hiddenPlayer")
          $(this).remove()
        }else{
          pos.top = pos.top + 15
          $(this).css("transform", "rotate(0deg)");
        }
      }

      pos = fieldPos2displayPos( pos.left, pos.top, "HalfVertOffense" )
      $("#courtImage").attr("src", res);
    }
    $(this).css("left", pos.left);
    $(this).css("top", pos.top);

  });
  return true;
}


////////////////////////////////////7
//function that changes from VertAttack to VertDefense
function VertAttack2VertDefense(){

  //we iterate for all players
  $(".rotates").each(function() {

    //we get the original top and left values
    var actualTop = parseFloat($(this).css('top'));
    var actualLeft = parseFloat($(this).css('left'));

    //we scalate the top and left values
    if ($("#orientationHidden").html()=="0"){
      pos = displayPos2fieldPos( actualLeft, actualTop, "HalfVertDefense" )
      pos = fieldPos2displayPos( pos.left, pos.top, "HalfVertOffense" )
    }else{
      pos = displayPos2fieldPos( actualLeft, actualTop, "HalfVertOffense" )
      pos = fieldPos2displayPos( pos.left, pos.top, "HalfVertDefense" )
    }
    //we asign the new values
    $(this).css("top", pos.top);
    $(this).css("left", pos.left);

  });

  if ($("#orientationHidden").html()=="0"){
    $(".arrow").css("transform", "rotate(0deg)");
  }else{
    $(".arrow").css("transform", "rotate(180deg)");
  }

}


////////////////////////////////////7
//function that changes from VertAttack to VertDefense
function FullAttack2FullDefense(){

  //we iterate for all players
  $(".rotates").each(function() {

    //we get the original top and left values
    var actualTop = parseFloat($(this).css('top'))
    var actualLeft = parseFloat($(this).css('left'))

    //we scalate the top and left values
    if ($("#orientationHidden").html()=="0"){
      pos = displayPos2fieldPos( actualLeft, actualTop, "FullDefense" )
      pos = fieldPos2displayPos( pos.left, pos.top, "FullOffense" )
    }else{
      pos = displayPos2fieldPos( actualLeft, actualTop, "FullOffense" )
      pos = fieldPos2displayPos( pos.left, pos.top, "FullDefense" )
    }

    //we asign the new values
    $(this).css("top", pos.top);
    $(this).css("left", pos.left);
  });

  if ($("#orientationHidden").html()=="0"){
    $(".arrow").css("transform", "rotate(0deg)");
  }else{
    $(".arrow").css("transform", "rotate(180deg)");
  }

}


////////////////////////////////////7
//function that reverts the orientation if necessary
function setOrientation(aux){
  if( $("#rotationHidden").html() == "HalfHoriz" && $("#orientationHidden").html() == "0" && $("#fieldTypeHidden").html() == "Half"){
    $(".rotates").each(function() {
      //we get the original top and left values
      var actualTop = parseFloat($(this).css('top'))
      var actualLeft = parseFloat($(this).css('left'))
      pos = displayPos2fieldPos( actualLeft, actualTop, "HalfVertOffense" )
      pos = fieldPos2displayPos( pos.left, pos.top, "HorizOffense" )
      //we asign the new values
      $(this).css("top", pos.top);
      $(this).css("left", pos.left);
    });
  }
  else if(  $("#rotationHidden").html() == "HalfVert" && $("#orientationHidden").html() == "180" && $("#fieldTypeHidden").html() == "Half"){
    $(".rotates").each(function() {
      //we get the original top and left values
      var actualTop = parseFloat($(this).css('top'))
      var actualLeft = parseFloat($(this).css('left'))
      pos = displayPos2fieldPos( actualLeft, actualTop, "HalfVertOffense" )
      pos = fieldPos2displayPos( pos.left, pos.top, "HalfVertDefense" )
      //we asign the new values
      $(this).css("top", pos.top);
      $(this).css("left", pos.left);
    });
  }
  else if(  $("#rotationHidden").html() == "HalfHoriz" && $("#orientationHidden").html() == "180" && $("#fieldTypeHidden").html() == "Half"){
    $(".rotates").each(function() {
      //we get the original top and left values
      var actualTop = parseFloat($(this).css('top'))
      var actualLeft = parseFloat($(this).css('left'))
      pos = displayPos2fieldPos( actualLeft, actualTop, "HalfVertOffense" )
      pos = fieldPos2displayPos( pos.left, pos.top, "HorizDefense" )
      //we asign the new values
      $(this).css("top", pos.top);
      $(this).css("left", pos.left);
    });
  }
  else if( $("#orientationHidden").html() == "0" && $("#fieldTypeHidden").html() == "Full"){
    $(".rotates").each(function() {
      //we get the original top and left values
      var actualTop = parseFloat($(this).css('top'))
      var actualLeft = parseFloat($(this).css('left'))
      pos = displayPos2fieldPos( actualLeft, actualTop, "HalfVertOffense" )
      pos = fieldPos2displayPos( pos.left, pos.top, "FullOffense" )
      $(this).css({'transform':'scale(0.5)', 'box-shadow' : '0px 0px'});
      $(this).css("font-size","3vw");
      $(".ball").css({'transform':'scale(0.5)'});
      //we asign the new values
      $(this).css("top", pos.top);
      $(this).css("left", pos.left);
    });
    if( aux==0 ){
      return
    }
    //setGoalkeeper();
  }
  else if( $("#orientationHidden").html() == "180" && $("#fieldTypeHidden").html() == "Full"){
    $(".rotates").each(function() {
      //we get the original top and left values
      var actualTop = parseFloat($(this).css('top'))
      var actualLeft = parseFloat($(this).css('left'))
      pos = displayPos2fieldPos( actualLeft, actualTop, "HalfVertOffense" )
      pos = fieldPos2displayPos( pos.left, pos.top, "FullDefense" )
      $(this).css({'transform':'scale(0.5)', 'box-shadow' : '0px 0px'});;
      $(this).css("font-size","3vw");
      $(".ball").css({'transform':'scale(0.5)'});
      //we asign the new values
      $(this).css("top", pos.top);
      $(this).css("left", pos.left);
    });
    if( aux==0 ){
      return
    }
    //setGoalkeeper();
  }

}


////////////////////////////////////
//function that resets to the original orientation
function resetOrientation(){
  if( $("#rotationHidden").html() == "HalfHoriz" && $("#orientationHidden").html() == "0" && $("#fieldTypeHidden").html() == "Half"){
    $(".rotates").each(function() {
      //we get the original top and left values
      var actualTop = parseFloat($(this).css('top'))
      var actualLeft = parseFloat($(this).css('left'))
      pos = displayPos2fieldPos( actualLeft, actualTop, "HorizOffense" )
      pos = fieldPos2displayPos( pos.left, pos.top, "HalfVertOffense" )
      //we asign the new values
      $(this).css("top", pos.top);
      $(this).css("left", pos.left);
    });
  }
  else if( $("#rotationHidden").html() == "HalfHoriz" && $("#orientationHidden").html() == "180" && $("#fieldTypeHidden").html() == "Half"){
    $(".rotates").each(function() {
      //we get the original top and left values
      var actualTop = parseFloat($(this).css('top'))
      var actualLeft = parseFloat($(this).css('left'))
      pos = displayPos2fieldPos( actualLeft, actualTop, "HorizDefense" )
      pos = fieldPos2displayPos( pos.left, pos.top, "HalfVertOffense" )
      //we asign the new values
      $(this).css("top", pos.top );
      $(this).css("left", pos.left);
    });
  }
  else if( $("#rotationHidden").html() == "HalfVert" && $("#orientationHidden").html() == "180"  && $("#fieldTypeHidden").html() == "Half"){
    $(".rotates").each(function() {
      //we get the original top and left values
      var actualTop = parseFloat($(this).css('top'))
      var actualLeft = parseFloat($(this).css('left'))
      pos = displayPos2fieldPos( actualLeft, actualTop, "HalfVertDefense" )
      pos = fieldPos2displayPos( pos.left, pos.top, "HalfVertOffense" )
      //we asign the new values
      $(this).css("top", pos.top );
      $(this).css("left", pos.left);
    });
  }
  else if( $("#orientationHidden").html() == "0" && $("#fieldTypeHidden").html() == "Full"){
    $(".rotates").each(function() {
      //we get the original top and left values
      var actualTop = parseFloat($(this).css('top'))
      var actualLeft = parseFloat($(this).css('left'))
      pos = displayPos2fieldPos( actualLeft, actualTop, "FullOffense" )
      pos = fieldPos2displayPos( pos.left, pos.top, "HalfVertOffense" )
      //we asign the new values
      $(this).css("top", pos.top);
      $(this).css("left", pos.left);
    });
  }
  else if( $("#orientationHidden").html() == "180" && $("#fieldTypeHidden").html() == "Full"){
    $(".rotates").each(function() {
      //we get the original top and left values
      var actualTop = parseFloat($(this).css('top'))
      var actualLeft = parseFloat($(this).css('left'))
      pos = displayPos2fieldPos( actualLeft, actualTop, "FullDefense" )
      pos = fieldPos2displayPos( pos.left, pos.top, "HalfVertOffense" )
      //we asign the new values
      $(this).css("top", pos.top );
      $(this).css("left", pos.left);
    });
  }

}


////////////////////////////////////7
//function that reverts the orientation if necessary
function setSinglePlayerOrientation(playerPosition){
  writeConsoleLog("playerPosition: "+playerPosition);
  if( $("#rotationHidden").html() == "HalfHoriz" && $("#orientationHidden").html() == "0" && $("#fieldTypeHidden").html() == "Half"){
    $(".position"+playerPosition).each(function() {
      //we get the original top and left values
      var actualTop = parseFloat($(this).css('top'))
      var actualLeft = parseFloat($(this).css('left'))
      pos = displayPos2fieldPos( actualLeft, actualTop, "HalfVertOffense" )
      pos = fieldPos2displayPos( pos.left, pos.top, "HorizOffense" )
      //we asign the new values
      $(this).css("top", pos.top);
      $(this).css("left", pos.left);

      writeConsoleLog("  actualTop: "+actualTop+"  actualLeft: "+actualLeft+"  pos.top: "+pos.top+"  pos.left: "+pos.left);
    });
  }
  else if(  $("#rotationHidden").html() == "HalfVert" && $("#orientationHidden").html() == "180" && $("#fieldTypeHidden").html() == "Half"){
    $(".position"+playerPosition).each(function() {
      //we get the original top and left values
      var actualTop = parseFloat($(this).css('top'))
      var actualLeft = parseFloat($(this).css('left'))
      pos = displayPos2fieldPos( actualLeft, actualTop, "HalfVertOffense" )
      pos = fieldPos2displayPos( pos.left, pos.top, "HalfVertDefense" )
      //we asign the new values
      $(this).css("top", pos.top);
      $(this).css("left", pos.left);
      writeConsoleLog("  actualTop: "+actualTop+"  actualLeft: "+actualLeft+"  pos.top: "+pos.top+"  pos.left: "+pos.left);
    });
  }
  else if(  $("#rotationHidden").html() == "HalfHoriz" && $("#orientationHidden").html() == "180" && $("#fieldTypeHidden").html() == "Half"){
    $(".position"+playerPosition).each(function() {
      //we get the original top and left values
      var actualTop = parseFloat($(this).css('top'))
      var actualLeft = parseFloat($(this).css('left'))
      pos = displayPos2fieldPos( actualLeft, actualTop, "HalfVertOffense" )
      pos = fieldPos2displayPos( pos.left, pos.top, "HorizDefense" )
      //we asign the new values
      $(this).css("top", pos.top);
      $(this).css("left", pos.left);
      writeConsoleLog("  actualTop: "+actualTop+"  actualLeft: "+actualLeft+"  pos.top: "+pos.top+"  pos.left: "+pos.left);
    });
  }
  else if( $("#orientationHidden").html() == "0" && $("#fieldTypeHidden").html() == "Full"){
    $(".position"+playerPosition).each(function() {
      //we get the original top and left values
      var actualTop = parseFloat($(this).css('top'))
      var actualLeft = parseFloat($(this).css('left'))
      pos = displayPos2fieldPos( actualLeft, actualTop, "HalfVertOffense" )
      pos = fieldPos2displayPos( pos.left, pos.top, "FullOffense" )
      $(this).css({'transform':'scale(0.5)', 'box-shadow' : '0px 0px'});
      $(this).css("font-size","3vw");
      $(".ball").css({'transform':'scale(0.5)'});
      //we asign the new values
      $(this).css("top", pos.top);
      $(this).css("left", pos.left);
      writeConsoleLog("  actualTop: "+actualTop+"  actualLeft: "+actualLeft+"  pos.top: "+pos.top+"  pos.left: "+pos.left);
    });
    if( aux==0 ){
      return
    }
    setGoalkeeper();
  }
  else if( $("#orientationHidden").html() == "180" && $("#fieldTypeHidden").html() == "Full"){
    $(".position"+playerPosition).each(function() {
      //we get the original top and left values
      var actualTop = parseFloat($(this).css('top'))
      var actualLeft = parseFloat($(this).css('left'))
      pos = displayPos2fieldPos( actualLeft, actualTop, "HalfVertOffense" )
      pos = fieldPos2displayPos( pos.left, pos.top, "FullDefense" )
      $(this).css({'transform':'scale(0.5)', 'box-shadow' : '0px 0px'});;
      $(this).css("font-size","3vw");
      $(".ball").css({'transform':'scale(0.5)'});
      //we asign the new values
      $(this).css("top", pos.top);
      $(this).css("left", pos.left);
      writeConsoleLog("  actualTop: "+actualTop+"  actualLeft: "+actualLeft+"  pos.top: "+pos.top+"  pos.left: "+pos.left);
    });
    if( aux==0 ){
      return
    }
    setGoalkeeper();
  }

}


/////////////////////////////////////
//function that sets the other goalkeeper
function setGoalkeeper(){

  offensiveTeam = game.offensiveTeam

  if( $("#fieldTypeHidden").html() == "Full"){
    if( $(".positionOGK." + offensiveTeam).length){
      var a=0;
    }else{
      var currentLayout = $("#courtImage").attr("src");
      res = currentLayout.replace("HalfVert", "Full");
      $("#courtImage").attr("src", res);
      $("#courtImage").attr('width', 'auto');
      $("#courtImage").attr('height', '100%');

      resetOrientation()
      setPlayer(offensiveTeam, "GK", [98, 49.5]);
      setOrientation(0)

      var teamB = 0
      $("."+offensiveTeam).each(function() {
        teamB++
      });
      if( teamB < 2 ){
        $(".positionOGK." + offensiveTeam).remove();
      }else{
        $(this).css({'transform':'scale(0.5)', 'box-shadow' : '0px 0px'});
        $(".positionOGK." + offensiveTeam).css("font-size","3vw");
      }
    }
  }else{
    $(".positionOGK." + offensiveTeam).remove();
    var currentLayout = $("#courtImage").attr("src");
    res = currentLayout.replace("Full", "HalfVert");
    $("#courtImage").attr("src", res);
    $("#courtImage").attr('width', '100%');
    $("#courtImage").attr('height', 'auto');
  }
  return;

}


/////////////////////////////////////////////////////
// function that changes from fieldPos to displayPos
function fieldPos2displayPos( fieldPosX, fieldPosY, type ){


  var height = $('.app').height();
  var width = $('.app').width();

  var startCourtTop = $(".court").position().top;
  var startCourtLeft = $(".court").position().left;
  var courtHeight = $(".court").height();
  var courtWidth = $(".court").width();

  var halfVertCourtWidth_svg = 233.905;
  var halfVertCourtHeight_svg = 257.094;
  var halfVertCourtMarginTop_svg = 11.323;
  var halfVertCourtMarginLeft_svg = 9.599;
  var halfVertInnerCourtWidth_svg = 214.333;
  var halfVertOffsetCentered = 0.5*(height*0.90-(halfVertCourtHeight_svg*width/halfVertCourtWidth_svg));

  var halfHorizCourtWidth_svg = 195.061;
  var halfHorizCourtHeight_svg = 233.905;
  var halfHorizCourtMarginTop_svg = 9.599;
  var halfHorizCourtMarginRight_svg = 11.324;
  var halfHorizInnerCourtHeight_svg = 214.333;
  var halfHorizOffsetCentered = 0.5*(height*0.90-(halfHorizCourtHeight_svg*width/halfHorizCourtWidth_svg));

  var fullCourtHeight_svg = 449.811;
  var fullCourtWidth_svg = 233.905;
  var fullCourtMarginTop_svg = 11.29;
  var fullCourtMarginBottom_svg = 11.054;
  var fullCourtMarginLeft_svg = 9.752;
  var fullCourtMarginRight_svg = 9.753;


  if( type=="HalfVertOffense" ){
    var playersHeight = height*0.06;
    a = width/halfVertCourtWidth_svg;
    offsetY = halfVertCourtMarginTop_svg*a - playersHeight/2 + halfVertOffsetCentered;
    offsetX = halfVertCourtMarginLeft_svg*a - playersHeight/2;
    kY = 50/(halfVertInnerCourtWidth_svg*a);
    kX = 100/(halfVertInnerCourtWidth_svg*a);
    left = fieldPosX/kX + offsetX;
    topo = fieldPosY/kY + offsetY;
    /*writeConsoleLog("\n\n\n")
    writeConsoleLog("kY:  " + kY + " kX:   " + kX)
    writeConsoleLog( "fieldPosX:  " + fieldPosX + " fieldPosY:  " + fieldPosY )
    writeConsoleLog( "left:  " + left + " topo:  " + topo )
    writeConsoleLog("a:  " + a)
    writeConsoleLog("players_height/2:  " + $(".players").innerHeight()/2 )
    writeConsoleLog("\n\n\n")*/
  }else if( type=="HalfVertDefense" ){
    var playersHeight = height*0.06;
    a = width/halfVertCourtWidth_svg;
    offsetY = (halfVertCourtHeight_svg-halfVertCourtMarginTop_svg)*a -  playersHeight/2+halfVertOffsetCentered;
    offsetX = (halfVertCourtWidth_svg-halfVertCourtMarginLeft_svg)*a -  playersHeight/2;
    kX = -100/(halfVertInnerCourtWidth_svg*a);
    kY = -50/(halfVertInnerCourtWidth_svg*a);
    left = fieldPosX/kX + offsetX;
    topo = fieldPosY/kY + offsetY;
  }else if( type=="HalfHorizOffense" ){
    var playersHeight = height*0.065;
    a = width/halfHorizCourtWidth_svg;
    offsetY = halfHorizCourtMarginTop_svg*a -  playersHeight/2 + halfHorizOffsetCentered;
    offsetX = (halfHorizCourtWidth_svg-halfHorizCourtMarginRight_svg)*a -  playersHeight/2;
    kX = -50/(halfHorizInnerCourtHeight_svg*a);
    kY = 100/(halfHorizInnerCourtHeight_svg*a);
    left = fieldPosY/kX + offsetX;
    topo = fieldPosX/kY + offsetY;
  }else if( type=="HalfHorizDefense" ){
    var playersHeight = height*0.065;
    a = width/halfHorizCourtWidth_svg;
    offsetY = (halfHorizCourtHeight_svg-halfHorizCourtMarginTop_svg)*a -  playersHeight/2+halfHorizOffsetCentered;
    offsetX = halfHorizCourtMarginRight_svg*a -  playersHeight/2;
    kX = 50/(halfHorizInnerCourtHeight_svg*a);
    kY = -100/(halfHorizInnerCourtHeight_svg*a);
    left = fieldPosY/kX + offsetX;
    topo = fieldPosX/kY + offsetY;
  }else if( type=="FullOffense" ){
    var playersHeight = height*0.055;
    a = 0.9*height/fullCourtHeight_svg;
    offsetY = fullCourtMarginTop_svg*a -  playersHeight/2;
    offsetX = (width - fullCourtWidth_svg*a)/2 + fullCourtMarginLeft_svg*a -  playersHeight/2;
    kY = (100)/((fullCourtHeight_svg-fullCourtMarginTop_svg-fullCourtMarginBottom_svg)*a);
    kX = (100)/((fullCourtWidth_svg-fullCourtMarginLeft_svg-fullCourtMarginRight_svg)*a);
    topo = fieldPosY/kY + offsetY;
    left = fieldPosX/kX + offsetX;
  }else if( type=="FullDefense" ){
    var playersHeight = height*0.055;
    a = 0.9*height/fullCourtHeight_svg;
    offsetY = (fullCourtHeight_svg-fullCourtMarginTop_svg)*a -  playersHeight/2;
    offsetX = (width - fullCourtWidth_svg*a)/2 + fullCourtWidth_svg*a - fullCourtMarginLeft_svg*a -  playersHeight/2;
    kY = (-100)/((fullCourtHeight_svg-fullCourtMarginTop_svg-fullCourtMarginBottom_svg)*a);
    kX = (-100)/((fullCourtWidth_svg-fullCourtMarginLeft_svg-fullCourtMarginRight_svg)*a);
    topo = fieldPosY/kY + offsetY;
    left = fieldPosX/kX + offsetX;
  }

  //topo = parseFloat( topo/ $(".court").height() * 100 );
  //left = parseFloat( left/ $(".court").width() * 100 );

  writeConsoleLog("fieldPos2displayPos Converted fieldPosX: "+fieldPosX+" fieldPosY: "+fieldPosY+" for Layout: "+type+" in display top: " + topo + "   left:  " + left+" height: "+height+" width: "+width+" startCourtTop: "+startCourtTop+" startCourtLeft: "+startCourtLeft+" courtHeight: "+courtHeight+" courtWidth: "+courtWidth+" $('.players').height(): "+$(".players").height()+" playersHeight: "+playersHeight)

  return {
          top: topo,
          left: left
      };

}


/////////////////////////////////////////////////////
// function that changes from displayPos to fieldPos
function displayPos2fieldPos( displayPosX, displayPosY, type){

  var height = $('.app').height();
  var width = $('.app').width();

  var startCourtTop = $(".court").position().top;
  var startCourtLeft = $(".court").position().left;
  var courtHeight = $(".court").height();
  var courtWidth = $(".court").width();

  var halfVertCourtWidth_svg = 233.905;
  var halfVertCourtHeight_svg = 257.094;
  var halfVertCourtMarginTop_svg = 11.323;
  var halfVertCourtMarginLeft_svg = 9.599;
  var halfVertInnerCourtWidth_svg = 214.333;
  var halfVertOffsetCentered = 0.5*(height*0.90-(halfVertCourtHeight_svg*width/halfVertCourtWidth_svg));

  var halfHorizCourtWidth_svg = 195.061;
  var halfHorizCourtHeight_svg = 233.905;
  var halfHorizCourtMarginTop_svg = 9.599;
  var halfHorizCourtMarginRight_svg = 11.324;
  var halfHorizInnerCourtHeight_svg = 214.333;
  var halfHorizOffsetCentered = 0.5*(height*0.90-(halfHorizCourtHeight_svg*width/halfHorizCourtWidth_svg));

  var fullCourtHeight_svg = 449.811;
  var fullCourtWidth_svg = 233.905;
  var fullCourtMarginTop_svg = 11.29;
  var fullCourtMarginBottom_svg = 11.054;
  var fullCourtMarginLeft_svg = 9.752;
  var fullCourtMarginRight_svg = 9.753;

  if( type=="HalfVertOffense" ){
    var playersHeight = height*0.06;
    a = width/halfVertCourtWidth_svg;
    offsetY = halfVertCourtMarginTop_svg*a -  playersHeight/2+halfVertOffsetCentered;
    offsetX = halfVertCourtMarginLeft_svg*a -  playersHeight/2;
    kX = 100/(halfVertInnerCourtWidth_svg*a);
    kY = 50/(halfVertInnerCourtWidth_svg*a);
    left = kX*( displayPosX - offsetX );
    topo = kY*( displayPosY - offsetY );
  }else if( type=="HalfVertDefense" ){
    var playersHeight = height*0.06;
    a = width/halfVertCourtWidth_svg;
    offsetY = (halfVertCourtHeight_svg-halfVertCourtMarginTop_svg)*a -  playersHeight/2+halfVertOffsetCentered;
    offsetX = (halfVertCourtWidth_svg-halfVertCourtMarginLeft_svg)*a -  playersHeight/2;
    kX = -100/(halfVertInnerCourtWidth_svg*a);
    kY = -50/(halfVertInnerCourtWidth_svg*a);
    left = kX*( displayPosX - offsetX );
    topo = kY*( displayPosY - offsetY );
  }else if( type=="HorizOffense" ){
    var playersHeight = height*0.065;
    a = width/halfHorizCourtWidth_svg;
    offsetY = halfHorizCourtMarginTop_svg*a -  playersHeight/2 + halfHorizOffsetCentered;
    offsetX = (halfHorizCourtWidth_svg-halfHorizCourtMarginRight_svg)*a -  playersHeight/2;
    kX = -50/(halfHorizInnerCourtHeight_svg*a);
    kY = 100/(halfHorizInnerCourtHeight_svg*a);
    topo = kX*( displayPosX - offsetX );
    left = kY*( displayPosY - offsetY );
  }else if( type=="HorizDefense" ){
    var playersHeight = height*0.065;
    a = width/halfHorizCourtWidth_svg;
    offsetY = (halfHorizCourtHeight_svg-halfHorizCourtMarginTop_svg)*a -  playersHeight/2 + halfHorizOffsetCentered;
    offsetX = halfHorizCourtMarginRight_svg*a -  playersHeight/2;
    kX = 50/(halfHorizInnerCourtHeight_svg*a);
    kY = -100/(halfHorizInnerCourtHeight_svg*a);
    topo = kX*( displayPosX - offsetX );
    left = kY*( displayPosY - offsetY );
  }else if( type=="FullOffense" ){
    var playersHeight = height*0.055;
    a = 0.9*height/fullCourtHeight_svg;
    offsetY = fullCourtMarginTop_svg*a -  playersHeight/2;
    offsetX = (width - fullCourtWidth_svg*a)/2 + fullCourtMarginLeft_svg*a -  playersHeight/2;
    kY = (100)/((fullCourtHeight_svg-fullCourtMarginTop_svg-fullCourtMarginBottom_svg)*a);
    kX = (100)/((fullCourtWidth_svg-fullCourtMarginLeft_svg-fullCourtMarginRight_svg)*a);
    topo = kY * ( displayPosY - offsetY );
    left = kX * ( displayPosX - offsetX );
  }else if( type=="FullDefense" ){
    var playersHeight = height*0.055;
    a = 0.9*height/fullCourtHeight_svg;
    offsetY = (fullCourtHeight_svg-fullCourtMarginTop_svg)*a -  playersHeight/2;
    offsetX = (width - fullCourtWidth_svg*a)/2 + fullCourtWidth_svg*a - fullCourtMarginLeft_svg*a -  playersHeight/2;
    kY = (-100)/((fullCourtHeight_svg-fullCourtMarginTop_svg-fullCourtMarginBottom_svg)*a);
    kX = (-100)/((fullCourtWidth_svg-fullCourtMarginLeft_svg-fullCourtMarginRight_svg)*a);
    topo = kY * ( displayPosY - offsetY );
    left = kX * ( displayPosX - offsetX );
  }

  return {
          top: topo,
          left: left
      };

}


/////////////////////////////////////////////////////
// function that sets the out of bounds arrow
function setArrow( playerId, flip ){
  $(".court").append("<svg height='8vh' width='8vh' class='arrow rotates' id='arrow"+playerId+"'><polygon points='25,10 75,10 75,45, 90,45 50,90 10,45 25,45 ' style='fill:orange;stroke:red;stroke-width:4%' /><text id='arrowText' x='47%' y='50%' font-size='60%' text-anchor='middle'>"+ playerId.substring(5) +"</text></svg>")
  color1 = $("#"+playerId).css("background-color")
  color2 = $("#"+playerId).css("border-color")
  $("#arrow"+playerId).css({"fill":color1, "stroke":color2, "stroke-width":"4%"})

  if(flip == 1){
    $("#arrow" + playerId).addClass("horizArrow")
    posArrow = fieldPos2displayPos( pos.left-3, 40, "HorizOffense")
  }else if( flip == 2){
    $("#arrow" + playerId).addClass("horizArrow")
    posArrow = fieldPos2displayPos( pos.left-3, 40, "HorizDefense")
  }else if( flip == 3){
    $("#arrow" + playerId).addClass("fullArrow")
    posArrow = fieldPos2displayPos( pos.left-3, 55, "FullOffense")
  }else if( flip == 4){
    $("#arrow" + playerId).addClass("fullArrow")
    posArrow = fieldPos2displayPos( pos.left-3, 55, "FullDefense")
  }
  $("#"+playerId).css('visibility', 'hidden');
  $("#"+playerId).addClass("hiddenPlayer")
  $("#arrow" + playerId).css({ "top": posArrow.top, "left": posArrow.left })

  //adding an offensive/defensive iddentifier to the arrow
  if( playerId.includes("teamA") ){
    $("#arrow"+playerId).addClass("teamAArrow")
  }else{
    $("#arrow"+playerId).addClass("teamBArrow")
  }

  return posArrow

}


//

function transformPlayersPosition(targetLayout){

  if(game.offensiveTeam == "teamA"){
    setTeam("teamA", game.teamA.offensiveTeam, targetLayout, false);
    setTeam("teamB", game.teamB.defensiveTeam, targetLayout, false);
  }
  else if(game.offensiveTeam == "teamB"){
    setTeam("teamA", game.teamA.defensiveTeam, targetLayout, false);
    setTeam("teamB", game.teamB.offensiveTeam, targetLayout, false);
  }


}

function getBallOffsetsForLayout(targetLayout){

  var offsetTop = 0;
  var offsetLeft = -2.5;

  // if(targetLayout == "HalfVertOffense"){
  //   offsetTop = 0;
  //   offsetLeft = -2.5;
  // }
  // else if(targetLayout == "HalfVertDefense"){
  //   offsetTop = 0;
  //   offsetLeft = -2.5;
  // }
  // else if(targetLayout == "FullOffense"){
  //   offsetTop = 0;
  //   offsetLeft = -2.5;
  // }
  // else if(targetLayout == "FullDefense"){
  //   offsetTop = 0;
  //   offsetLeft = -2.5;
  // }
  // else if(targetLayout == "HalfHorizOffense"){
  //   offsetTop = 0;
  //   offsetLeft = -2.5;
  // }
  // else if(targetLayout == "HalfHorizDefense"){
  //   offsetTop = 0;
  //   offsetLeft = -2.5;
  // }

  return {
          top: offsetTop,
          left: offsetLeft
      };
}



//
