
$(document).ready(function() {
// $(document).ready(function($) {

	var i=0
	var orientation = 0;

	$("#offensiveTeam").text("Attacking: Loading..."); // At the beginning always teamA as deffending

	$("#courtImage").attr("max-height", "100%");
	$("#courtImage").attr("width", "100%");
	$("#courtImage").attr("vertical-align", "middle");

	////////////////////////////////////////////
	//method that switches the field orientation
	$("#homeLogo").click(function(){

		if(game.offensiveTeam == "teamA") // TeamA was attacking
		{
			setTeam("teamB", game.teamB.defensiveTeam, getCurrentLayout(), true);
			setTeam("teamA", game.teamA.offensiveTeam, getCurrentLayout(), true);

		}
		else if(game.offensiveTeam == "teamB") // TeamB was attacking
		{
			setTeam("teamB", game.teamB.offensiveTeam, getCurrentLayout(), true);
			setTeam("teamA", game.teamA.defensiveTeam, getCurrentLayout(), true);
		}
		$( "#buttonStop" ).click();

		resetCanvas()
		$("#playState").html("")
		setState("FreeDraw")

	});


	//
	// ////////////////////////////////////////////
	// //method that switches the field orientation
	// $("#buttonRecord").click(function(){
	//
	// 	console.log("in")
	//
	// 	if( $("#modeHidden").html() == "recording" && $("#playState").html() != "edit" ){
	// 		alert("A tactic is already being recorded");
	// 		return
	// 	}
	//
	// 	// in beta version, only 50 tactics are allowed.
	// 	if(allPlays.length >= 50){
	// 		$("#beta_version_modal").modal("show")
	// 		$("#beta_version_modal_message").html("You have reached the maximum of 5 tactics.<br>Remove one before creating new ones.<br>To delete: Open Play > Long Press in one Play > Remove Play")
	// 		return
	// 	}
	//
	// 	// ball must be visible to start recording
	// 	if($("#ball").attr("id")==undefined){
	// 		$("#random_error_modal").modal("show")
	// 		$("#random_error_modal_message").html("Ball must be visible to start recording")
	// 		return
	// 	}
	//
	// 	//setting an empty play to start to record on top of it //  play.id, play.playName, play.type, play.favorite, []
	// 	actualPlay = new Play( allPlays.length + 1 , "", "", "", [], "", "")
	//
	// 	$("#modeHidden").html("recording");
	// 	setState("Recording");
	//
	// 	movesForFirstSnapshot = []
	// 	movesForSecondSnapshot = []
	// 	players = 0
	// 	playerWithBall = ""
	//
	// 	// Save offensive, defensive strategy and initial player with ball
	// 	actualPlay.offensiveStrategy = game[game.offensiveTeam].offensiveTeam;
	// 	actualPlay.defensiveStrategy = game[getDefensiveTeam()].defensiveTeam;
	// 	//writeConsoleLog("PLAYINIT actualPlay.offensiveStrategy: "+actualPlay.offensiveStrategy+"  actualPlay.defensiveStrategy: "+actualPlay.defensiveStrategy);
	//
	// 	var offensivePositions = getPositionsForSystem(actualPlay.offensiveStrategy);
	// 	var defensivePositions = getPositionsForSystem(actualPlay.defensiveStrategy);
	// 	// var gamePositions = offensivePositions.concat(defensivePositions);
	// 	//writeConsoleLog("PLAYINIT offensivePositions: "+offensivePositions+"defensivePositions"+defensivePositions);
	//
	// 	for(i=0; i<offensivePositions.length; i++){
	//
	// 		//we get the position and convert it to fieldPos
	// 		var actualTop = getPlayerValuePerID(game[game.offensiveTeam].courtPlayers[offensivePositions[i]],'fieldPosY');
	// 		var actualLeft = getPlayerValuePerID(game[game.offensiveTeam].courtPlayers[offensivePositions[i]],'fieldPosX')
	//
	// 		//writeConsoleLog("PLAYINIT actualTop: "+actualTop+" actualLeft: "+actualLeft);
	//
	// 		//creating a move for the player, which corresponds with its initial position
	// 		player = game[game.offensiveTeam].courtPlayers[offensivePositions[i]];
	// 		//writeConsoleLog("PLAYINIT player: "+player);
	//
	// 		//getting the position of the actual player (or ball)
	// 		position = offensivePositions[i];
	//
	// 		playName = actualPlay.playName
	// 		snapshotNumber = 0
	//
	// 		positions = []
	// 		positions2 = []
	// 		positions.push(actualLeft + "&&" +  actualTop)
	// 		positions.push(actualLeft + "&&" +  actualTop)
	// 		positions2.push(actualLeft + "&&" +  actualTop)
	//
	// 		//writeConsoleLog( "recorded:  " + player + "  " + team + "  " + position + "  " + pos.left + "  " + pos.top)
	//
	// 		move = new Move( position, playName, snapshotNumber, positions )
	// 		movesForFirstSnapshot.push( move )
	//
	// 		move2 = new Move( position, playName, snapshotNumber+1, positions2 )
	// 		movesForSecondSnapshot.push( move2 )
	//
	// 		players++
	//
	// 	}
	//
	// 	for(i=0; i<defensivePositions.length; i++){
	//
	// 	 //we get the position and convert it to fieldPos
	// 		var actualTop = getPlayerValuePerID(game[getDefensiveTeam()].courtPlayers[defensivePositions[i]],'fieldPosY');
	// 		var actualLeft = getPlayerValuePerID(game[getDefensiveTeam()].courtPlayers[defensivePositions[i]],'fieldPosX')
	//
	// 		writeConsoleLog("PLAYINIT actualTop: "+actualTop+" actualLeft: "+actualLeft);
	//
	//
	// 		//creating a move for the player, which corresponds with its initial position
	// 		player = game[getDefensiveTeam()].courtPlayers[defensivePositions[i]];
	// 		writeConsoleLog("PLAYINIT player: "+player);
	//
	// 		//getting the position of the actual player (or ball)
	// 		position = defensivePositions[i];
	//
	// 		playName = actualPlay.playName
	// 		snapshotNumber = 0
	//
	// 		positions = []
	// 		positions2 = []
	// 		positions.push(actualLeft + "&&" +  actualTop)
	// 		positions.push(actualLeft + "&&" +  actualTop)
	// 		positions2.push(actualLeft + "&&" +  actualTop)
	//
	// 		//writeConsoleLog( "recorded:  " + player + "  " + team + "  " + position + "  " + pos.left + "  " + pos.top)
	//
	// 		move = new Move( position, playName, snapshotNumber, positions )
	// 		movesForFirstSnapshot.push( move )
	//
	// 		move2 = new Move( position, playName, snapshotNumber+1, positions2 )
	// 		movesForSecondSnapshot.push( move2 )
	//
	// 		players++
	//
	// 	}
	//
	// 	playerWithBall = game.playerWithBall
	//
	// 	if( players == 0 ){
	// 		alert( "You should have players on the field to be able to create a Tactic" )
	// 		setState("FreeDraw");
	// 	}
	//
	// 	//initializing variables to create the objects
	// 	datetime = new Date().toLocaleString();
	// 	//playName = "newPlay" + datetime
	// 	playName = actualPlay.playName
	// 	type = actualPlay.type
	// 	favorite = "false"
	// 	team = $("#teamA").html();
	// 	var offensiveStrategyPlay = actualPlay.offensiveStrategy;
	// 	var defensiveStrategyPlay = actualPlay.defensiveStrategy;
	//
	// 	writeConsoleLog("PLAYINIT offensiveStrategyPlay: "+offensiveStrategyPlay);
	//
	// 	//creating the Play and filling it with the initial parameters and with the first snapshot (initial positions)
	// 	snapshot = new Snapshot( snapshotNumber, playName, playerWithBall, movesForFirstSnapshot )
	// 	actualPlay = new Play( allPlays.length + 1 , playName, type, favorite, [snapshot], offensiveStrategyPlay, defensiveStrategyPlay )
	// 	allPlays.push(actualPlay)
	// 	//alert("Starting to record")
	//
	// 	//initializing the second snapshot
	// 	actualMoves = movesForSecondSnapshot
	// 	actualSnapshot = new Snapshot( snapshotNumber+1, playName, playerWithBall, movesForSecondSnapshot )
	//
	// 	processSnapshotDisplay()
	//
	// });
	//

	////////////////////////////////////////////
	//method that switches the field orientation
	$("#switchOrientation").click(function(){


		var sourceLayout = getCurrentLayout();
		var targetLayout = "";

		writeConsoleLog("sourceLayout: "+sourceLayout);

		if(sourceLayout.includes("Offense")) // TeamA was attacking
		{
			targetLayout = sourceLayout.replace("Offense", "Defense");
			game.offensiveTeam = "teamB";
			setTeam("teamB", game.teamB.offensiveTeam, targetLayout, true);
			setTeam("teamA", game.teamA.defensiveTeam, targetLayout, true);

			setLayout(targetLayout, true);
			GAME_SETTINGS.layout = targetLayout
			GAME_SETTINGS.orientation = 180

			writeConsoleLog("Arrow: targetLayout: "+targetLayout+"   game.offensiveTeam: "+ game.offensiveTeam+"    game.teamA.offensiveTeam: "+game.teamA.offensiveTeam+"   game.teamB.offensiveTeam: "+game.teamB.offensiveTeam);
		}
		else if(sourceLayout.includes("Defense")) // TeamB was attacking
		{
			targetLayout = sourceLayout.replace("Defense", "Offense");
			game.offensiveTeam = "teamA";
			setTeam("teamB", game.teamB.defensiveTeam, targetLayout, true);
			setTeam("teamA", game.teamA.offensiveTeam, targetLayout, true);

			setLayout(targetLayout,true);
			GAME_SETTINGS.layout = targetLayout
			GAME_SETTINGS.orientation = 0

			writeConsoleLog("Arrow: targetLayout: "+targetLayout+"   game.offensiveTeam: "+ game.offensiveTeam+"    game.teamA.offensiveTeam: "+game.teamA.offensiveTeam+"   game.teamB.offensiveTeam: "+game.teamB.offensiveTeam);
		}

		offensiveTeam = game.offensiveTeam;

	});


	////////////////////////////////////////////
	//method that switches between half and full field
	$("#switchHalfFull").click(function(){

		var sourceLayout = getCurrentLayout();
		var targetLayout = "";

		if(sourceLayout.includes("HalfVert")){
			targetLayout = sourceLayout.replace("HalfVert", "Full");
			GAME_SETTINGS.halfFull = "Full"
		}
		else if(sourceLayout.includes("HalfHoriz")){
			targetLayout = sourceLayout.replace("HalfHoriz", "Full");
			GAME_SETTINGS.halfFull = "Full"
		}
		else if(sourceLayout.includes("Full")){
			targetLayout = sourceLayout.replace("Full", "HalfVert");
			GAME_SETTINGS.halfFull = "HalfVert"
		}
		else {
			alert("Court cannot be changed for this layout");
		}

		transformPlayersPosition(targetLayout);
		setLayout(targetLayout,false);
		GAME_SETTINGS.layout = targetLayout
    });


	////////////////////////////////////////////
	//method that switches the side of the bench
	$("#switchBench").click(function(){
		var currentLayout = $("#courtImage").attr("src");
		var res;

		if(currentLayout.includes("Left")){
			res = currentLayout.replace("Left", "Right");
			$("#benchHidden").html("Right");
		}
		else if(currentLayout.includes("Right")){
			res = currentLayout.replace("Right", "Left");
			$("#benchHidden").html("Left");
		}
		else {
			alert("Bench side cannot be changed for this layout");
		}

        $("#courtImage").attr("src", res);
    });


	////////////////////////////////////////////
	//method that rotates the field on the screen
  $("#switchRotation").click(function(){

	var sourceLayout = getCurrentLayout();
	var targetLayout = "";

		if(sourceLayout.includes("HalfVert")){
			targetLayout = sourceLayout.replace("HalfVert", "HalfHoriz");
			GAME_SETTINGS.halfFull = "HalfHoriz"
		}
		else if(sourceLayout.includes("HalfHoriz")){
			targetLayout = sourceLayout.replace("HalfHoriz", "HalfVert");
			GAME_SETTINGS.halfFull = "HalfVert"
		}
		else if(sourceLayout.includes("Full")){
			targetLayout = sourceLayout.replace("Full", "HalfHoriz");
			GAME_SETTINGS.halfFull = "HalfHoriz"
		}
		else {
			alert("Court cannot be changed for this layout");
		}

		transformPlayersPosition(targetLayout);
		setLayout(targetLayout,false);
		GAME_SETTINGS.layout = targetLayout
    });


	////////////////////////////////////////////
	//method that changes the field layout
  $("#switchLayout").click(function(){
  	var currentLayout = $("#courtImage").attr("src");
		var res;

	if(currentLayout.includes("00")){
  		res = currentLayout.replace("00", "01");
		$("#courtImage").attr("src", res);
  	}
  	else if(currentLayout.includes("01")){
  		res = currentLayout.replace("01", "02");
		$("#courtImage").attr("src", res);
  	}
  	else if(currentLayout.includes("02")){
  		res = currentLayout.replace("02", "00");
		$("#courtImage").attr("src", res);
  	}
  });



	////////////////////////////////////////////////
	// Create Canvas - Arrows for tracing players/ball movements
	var windowWidth = window.innerWidth;
	var windowHeight = window.innerHeight;
	// var pixelRatio = window.devicePixelRatio || 1; /// get pixel ratio of device

	$(".court").append("<canvas class='arrowCanvas' id='courtCanvas'></canvas>");
	canvasMain = document.getElementById("courtCanvas");

	canvasMain.width = windowWidth;
	canvasMain.height = parseInt(0.90*windowHeight);

	canvasMain.style.width = windowWidth + 'px';   /// CSS size of canvas
	canvasMain.style.height = parseInt(0.90*windowHeight) + 'px';

	//////////////////////////////////////////////////

	////////////////////////////////////////////////
	// Create Canvas - Arrows for players out of sight

	$(".court").append("<div class='arrowCanvasOutOfSight' id='courtCanvasOutOfSight'></div>");
	canvasArrowsOutOfSight = document.getElementById("courtCanvasOutOfSight");

	canvasArrowsOutOfSight.width = windowWidth;
	canvasArrowsOutOfSight.height = parseInt(0.90*windowHeight);

	canvasArrowsOutOfSight.style.width = windowWidth + 'px';   /// CSS size of canvas
	canvasArrowsOutOfSight.style.height = parseInt(0.90*windowHeight) + 'px';


	//////////////////////////////////////////////////
	// Game Settings Popup
	$("#gameSettings").click(function(){
  	$('#teamLogoTeamA').attr('src',game.teamA.image_path);
		$('#teamNameTeamA').text(game.teamA.teamName);
		$("#teamCircleTeamA").css({"border-color":game.teamA.lineColorA, "background-color": game.teamA.filledColorA})
		$('#teamOffensiveStrategyTeamA').text(game.teamA.offensiveTeam);
		$('#teamDefensiveStrategyTeamA').text(game.teamA.defensiveTeam);

		$('#teamOffensivePlayersTeamA').html("");
		positions = getPositionsForSystem(game.teamA.offensiveTeam);
		for(var i=0; i<positions.length; i++){
			tempNum = getPlayerValuePerID(game.teamA.courtPlayers[positions[i]], "number");
			tempName = getPlayerValuePerID(game.teamA.courtPlayers[positions[i]], "playerNameShort");
			tempFieldPosX = getPlayerValuePerID(game.teamA.courtPlayers[positions[i]], "fieldPosX");
			tempFieldPosY = getPlayerValuePerID(game.teamA.courtPlayers[positions[i]], "fieldPosY");
			$('#teamOffensivePlayersTeamA').append("<div class='row'><div class='col-2 my-auto team-player-position'>"+positions[i]+"</div><div class='col-1 my-auto team-player-number'>"+tempNum+"</div><div class='col-4'>"+tempName+"</div><div class='col-2'>"+parseFloat(tempFieldPosX).toFixed(1)+"</div><div class='col-2'>"+parseFloat(tempFieldPosY).toFixed(1)+"</div></div>");
		}

		$('#teamDefensivePlayersTeamA').html("");
		positions = getPositionsForSystem(game.teamA.defensiveTeam);
		for(var i=0; i<positions.length; i++){
			tempNum = getPlayerValuePerID(game.teamA.courtPlayers[positions[i]], "number");
			tempName = getPlayerValuePerID(game.teamA.courtPlayers[positions[i]], "playerNameShort");
			tempFieldPosX = getPlayerValuePerID(game.teamA.courtPlayers[positions[i]], "fieldPosX");
			tempFieldPosY = getPlayerValuePerID(game.teamA.courtPlayers[positions[i]], "fieldPosY");
			$('#teamDefensivePlayersTeamA').append("<div class='row'><div class='col-2 my-auto team-player-position'>"+positions[i]+"</div><div class='col-1 my-auto team-player-number'>"+tempNum+"</div><div class='col-4'>"+tempName+"</div><div class='col-2'>"+parseFloat(tempFieldPosX).toFixed(1)+"</div><div class='col-2'>"+parseFloat(tempFieldPosY).toFixed(1)+"</div></div>");
		}

		$('#teamLogoTeamB').attr('src',game.teamB.image_path);
		$('#teamNameTeamB').text(game.teamB.teamName);
		$("#teamCircleTeamB").css({"border-color":game.teamB.lineColorA, "background-color": game.teamB.filledColorA})
		$('#teamOffensiveStrategyTeamB').text(game.teamB.offensiveTeam);
		$('#teamDefensiveStrategyTeamB').text(game.teamB.defensiveTeam);

		$('#teamOffensivePlayersTeamB').html("");
		positions = getPositionsForSystem(game.teamB.offensiveTeam);
		for(var i=0; i<positions.length; i++){
			tempNum = getPlayerValuePerID(game.teamB.courtPlayers[positions[i]], "number");
			tempName = getPlayerValuePerID(game.teamB.courtPlayers[positions[i]], "playerNameShort");
			tempFieldPosX = getPlayerValuePerID(game.teamB.courtPlayers[positions[i]], "fieldPosX");
			tempFieldPosY = getPlayerValuePerID(game.teamB.courtPlayers[positions[i]], "fieldPosY");
			$('#teamOffensivePlayersTeamB').append("<div class='row'><div class='col-2 my-auto team-player-position'>"+positions[i]+"</div><div class='col-1 my-auto team-player-number'>"+tempNum+"</div><div class='col-4'>"+tempName+"</div><div class='col-2'>"+parseFloat(tempFieldPosX).toFixed(1)+"</div><div class='col-2'>"+parseFloat(tempFieldPosY).toFixed(1)+"</div></div>");
		}

		$('#teamDefensivePlayersTeamB').html("");
		positions = getPositionsForSystem(game.teamB.defensiveTeam);
		for(var i=0; i<positions.length; i++){
			tempNum = getPlayerValuePerID(game.teamB.courtPlayers[positions[i]], "number");
			tempName = getPlayerValuePerID(game.teamB.courtPlayers[positions[i]], "playerNameShort");
			tempFieldPosX = getPlayerValuePerID(game.teamB.courtPlayers[positions[i]], "fieldPosX");
			tempFieldPosY = getPlayerValuePerID(game.teamB.courtPlayers[positions[i]], "fieldPosY");
			$('#teamDefensivePlayersTeamB').append("<div class='row'><div class='col-2 my-auto team-player-position'>"+positions[i]+"</div><div class='col-1 my-auto team-player-number'>"+tempNum+"</div><div class='col-4'>"+tempName+"</div><div class='col-2'>"+parseFloat(tempFieldPosX).toFixed(1)+"</div><div class='col-2'>"+parseFloat(tempFieldPosY).toFixed(1)+"</div></div>");
		}
	});

});



function getCurrentLayout(){

  var res = undefined;

  if( $("#rotationHidden").html() == "HalfVert" && $("#orientationHidden").html() == "0" && $("#fieldTypeHidden").html() == "Half"){
    res = "HalfVertOffense";
  }
  else if(  $("#rotationHidden").html() == "HalfVert" && $("#orientationHidden").html() == "180" && $("#fieldTypeHidden").html() == "Half"){
    res = "HalfVertDefense";
  }
  else if(  $("#rotationHidden").html() == "HalfVert" && $("#orientationHidden").html() == "0" && $("#fieldTypeHidden").html() == "Full"){
    res = "FullOffense";
  }
  else if(  $("#rotationHidden").html() == "HalfVert" && $("#orientationHidden").html() == "180" && $("#fieldTypeHidden").html() == "Full"){
    res =  "FullDefense";
  }
  else if(  $("#rotationHidden").html() == "HalfHoriz" && $("#orientationHidden").html() == "0" && $("#fieldTypeHidden").html() == "Half"){
    res =  "HalfHorizOffense";
  }
  else if(  $("#rotationHidden").html() == "HalfHoriz" && $("#orientationHidden").html() == "180" && $("#fieldTypeHidden").html() == "Half"){
    res =  "HalfHorizDefense";
  }

  return res;
}



function setLayout(targetLayout,changeTeams){


	var currentLayout = $("#courtImage").attr("src");
	var res;

	if(targetLayout == "HalfVertOffense" || targetLayout == "HalfVertDefense"){
		if(currentLayout.includes("Full")){
			res = currentLayout.replace("Full", "HalfVert");
		}
		else if(currentLayout.includes("HalfHoriz")){
			res = currentLayout.replace("HalfHoriz", "HalfVert");
		}
		else{
			res = currentLayout;
		}
		$("#fieldTypeHidden").html("Half");
		$("#rotationHidden").html("HalfVert");

		$("#courtImage").attr('width', '100%');
        $("#courtImage").attr('height', 'auto');
        $(".nav-item-rotation").css({"transform":"rotate(0deg)","transition":".4s linear"});
    }

	else if(targetLayout == "FullOffense" || targetLayout == "FullDefense"){
		if(currentLayout.includes("HalfVert")){
			res = currentLayout.replace("HalfVert", "Full");
		}
		else if(currentLayout.includes("HalfHoriz")){
			res = currentLayout.replace("HalfHoriz", "Full");
		}
		else{
			res = currentLayout;
		}
		$("#fieldTypeHidden").html("Full");
		$("#rotationHidden").html("HalfVert");

		// $("#courtImage").attr("width", "auto");

		var height = $('.app').height();
  		var width = $('.app').width();
  		var fullCourtWidth = 46.8*height/width;

		$("#courtImage").attr("height", "100%");
		$("#courtImage").attr("width", fullCourtWidth+"%");
		$(".court-center").attr("height", "100%");
		$(".nav-item-rotation").css({"transform":"rotate(0deg)","transition":".4s linear"});
	}

	else if(targetLayout == "HalfHorizOffense" || targetLayout == "HalfHorizDefense"){
		if(currentLayout.includes("Full")){
			res = currentLayout.replace("Full", "HalfHoriz");
		}
		else if(currentLayout.includes("HalfVert")){
			res = currentLayout.replace("HalfVert", "HalfHoriz");
		}
		else{
			res = currentLayout;
		}
		$("#fieldTypeHidden").html("Half");
		$("#rotationHidden").html("HalfHoriz");

		$("#courtImage").attr('width', '100%');
    	$("#courtImage").attr('height', 'auto')
    	$(".nav-item-rotation").css({"transform":"rotate(90deg)","transition":".4s linear"});
	}


	if(targetLayout.includes("Offense")){
		$("#orientationHidden").html("0");
		$("#courtImage").css("transform", "rotate(0deg)");
	}
	else if(targetLayout.includes("Defense")){
		$("#orientationHidden").html("180");
		$("#courtImage").css("transform", "rotate(180deg)");
	}

	$("#courtImage").attr("src", res);


	if( changeTeams==false ){
		redrawArrows()
	}
	else{
		resetCanvas()
		actualSnapshotIndex = 1
		processSnapshotDisplay()
	}


}



function redrawArrows(){
	//redrawing the arrows in the new layout
	try {
		arrowsManager(snapshots, actualSnapshotIndex)
	} catch (e) {
		l=0
	}
}



function processSnapshotDisplay(){

	//in case of recording, we show idx/idx
	if( $("#modeHidden").html()=="recording" &&  $("#playState").html() != "edit" ){
		var numberOfSnapshots = (actualPlay.snapshots.length)
		$("#snapshotDisplay").html("Recording " +  numberOfSnapshots + "/" + numberOfSnapshots)
	}

	else if(  $("#playState").html() == "playing" || $("#playState").html() == "opened" || $("#playState").html() == "paused" || $("#playState").html() == "stopped" ){
		var actualIndex = actualSnapshotIndex
		console.log("Modeee:  " + actualIndex )
		var numberOfSnapshots = (actualPlay.snapshots.length)-2
		if( actualIndex>numberOfSnapshots ){
			actualIndex=numberOfSnapshots
		}
		$("#snapshotDisplay").html("Playing " +  actualIndex + "/" + numberOfSnapshots)
	}

	else if( $("#playState").html() == "edit" ){
		var numberOfSnapshots = (auxPlay.snapshots.length)
		$("#snapshotDisplay").html("Editing " +  numberOfSnapshots + "/" + numberOfSnapshots)
	}

	else{
		$("#snapshotDisplay").html("Free-Draw")
	}



}


//
