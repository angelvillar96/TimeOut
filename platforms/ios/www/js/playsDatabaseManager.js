
plays_db = undefined
PLAYS_VERSION = 4
snapshots_db = undefined
SNAPSHOTS_VERSION = 4
moves_db = undefined
MOVES_VERSION = 4

$(document).ready(function() {

  console.log("PLAYS Table to be created");

  window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;

  /*#####################
  ##### PLAYS TABLE #####
  #####################*/
  plays_db = window.indexedDB.open("timeOut_plays", PLAYS_VERSION);
  plays_db.onsuccess = function(event) {
    console.log("IN PLAYS onsuccess");
    load_plays(event.target.result);
  }
  plays_db.onfailure = function(event) {
    console.log("IN PLAYS onerror");
  }
  plays_db.onupgradeneeded = function(event) {
    console.log("IN PLAYS onupgradeneeded")
    // Save the IDBDatabase interface
    results_plays_db = event.target.result;
    // Create an objectStore for this database
    createPlaysObjectStore(results_plays_db);
  };

  /*#########################
  ##### SNAPSHOTS TABLE #####
  #########################*/
  snapshots_db = window.indexedDB.open("timeOut_snapshots", SNAPSHOTS_VERSION);
  snapshots_db.onsuccess = function(event) {
    console.log("IN SNAPSHOTS onsuccess");
    load_snapshots(event.target.result);
  }
  snapshots_db.onfailure = function(event) {
    console.log("IN SNAPSHOTS onerror");
  }
  snapshots_db.onupgradeneeded = function(event) {
    console.log("IN SNAPSHOTS onupgradeneeded")
    // Save the IDBDatabase interface
    results_snapshots_db = event.target.result;
    // Create an objectStore for this database
    createSnapshotsObjectStore(results_snapshots_db);
  };

  /*#####################
  ##### MOVES TABLE #####
  #####################*/
  moves_db = window.indexedDB.open("timeOut_moves", MOVES_VERSION);
  moves_db.onsuccess = function(event) {
    console.log("IN MOVES onsuccess");
    load_moves(event.target.result);
  }
  moves_db.onfailure = function(event) {
    console.log("IN SNAPSHOTS onerror");
  }
  moves_db.onupgradeneeded = function(event) {
    console.log("IN SNAPSHOTS onupgradeneeded")
    // Save the IDBDatabase interface
    results_moves_db = event.target.result;
    // Create an objectStore for this database
    createMovesObjectStore(results_moves_db);
  };

});

function createPlaysObjectStore(db){
	console.log("IN createPlaysObjectStore");

  try {
    db.deleteObjectStore("Plays");
  } catch (e) {
    console.log("Error deleting PLAYS DB")
  }
  var store = db.createObjectStore("Plays", { keyPath: "id" });

  store.createIndex('playName', 'playName', { unique: false });
  store.createIndex('type', 'type', { unique: false });
  store.createIndex('favorite', 'favorite', { unique: false });
  store.createIndex('snapshots', 'snapshots', { unique: false });
  store.createIndex('offensiveStrategy', 'offensiveStrategy', { unique: false });
  store.createIndex('defensiveStrategy', 'defensiveStrategy', { unique: false });
}


function createSnapshotsObjectStore(db){
	console.log("IN createSnapshotsObjectStore");

  try {
    db.deleteObjectStore("Snapshots");
  } catch (e) {
    console.log("Error deleting SNAPSHOTS DB")
  }
  var store = db.createObjectStore("Snapshots", { keyPath: "snapshotIdx" });

  store.createIndex('snapshotNumber', 'snapshotNumber', { unique: false });
  store.createIndex('playName', 'playName', { unique: false });
  store.createIndex('playerWithBall', 'playerWithBall', { unique: false });
  store.createIndex('moves', 'moves', { unique: false });
}


function createMovesObjectStore(db){
	console.log("IN createMovesObjectStore");

  try {
    db.deleteObjectStore("Moves");
  } catch (e) {
    console.log("Error deleting MOVES DB")
  }
  var store = db.createObjectStore("Moves", { keyPath: "moveIdx" });

  store.createIndex('position', 'position', { unique: false });
  store.createIndex('playName', 'playName', { unique: false });
  store.createIndex('snapshotNumber', 'snapshotNumber', { unique: false });
  store.createIndex('positions', 'positions', { unique: false });
}





//
