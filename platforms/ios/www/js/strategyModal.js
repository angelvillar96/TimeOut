///////////////////////////////////////////////////////////////////////////
// STRATEGY MODAL
///////////////////////////////////////////////////////////////////////////

$(document).ready(function () {

	$('#offensiveStrategy').click(function () {
		loadValuesOffStrategyModal();
		$("#offensiveStrategyChoosePlayersAlert").hide();
	});

	$('#offensiveStrategyApply').click(function () {
		var offensiveTeam = game.offensiveTeam
		// Read currently selected strategy
		var goalStrategy = getPreselectedStrategy(game[game.offensiveTeam], "offensive");

		writeConsoleLog("APPLYPOS goalStrategy2 to apply: "+goalStrategy);

		// Read currently selected players
		var goalPositions = getPositionsForSystem(goalStrategy);
		for (var i = 0; i < goalPositions.length; i++) {
			var goalPlayerInPosition = $( "input[type=radio][name="+goalPositions[i]+"]:checked" ).val();
			if(goalPlayerInPosition != undefined){
				goalPlayerInPosition = goalPlayerInPosition.split("_")[1];
				writeConsoleLog("APPLYPOS goalPlayerInPosition to apply in "+goalPositions[i]+" is "+goalPlayerInPosition);
				game[game.offensiveTeam].courtPlayers[goalPositions[i]] = goalPlayerInPosition;
			}
		}

		// Apply changes
		resetCanvas()
		actualSnapshotIndex = 0
		$("#playState").html("noPlay")
		setState("FreeDraw");
		processSnapshotDisplay()

		$("."+offensiveTeam).remove();
		$("."+offensiveTeam+"Arrow").remove();

		setTeam(offensiveTeam, game[offensiveTeam].offensiveTeam, getCurrentLayout(), true);
		updateCourtPlayersOnDatabase(game[offensiveTeam]);
		writeConsoleLog("APPLYPOS game.offensiveTeam: "+ game.offensiveTeam+"    game.teamA.offensiveTeam: "+game.teamA.offensiveTeam+"   game.teamB.offensiveTeam: "+game.teamB.offensiveTeam);

	});


	$('.offStrategyRadio').click(function () {
		var goalStrategy = $(this).attr('id').slice(0,-5);
	   	writeConsoleLog("Radio Button Pressed: "+goalStrategy);

	   	populatePlayersListForStrategy(goalStrategy, game[game.offensiveTeam], "offensive");

	});

	$('#offensiveStrategyChoosePlayers').on("click","label", checkPlayersConflict);

	function checkPlayersConflict(){
		writeConsoleLog("Checking conflicts!");

		var radioButtonId = $(this).attr('id');
		var clickedPosition = radioButtonId.split("_")[0];
		var clickedPlayer = radioButtonId.split("_")[1].slice(0,-5);
		writeConsoleLog("CONFLICTS clickedPosition: "+clickedPosition+" clickedPlayer: "+clickedPlayer);
		if(clickedPosition.slice(0,1) == "O"){
			var team = game[game.offensiveTeam];
			var offDef = "offensive";
		}
		else{
			var team = game[getDefensiveTeam()];
			var offDef = "defensive";
		}
		writeConsoleLog("CONFLICTS team: "+team+" offDef: "+offDef);
		var preselectedStrategy = getPreselectedStrategy(team, offDef);
		var positions = getPositionsForSystem(preselectedStrategy);
		var currentlyPreselectedPlayers = [];

		currentlyPreselectedPlayers.push(clickedPlayer);

		for(i=0; i < positions.length; i++){
			if(positions[i] != clickedPosition){
				var goalPlayerInPosition = $( "input[type=radio][name="+positions[i]+"]:checked" ).val();
				if(goalPlayerInPosition == undefined){
					goalPlayerInPosition = team.courtPlayers[positions[i]];
				}
				else{
					goalPlayerInPosition = goalPlayerInPosition.split("_")[1];
				}
				// if((positions[i] != clickedPosition) && (clickedPlayer == goalPlayerInPosition)){
				// 	writeConsoleLog("Conflict found!")
				// 	break;
				// }
				if(currentlyPreselectedPlayers.includes(goalPlayerInPosition)){
					writeConsoleLog("CONFLICTS Conflict found!");
					$("#"+offDef+"StrategyChoosePlayersAlert").show();
					break;
				}
				else{
					$("#"+offDef+"StrategyChoosePlayersAlert").hide();
				}
				currentlyPreselectedPlayers.push(team.courtPlayers[positions[i]]);
			}
		}
	}

	function loadValuesOffStrategyModal(){
		$('.offStrategyRadio').removeClass('active');

		currentOffensiveStrategy = game[game.offensiveTeam].offensiveTeam;
		writeConsoleLog("currentOffensiveStrategy: "+currentOffensiveStrategy);
		$('#'+currentOffensiveStrategy+'Radio').addClass('active');
//		$("#editPlayModalSubtitle").html("Choose Strategy for "+game[game.offensiveTeam].teamName);
		populatePlayersListForStrategy(currentOffensiveStrategy, game[game.offensiveTeam], "offensive");
	}


	function populatePlayersListForStrategy(goalStrategy, team, offDef){

		writeConsoleLog("Team "+team+" goalStrategy: "+goalStrategy);
		$("#"+offDef+"StrategyChoosePlayersTitle").html("2. Choose Players for "+goalStrategy);
		$("#"+offDef+"StrategyChoosePlayers").html("");
		var goalPositions = getPositionsForSystem(goalStrategy);
		//var offensiveTeam = game.offensiveTeam;
		for (var i = 0; i < goalPositions.length; i++) {

			var playersInPosition = getPlayersOfTeamForPosition(team, goalPositions[i], true);
			writeConsoleLog("Players for position "+goalPositions[i]+" are "+playersInPosition);

			var htmlStr = "";
			htmlStr += "<div class='row justify-content-md-center mt-3 md-3'>";
			htmlStr += "<div class='col-1 align-self-center'><div>"+goalPositions[i]+"</div></div><div class='col-8'><div class='row btn-group btn-group-toggle container' data-toggle='buttons'>";

			for(k=0; k<playersInPosition.length; k++){
				htmlStr += "<label class='btn btn-light col-4 playerRadioButtonStrategyModal' id='"+goalPositions[i]+"_"+playersInPosition[k]+"Radio'><input type='radio' name='"+goalPositions[i]+"' value='"+goalPositions[i]+"_"+playersInPosition[k]+"'autocomplete='off'><div class='CropLongTexts'>"+getPlayerValuePerID(playersInPosition[k], "number")+"-"+getPlayerValuePerID(playersInPosition[k], "playerNameShort")+"</div></label>";
			}

			// htmlStr += "<label class='btn btn-info col-1 playerRadioButtonStrategyModal' id='"+goalPositions[i]+"AddRadio'><input type='radio' name='"+goalPositions[i]+"' autocomplete='off'><div>+</div></label>";
			htmlStr += "</div></div><div class='dropdown-toggle col-2 align-self-center' href='#'' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>add</div><div class='dropdown-menu' id='addPlayersDropdown' aria-labelledby='navbarDropdown'>";

			var playersNotInPosition = getPlayersOfTeamForPosition(team, goalPositions[i], false);
			for(k=0; k<playersNotInPosition.length; k++){
				htmlStr += "<button class='dropdown-item dropdown-add-player' href='#' id='AddPlayer_"+getPlayerValuePerID(playersNotInPosition[k], "playerID")+"_"+goalPositions[i]+"'>"+getPlayerValuePerID(playersNotInPosition[k], "number")+"-"+getPlayerValuePerID(playersNotInPosition[k], "playerNameShort")+"</button>";
			}
			htmlStr += "</div>";
			htmlStr += "<div class='col-1 align-self-center'><input type='checkbox' class='form-check-input' id='checkboxOffDef"+goalPositions[i]+"'></div>";
			htmlStr += "</div>";
			$("#"+offDef+"StrategyChoosePlayers").append(htmlStr);

			currentPlayerInPosition = team.courtPlayers[goalPositions[i]];
			writeConsoleLog("Current players for position "+goalPositions[i]+" is "+currentPlayerInPosition);
			if(currentPlayerInPosition != undefined){
				$('#'+goalPositions[i]+'_'+currentPlayerInPosition+'Radio').addClass('active');
			}
		}

		//    checkPlayersConflict();

	}

	function getPreselectedStrategy(team, offDef){
		var offensiveTeam = game.offensiveTeam;

		goalStrategy = $( "input[type=radio][name=options]:checked" ).val();
		writeConsoleLog("APPLYPOS goalStrategy1 to apply: "+goalStrategy);

		if(goalStrategy == undefined){
			goalStrategy = team[offDef+"Team"];
		}
		else{
			team[offDef+"Team"] = goalStrategy;
		}

		return goalStrategy;
	}


	function getPlayersOfTeamForPosition(team, position, boolean){
		// Boolean = TRUE: Returns all players for the given position.
		// Boolean = FALSE: Returns all players which don't have the given position
		var playersInPosition = [];
		for(var j=0; j<team.players.length; j++){
			if(boolean){
				if(team.players[j].validPositions.includes(position)){
					playersInPosition.push(team.players[j].playerID)
				}
			}
			else{
				if(!team.players[j].validPositions.includes(position)){
					playersInPosition.push(team.players[j].playerID)
				}
			}
		}
		return playersInPosition;
	}

	$(document).on("click",".dropdown-add-player", function () {
		var playerID = $(this).attr('id').split("_")[1];
		var position = $(this).attr('id').split("_")[2];

		writeConsoleLog("Player added: "+playerID+" to position: "+position);

		// Add position to playerID
		editPlayerValidPosition(playerID, false, position);
		// Load all values
	   	loadValuesOffStrategyModal();
	   	loadValuesDefStrategyModal();
	});






	// Open Defensive Strategy Modal
	$('#defensiveStrategy').click(function () {
		loadValuesDefStrategyModal();
		$("#defensiveStrategyChoosePlayersAlert").hide();
	});


	function loadValuesDefStrategyModal(){
		$('.defStrategyRadio').removeClass('active');

		currentDefensiveStrategy = game[getDefensiveTeam()].defensiveTeam;
		writeConsoleLog("currentDefensiveStrategy: "+currentDefensiveStrategy);
		$('#'+currentDefensiveStrategy+'Radio').addClass('active');
	//	$("#editPlayModalSubtitle").html("1. Choose Strategy for "+game[getDefensiveTeam()].teamName);
		populatePlayersListForStrategy(currentDefensiveStrategy, game[getDefensiveTeam()], "defensive");
	}

	$('#defensiveStrategyApply').click(function () {
		// Read currently selected strategy
		var goalStrategy = getPreselectedStrategy(game[getDefensiveTeam()], "defensive");

		writeConsoleLog("APPLYPOS goalStrategy2 to apply: "+goalStrategy);

		// Read currently selected players
		var goalPositions = getPositionsForSystem(goalStrategy);
		for (var i = 0; i < goalPositions.length; i++) {
			var goalPlayerInPosition = $( "input[type=radio][name="+goalPositions[i]+"]:checked" ).val();
			if(goalPlayerInPosition != undefined){
				goalPlayerInPosition = goalPlayerInPosition.split("_")[1];
				writeConsoleLog("APPLYPOS goalPlayerInPosition to apply in "+goalPositions[i]+" is "+goalPlayerInPosition);
				game[getDefensiveTeam()].courtPlayers[goalPositions[i]] = goalPlayerInPosition;
			}
		}

		// Apply changes
		resetCanvas()
		actualSnapshotIndex = 0
		$("#playState").html("noPlay")
		setState("FreeDraw");
		processSnapshotDisplay()

		$("."+defensiveTeam).remove();
		$("."+defensiveTeam+"Arrow").remove();

		setTeam(getDefensiveTeam(), game[getDefensiveTeam()].defensiveTeam, getCurrentLayout(), true);
		updateCourtPlayersOnDatabase(game[getDefensiveTeam()]);
		writeConsoleLog("APPLYPOS game.offensiveTeam: "+ getDefensiveTeam()+"    game.teamA.offensiveTeam: "+game.teamA.defensiveTeam+"   game.teamB.offensiveTeam: "+game.teamB.defensiveTeam);

	});


	$('.defStrategyRadio').click(function () {
		var goalStrategy = $(this).attr('id').slice(0,-5);
	   	writeConsoleLog("Radio Button Pressed: "+goalStrategy);

	   	populatePlayersListForStrategy(goalStrategy, game[getDefensiveTeam()], "defensive");

	});

	$('#defensiveStrategyChoosePlayers').on("click","label", checkPlayersConflict);
});
