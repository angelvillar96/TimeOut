

// $(document).ready(function($) {
$(document).ready(function() {
  
  writeConsoleLog("Teams Table to be created");
  var db = openDatabase('timeOutDB', '1.0', 'Database with Game, Team and Player data', 10 * 1024 * 1024);


  db.transaction(function (tx) {

     // var ret1 = tx.executeSql("DROP TABLE IF EXISTS Teams")
     var ret2 = tx.executeSql("CREATE TABLE IF NOT EXISTS Teams (teamID varchar(255) PRIMARY KEY, teamName varchar(255), filledColorA varchar(255), lineColorA varchar(255), textColorA varchar(255), filledColorB varchar(255), lineColorB varchar(255), textColorB varchar(255), offensiveTeam varchar(255), defensiveTeam varchar(255), courtPlayers varchar(255), image_path varchar(1023))");
     tx.executeSql("INSERT INTO Teams (teamID, teamName, filledColorA, lineColorA, textColorA, filledColorB, lineColorB, textColorB, offensiveTeam, defensiveTeam, courtPlayers, image_path) VALUES ('0000', 'Team Red', '#DDCB77', '#D41A21', 'red', 'blue', 'purple', 'blue', 'offensive33', 'defensive60', '10001,10002,10003,10004,10005,10006,10007,10008,10009,10010,10011,10012,10013,10014,10015,10016,10017,10018,10019,10020,10021', '')")
     tx.executeSql("INSERT INTO Teams (teamID, teamName, filledColorA, lineColorA, textColorA, filledColorB, lineColorB, textColorB, offensiveTeam, defensiveTeam, courtPlayers, image_path) VALUES ('0001', 'Team Blue', '#ffffff', '#0000CC', 'blue', '#ffffff', '#000000', '#ffffff', 'offensive33', 'defensive60', '10031,10032,10033,10034,10035,10036,10037,10038,10039,10040,10041,10042,10043,10044,10045,10046,10047,10048,10049,10050,10051', '')")
   });

});


// function createTeamsDb(db){

// 	console.log("Teams Table to be created");

// 	//window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
// 	request = window.indexedDB.open("timeOutDB", 11);

// 	//request = window.indexedDB.open("timeOutDB");

// 	console.log(request);

// 	request.onsuccess = function(event) {
// 		console.log("Teams ObjectStore was loaded successfully")
// 		console.log(event.target.result)
// 		load_teams(event.target.result)
// 	}


// 	request.onupgradeneeded = function(event) {

// 		console.log("onupgradeneeded was called")
// 		// Save the IDBDatabase interface
// 		games_db = event.target.result;

// 		// Create an objectStore for this database
// 		console.log(games_db);
// 		games_db.deleteObjectStore("Teams");
// 		var store = games_db.createObjectStore("Teams", { keyPath: "teamID" });
// 		console.log("\nThe Teams objectStore has been created\n");

// 		// store.createIndex('gameID', 'gameID', { unique: false });
// 		store.createIndex('teamName', 'teamName', { unique: false });
// 		store.createIndex('filledColorA', 'filledColorA', { unique: false });
// 		store.createIndex('lineColorA', 'lineColorA', { unique: false });
// 		store.createIndex('textColorA', 'textColorA', { unique: false });
// 		store.createIndex('filledColorB', 'filledColorB', { unique: false });
// 		store.createIndex('lineColorB', 'lineColorB', { unique: false });
// 		store.createIndex('textColorB', 'textColorB', { unique: false });
// 		store.createIndex('offensiveTeam', 'offensiveTeam', { unique: false });
// 		store.createIndex('defensiveTeam', 'defensiveTeam', { unique: false });
// 		store.createIndex('courtPlayers', 'courtPlayers', { unique: false });
// 		store.createIndex('image_path', 'image_path', { unique: false });

// 		console.log(games_db);

// 		const current_games = [
// 		  	{teamID: '0000', teamName: 'Team Red', filledColorA: '#DDCB77', lineColorA: '#D41A21', textColorA: 'red', filledColorB: 'blue', lineColorB: 'purple', textColorB:'blue' , offensiveTeam: 'offensive33', defensiveTeam: 'defensive60', courtPlayers: '10001,10002,10003,10004,10005,10006,10007,10008,10009,10010,10011,10012,10013,10014,10015,10016,10017,10018,10019,10020,10021', image_path: ''},
// 			{teamID: '0001', teamName: 'Team Blue', filledColorA: '#ffffff', lineColorA: '#0000CC', textColorA: 'blue', filledColorB: '#ffffff', lineColorB: '#000000', textColorB: '#ffffff', offensiveTeam: 'offensive33', defensiveTeam: 'defensive60', courtPlayers: '10031,10032,10033,10034,10035,10036,10037,10038,10039,10040,10041,10042,10043,10044,10045,10046,10047,10048,10049,10050,10051', image_path: ''}
// 		];

// 		store.transaction.oncomplete = function(event) {
// 		  console.log("transaction.oncomplete")
// 		  console.log(games_db)
// 		  var gamesObjectStore = games_db.transaction("Games", "readwrite").objectStore("Teams");
// 		  current_games.forEach(function(game) {
// 		   gamesObjectStore.add(game);
// 		  });
// 		  console.log(games_db)
// 		};
// 	};

// 	load_teams(db);
// }
