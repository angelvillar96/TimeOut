
games_db = undefined
GAMES_VERSION = 2
teams_db = undefined
TEAMS_VERSION = 1
players_db = undefined
PLAYERS_VERSION = 1

$(document).ready(function() {
// $(document).ready(function($) {

  console.log("Games Table to be created");

  window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;

  /*#####################
  ##### GAMES TABLE #####
  #####################*/
  games_db = window.indexedDB.open("timeOut_games", GAMES_VERSION);
  games_db.onsuccess = function(event) {
    console.log("IN GAMES onsuccess");
    load_games(event.target.result);
  }
  games_db.onfailure = function(event) {
    console.log("IN GAMES onerror");
  }
  games_db.onupgradeneeded = function(event) {
    console.log("IN GAMES onupgradeneeded")
    // Save the IDBDatabase interface
    results_games_db = event.target.result;
    // Create an objectStore for this database
    createGamesObjectStore(results_games_db);
  };

  /*#####################
  ##### TEAMS TABLE #####
  #####################*/
  teams_db = window.indexedDB.open("timeOut_teams", TEAMS_VERSION);
  teams_db.onsuccess = function(event) {
    console.log("IN TEAMS onsuccess");
    load_teams(event.target.result);
  }
  teams_db.onfailure = function(event) {
    console.log("IN TEAMS onerror");
  }
  teams_db.onupgradeneeded = function(event) {
    console.log("IN TEAMS onupgradeneeded")
    // Save the IDBDatabase interface
    results_teams_db = event.target.result;
    // Create an objectStore for this database
    createTeamsObjectStore(results_teams_db);
  };

  /*#######################
  ##### PLAYERS TABLE #####
  #######################*/
  players_db = window.indexedDB.open("timeOut_players", PLAYERS_VERSION);
  players_db.onsuccess = function(event) {
    console.log("IN PLAYERS onsuccess");
    load_players(event.target.result);
  }
  players_db.onfailure = function(event) {
    console.log("IN PLAYERS onerror");
  }
  players_db.onupgradeneeded = function(event) {
    console.log("IN PLAYERS onupgradeneeded")
    // Save the IDBDatabase interface
    results_players_db = event.target.result;
    // Create an objectStore for this database
    createPlayersObjectStore(results_players_db);
  };

});


function createGamesObjectStore(db){

	console.log("IN createGamesObjectStore");

  try {
    db.deleteObjectStore("Games");
  } catch (e) {
    console.log("Error deleting GAMES DB")
  }
  var store = db.createObjectStore("Games", { keyPath: "gameID" });

  store.createIndex('teamA', 'teamA', { unique: false });
  store.createIndex('teamB', 'teamB', { unique: false });

  console.log(db);

  const current_games = [
    {gameID: "0000", teamA: "0000", teamB: "0001"},
    {gameID: "0001", teamA: "0001", teamB: "0000"}
  ];

  store.transaction.oncomplete = function(event) {
    console.log("transaction.oncomplete")
    console.log(db)
    var gamesObjectStore = db.transaction("Games", "readwrite").objectStore("Games");
    current_games.forEach(function(game) {
     gamesObjectStore.add(game);
    });
  };
}


function createTeamsObjectStore(db){

	console.log("IN createTeamsObjectStore");
  console.log(db);

  try {
    db.deleteObjectStore("Teams");
  } catch (e) {
    console.log("Error deleting TEAMS DB")
  }
	var store = db.createObjectStore("Teams", { keyPath: "teamID" });

	console.log("\nThe Teams objectStore has been created\n");

	store.createIndex('teamName', 'teamName', { unique: false });
	store.createIndex('filledColorA', 'filledColorA', { unique: false });
	store.createIndex('lineColorA', 'lineColorA', { unique: false });
	store.createIndex('textColorA', 'textColorA', { unique: false });
	store.createIndex('filledColorB', 'filledColorB', { unique: false });
	store.createIndex('lineColorB', 'lineColorB', { unique: false });
	store.createIndex('textColorB', 'textColorB', { unique: false });
	store.createIndex('offensiveTeam', 'offensiveTeam', { unique: false });
	store.createIndex('defensiveTeam', 'defensiveTeam', { unique: false });
	store.createIndex('courtPlayers', 'courtPlayers', { unique: false });
	store.createIndex('image_path', 'image_path', { unique: false });

	const current_teams = [
	  	{teamID: '0000', teamName: 'Team Red', filledColorA: '#DDCB77', lineColorA: '#D41A21', textColorA: 'red',
       filledColorB: 'blue', lineColorB: 'purple', textColorB:'blue' , offensiveTeam: 'offensive33', defensiveTeam: 'defensive60',
       courtPlayers: '10001,10002,10003,10004,10005,10006,10007,10008,10009,10010,10011,10012,10013,10014,10015,10016,10017,10018,10019,10020,10021',
       image_path: ''},
		  {teamID: '0001', teamName: 'Team Blue', filledColorA: '#ffffff', lineColorA: '#0000CC', textColorA: 'blue',
       filledColorB: '#ffffff', lineColorB: '#000000', textColorB: '#ffffff', offensiveTeam: 'offensive33', defensiveTeam: 'defensive60',
       courtPlayers: '10031,10032,10033,10034,10035,10036,10037,10038,10039,10040,10041,10042,10043,10044,10045,10046,10047,10048,10049,10050,10051',
       image_path: ''}
	];

	store.transaction.oncomplete = function(event) {
	  console.log("transaction.oncomplete")
	  var teamsObjectStore = db.transaction("Teams", "readwrite").objectStore("Teams");
	  current_teams.forEach(function(team) {
	   teamsObjectStore.add(team);
	  });
	};

}


function createPlayersObjectStore(db){

  console.log("IN createPlayerObjectStore");
  console.log(db);

  try {
    db.deleteObjectStore("Players");
  } catch (e) {
    console.log("Error deleting PLAYERS DB")
  }
	var store = db.createObjectStore("Players", { keyPath: "playerID" });

	console.log("\nThe Players objectStore has been created\n");

	store.createIndex('team', 'team', { unique: false });
	store.createIndex('number', 'number', { unique: false });
	store.createIndex('playerNameLong', 'playerNameLong', { unique: false });
	store.createIndex('playerNameShort', 'playerNameShort', { unique: false });
	store.createIndex('validPositions', 'validPositions', { unique: false });
	store.createIndex('fieldPosX', 'fieldPosX', { unique: false });
	store.createIndex('fieldPosY', 'fieldPosY', { unique: false });
	store.createIndex('displayPosX', 'displayPosX', { unique: false });
	store.createIndex('displayPosY', 'displayPosY', { unique: false });
	store.createIndex('starter', 'starter', { unique: false });
	store.createIndex('image_path', 'image_path', { unique: false });

  store.transaction.oncomplete = function(event) {
	  console.log("transaction.oncomplete")
	  var playersObjectStore = db.transaction("Players", "readwrite").objectStore("Players");
	  CURRENT_PLAYERS.forEach(function(player) {
	   playersObjectStore.add(player);
	  });
	};


}



//
