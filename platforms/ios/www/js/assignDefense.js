
function assignPairs(callback){

	writeConsoleLog("ASSIGNPAIRS Start");

	var distancesMatrix = calculateDistanceMatrix();
	game.offDefPairsArray = calculateOffDefPairs(distancesMatrix);
	game.offDefPairsArray = [undefined, 6, 5, 3, 2, 1, 4];
	writeConsoleLog("ASSIGNPAIRS offDefPairsArray "+game.offDefPairsArray);
	//$(".line-offDef").hide();
	callback(game.offDefPairsArray);
}



function findPair4Pos(offDefPos){
	var offPositions = getPositionsForSystem(game[game.offensiveTeam].offensiveTeam);
	var defPositions = getPositionsForSystem(game[getDefensiveTeam()].defensiveTeam);

	if(offDefPos[0]=="O"){
		var defPos = undefined;
		for(var i=0; i<offPositions.length; i++){
			if(offPositions[i]==offDefPos){
				defPos = defPositions[game.offDefPairsArray[i]];
				break;
				// writeConsoleLog("ASSIGNPAIRS findPair4OffPos offDefPos: "+offDefPos+" i: "+i+" defPos "+defPos);
			}
		}
		writeConsoleLog("ASSIGNPAIRS findPair4Pos offDefPos: "+offDefPos+" defPos: "+defPos);

		return defPos;
	}
	else if(offDefPos[0]=="D"){
		var offPos = undefined;
		writeConsoleLog("ASSIGNPAIRS findPair4Pos Entering D");

		for(var i=0; i<defPositions.length; i++){
			if(defPositions[i]==offDefPos){
				writeConsoleLog("ASSIGNPAIRS findPair4Pos i: "+i);
				for(var k=0; k<game.offDefPairsArray.length; k++){
					if(game.offDefPairsArray[k] == i){
						writeConsoleLog("ASSIGNPAIRS findPair4Pos k: "+k);
						offPos = offPositions[k];
						break;
					}
				}
				// writeConsoleLog("ASSIGNPAIRS findPair4OffPos offDefPos: "+offDefPos+" i: "+i+" defPos "+defPos);
			}
		}
		writeConsoleLog("ASSIGNPAIRS findPair4Pos offDefPos: "+offDefPos+" offPos: "+offPos);

		return offPos;

	}
}



function drawOffDefLines(inputArray){

	var offset = 3;
	var offsetDisplay = getDisplayPlayerOffset(getCurrentLayout());
	// var c = document.getElementById("courtCanvas");
 //    var ctx = c.getContext("2d");

    writeConsoleLog("ASSIGNPAIRS inputArray "+inputArray);



    var offPositions = getPositionsForSystem(game[game.offensiveTeam].offensiveTeam);
    var defPositions = getPositionsForSystem(game[getDefensiveTeam()].defensiveTeam);

	for(var i=0; i<inputArray.length; i++){

		offPos = offPositions[i];
		defPos = defPositions[inputArray[i]];

		var initArrowFieldPosLeft = getPlayerValuePerID(game[game.offensiveTeam].courtPlayers[offPos], "fieldPosX");//+offset;
		var initArrowFieldPosTop = getPlayerValuePerID(game[game.offensiveTeam].courtPlayers[offPos], "fieldPosY");//+offset;
		var targetArrowFieldPosLeft = getPlayerValuePerID(game[getDefensiveTeam()].courtPlayers[defPos], "fieldPosX");//+offset;
		var targetArrowFieldPosTop = getPlayerValuePerID(game[getDefensiveTeam()].courtPlayers[defPos], "fieldPosY");//+offset;

		var initArrowPos = fieldPos2displayPos( initArrowFieldPosLeft, initArrowFieldPosTop, getCurrentLayout());
		var targetArrowPos = fieldPos2displayPos( targetArrowFieldPosLeft, targetArrowFieldPosTop, getCurrentLayout());

		writeConsoleLog("ASSIGNPAIRS Transformations for: "+game[game.offensiveTeam].courtPlayers[offPos]+" initArrowFieldPosLeft "+initArrowFieldPosLeft+" initArrowFieldPosTop "+initArrowFieldPosTop+" initArrowPosLeft: "+initArrowPos.left+" initArrowPosTop "+initArrowPos.top);

		writeConsoleLog("ASSIGNPAIRS DrawLine for: "+game[game.offensiveTeam].courtPlayers[offPos]+" initArrowPosLeft: "+initArrowPos.left+" initArrowPosTop "+initArrowPos.top);

		createLine(offPos, parseInt(initArrowPos.left)+offsetDisplay, parseInt(initArrowPos.top)+offsetDisplay, parseInt(targetArrowPos.left)+offsetDisplay, parseInt(targetArrowPos.top)+offsetDisplay);

		// ctx.beginPath()
  //   	ctx.moveTo(parseInt(initArrowPos.left), parseInt(initArrowPos.top));
  //   	ctx.lineTo(parseInt(targetArrowPos.left), parseInt(targetArrowPos.top));
  //   	ctx.lineWidth = 3;
  //   	ctx.strokeStyle = "yellow";
  //   	ctx.stroke();
	}

}



function createLine(offPos, x1,y1, x2,y2){
  var length = Math.sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
  var angle  = Math.atan2(y2 - y1, x2 - x1) * 180 / Math.PI;
  var transform = 'rotate('+angle+'deg)';

  writeConsoleLog("ASSIGNPAIRS createLine for x1,y1, x2,y2: "+x1+" "+y1+" "+x2+" "+y2+" length: "+length+" angle "+angle);


    // var line = $('<div>')
    //   .appendTo('.court')
    //   .addClass('line-offDef')
    //   .css({
    //     'position': 'absolute',
    //     'transform': transform
    //   })
    //   .width(length)
    //   .offset({left: x1, top: y1});

  // $(".court").append("<div class='line-offDef' id='line"+parseInt(x1)+"'><div>Hello</div></div>");
  // $("#line"+parseInt(x1)).css({"border-color":"yellow", "color": "yellow"});
  // $("#line"+parseInt(x1)).css({"left":left, "top":top, "width":length});

//	$("#courtCanvasOutOfSight").append("<div class='arrowsOutOfSightTeamA' id='line"+parseInt(x1)+"'><img class='ArrowOutOfSightImg' src='img/arrow-red.svg' alt='Arrow'></div>");
	if($("#line"+offPos)){
		$("#line"+offPos).remove();
		$("#courtCanvasOutOfSight").append("<div class='line-offDef' id='line"+offPos+"'></div>");
		$("#line"+offPos).css({'top': y1+"px", 'left': x1+"px", "position":"absolute", "background-color":"black", "height":"5px", "width":length+"px", "transform":transform, "transformOrigin":"0 0"});
  	}
  //return line;
}



function updateLine(playerID, initArrowDisplayPosLeft, initArrowDisplayPosTop){

	var offPos = undefined;
	var offPositions = getPositionsForSystem(game[game.offensiveTeam].offensiveTeam);
	var offsetDisplay = getDisplayPlayerOffset(getCurrentLayout());
	var offset = 3; // This offset is in field position --> To do: Change it to getDisplayPlayerOffset() and then convert it to field %.

	for(i=0; i<offPositions.length; i++){
		if(game[game.offensiveTeam].courtPlayers[offPositions[i]]==playerID){
			offPos = offPositions[i];
		}
	}

	if(offPos != undefined){ // If playerId has been found to be playing as offensive player
		var defPos = findPair4Pos(offPos);

		var targetArrowFieldPosLeft = getPlayerValuePerID(game[getDefensiveTeam()].courtPlayers[defPos], "fieldPosX");// +offset;
		var targetArrowFieldPosTop = getPlayerValuePerID(game[getDefensiveTeam()].courtPlayers[defPos], "fieldPosY");// +offset;

		var targetArrowPos = fieldPos2displayPos( targetArrowFieldPosLeft, targetArrowFieldPosTop, getCurrentLayout());

		writeConsoleLog("ASSIGNPAIRS updateLine for player:"+playerID+" with Offpos: "+offPos+" matching with defPos: "+defPos+" initArrowDisplayPosLeft "+initArrowDisplayPosLeft+" initArrowDisplayPosTop "+initArrowDisplayPosTop+" targetArrowPos.left: "+targetArrowPos.left+" targetArrowPos.top: "+targetArrowPos.top);

		createLine(offPos, parseInt(initArrowDisplayPosLeft)+offsetDisplay, parseInt(initArrowDisplayPosTop)+offsetDisplay, parseInt(targetArrowPos.left)+offsetDisplay, parseInt(targetArrowPos.top)+offsetDisplay);
	}
	else{ // Otherwise it is a defensive Player
		var defPos = undefined;
		var defPositions = getPositionsForSystem(game[getDefensiveTeam()].defensiveTeam);

		for(i=0; i<defPositions.length; i++){
			if(game[getDefensiveTeam()].courtPlayers[defPositions[i]]==playerID){
				defPos = defPositions[i];
			}
		}

		var offPos = findPair4Pos(defPos);
		var targetArrowFieldPosLeft = getPlayerValuePerID(game[game.offensiveTeam].courtPlayers[offPos], "fieldPosX");//+offset;
		var targetArrowFieldPosTop = getPlayerValuePerID(game[game.offensiveTeam].courtPlayers[offPos], "fieldPosY");//+offset;

		var targetArrowPos = fieldPos2displayPos( targetArrowFieldPosLeft, targetArrowFieldPosTop, getCurrentLayout());

		writeConsoleLog("ASSIGNPAIRS updateLine for 'defensive' player:"+playerID+" with defPos: "+defPos+" matching with Offpos: "+offPos+" initArrowDisplayPosLeft "+initArrowDisplayPosLeft+" initArrowDisplayPosTop "+initArrowDisplayPosTop+" targetArrowPos.left: "+targetArrowPos.left+" targetArrowPos.top: "+targetArrowPos.top);

		// CAUTION! Here a workaround!! Init and Target pos not being used right!!!
		createLine(offPos, parseInt(targetArrowPos.left)+offsetDisplay, parseInt(targetArrowPos.top)+offsetDisplay, parseInt(initArrowDisplayPosLeft)+offsetDisplay, parseInt(initArrowDisplayPosTop)+offsetDisplay);

	}
}



// Function that returns the player offset in px
function getDisplayPlayerOffset(type){
	height = $('.app').height();

	if( type=="HalfVertOffense" ){
	    var playersHeight = height*0.06;

	}else if( type=="HalfVertDefense" ){
	    var playersHeight = height*0.06;

	}else if( type=="HalfHorizOffense" ){
	    var playersHeight = height*0.065;

	}else if( type=="HalfHorizDefense" ){
	    var playersHeight = height*0.065;

	}else if( type=="FullOffense" ){
		var playersHeight = height*0.055;

	}else if( type=="FullDefense" ){
	    var playersHeight = height*0.055;
	}

	var offset = playersHeight/2;

	writeConsoleLog("ASSIGNPAIRS Offset: "+offset+" Type: "+type+" height of app: "+height);

	return offset;
}



function deleteOffDefPairsLines(){
	writeConsoleLog("deleteOffDefPairsLines called");
	$(".line-offDef").remove();
}


//Function which returns a matrix containing the distances between defensive and offensive players
function calculateDistanceMatrix(){


	// Get offensive and defensive players and delete OGK and DGK from the array
	var offPositions = getPositionsForSystem(game[game.offensiveTeam].offensiveTeam);
	for( var i = 0; i < offPositions.length; i++){
	   if ( offPositions[i] === "OGK") {
	     offPositions.splice(i, 1);
	     i--;
	   }
	}

	var defPositions = getPositionsForSystem(game[getDefensiveTeam()].defensiveTeam)
	for( var i = 0; i < defPositions.length; i++){
	   if ( defPositions[i] === "OGK") {
	     defPositions.splice(i, 1);
	     i--;
	   }
	}

	// Create empty matrix with as many rows as offPositions and as many columns as defPositions
	var distancesMatrix = [];
	for(var i=0; i<offPositions.length; i++) {
    	distancesMatrix[i] = new Array(defPositions.length);
	}

	// Populate the matrix with distances
	for(var i=0; i<offPositions.length; i++) {
		for(var j=0; j<defPositions.length; j++){

			var offPosX = parseFloat(getPlayerValuePerID(game[game.offensiveTeam].courtPlayers[offPositions[i]], "fieldPosX"));
			var offPosY = parseFloat(getPlayerValuePerID(game[game.offensiveTeam].courtPlayers[offPositions[i]], "fieldPosY"));
			var defPosX = parseFloat(getPlayerValuePerID(game[getDefensiveTeam()].courtPlayers[defPositions[j]], "fieldPosX"));
			var defPosY = parseFloat(getPlayerValuePerID(game[getDefensiveTeam()].courtPlayers[defPositions[j]], "fieldPosY"));

			var distanceValue = Math.abs(Math.sqrt(Math.pow(offPosX-defPosX, 2)+Math.pow(offPosY-defPosY, 2)));
			// writeConsoleLog("calculateDistanceMatrix distanceValue: "+distanceValue+" offPosX "+offPosX+" defPosX "+defPosX+" offPosY "+offPosY+" defPosY "+defPosY)
			distancesMatrix[i][j] = distanceValue;
		}
	}

	writeConsoleLog("calculateDistanceMatrix distancesMatrix: "+distancesMatrix+" with minimum value: "+distancesMatrix[0].length);
	return distancesMatrix;


}

function calculateOffDefPairs(distancesMatrix){

	// var pairIndex = new Array(7);
	// pairIndex[0]=undefined;
	// for(var i=0; i<6; i++){
	// 	tempVector = distancesMatrix[i+1];
	// 	var minValue = 100;
	// 	for(var j=0; j<tempVector.length; j++){
	// 		if(tempVector[j]<minValue){
	// 			minValue = tempVector[j];
	// 			pairIndex[i+1]=j;
	// 		}
	// 	}
	// 	writeConsoleLog("tempVector: "+tempVector+" minValue: "+minValue+" pairIndex: "+pairIndex);
	// 	//pairIndex[i+1]=tempVector.indexOf(Math.min(...tempVector));
	// }
	// writeConsoleLog("calculateDistanceMatrix pairIndex"+pairIndex);
	// return pairIndex;
}
