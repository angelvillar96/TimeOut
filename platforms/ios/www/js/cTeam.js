class Team{

    constructor(teamID, teamName, image_path, filledColorA, lineColorA, textColorA, filledColorB, lineColorB,
                textColorB, offensiveTeam, defensiveTeam, players, courtPlayers){
        this.teamID = teamID;
        this.teamName = teamName;
        this.filledColorA = filledColorA;
        this.lineColorA = lineColorA;
        this.textColorA = textColorA;
        this.filledColorB = filledColorB;
        this.lineColorB = lineColorB;
        this.textColorB = textColorB;
        this.offensiveTeam = offensiveTeam;
        this.defensiveTeam = defensiveTeam;
        this.players = players;
        this.courtPlayers = courtPlayers;

        var new_img_path

        // processing image path
        if( image_path==undefined || image_path.length==0 ){
          var id_reshaped = ("0000" + teamID).slice(-4)
          new_img_path = "img/teams/"+ id_reshaped + ".png"
        }else{
          new_img_path = image_path
        }
        this.image_path = new_img_path

    }

    setTeamPlayers( players ){
      this.players = players;
    }

    deletePlayer(){
    }

}



///////////////////////////////////////////////////////
//method that loads the data onto the EDIT PLAYER modal
function getFreeID(team){

  var largestID = 0
  var currentID = 0

  // obtaining the largest ID so far
  for(var i=0; i<team.players.length;i++){
    currentID = team.players[i].playerID
    currentID = parseInt(currentID)
    if( largestID<currentID){
      largestID = currentID
    }
  }

  // obtaining new id and converting to correct format
  var nextID = largestID+1
  nextID = "000000000" + nextID
  nextID = nextID.slice(-5)

  return nextID

}




//
