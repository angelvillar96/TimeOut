/*##############################################################
File that contains the necessary functions to manage the tactics
##############################################################*/

allPlays = [];
actualPlay = [];
actualSnapshot = [];
actualMoves = [];
actualSnapshotIndex = 1;
snapshotNumber = 0;

auxPlay = [];
auxSnapshots = [];
auxMoves = [];

lastSnapshotUndone = false;

$(document).ready(function() {
  // console.Warn("Play manager loads1")
// $(document).ready(function($) {

  ////////////////////////////////////////////
  //method that switches the field orientation
  $("#buttonRecord").click(function(){

    console.log("in")

    if( $("#modeHidden").html() == "recording" && $("#playState").html() != "edit" ){
      alert("A tactic is already being recorded");
      return
    }

    // in beta version, only 50 tactics are allowed.
    if(allPlays.length >= 50){
      $("#beta_version_modal").modal("show")
      $("#beta_version_modal_message").html("You have reached the maximum of 5 tactics.<br>Remove one before creating new ones.<br>To delete: Open Play > Long Press in one Play > Remove Play")
      return
    }

    // ball must be visible to start recording
    if($("#ball").attr("id")==undefined){
      $("#random_error_modal").modal("show")
      $("#random_error_modal_message").html("Ball must be visible to start recording")
      return
    }

    //setting an empty play to start to record on top of it //  play.id, play.playName, play.type, play.favorite, []
    actualPlay = new Play( allPlays.length + 1 , "", "", "", [], "", "")

    $("#modeHidden").html("recording");
    setState("Recording");

    movesForFirstSnapshot = []
    movesForSecondSnapshot = []
    players = 0
    playerWithBall = ""

    // Save offensive, defensive strategy and initial player with ball
    actualPlay.offensiveStrategy = game[game.offensiveTeam].offensiveTeam;
    actualPlay.defensiveStrategy = game[getDefensiveTeam()].defensiveTeam;
    //writeConsoleLog("PLAYINIT actualPlay.offensiveStrategy: "+actualPlay.offensiveStrategy+"  actualPlay.defensiveStrategy: "+actualPlay.defensiveStrategy);

    var offensivePositions = getPositionsForSystem(actualPlay.offensiveStrategy);
    var defensivePositions = getPositionsForSystem(actualPlay.defensiveStrategy);
    // var gamePositions = offensivePositions.concat(defensivePositions);
    //writeConsoleLog("PLAYINIT offensivePositions: "+offensivePositions+"defensivePositions"+defensivePositions);

    for(i=0; i<offensivePositions.length; i++){

      //we get the position and convert it to fieldPos
      var actualTop = getPlayerValuePerID(game[game.offensiveTeam].courtPlayers[offensivePositions[i]],'fieldPosY');
      var actualLeft = getPlayerValuePerID(game[game.offensiveTeam].courtPlayers[offensivePositions[i]],'fieldPosX')

      //writeConsoleLog("PLAYINIT actualTop: "+actualTop+" actualLeft: "+actualLeft);

      //creating a move for the player, which corresponds with its initial position
      player = game[game.offensiveTeam].courtPlayers[offensivePositions[i]];
      //writeConsoleLog("PLAYINIT player: "+player);

      //getting the position of the actual player (or ball)
      position = offensivePositions[i];

      playName = actualPlay.playName
      snapshotNumber = 0

      positions = []
      positions2 = []
      positions.push(actualLeft + "&&" +  actualTop)
      positions.push(actualLeft + "&&" +  actualTop)
      positions2.push(actualLeft + "&&" +  actualTop)

      //writeConsoleLog( "recorded:  " + player + "  " + team + "  " + position + "  " + pos.left + "  " + pos.top)

      move = new Move( position, playName, snapshotNumber, positions )
      movesForFirstSnapshot.push( move )

      move2 = new Move( position, playName, snapshotNumber+1, positions2 )
      movesForSecondSnapshot.push( move2 )

      players++

    }

    for(i=0; i<defensivePositions.length; i++){

     //we get the position and convert it to fieldPos
      var actualTop = getPlayerValuePerID(game[getDefensiveTeam()].courtPlayers[defensivePositions[i]],'fieldPosY');
      var actualLeft = getPlayerValuePerID(game[getDefensiveTeam()].courtPlayers[defensivePositions[i]],'fieldPosX')

      writeConsoleLog("PLAYINIT actualTop: "+actualTop+" actualLeft: "+actualLeft);


      //creating a move for the player, which corresponds with its initial position
      player = game[getDefensiveTeam()].courtPlayers[defensivePositions[i]];
      writeConsoleLog("PLAYINIT player: "+player);

      //getting the position of the actual player (or ball)
      position = defensivePositions[i];

      playName = actualPlay.playName
      snapshotNumber = 0

      positions = []
      positions2 = []
      positions.push(actualLeft + "&&" +  actualTop)
      positions.push(actualLeft + "&&" +  actualTop)
      positions2.push(actualLeft + "&&" +  actualTop)

      //writeConsoleLog( "recorded:  " + player + "  " + team + "  " + position + "  " + pos.left + "  " + pos.top)

      move = new Move( position, playName, snapshotNumber, positions )
      movesForFirstSnapshot.push( move )

      move2 = new Move( position, playName, snapshotNumber+1, positions2 )
      movesForSecondSnapshot.push( move2 )

      players++

    }

    playerWithBall = game.playerWithBall

    if( players == 0 ){
      alert( "You should have players on the field to be able to create a Tactic" )
      setState("FreeDraw");
    }

    //initializing variables to create the objects
    datetime = new Date().toLocaleString();
    //playName = "newPlay" + datetime
    playName = actualPlay.playName
    type = actualPlay.type
    favorite = "false"
    team = $("#teamA").html();
    var offensiveStrategyPlay = actualPlay.offensiveStrategy;
    var defensiveStrategyPlay = actualPlay.defensiveStrategy;

    writeConsoleLog("PLAYINIT offensiveStrategyPlay: "+offensiveStrategyPlay);

    //creating the Play and filling it with the initial parameters and with the first snapshot (initial positions)
    snapshot = new Snapshot( snapshotNumber, playName, playerWithBall, movesForFirstSnapshot )
    actualPlay = new Play( allPlays.length + 1 , playName, type, favorite, [snapshot], offensiveStrategyPlay, defensiveStrategyPlay )
    allPlays.push(actualPlay)
    //alert("Starting to record")

    //initializing the second snapshot
    actualMoves = movesForSecondSnapshot
    actualSnapshot = new Snapshot( snapshotNumber+1, playName, playerWithBall, movesForSecondSnapshot )

    processSnapshotDisplay()

  });



  ////////////////////////////////////////////
  //method that saves the actual tactic
  $("#buttonSave").click(function(){

    //if no play was being recorded, an error is displayed
    if( $("#modeHidden").html() != "recording" ){
      alert("No play was being recorded. Hence, nothin can be saved")
      return
    }

    //saving the last set of moves, snapshot and play
    //newSnapshot()
    actualSnapshot.moves = actualMoves;

    (actualPlay.snapshots).push(actualSnapshot)

    //if we were on FreeDrawWithBackup mode (edit mode), we save on the all plays vector
    console.log("Mode:  " + $("#playState").html() )
    if( $("#playState").html() == "edit" ){
      allPlays.push(actualPlay)
    }

    //check that things are being saved
    /*for( i=0; i<actualMoves.length; i++ ){
      writeConsoleLog( actualPlay.snapshots[1].moves[i].player )
      writeConsoleLog( actualPlay.snapshots[1].moves[i].positions )
    }*/

    //showing the popup to save the play
    $("#inputNewPlayName").val("")
    $("#infoCreateNewPlay").html("")
    $("#newPlayPopup").modal("show")


    setState("FreeDraw");

    //check of the snapshots that have been saved
    //for(q=0;q<(actualPlay.snapshots).length;q++){
    //  writeConsoleLog( q )
    //  writeConsoleLog( actualPlay.snapshots[q].playerWithBall)
    //}

  });



  /////////////////////////////////////////////
  //method that opens a previously saved tactic
  $("#buttonOpen").click(function(){

    //restarting the lists
    $("#listOfPlaysToAdd").empty();
    $("#playsAlreadySaveToBeOpened").empty();

    //if there are no tactics, display message
    if( allPlays.length == 0 ){
      $("#playsAlreadySaveToBeOpened").append("<div class='row' id='text-no-plays-device'><div class='col-11 playAttribute'>There are no tactics saved on this device</div></div> ");
    }

    displays = []

    //we iterate through all the saved plays and we display the tactics
    for( i=0 ; i<allPlays.length ; i++ ){
      name = allPlays[i].playName
      type = allPlays[i].type
      favorite = allPlays[i].favorite
      offStrategy = allPlays[i].offensiveStrategy
      defStrategy = allPlays[i].defensiveStrategy
      display = name
      idForButton = display.split(' ').join('---');
      writeConsoleLog(idForButton)
      //adding the favorite tactics at the top of the list ( .prepend appends in the first possition)
      if(favorite=="true"){
        //$(".list-group").prepend("<button type='button' id='" + idForButton + "' class='list-group-item list-group-item-action playButton' data-dismiss='modal'>" + display + "<img src='img/Icon_Favorite.svg' width='20' height='20'/></button> ");
        $("#playsAlreadySaveToBeOpened").append("<button type='button' id='" + idForButton + "' class='list-group-item list-group-item-action playButton mt-3' data-dismiss='modal'><div class='row'><div class='col-8 col-md-5 playAttribute'>" + display + "<img src='img/Icon_Favorite.svg' width='20' height='20'/></div><div class='col-4 col-md-2 align-middle playAttribute'>"+type+"</div><div class='col-2 align-middle playAttribute'>"+getTacticReadableName(offStrategy)+"</div><div class='col-2 align-middle playAttribute'>"+getTacticReadableName(defStrategy)+"</div></div></button>");
        $("#"+idForButton).addClass("favoriteButton")
      }
      //append non-favorite tactics
      else{
        $(".list-group").append("<button type='button' id='" + idForButton + "' class='list-group-item list-group-item-action playButton mt-3' data-dismiss='modal'><div class='row'><div class='col-8 col-md-5 playAttribute'>" + display + "</div><div class='col-4 col-md-2 align-middle playAttribute'>"+type+"</div><div class='col-2 align-middle openPlayPopupOff playAttribute'>"+getTacticReadableName(offStrategy)+"</div><div class='col-2 align-middle openPlayPopupDef playAttribute'>"+getTacticReadableName(defStrategy)+"</div></div></button>");
      }
      displays.push(idForButton)
    }


    for(  i=0; i<displays.length; i++){

      //creating the event listener for a normal click to open the tactic
      document.getElementById(displays[i]).addEventListener("click", function(){
        loadTactic( $(this).attr("id") );
      }, false);

      //creating the event to edit the tactic: edit name, edit type, delete tactic
      id = document.getElementById(displays[i]).id
      writeConsoleLog( $("#"+id) )
      $("#"+id).bind( "taphold", editPlayMenu);

    }

  });



  /////////////////////////////////////////////
  //method that plays the open tactic
  $("#buttonPlay").click(function(){

    //if the play is recently opened, we play from the start
    if( $("#playState").html() == "playing" || $("#playState").html() == "opened" || $("#playState").html() == "paused" || $("#playState").html() == "stopped" ){
      //we call the carryOutMoveForward in sequential mode (true) starting from snapshot 1 (snapshot 0 is just for positioning)
      $("#playState").html("playing")
      carryOutMoveForward( actualSnapshotIndex, true )
      $("#buttonPause").show();
      $("#buttonPlay").hide();
    }
    else{
      alert("There is no tactic loaded.")
    }

    console.log("playState:" + $('#playState').html());

  });



  ////////////////////////////////////////////////////////////////
  //method that pauses the execution of the play in continuous mode
  $("#buttonPause").click(function(){

    //if the play is recently opened, we play from the start
    if( $("#playState").html() == "playing" ){
      $("#playState").html("paused")
      processSnapshotDisplay()
      $("#buttonPause").hide();
      $("#buttonPlay").show();
    }
    else if( $("#playState").html() == "paused" ){
      writeConsoleLog("Execution is already paused.")
    }

    console.log("playState:" + $('#playState').html());

  });



  ////////////////////////////////////////////////////////////////
  //method that stops the execution and sends it to snapshot 1
  $("#buttonStop").click(function(){

    if($("#playState").html() == "playing" ){
      $("#playState").html("paused")
      processSnapshotDisplay()
      $("#buttonPause").hide();
      $("#buttonPlay").show();
      console.log("playState:" + $('#playState').html());
    }
    else if( $("#playState").html() != "noPlay" ){//if there is a tactic loaded, it sends back to snapshot1

      //if we are already on the first snapshot, we go to no-play mode
      if( actualSnapshotIndex<=1 ){
        $("#playState").html("noPlay")
        setState("FreeDraw");
        processSnapshotDisplay()
        // resetCanvas()
        return
      }

      //changing the play state to stopped
      $("#playState").html("stopped")
      $("#buttonPause").hide();
      $("#buttonPlay").show();
      console.log("playState:" + $('#playState').html());
      actualSnapshotIndex = 1
      loadTactic( actualPlay.playName )
    }

    if( $("#modeHidden").html() == "recording" ){

      //if we were recording, we pop the cancelled play from the array
      allPlays = allPlays.splice( 0 , allPlays.length-1)

      $("#playState").html("noPlay")
      setState("FreeDraw");
      processSnapshotDisplay()
      // resetCanvas()
      return
    }

  });



  ///////////////////////////////////////////////
  //method that undos the last snapshot recorded
  $("#buttonUndo").click(function(){

    //if no play is being recorded, an error is displayed
    if( $("#modeHidden").html() != "recording" ){
      alert("No play was being recorded. Hence, nothin can be saved")
      return
    }

    writeConsoleLog("Undoing last snapshot\n");

    //if we are on the first snapshot, nothing can be undone
    if( actualPlay.snapshots.length < 2 ){
      writeConsoleLog("everything possible undone")
      return;
    }

    //deleting the last snapshot from the list of snapshots
    actualPlay.snapshots = actualPlay.snapshots.slice( 0 , actualPlay.snapshots.length-1 );

    snapshotNumber -= 1;
    processSnapshotDisplay()

    //getting the last moves for the players
    var moves = actualPlay.snapshots[ actualPlay.snapshots.length - 1 ].moves;

    //getting the variables for the player with the ball and whether to undo a pass or movement
    var playerWithBallInThisSnapshot = actualPlay.snapshots[ actualPlay.snapshots.length - 1 ].playerWithBall;

    var doneRemoving = false
    var doneAdding = false

    //iteratiing over all moves in the snapshot
    for( i=0; i<moves.length; i++){

      //obtaining the player to process
      playerPosition = moves[i].position;

      var playerID = $(".position"+playerPosition).attr("id")


      if(playerPosition=="ball"){
        pos = moves[i].positions[ moves[i].positions.length-1 ]
        continue;
      }

      //getting the last position from last snapshot to set the player
      pos = moves[i].positions[ moves[i].positions.length-1 ]
      setPositionLeft = pos.split("&&")[0]
      setPositionTop = pos.split("&&")[1]

      //converting the serialized position to display coordinates
      pos = fieldPos2displayPos( setPositionLeft, setPositionTop, getCurrentLayout() )

      //adding the ball to the correct player
      if( playerPosition == game.playerWithBall && doneRemoving == false ){
        doneRemoving = true
      }
      if( playerPosition == playerWithBallInThisSnapshot && doneAdding == false ){
        doneAdding = true
        nextPlayerWithBall = playerPosition
        $( ".ball" ).animate({
          top: pos.top,
          left: pos.left
        },0);
      }

      //moving the player to the correct position
      $( ".position" + playerPosition ).animate({
        top: pos.top,
        left: pos.left
      },0);

    }


    game.playerWithBall = nextPlayerWithBall
    actualMoves = actualPlay.snapshots[actualPlay.snapshots.length-1].moves
    actualSnapshot = actualPlay.snapshots[actualPlay.snapshots.length-1]

    //redoing the arrows
    arrowsManager(actualPlay.snapshots, actualPlay.snapshots.length);

    var newData = initializeEmptySnapshot()
    actualSnapshot = newData.newSnapshot
    actualMoves = newData.newMoves


  });



  /////////////////////////////////////////////
  $("#buttonForwards").click(function() {

    //if the play is recently opened, we play from the start
    if( $("#playState").html() == "playing" || $("#playState").html() == "opened" || $("#playState").html() == "paused" || $("#playState").html() == "stopped" ){

      $("#playState").html("playing")
      $("#buttonPause").hide();
      $("#buttonPlay").show();
      snapshots = actualPlay.snapshots

      writeConsoleLog( "actual snapshot index:  " + actualSnapshotIndex )
      writeConsoleLog( "number of snapshots: " + snapshots.length )
      writeConsoleLog( "playerWithTheBall:  " +  snapshots[ actualSnapshotIndex ].playerWithBall )

      if( actualSnapshotIndex == snapshots.length-1 ){
        //alert("All snapshots have already been shown")
        return
      }

      idPlayerWithBall = game.playerWithBall
      writeConsoleLog("removing the ball from:  " + idPlayerWithBall)
      playerWithBall = snapshots[ actualSnapshotIndex ].playerWithBall
      writeConsoleLog("adding the ball to:  " + playerWithBall)
      game.playerWithBall = playerWithBall

      //carrying out the move
      //removePlayerArrows()
      carryOutMoveForward( actualSnapshotIndex )

      //actualizing the actual snapshot index
      if( actualSnapshotIndex < snapshots.length-1 ){
        actualSnapshotIndex += 1
        $('#snapshotDisplay').val( actualSnapshotIndex );
      }

    }
    else{
      alert("There is no tactic loaded.")
    }



  });



  /////////////////////////////////////////////
  $("#buttonBackwards").click(function() {

    //if the play is recently opened, we play from the start
    if( $("#playState").html() == "playing" || $("#playState").html() == "opened" || $("#playState").html() == "paused" || $("#playState").html() == "stopped" ){

      $("#playState").html("playing")
      $("#buttonPause").hide();
      $("#buttonPlay").show();
      snapshots = actualPlay.snapshots
      if( actualSnapshotIndex == 1 ){
        //alert("First snapshot is shown. Not possible to go backwards")
        return
      }

      if( actualSnapshotIndex == snapshots.length ){
        actualSnapshotIndex -= 0
      }

      //actualizing the actual snapshot index
      if( actualSnapshotIndex > 1 ){
        actualSnapshotIndex -= 1
        $('#snapshotDisplay').val( actualSnapshotIndex );
      }

      writeConsoleLog( actualSnapshotIndex )
      writeConsoleLog( snapshots.length )
      writeConsoleLog( "playerWithTheBall:  " +  snapshots[ actualSnapshotIndex ].playerWithBall )

      playerWithBall = game.playerWithBall
      writeConsoleLog("removing the ball from:  " + playerWithBall)
      playerWithBall = snapshots[ actualSnapshotIndex-1 ].playerWithBall
      writeConsoleLog("adding the ball to:  " + playerWithBall)
      game.playerWithBall = playerWithBall

      //carrying out the move
      // removePlayerArrows()
      carryOutMoveBackwards( actualSnapshotIndex )



    }
    else{
      alert("There is no tactic loaded.")
    }



  });



  ////////////////////////////////////////////////////////////////////
  //function that goes to a given snapshot when moving the range input
  // NOT IN USE ANYMORE
  $('#snapshotDisplay').on("change", function() {

    index = $('#snapshotDisplay').val()

    if( actualSnapshotIndex == index || actualPlay.length==0 ){
      return
    }

    $(".teamA").remove();
    $(".teamB").remove();
    RemoveBall();

    targetPositions =  snapshots[ index ].moves

    //load every player in the position
    for( k=0 ; k<targetPositions.length ; k++ ){
      //getting the data about the current element
      player = targetPositions[k].player
      position = targetPositions[k].position
      setPosition = targetPositions[k].positions[0]
      setPositionLeft = setPosition.split("&&")[0]
      setPositionTop = setPosition.split("&&")[1]
      writeConsoleLog("iteration: " + k + "  " + position + "  " + setPositionLeft + "  " + setPositionTop )

      //checking if it is the ball or the player
      if( position == "ball" ){
        asignBall( setPositionLeft, setPositionTop )
      }else{
        setPlayer(team, position, [setPositionTop, setPositionLeft], getCurrentLayout());
      }

      //asign the ball to the corresponding player
      if( snapshots[0].playerWithBall == getPositionOfPlayerPerId(player) ){
        game.playerWithBall = getPositionOfPlayerPerId(player)
      }

    }

    // setOrientation();

  });



  /////////////////////////////////////////////////
  //Button that cancels the new play popup
  $("#cancelNewPlayPopup").click(function() {
    $('#newPlayPopup').modal('show');
    return
  });



  /////////////////////////////////////////
  //Button set position first on the new play popup
  $("#setPosNewPlayPopup").click(function() {

    playName = $("#inputNewPlayName").val()
    if( $("#inlineRadio1").prop('checked') ){
      type = "offense"
    }else if( $("#inlineRadio2").prop('checked') ){
      type = "defense"
    }else{
      type = "all"
    }

    if( playName=="" ){
      actualPlay = new Play( allPlays.length + 1 , team, "", "", [] )
      $('#newPlayPopup').modal('hide');
      alert("ERROR: No name given to the tactic")
      return
    }

    actualPlay = new Play( allPlays.length + 1 , playName, type, [] )
    $('#newPlayPopup').modal('hide');
    return

  });



  ////////////////////////////////////////
  //Button that starts saving the new play
  $("#startNewPlayPopup").click(function() {

    playName = $("#inputNewPlayName").val()
    if( playName == "" ){
      $("#infoCreateNewPlay").html("Please introduce a play name")
      return;
    }
    //checking if a play with that name already exists
    else{
      for(i=0;i<allPlays.length;i++){
        if(playName == allPlays[i].playName){
          $("#infoCreateNewPlay").html("A play with that name already exists")
          return;
        }
      }
    }

    // accepting only alphanumeric, spaces and underscores
    if( parse_play_name(playName, "#infoCreateNewPlay")==false ){
      return true
    }


    if( $("#inlineRadio1").prop('checked') ){
      type = "offense"
    }else if( $("#inlineRadio2").prop('checked') ){
      type = "defense"
    }else{
      type = "all"
    }

    writeConsoleLog("Playname:  " + playName)
    writeConsoleLog("Type: " + type)
    actualPlay.playName = playName
    actualPlay.type = type
    actualPlay.favorite = "false"

    //saving the playName into moves and snapshots too
    for( i=0;i<actualPlay.snapshots.length;i++ ){
      actualPlay.snapshots[i].playName = playName;
      for( j=0;j<actualPlay.snapshots[i].moves.length;j++ ){
        actualPlay.snapshots[i].moves[j].playName = playName;
      }
    }

    //including the tactic in the list and in the database
    for( i=0 ; i<allPlays.length ; i++){
      if( allPlays[i].playName == actualPlay.playName ){
        allPlays[i] = actualPlay
        //alert("play was saved in file: " + actualPlay.playName + "  !!")
        break
      }
    }
    savePlayInDatabase( actualPlay )

    $('#newPlayPopup').modal('hide');
    setState("FreeDraw");
    $("#snapshotDisplay").html("Free-Draw");
    return



  });



  /////////////////////////////////////////////////////////////////////
  //button that re-saves the information of a tactic after being edited
  $("#saveEditPlayPopup").click(function() {


    //obtaining the new values for the tactic variables
    oldPlayName = $("#editPlayModalTitle").text().split(": ")[1]

    playName = $("#inputEditPlayName").val()
    if( playName == "" ){
      $("#infoEditPlay").html("Please introduce a play name")
      writeConsoleLog("Please introduce a play name")
      return;
    }
    //checking if a play with that name already exists
    else{
      for(i=0;i<allPlays.length;i++){
        if(playName == allPlays[i].playName){
          writeConsoleLog("A play with that name already exists")
          $("#infoEditPlay").html("A play with that name already exists")
          return;
        }
      }
    }

    // accepting only alphanumeric, spaces and underscores
    if( parse_play_name(playName, "#infoEditPlay")==false ){
      return true
    }

    writeConsoleLog( "playName " + playName )

    if( $("#inlineRadio1Edit").prop('checked') ){
      playType = "offense"
    }else if( $("#inlineRadio2Edit").prop('checked') ){
      playType = "defense"
    }else{
      playType = "all"
    }


    //iterating the vector with all plays and updating the correct tactic
    for( i=0; i<allPlays.length; i++ ){

      if( allPlays[i].playName == oldPlayName ){
        allPlays[i].playName = playName
        allPlays[i].type = playType

        snapshotsToChange = allPlays[i].snapshots

        //updating also the value of the variables in the snapshots and moves
        for( j=0; j<snapshotsToChange.length; j++ ){
          snapshotsToChange[j].playName = playName;
          movesToChange = snapshotsToChange[j].moves

          for( k=0; k<movesToChange.length; k++ ){
            movesToChange[k].playName = playName
          }
        }

        break;
      }
    }


    //checking the fav buttons and updating if necessary
    if( GAME_SETTINGS.favPlay1 == oldPlayName ){
      GAME_SETTINGS.favPlay1 = playName
      $("#favPlay1").html(playName)
    }
    else if( GAME_SETTINGS.favPlay2 == oldPlayName ){
      GAME_SETTINGS.favPlay2 = playName
      $("#favPlay1").html(playName)
    }
    else if( GAME_SETTINGS.favPlay3 == oldPlayName ){
      GAME_SETTINGS.favPlay3 = playName
      $("#favPlay1").html(playName)
    }
    else if( GAME_SETTINGS.favPlay4 == oldPlayName ){
      GAME_SETTINGS.favPlay4 = playName
      $("#favPlay1").html(playName)
    }
    else if( GAME_SETTINGS.favPlay5 == oldPlayName ){
      GAME_SETTINGS.favPlay5 = playName
      $("#favPlay1").html(playName)
    }


    //updating the values on the database
    var db = openDatabase('timeOutDB', '1.0', 'Database for the TimeOutApp', 10 * 1024 * 1024)
    db.transaction(function (tx) {
      tx.executeSql("UPDATE Plays SET playName='" + playName + "', type='" + playType + "'  WHERE playName='" + oldPlayName + "' ")
      tx.executeSql("UPDATE Snapshots SET playName='" + playName + "' WHERE playName='" + oldPlayName + "' ")
      tx.executeSql("UPDATE Moves SET playName='" + playName + "' WHERE playName='" + oldPlayName + "' ")
    });

    writeConsoleLog( "Tactic " + oldPlayName + " successfully updated to " + playName )

    $('#editPlayPopup').modal('hide');


  });



  /////////////////////////////////////////////////////////////////////
  //button that deletes a tactic after being edited
  $("#deleteEditPlayPopup").click(function() {

    playName = $("#editPlayModalTitle").text().split(": ")[1]

    //iterating the vector with all plays and deleting the correct tactic
    for( i=0; i<allPlays.length; i++ ){
      writeConsoleLog( allPlays[i].playName + "   " + playName )
      if( allPlays[i].playName == playName ){
        allPlays.splice(i,1);
        break;
      }
    }

    //removing the tactic from the favortites if necessary
    if( GAME_SETTINGS.favPlay1 == playName ){
      GAME_SETTINGS.favPlay1 = ""
      $("#star1").attr("src","img/Icon_Star_Gray.svg");
      $("#favPlay1").html("")
    }
    if( GAME_SETTINGS.favPlay2 == playName ){
      GAME_SETTINGS.favPlay2 = ""
      $("#star2").attr("src","img/Icon_Star_Gray.svg");
      $("#favPlay2").html("")
    }
    if( GAME_SETTINGS.favPlay3 == playName ){
      GAME_SETTINGS.favPlay3 = ""
      $("#star3").attr("src","img/Icon_Star_Gray.svg");
      $("#favPlay3").html("")
    }
    if( GAME_SETTINGS.favPlay4 == playName ){
      GAME_SETTINGS.favPlay4 = ""
      $("#star4").attr("src","img/Icon_Star_Gray.svg");
      $("#favPlay4").html("")
    }
    if( GAME_SETTINGS.favPlay5 == playName ){
      GAME_SETTINGS.favPlay5 = ""
      $("#star5").attr("src","img/Icon_Star_Gray.svg");
      $("#favPlay5").html("")
    }


    //removing the information relevant to the given tactic from the database
    var db = openDatabase('timeOutDB', '1.0', 'Database for the TimeOutApp', 10 * 1024 * 1024)
    db.transaction(function (tx) {
      tx.executeSql("DELETE FROM Plays WHERE playName='" + playName + "' ")
      tx.executeSql("DELETE FROM Snapshots WHERE playName='" + playName + "' ")
      tx.executeSql("DELETE FROM Moves WHERE playName='" + playName + "' ")
    });

    writeConsoleLog( "Tactic " + playName + " successfully removed" )
    $('#editPlayPopup').modal('hide');

    setState("FreeDraw")
    processSnapshotDisplay()
    // resetCanvas()

  });



  /////////////////////////////////////////////////////////////////////
  //OLD VERSION: NOT BEING USED
  //button that adds/removes a tactic from the favorites list
  $("#addTacticToFavorites").click(function() {

    //obtaining the new values for the tactic variables
    playName = $("#editPlayModalTitle").text().split(": ")[1]
    writeConsoleLog( "playName to add/remove to favorites:  " + playName )

    //iterating the vector with all plays and updating the correct tactic
    for( i=0; i<allPlays.length; i++ ){

      if( allPlays[i].playName == playName ){
        if( allPlays[i].favorite=="true" ){
          favorite = "false"
        }else{
          favorite = "true"
        }

        allPlays[i].favorite = favorite
        break;
      }
    }

    //updating the values on the database
    var db = openDatabase('timeOutDB', '1.0', 'Database for the TimeOutApp', 10 * 1024 * 1024)
    writeConsoleLog(favorite);
    db.transaction(function (tx) {
      tx.executeSql("UPDATE Plays SET favorite='" + favorite + "' WHERE playName='" + playName + "' ")
    });

    writeConsoleLog( "Tactic " + playName + " successfully updated")

  });



  ///////////////////////////////////////////////////////////////////////
  //method that processes clicking on the favorite tactic buttons
  $(".favButtons").click(function(){

    var buttonNumber = $(this).attr("id")[ $(this).attr("id").length - 1 ]
    $("#favModalTitle").html("Favorite Play Button " + buttonNumber)
    var playName = $("#favPlay"+buttonNumber).html()

    if( playName == "" || playName.length<1 ){
      allowUserToChooseFavTactic(buttonNumber)
      $("#favPlayPopup").modal("show")
      //alert("No Tactic Saved in this button")
      return
    }

    //processSnapshotDisplay()
    $("#playState").html("opened")
    $("#buttonPause").hide();
    $("#buttonPlay").show();
    actualSnapshotIndex = 1
    setState("Playing");

    loadTactic( playName )


  });



  ///////////////////////////////////////////////////////////////////////
  //method that processes the long clicks on the favorite tactic buttons
  $(".favButtons").on("taphold",function(){

    //getting the favorite button pressed and the (if existent) id of the tactic assigned to the button
    var buttonNumber = $(this).attr("id")[ $(this).attr("id").length - 1 ]
    var tacticLoaded = $("#favPlay"+buttonNumber).html()
    $("#favModalTitle").html("Favorite Play Button " + buttonNumber)

    //case for adding a fav play to this button
    if( tacticLoaded == "" || tacticLoaded.length==0 ){

      allowUserToChooseFavTactic(buttonNumber)

    }


    //case for deleting the play saved on the button
    else{
      $("#ButtonFavEmpty").hide()
      $("#ButtonFavFull").show()

      var playName = undefined
      for( i=0;i<allPlays.length;i++ ){
        tacticLoaded = tacticLoaded.split('---').join(' ')
        if( allPlays[i].playName == tacticLoaded ){
          playName = allPlays[i].playName;
          break
        }
      }
      //$("#favPlaySaved").html("Play Assigned to this button: " + playName)
      $("#favPlaySaved").html(playName)
    }


    $("#favPlayPopup").modal("show")


  });



  //////////////////////////////////////////////////////////////
  //method that handles removing a play from the favorite button
  $("#removeFromFavorites").click(function(){

      var playName = $("#favPlaySaved").html()
      var buttonNumber = $("#favModalTitle").html()
      buttonNumber = buttonNumber[buttonNumber.length-1]
      $("#favPlay"+buttonNumber).html("")
      if( buttonNumber=="1" ){
        GAME_SETTINGS.favPlay1 = ""
        $("#star1").attr("src","img/Icon_Star_Gray.svg");
      }
      else if( buttonNumber=="2" ){
        GAME_SETTINGS.favPlay2 = ""
        $("#star2").attr("src","img/Icon_Star_Gray.svg");
      }
      else if( buttonNumber=="3" ){
        GAME_SETTINGS.favPlay3 = ""
        $("#star3").attr("src","img/Icon_Star_Gray.svg");
      }
      else if( buttonNumber=="4" ){
        GAME_SETTINGS.favPlay4 = ""
        $("#star4").attr("src","img/Icon_Star_Gray.svg");
      }
      else if( buttonNumber=="5" ){
        GAME_SETTINGS.favPlay5 = ""
        $("#star5").attr("src","img/Icon_Star_Gray.svg");
      }

    $("#homeLogo").click()
    processSnapshotDisplay()
    // resetCanvas()

  });


  //////////////////////////////////////////////////////////////
  //method that handles changes on the new_play name input
  $("#inputNewPlayName").on("input", function(){

    playName = $("#inputNewPlayName").val()
    parse_play_name(playName, "#infoCreateNewPlay")

  });


  //////////////////////////////////////////////////////////////
  //method that handles changes on the edit_play name input
  $("#inputEditPlayName").on("input", function(){

    playName = $("#inputEditPlayName").val()
    parse_play_name(playName, "#infoEditPlay")

  });


});



/////////////////////////////////////////////////////////////////
//method that copies the progress of a tactic to allow editing
function copy_tactic(){

  //deep copying actual play into the auxiliar object
  auxPlay = JSON.parse(JSON.stringify(actualPlay))
  auxSnapshots = JSON.parse(JSON.stringify(actualPlay.snapshots))
  auxPlay.id = allPlays[allPlays.length-1].id+1

  //keeping only the snapshots up to the moment
  auxSnapshots = auxSnapshots.slice(0, actualSnapshotIndex);
  auxPlay.snapshots = auxSnapshots
  auxPlay.favorite = "false"
  auxPlay.name = "aux"

  //setting as actual play
  actualPlay = auxPlay

  var newData = initializeEmptySnapshot()
  actualSnapshot = newData.newSnapshot
  actualMoves = newData.newMoves


}



/////////////////////////////////////////////////////////////////
//method that initializes a semi-empty snapshot for after undoing
function initializeEmptySnapshot(){


  snapshotNumber = (actualPlay.snapshots).length
  moves = []
  playName = actualPlay.playName


  $(".players").each(function() {

    //we get the position and convert it to fieldPos
    var actualTop = parseFloat($(this).css('top'))
    var actualLeft = parseFloat($(this).css('left'))
    pos = convert2Field( actualLeft, actualTop )

    //creating a move for the player, which corresponds with its initial position
    player = $(this).attr("id")
    playerName = $(this).attr("id")

    positions = []
    positions.push(pos.left + "&&" +  pos.top)


    if( game.playerWithBall == getPositionOfPlayerPerId(player) ){
      playerWithBall = game.playerWithBall
    }


    position = ""
    classes = $(this).attr("class").split(' ');
    for( j=0; j<classes.length; j++ ){
      if( classes[j].includes("position") ){
        position = (classes[j]).substring(8, classes[j].length )
      }
    }
    if( position == "" ){
      //writeConsoleLog("New Initial Position:  " + pos.left + "   " + pos.top)
      position = "ball"
    }

    //writeConsoleLog("Snapshot:  " + (actualSnapshot.snapshotNumber) + "   Move corresponds to: " + snapshotNumber)
    move = new Move( position, playerName, snapshotNumber, positions )
    moves.push( move )
    processSnapshotDisplay()

  });

  actualMoves = moves
  actualSnapshot.playerWithBall = playerWithBall
  var newSnapshot = new Snapshot( snapshotNumber, playName, playerWithBall, moves )

  return {
    "newMoves":moves,
    "newSnapshot":newSnapshot
  };

}



///////////////////////////////////////////////////////////////////
//method that processes the model to let the user choose a fav play
function allowUserToChooseFavTactic(buttonNumber){

  $("#ButtonFavEmpty").show()
  $("#ButtonFavFull").hide()

  //restarting the lists
  $("#listOfPlaysToAdd").empty();
  $("#playsAlreadySaveToBeOpened").empty();

  //if there are no tactics, display message
  if( allPlays.length == 0 ){
    $("#listOfPlaysToAdd").append("<a class='list-group-item list-group-item-action' data-dismiss='modal'> There are no tactics saved on this device </a> ");
  }

  var possiblePlays = []

  //we iterate through all the saved plays and we display the tactics
  for( i=0 ; i<allPlays.length ; i++ ){
    name = allPlays[i].playName
    type = allPlays[i].type
    favorite = allPlays[i].favorite
    display = name
    var idForButton = display.split(' ').join('---');
    if(favorite!="true"){
      $("#listOfPlaysToAdd").append("<button type='button' id='" + idForButton + "' class='list-group-item list-group-item-action playButton' data-dismiss='modal'>" + display + "</button> ");
    }
    possiblePlays.push(idForButton)
  }

  //creating the event listener for a normal click to add to the button
  for(  i=0; i<possiblePlays.length; i++){

    var id = document.getElementById(possiblePlays[i]).id
    writeConsoleLog( "button " + buttonNumber + " was pressed" )
    writeConsoleLog("Creating binder for: %%" + id + "%%")

    //$("#"+id).bind("click", function(){
    $("#"+id).click( function(){

      writeConsoleLog("\n\n\nasdasdadasdadsasd\n\n\n")

      var name = $(this).attr("id")
      //var name = $(this).attr("id").split('---').join(' ')
      $("#favPlay"+buttonNumber).html(name)

      if( buttonNumber=="1" ){
        GAME_SETTINGS.favPlay1 = name
        $("#star1").attr("src","img/Icon_Star_Yellow.svg");
      }
      if( buttonNumber=="2" ){
        GAME_SETTINGS.favPlay2 = name
        $("#star2").attr("src","img/Icon_Star_Yellow.svg");
      }
      if( buttonNumber=="3" ){
        GAME_SETTINGS.favPlay3 = name
        $("#star3").attr("src","img/Icon_Star_Yellow.svg");
      }
      if( buttonNumber=="4" ){
        GAME_SETTINGS.favPlay4 = name
        $("#star4").attr("src","img/Icon_Star_Yellow.svg");
      }
      else if( buttonNumber=="5" ){
        GAME_SETTINGS.favPlay5 = name
        $("#star5").attr("src","img/Icon_Star_Yellow.svg");
      }
    });

  }

}



/////////////////////////////////////////////////////
//function that carries out the actual move backwards
function carryOutMoveBackwards( index ){

  snapshots = actualPlay.snapshots

  initialPositions = snapshots[ index ].moves
  ballMoved = false

  processSnapshotDisplay()

  //iterating through all moves in the snapshot
  for( j=0; j<initialPositions.length; j++ ){

    positions = initialPositions[j].positions
    playerPosition = initialPositions[j].position
    playerID = $(".position"+playerPosition).attr("id")

    arrowPositions = getBezierPoints( positions, playerID );

    //iterating through the movements of a player in the actual snapshot
    //for(  k = 3 ; k>-1 ; k-- ){

      //creating the player if he comes from a part of the field which is not visible
      if( playerID == undefined ){
        playerID = createPlayerFromHiddenPlace( playerPosition, arrowPositions, 0, index )
      }else{
        hidePlayerWhenGoingOutOfTheField( playerPosition, arrowPositions, 0, index )
      }

      //getting the position and converting it to display
      // setPositionLeft = arrowPositions[0][0]
      // setPositionTop = arrowPositions[0][1]
      pos = []
      for( k=0; k<4;k++ ){
        pos[k] = fieldPos2displayPos( arrowPositions[k][0], arrowPositions[k][1], getCurrentLayout());
        writeConsoleLog("BackwardsF fieldPos   top: " + arrowPositions[k][0] + "   left:  " + arrowPositions[k][1] + "  layout: " + getCurrentLayout() )
        writeConsoleLog("BackwardsF diplayPos   top: " + pos[k].top + "   left:  " + pos[k].left)
      }

      // Updating position
      setPlayerValuePerID(playerID, "fieldPosX", arrowPositions[0][0]);
      setPlayerValuePerID(playerID, "fieldPosY", arrowPositions[0][1]);

      //creating the animators

       var bezier_params = {
        start: {
          x: pos[3].left,
          y: pos[3].top,
          x1: pos[2].left,
          y1: pos[2].top,
          x2: pos[1].left,
          y2: pos[1].top,
          angle: 0 //dummy value
        },
        end: {
          x:pos[0].left,
          y:pos[0].top,
          angle: 0, // dummy value
          length: 0.3 // dummy value
        }
      }

      if( game.playerWithBall != playerPosition ){
        $( ".position" + playerPosition ).animate({
          path: new $.path.bezier(bezier_params)
        }, parseInt(GAME_SETTINGS.playSpeed)/2 );
      }
      //for the player with the ball if it is not a pass snapshot
      else if( game.playerWithBall == playerPosition &&  isPassSnapshot(snapshots, index) == false ){
        writeConsoleLog( "playerWithTheBall:  " +  snapshots[ index ].playerWithBall )
        $( ".position" + playerPosition ).animate({
          path: new $.path.bezier(bezier_params)
        }, parseInt(GAME_SETTINGS.playSpeed)/2 );
        $( ".ball" ).animate({
          path: new $.path.bezier(bezier_params)
        }, parseInt(GAME_SETTINGS.playSpeed)/2 );
      }
      //for the player with the ball in a pass snapshot
      else{
        $( ".position" + playerPosition ).animate({
          path: new $.path.bezier(bezier_params)
        }, parseInt(GAME_SETTINGS.playSpeed)/2 );
      }

      //checking if player goes out of bounds
      var teamFromID = getTeam( playerID, playerPosition )
      teamFromID = reverseID(teamFromID)
      if( !playerFitsInScreen( arrowPositions[0][1], getCurrentLayout() )){
        $("#ArrowOutOfSight"+teamFromID+playerPosition).remove()
        createArrow( teamFromID , getCurrentLayout() , playerPosition, arrowPositions[0][0]  )
      }else{
        $("#ArrowOutOfSight"+teamFromID+playerPosition).remove()
      }

  }


  //for the ball in a pass snapshot
  if( isPassSnapshot(snapshots, index) ){
    writeConsoleLog("we are passing")
    offset=0

    var previousPlayerWithBall = snapshots[index-1].playerWithBall
    console.log(previousPlayerWithBall)
    pos = $(".position" + previousPlayerWithBall).position()

    // if($("#ball").attr("id")==undefined){
    //   $(".court").append("<h1 class='ball rotates' id='ball' </h1> ");
    //   $(".ball").css({'top':(pos.top), 'left': (pos.left)});
    // }
    try {
      $( ".ball" ).animate({
        top: pos.top,
        left: pos.left - offset
      }, parseInt(GAME_SETTINGS.playSpeed)/3 );
    } catch (e) {
    }

  }

  arrowsManager(snapshots, index);


}



///////////////////////////////////////////////////
//function that carries out the actual move forward
function carryOutMoveForward( index, continuous ){

  if( $("#playState").html()=="stopped" ){
    actualSnapshotIndex = 1
    return
  }

  snapshots = actualPlay.snapshots;

  initialPositions = snapshots[ index ].moves;
  ballMoved = false;
  ballTopPos = 0;
  ballLeftPos = 0;
  durationAnimation = 0;
  startPassAnimationLater = false;
  ballPositionsForPass = 0;

  processSnapshotDisplay()

  //iterating through all moves in the snapshot
  for( j=0; j<initialPositions.length; j++ ){

    positions = initialPositions[j].positions
    playerPosition = initialPositions[j].position
    playerID = $(".position"+playerPosition).attr("id")

    // Test create arrow when playing play
    if( isPassSnapshot(snapshots, index)==false || playerPosition != "ball"){
      arrowPoints = getBezierPoints( positions, playerID);
    }

    var pos = [0,0,0,0]
    for( var k=0; k<4;k++ ){
      pos[k] = fieldPos2displayPos( arrowPoints[k][0], arrowPoints[k][1], getCurrentLayout() );
      writeConsoleLog("carryOutMoveForward PlayerId: "+playerID+" index: "+index+" fieldPos   top["+k+"]: " + arrowPoints[k][0] + "   left["+k+"]:  " + arrowPoints[k][1] + "  layout: " + getCurrentLayout() )
      writeConsoleLog("carryOutMoveForward PlayerId: "+playerID+" index: "+index+" diplayPos   top["+k+"]: " + pos[k].top + "   left["+k+"]:  " + pos[k].left)
    }

    //creating the player if he comes from a part of the field which is not visible
    if( playerID == undefined && playerPosition!="ball"){
      playerID = createPlayerFromHiddenPlace( playerPosition, arrowPoints, 3, index)
    }else{
      console.log("__"+snapshots[ index ].playerWithBall)
      hidePlayerWhenGoingOutOfTheField( playerPosition, arrowPoints, 3, index )
    }

    //Updating position
    setPlayerValuePerID(playerID, "fieldPosX", arrowPoints[3][0]);
    setPlayerValuePerID(playerID, "fieldPosY", arrowPoints[3][1]);

    //creating the animators
    var bezier_params = {
      start: {
        x: pos[0].left,
        y: pos[0].top,
        x1: pos[1].left,
        y1: pos[1].top,
        x2: pos[2].left,
        y2: pos[2].top,
        angle: 100
      },
      end: {
        x:pos[3].left,
        y:pos[3].top,
        angle: -100,
        length: 0.3
      }
    }

    writeConsoleLog("BezierPath for PlayerId: "+playerID+"  set to start: {x:"+pos[0].left+", y:"+pos[0].top+"  end:   {x:"+pos[3].left+", y:"+pos[3].top);

    if( playerPosition != "ball"){
      //for player other than the player with the ball
      if( game.playerWithBall != playerPosition ){
        $( ".position" + playerPosition ).animate({
          path: new $.path.bezier(bezier_params)
        }, parseInt(GAME_SETTINGS.playSpeed));
        writeConsoleLog("BezierPath Animation for PlayerId: "+playerID);
      }
      //for the player with the ball if it is not a pass snapshot
      else if( game.playerWithBall == playerPosition &&  isPassSnapshot(snapshots, index) == false ){
        writeConsoleLog( "playerWithTheBall:  " +  snapshots[ index ].playerWithBall )
        $( ".position" + playerPosition ).animate({
          path: new $.path.bezier(bezier_params)
        }, parseInt(GAME_SETTINGS.playSpeed));
        $( ".ball" ).animate({
          path: new $.path.bezier(bezier_params)
        }, parseInt(GAME_SETTINGS.playSpeed));
        writeConsoleLog("BezierPath Animation player with ball for not a pass snapshot for PlayerId: "+playerID +" With speed: "+parseInt(GAME_SETTINGS.playSpeed));
      }
      //for the player with the ball in a pass snapshot
      else{
        durationAnimation += (parseInt(GAME_SETTINGS.playSpeed));
        $( ".position" + playerPosition ).animate({
          path: new $.path.bezier(bezier_params)
        }, parseInt(GAME_SETTINGS.playSpeed));
        writeConsoleLog("BezierPath Animation player with ball in a pass snapshot for PlayerId: "+playerID);
      }

      //checking if player goes out of bounds
      var teamFromID = getTeam( playerID, playerPosition )
      teamFromID = reverseID(teamFromID)
      if( !playerFitsInScreen( arrowPoints[3][1], getCurrentLayout() )){
        $("#ArrowOutOfSight"+teamFromID+playerPosition).remove()
        createArrow( teamFromID , getCurrentLayout() , playerPosition, arrowPoints[3][0]  )
      }else{
        $("#ArrowOutOfSight"+teamFromID+playerPosition).remove()
      }
      console.log("test: \n\n")

    }

  }


  //for the ball in a pass snapshot
  if( isPassSnapshot(snapshots, index) ){
    id = game.playerWithBall
    offset=0

    var playerWithBall_old = snapshots[index-1].playerWithBall
    var playerWithBall_aux = snapshots[index].playerWithBall

    // obtaining the id of the passer player
    var id = $(".position"+playerWithBall_old).attr("id")
    ballPositionsForPass = []

    //try catch is necessary for cases in which players are out of bounds and we cannot access the positions nicely
    try{
      posLeftSender = $("#"+id).position().left;
      posTopSender = $("#"+id).position().top;
      pos_aux = displayPos2fieldPos( posLeftSender, posTopSender, getCurrentLayout());
      ballTopPos = pos_aux.top;
      ballLeftPos = pos_aux.left;
      ballPositionsForPass.push( ballLeftPos + "&&" + ballTopPos )
    }catch{
    }


    //finding the target and saving the ball target positions in variables
    for(i=0;i<snapshots[index].moves.length;i++){

      if( snapshots[index].moves[i].position != playerWithBall_aux ){
        continue
      }
      //pos_aux = $(".position"+playerWithBall_aux).position()
      //pos_aux = displayPos2fieldPos( ballLeftPos, ballTopPos, getCurrentLayout());
      //ballPositionsForPass.push( pos_aux.left + "&&" + pos_aux.top )
      pos_aux = snapshots[index].moves[i].positions[ snapshots[index].moves[i].positions.length-1 ]
      ballPositionsForPass.push( pos_aux )
      pos_aux = fieldPos2displayPos( pos_aux.split("&&")[0], pos_aux.split("&&")[1], getCurrentLayout());
      ballTopPos = pos_aux.top;
      ballLeftPos = pos_aux.left;
      break
    }

    writeConsoleLog("\n\n\n")
    writeConsoleLog(ballPositionsForPass)

    //changing the value of the variables to later trigger the animation and the arrow
    startPassAnimationLater = true;

  }


  //creating the arrows up to this point
  if( startPassAnimationLater==false){
    arrowsManager( snapshots, index+1)
  }


  //calling the ball passing animation if necessary
  if( startPassAnimationLater==true){
    setTimeout (function (){
      $( ".ball" ).animate({
        top: ballTopPos,
        left: ballLeftPos - offset
      }, parseInt(GAME_SETTINGS.playSpeed)/2)
      arrowsManager( snapshots, index+1)
    }, parseInt(durationAnimation/2));
  }



  ///////////////////////////////////////////////////////////////////////////
  // CODE THAT BELONGS TO THE CONTINUOUS PLAYING MODE
  ////////////////////////////////////////////////////////////////////////////
  //if we are in continuous mode, we call the next spashot after some delay
  if( continuous!=null && continuous==true){

    //if pause button is pressed, continuous mode is paused
    if( $("#playState").html() == "paused"){
      actualSnapshotIndex = index+1
      return;
    }else if( $("#playState").html() == "stopped" ){
      actualSnapshotIndex = 1
      return;
    }

    //counting the number of positions taking place in this snapshot
    var numberOfMoves = 0;
    currentMoves = snapshots[ index ].moves

    for( k=0; k<currentMoves.length; k++ ){
      positions = currentMoves[k].positions
      //adding 4 moves for every player that performs an actual move
      if(positions.length>=2){
        numberOfMoves = numberOfMoves + 4
      }
    }
    //adding two more beacause of the ball
    numberOfMoves+=2

    //setting a minimum number of moves
    if(numberOfMoves<6){
      numberOfMoves=6
    }

    //setting the right parameters and calling the next snapshot after the delay
    setTimeout (function (){

      actualSnapshotIndex = index+1

      if( $("#playState").html()=="paused" || $("#playState").html()=="stopped" ){
        return
      }

      if( actualSnapshotIndex < snapshots.length-1 ){
        playerWithBall = game.playerWithBall
        playerWithBall = snapshots[ index ].playerWithBall
        game.playerWithBall = playerWithBall
        carryOutMoveForward( actualSnapshotIndex, true )
      }
      //changing the state back to openend when the play is finished
      if( actualSnapshotIndex == snapshots.length-1  ){
        $("#playState").html("opened")
        $("#buttonPause").hide();
        $("#buttonPlay").show();
      }
    }, (1.2)*parseInt(GAME_SETTINGS.playSpeed));
    //}, numberOfMoves*parseInt(GAME_SETTINGS.playSpeed)+10);
    //}, parseInt(numberOfMoves/2)*parseInt(0.5*GAME_SETTINGS.playSpeed));

  }

}



//////////////////////////////////////////////////////////
//method that returns if the actual one is a pass snapshot
function isPassSnapshot( snapshots , actualSnapshotIndex){

  actualPlayerWithBall = snapshots[actualSnapshotIndex-1].playerWithBall
  nextPlayerWithBall = snapshots[actualSnapshotIndex].playerWithBall

  if(nextPlayerWithBall!=actualPlayerWithBall){
    return true
  }else{
    return false
  }

}



/////////////////////////////////////////////////////
//function that waits for a given time of miliseconds
function wait(ms){
  var d = new Date();
  var d2 = null;
  do { d2 = new Date(); }
  while(d2-d < ms);
}



/////////////////////////////////////////////////////
function loadTactic( tacticId ){

  //removing arrows and changing the state for the app and for the buttons
  removePlayerArrows()
  $("#playState").html("opened")
  $("#buttonPause").hide();
  $("#buttonPlay").show();
  actualSnapshotIndex = 1
  setState("Playing");

  tacticName = tacticId.split('---').join(' ');
  tactic = ""

  //getting the tactic from the array with all tactics
  for( i=0; i < allPlays.length; i++ ){
    if( allPlays[i].playName == tacticName ){
      tactic = allPlays[i]
      actualPlay = allPlays[i]
      writeConsoleLog("loadTactic -->allPlays[i].offensiveStrategy:  "+allPlays[i].offensiveStrategy);
    }
  }

  //in case the tactic was not found, we give an error
  if( tactic == "" ){
    alert("An error occured while opening the tactic")
    return
  }

  snapshots = tactic.snapshots
  numberSnapshots = snapshots.length
  writeConsoleLog( "loadTactic -->"+tactic.playName )
  writeConsoleLog( "loadTactic -->"+snapshots[0].playName )
  writeConsoleLog("###### ")

  //setting the min and max to the snapshot display
  $('#snapshotDisplay').attr('min', 1);
  $('#snapshotDisplay').attr('max', numberSnapshots-1);
  $('#snapshotDisplay').val(1);

  var c = document.getElementById("courtCanvas");
  var ctx = c.getContext("2d");
  ctx.canvas.width = ctx.canvas.width;

  //setting the first position
  initialPositions = snapshots[0].moves
  writeConsoleLog( initialPositions )
  writeConsoleLog( initialPositions.length + " moves on the first snapshot" )
  writeConsoleLog(snapshots)

  var ballPosLeft, ballPosTop

  //load every player in the position
  for( k=0 ; k<initialPositions.length ; k++ ){
    //getting the data about the current element
    //number = initialPositions[k].number
    //team = initialPositions[k].team
    // writeConsoleLog(number)
    position = initialPositions[k].position
    setPosition = initialPositions[k].positions[0]
    setPositionLeft = setPosition.split("&&")[0]
    setPositionTop = setPosition.split("&&")[1]
    writeConsoleLog("loadTactic --> iteration: " + k + "  "  + position + "  " + setPositionLeft + "  " + setPositionTop )

    //checking if it is the ball or the player
    if( position[0]=="O" ){
      team = game.offensiveTeam
    }else{
      team = getDefensiveTeam()
    }
    setPlayerValuePerID(game[team].courtPlayers[position], "fieldPosX", setPositionLeft);
    setPlayerValuePerID(game[team].courtPlayers[position], "fieldPosY", setPositionTop);
    writeConsoleLog("loadTactic --> game[team].courtPlayers[position]: "+game[team].courtPlayers[position])
    // setPlayer(team, position, [setPositionTop, setPositionLeft], getCurrentLayout());

    // setPlayerValuePerPosition(undefined, position, "fieldPosY", setPositionTop);
    // setPlayerValuePerPosition(undefined, position, "fieldPosX", setPositionLeft);

    //asign the ball to the corresponding player
    // if( snapshots[0].playerWithBall == position ){
    //   game.playerWithBall = position
    //   ballPosTop = setPositionTop
    //   ballPosLeft = setPositionLeft
    // }

  }

  writeConsoleLog("loadTactic -->tactic.offensiveStrategy:  "+tactic.offensiveStrategy);
  writeConsoleLog("loadTactic -->tactic.playName:  "+tactic.playName);
  game[game.offensiveTeam].offensiveTeam = tactic.offensiveStrategy;
  game[getDefensiveTeam()].defensiveTeam = tactic.defensiveStrategy;
  setTeam(game.offensiveTeam, tactic.offensiveStrategy, getCurrentLayout(), false); // Temporary solution
  setTeam(getDefensiveTeam(), tactic.defensiveStrategy, getCurrentLayout(), false); // Temporary solution

  game.playerWithBall = snapshots[0].playerWithBall;
  SetBall(false, getCurrentLayout());

  actualSnapshotIndex = 1
  $("#modeHidden").html("normal")
  $("#playState").html("opened")
  processSnapshotDisplay()
  // setOrientation();

}



////////////////////////////////////////
//method that initializes a new snapshot
function newSnapshot(){

  lastSnapshotUndone = false
  writeConsoleLog("Creating a new snapshot")

  //appending the old moves to the snapshot, and the snapshot to the play
  actualSnapshot.moves = actualMoves;
  snapshotNumber = (actualPlay.snapshots).length

  writeConsoleLog(actualPlay.snapshots)

  moves = []
  playName = actualPlay.playName


  //we iterate for all players and ball
  $(".players").each(function() {  //CHANGE

    //we get the position and convert it to fieldPos
    //var actualTop = parseFloat($(this).css('top'))
    //var actualLeft = parseFloat($(this).css('left'))
    //pos = convert2Field( actualLeft, actualTop )
    var pos = {}
    playerId = $(this).attr("id")
    pos.left = getPlayerValuePerID( playerId, "fieldPosX" )
    pos.top = getPlayerValuePerID( playerId, "fieldPosY")

    //creating a move for the player, which corresponds with its initial position
    player = $(this).attr("id")
    playerName = $(this).attr("id")

    positions = []
    positions.push(pos.left + "&&" +  pos.top)


    if( game.playerWithBall == getPositionOfPlayerPerId(player) ){
      playerWithBall = game.playerWithBall
    }


    position = ""
    classes = $(this).attr("class").split(' ');
    for( j=0; j<classes.length; j++ ){
      if( classes[j].includes("position") ){
        position = (classes[j]).substring(8, classes[j].length )
      }
    }
    if( position == "" ){
      //writeConsoleLog("New Initial Position:  " + pos.left + "   " + pos.top)
      position = "ball"
    }


    writeConsoleLog("Snapshot:  " + (actualSnapshot.snapshotNumber) + "   Move corresponds to: " + snapshotNumber)
    move = new Move( position, playerName, snapshotNumber + 1, positions )
    moves.push( move )

    processSnapshotDisplay()

  });


  // we add the players out of the field which have been removed
  // for example, the OGK
  if( moves.length<actualPlay.snapshots[snapshotNumber-1].moves.length ){

    var prev_snapshot_moves = actualPlay.snapshots[0].moves;
    var already_found;

    for(var i=0;i<prev_snapshot_moves.length;i++){
      already_found = false
      for(var j=0;j<moves.length;j++){
        if( moves[j].position==prev_snapshot_moves[i].position ){
          already_found = true
          break
        }
      }

      // adding the missong player
      if( already_found==false ){
        moves.push( prev_snapshot_moves[i] )
      }
    }
  }


  //initializing the next snapshot
  actualMoves = moves
  actualSnapshot.playerWithBall = playerWithBall

  actualPlay.snapshots.push(actualSnapshot)
  writeConsoleLog( "pushed snapshot #" + snapshotNumber )

  snapshotNumber = (actualPlay.snapshots).length

  writeConsoleLog( "now working on snapshot #" + snapshotNumber )

  actualSnapshot = new Snapshot( snapshotNumber, playName, playerWithBall, moves )

}



//////////////////////////////////////////////
//method that returns the Bezier anchor points
function getBezierPoints( arrowPositions, playerId ){

  initTop = arrowPositions[0].split("&&")[1]
  initLeft = arrowPositions[0].split("&&")[0]
  secondPointTop = arrowPositions[parseInt(0.33*(arrowPositions.length-1))].split("&&")[1]
  secondPointLeft = arrowPositions[parseInt(0.33*(arrowPositions.length-1))].split("&&")[0]
  thirdPointTop = arrowPositions[parseInt(0.66*(arrowPositions.length-1))].split("&&")[1]
  thirdPointLeft = arrowPositions[parseInt(0.66*(arrowPositions.length-1))].split("&&")[0]
  targetTop = arrowPositions[arrowPositions.length-1].split("&&")[1]
  targetLeft = arrowPositions[arrowPositions.length-1].split("&&")[0]

  return [ [initLeft, initTop], [secondPointLeft, secondPointTop], [thirdPointLeft, thirdPointTop], [targetLeft, targetTop] ]


}



///////////////////////////////////////////////////
//method that creates the arrow for player movement
function createArrowMove( arrowPositions, playerId, playerPosition){

    //if the checkbox "show traces" is deactivates, we do not draw the lines
    if( GAME_SETTINGS.showTraces == "false" ){
      return
    }

    ////////////////////////////////////
    //// Test with bezier lines
    initTop = arrowPositions[0].split("&&")[1]
    initLeft = arrowPositions[0].split("&&")[0]
    secondPointTop = arrowPositions[parseInt(0.33*(arrowPositions.length-1))].split("&&")[1]
    secondPointLeft = arrowPositions[parseInt(0.33*(arrowPositions.length-1))].split("&&")[0]
    thirdPointTop = arrowPositions[parseInt(0.66*(arrowPositions.length-1))].split("&&")[1]
    thirdPointLeft = arrowPositions[parseInt(0.66*(arrowPositions.length-1))].split("&&")[0]
    targetTop = arrowPositions[arrowPositions.length-1].split("&&")[1]
    targetLeft = arrowPositions[arrowPositions.length-1].split("&&")[0]

    // initPos = convert2Display( initLeft, initTop )
    // secondPos = convert2Display(secondPointLeft, secondPointTop)
    // thirdPos = convert2Display(thirdPointLeft, thirdPointTop)
    // targetPos = convert2Display( targetLeft, targetTop )

    initPos = fieldPos2displayPos( initLeft, initTop, getCurrentLayout());
    secondPos = fieldPos2displayPos(secondPointLeft, secondPointTop, getCurrentLayout());
    thirdPos = fieldPos2displayPos(thirdPointLeft, thirdPointTop, getCurrentLayout());
    targetPos = fieldPos2displayPos( targetLeft, targetTop, getCurrentLayout());

    initArrowPosLeft = initPos.left + $(".players").height()/2;
    secondArrowPosLeft = secondPos.left + $(".players").height()/2;
    thirdArrowPosLeft = thirdPos.left + $(".players").height()/2;
    targetArrowPosLeft = targetPos.left + $(".players").height()/2;

    initArrowPosTop = initPos.top + $(".players").height()/2;
    secondArrowPosTop = secondPos.top + $(".players").height()/2;
    thirdArrowPosTop = thirdPos.top + $(".players").height()/2;
    targetArrowPosTop = targetPos.top + $(".players").height()/2;

    var c = document.getElementById("courtCanvas");
    var ctx = c.getContext("2d");

    //if( initTop>60 && secondPointTop>60 && thirdPointTop>60 && targetTop>60  ){
    //  return [ [initLeft, initTop], [secondPointLeft, secondPointTop], [thirdPointLeft, thirdPointTop], [targetLeft, targetTop] ]
    //}

    ctx.beginPath()
    ctx.moveTo(parseInt(initArrowPosLeft), parseInt(initArrowPosTop));
    ctx.bezierCurveTo(parseInt(secondArrowPosLeft), parseInt(secondArrowPosTop), parseInt(thirdArrowPosLeft), parseInt(thirdArrowPosTop), parseInt(targetArrowPosLeft), parseInt(targetArrowPosTop));
    ctx.lineTo(parseInt(targetArrowPosLeft), parseInt(targetArrowPosTop));

    writeConsoleLog("\n\n\n")
    writeConsoleLog( playerId )
    writeConsoleLog( [ [initLeft, initTop], [secondPointLeft, secondPointTop], [thirdPointLeft, thirdPointTop], [targetLeft, targetTop] ] )
    writeConsoleLog("\n\n\n")


    var team = getTeam( playerId )
    var lineColor


    if(playerId == undefined){
      if(playerPosition.substring(0,1)=="O"){
        team = "teamA"
        team = reverseID(team)
      }else if(playerPosition.substring(0,1)=="D"){
        team = "teamB"
        team = reverseID(team)
      }
    }

    // screen factor that adjusts for the responsivenes of the lines
    var screen_factor = 0
    var init_arrow_factor = 20
    if(screen.width > 800){
      screen_factor = Math.ceil(GAME_SETTINGS.lineThickness/2)
      init_arrow_factor = 30
    }else if(screen.width < 500){
      screen_factor = -Math.ceil(GAME_SETTINGS.lineThickness/2)
      init_arrow_factor = 10
    }

    lineThickness = parseInt(GAME_SETTINGS.lineThickness) + parseInt(screen_factor)
    ballLineThickness = Math.ceil( parseInt(lineThickness)/2 )

    if( team == "teamA"){
      ctx.setLineDash([]);
      ctx.lineWidth = lineThickness;
      lineColor = game.teamA.lineColorA;
    }
    else if( team == "teamB" ){
      ctx.setLineDash([]);
      ctx.lineWidth = lineThickness;
      lineColor = game.teamB.lineColorA;
    }
    else{
      ctx.setLineDash([15, 15]);
      ctx.lineWidth = ballLineThickness;
      lineColor = "black";
      //lineColor = game.teamA.lineColorA;
    }
    ctx.strokeStyle = lineColor
    ctx.stroke();

    //small cheat not to draw extra arrows on the ball from the arrowsManager function
    if(arrowPositions.length < 2 || GAME_SETTINGS.showTraces == "false"){
      return [ [initLeft, initTop], [secondPointLeft, secondPointTop], [thirdPointLeft, thirdPointTop], [targetLeft, targetTop] ]
    }

    //adding the arrow head to the arrow
    var triangleWidth = ( targetArrowPosLeft - thirdArrowPosLeft )
    var triangleHeight = ( targetArrowPosTop - thirdArrowPosTop )
    var angle = Math.atan2( triangleHeight, triangleWidth )

    var arrow_factor = init_arrow_factor + lineThickness

		ctx.beginPath()
		ctx.moveTo( targetArrowPosLeft + arrow_factor*Math.cos(angle)*0.5, targetArrowPosTop + arrow_factor*Math.sin(angle)*0.5)
		ctx.lineTo( targetArrowPosLeft - arrow_factor*Math.cos(angle - Math.PI/4) , targetArrowPosTop - arrow_factor*Math.sin(angle - Math.PI/4))
    ctx.lineTo( targetArrowPosLeft - arrow_factor*Math.cos(angle + Math.PI/4) , targetArrowPosTop - arrow_factor*Math.sin(angle + Math.PI/4))
		ctx.closePath();
		ctx.fillStyle  = lineColor
		ctx.fill();

    return [ [initLeft, initTop], [secondPointLeft, secondPointTop], [thirdPointLeft, thirdPointTop], [targetLeft, targetTop] ]

}



/////////////////////////////////////
//function that converts to field pos
function convert2Field( actualLeft, actualTop ){

  console.log("Initial log Convert2Field")

  if( $("#rotationHidden").html() == "HalfVert" && $("#orientationHidden").html() == "0" && $("#fieldTypeHidden").html() == "Half"){
    pos = displayPos2fieldPos( actualLeft, actualTop, "HalfVertOffense" )
  }
  else if(  $("#rotationHidden").html() == "HalfVert" && $("#orientationHidden").html() == "180" && $("#fieldTypeHidden").html() == "Half"){
    pos = displayPos2fieldPos( actualLeft, actualTop, "HalfVertDefense" )
  }
  else if(  $("#rotationHidden").html() == "HalfVert" && $("#orientationHidden").html() == "0" && $("#fieldTypeHidden").html() == "Full"){
    pos = displayPos2fieldPos( actualLeft, actualTop, "FullOffense" )
  }
  else if(  $("#rotationHidden").html() == "HalfVert" && $("#orientationHidden").html() == "180" && $("#fieldTypeHidden").html() == "Full"){
    pos = displayPos2fieldPos( actualLeft, actualTop, "FullDefense" )
  }
  else if(  $("#rotationHidden").html() == "HalfHoriz" && $("#orientationHidden").html() == "0" && $("#fieldTypeHidden").html() == "Half"){
    pos = displayPos2fieldPos( actualLeft, actualTop, "HorizOffense" )
  }
  else if(  $("#rotationHidden").html() == "HalfHoriz" && $("#orientationHidden").html() == "180" && $("#fieldTypeHidden").html() == "Half"){
    pos = displayPos2fieldPos( actualLeft, actualTop, "HorizDefense" )
  }

  console.log("Log log")

  return {
          top: pos.top,
          left: pos.left
      };

}



/////////////////////////////////////
//function that converts to dispaly pos
function convert2Display( actualLeft, actualTop ){

  if( $("#rotationHidden").html() == "HalfVert" && $("#orientationHidden").html() == "0" && $("#fieldTypeHidden").html() == "Half"){
    pos = fieldPos2displayPos( actualLeft, actualTop, "halfVertOffense" )
  }
  else if(  $("#rotationHidden").html() == "HalfVert" && $("#orientationHidden").html() == "180" && $("#fieldTypeHidden").html() == "Half"){
    pos = fieldPos2displayPos( actualLeft, actualTop, "halfVertDefense" )
  }
  else if(  $("#rotationHidden").html() == "HalfVert" && $("#orientationHidden").html() == "0" && $("#fieldTypeHidden").html() == "Full"){
    pos = fieldPos2displayPos( actualLeft, actualTop, "fullOffense" )
  }
  else if(  $("#rotationHidden").html() == "HalfVert" && $("#orientationHidden").html() == "180" && $("#fieldTypeHidden").html() == "Full"){
    pos = fieldPos2displayPos( actualLeft, actualTop, "fullDefense" )
  }
  else if(  $("#rotationHidden").html() == "HalfHoriz" && $("#orientationHidden").html() == "0" && $("#fieldTypeHidden").html() == "Half"){
    pos = fieldPos2displayPos( actualLeft, actualTop, "horizOffense" )
  }
  else if(  $("#rotationHidden").html() == "HalfHoriz" && $("#orientationHidden").html() == "180" && $("#fieldTypeHidden").html() == "Half"){
    pos = fieldPos2displayPos( actualLeft, actualTop, "horizDefense" )
  }

  return {
          top: pos.top,
          left: pos.left
      };

}



/////////////////////////////////////
//function that asigns the ball
function asignBall( actualLeft, actualTop ){

  pos = fieldPos2displayPos( actualLeft, actualTop, getCurrentLayout() );

  $(".court").append("<h1 class='ball rotates' id='ball' </h1> ");
  $(".ball").css("top", pos.top);
  $(".ball").css("left", pos.left);


}



/////////////////////////////////////////////
//method that erases all arrows on the screen
function resetCanvas(){

  //  Erase current canvas
  // writeConsoleLog("Removing arrows!!!")
  console.log("Removing arrows!!!")
  var c = document.getElementById("courtCanvas");
  var ctx = c.getContext("2d");
  ctx.canvas.width = ctx.canvas.width;

}



/////////////////////////////////////////////////////////////////
// function that draws all arrows till the given screenshot index
function arrowsManager(snapshots, positionIndex){

  // Erase current canvas
  // resetCanvas()

  //if( $("#playState").html() == "edit" ){
  //  return
  //}

  //obtaining the starting point to draw arrows based on the memoryArrows parameter
  var memory = GAME_SETTINGS.memoryArrows;
  var starting_point;
  if(parseInt(memory)==9 || memory>=positionIndex ){
    starting_point = 1
  }else{
    starting_point = positionIndex - GAME_SETTINGS.memoryArrows
  }

  console.log("start: " + starting_point)
  console.log("end: " + positionIndex)

  //iterating snapshots and moves to create the arrows up to the point
  for ( i=starting_point; i < positionIndex; i++) {

    var previousPlayerWithBallPos = ""
    var actualPlayerWithBallPos = ""

    for( j=0; j<snapshots[i].moves.length; j++ ){

      var playerId = $(".position"+snapshots[i].moves[j].position).attr("id")
      var playerPosition = snapshots[i].moves[j].position
      var team = getTeam( playerId )

      //getting the postions of sender and receiver to make the arrow for the pass
      if( i>0 && snapshots[ i-1 ].playerWithBall == snapshots[i].moves[j].position ){
        previousPlayerWithBallPos = snapshots[i].moves[j].positions[0]
      }else if(i==0){
        var posLeftSender = $("#ball").position().left;
        var posTopSender = $("#ball").position().top;
        var pos_aux = displayPos2fieldPos( posLeftSender, posTopSender, getCurrentLayout());
        var ballTopPos = pos_aux.top;
        var ballLeftPos = pos_aux.left;
        previousPlayerWithBallPos = ballLeftPos + "&&" + ballTopPos
      }
      if( snapshots[ i ].playerWithBall == snapshots[i].moves[j].position ){
        actualPlayerWithBallPos = snapshots[i].moves[j].positions[ snapshots[i].moves[j].positions.length-1 ]
      }

      //skipping cases where players do not move
      if( snapshots[i].moves[j].positions.length <= 2 && snapshots[i].moves[j].position != "ball" ){
        continue
      }
      //creating the arrow otherwise
      else{
        console.log("__playerID: " + playerId)
        createArrowMove(snapshots[i].moves[j].positions, playerId, playerPosition);
      }
    }

    //arrow for the pass
    if( isPassSnapshot(snapshots, i) ){

      var ballPositionsForPass = ["",""]
      ballPositionsForPass[0] = previousPlayerWithBallPos
      ballPositionsForPass[1] = actualPlayerWithBallPos

      createArrowMove(ballPositionsForPass, "ball", "-");

    }

  }
}



///////////////////////////////////////////////////////77
//function that shows the modal that allows the user to modify name, type or delete of tactic
function editPlayMenu( event ){

  // obtaining the name of the tactic to edit given the pressed button. Depending on when the user
  // presses, event.target corresponds to a different div
  playName = event.target.id
  if(playName.length==0){
    playName = event.target.parentNode.parentNode.id
  }
  playName = playName.split('---').join(' ');


  //finding the type of the play
  for( i=0;i<allPlays.length;i++ ){
    //writeConsoleLog( allPlays[i].playName + "   " + playName )
    if( allPlays[i].playName == playName ){
      playType = allPlays[i].playType
      favorite = allPlays[i].favorite
    }
  }

  writeConsoleLog(playType)
  writeConsoleLog(playName)

  //hiding the open play popup from the screen
  $('#openPlayPopup').modal('hide');

  //giving the correct information to the edit play popup
  $("#editPlayModalTitle").text( "Edit play: " +  playName );
  $("#inputEditPlayName").val( playName )

  if( playType == "all" ){
    $('#inlineRadio3Edit').prop('checked', true)
  }else if( playType == "defense"  ){
    $('inlineRadio2Edit').prop('checked', true)
  }else{
    $('#inlineRadio1Edit').prop('checked', true)
  }

  //setting the text of the favorite button
  if( favorite == "true" ){
    $("#addTacticToFavorites").html("Remove from Favorites")
  }else{
    $("#addTacticToFavorites").html("Add to Favorites")
  }

  //finally showing the edit play popup on the screen
  $("#infoEditPlay").html("")
  $('#editPlayPopup').modal('show');


}



//////////////////////////////////////////////////////////////////////////
// method that creates the player if he does not previously exist
function createPlayerFromHiddenPlace( position, coordinates, idx, snapshot_index){

  var playerID = undefined
  var targetLayout = getCurrentLayout()

  //esto es horrible pero no se me ocurrio otra cosa
  //getting the team of the player to set
  var team
  if( position[0]=="O" ){
    team = "teamA"
    team = reverseID(team)
  }else{
    team = "teamB"
    team = reverseID(team)
  }

  //check if the player should be added or if he remains hidden
  var coordinateY = coordinates[idx][1]
  var playerFits = playerFitsInScreen(coordinateY, targetLayout)
  //creating the player
  if( playerFits==true){

    if( targetLayout.includes("HalfHoriz") ){
      maxY = "42"
    }else if( targetLayout.includes("HalfVert") ){
      maxY = "58"
    }

    //adjusting the Y coordinate to be inside the field
    for( var k=0;k<coordinates.length;k++){
      if(coordinates[k][1]>maxY){
        coordinates[k][1] = maxY
      }
    }

    setPlayer( team, position, [maxY,coordinates[0][0]], targetLayout )
    //writeConsoleLog(coordinates)
    playerID = $(".position"+position).attr("id")
    //writeConsoleLog(playerID)

    if(snapshots[snapshot_index].playerWithBall==position){
      writeConsoleLog( "__deleting ball as well")
      SetBall(false, getCurrentLayout());
    }

  }

  return playerID

}



//////////////////////////////////////////////////////////////////////////
// method that deletes a player if he leaves the visible field
function hidePlayerWhenGoingOutOfTheField( position, coordinates, idx, snapshot_index){

  var targetLayout = getCurrentLayout()

  //check if the player should be added or if he remains hidden
  var coordinateY = coordinates[idx][1]
  var playerFits = playerFitsInScreen(coordinateY, targetLayout)

  writeConsoleLog("position:  " + position)
  writeConsoleLog("coordinates:  " + coordinates[idx] )
  writeConsoleLog("playerFits:  " + playerFits)

  //we only delelte if idx=3 so the player makes the animation before being removed
  if( playerFits==false){
    writeConsoleLog( "__deleting player:  " + position )
    if(snapshots[snapshot_index].playerWithBall==position){
      writeConsoleLog( "__deleting ball as well")
      RemoveBall()
    }
    deletePlayer(position)
  }

  return
}


//////////////////////////////////////////////////////////////////////////
// method that parses tghe play name so there are only alphanumeric characters, _ and spaces
function parse_play_name(playName, element){

  // accepting only alphanumeric, spaces and underscores
  var regex = new RegExp("^[0-9a-zA-Z_\. ]+$");
  if( regex.test(playName)==false ){
    $(element).html("Only alphanumeric charactest, spaces and underscores are allowed")
    return false;
  }else{
    $(element).html("")
    return true
  }

}


//
