

//////////////////////////////////
// global variables
// var DEPTH = 3 // Depth for contact
// var BASCULACION =  3 // Max basculation
// var RISK = 1

var offensiveTeam = undefined
var defensiveTeam = undefined

// var anchorPoints = {}

var automaticDefenseMatrix = [];
var goalPosition = [];
var generalDefenseDepth = undefined;
var generalDefenseBasculation = undefined;
var generalMinPlayersDistance = undefined;

// To be called when starting App or switching Offensive/Defensive
function initAutomaticDefenseValues(){

  // 1. Init automaticDefenseMatrix

  automaticDefenseMatrix = [];

  var defPositions = getPositionsForSystem(game[getDefensiveTeam()].defensiveTeam);

  for(var i=0; i<defPositions.length; i++) {
    defPos = defPositions[i];
    automaticDefenseMatrix[i] = new Array(10);

    // Position 0: Defensive Player ID
    automaticDefenseMatrix[i][0]=game[getDefensiveTeam()].courtPlayers[defPos];

    // Position 1: Initial Position
    var posX = getPlayerValuePerID(game[getDefensiveTeam()].courtPlayers[defPos], "fieldPosX");// +offset;
    var posY = getPlayerValuePerID(game[getDefensiveTeam()].courtPlayers[defPos], "fieldPosY");
    automaticDefenseMatrix[i][1]=[posX, posY];

    // Position 2: true for DGK, false for else
    if(defPos == "DGK"){
      automaticDefenseMatrix[i][2]= true;
    }
    else{
      automaticDefenseMatrix[i][2]= false;
    }

    // Position 3: Current Field Position (in this case, same as initial)
    automaticDefenseMatrix[i][3]=[posX, posY];

    // Position 4: ID of the defensive pair
    automaticDefenseMatrix[i][4]=undefined;

    // Position 5: Defensive Pair Field Position
    automaticDefenseMatrix[i][5]=undefined;

    // Position 6: Current WidthDepth Position
    automaticDefenseMatrix[i][6]=undefined;

    // Position 7: Defensive Pair WidthDepth Position
    automaticDefenseMatrix[i][7]=undefined;

    // Position 8: DesiredPosition
    automaticDefenseMatrix[i][8]=undefined;

    // Position 9: DesiredPositionAchievable
    automaticDefenseMatrix[i][9]=undefined;

    console.log("automaticDefenseMatrix["+i+"]: "+automaticDefenseMatrix[i]+"\n")
  }


  // 2. Init GeneralValues
  goalPosition = [50.00, 0.00]



}


function updateAutomaticDefense(){

  generalDefenseDepth = 1.5;
  generalDefenseBasculation = 2;
  generalMinPlayersDistance = 1;

  // 1: Define Movement specific variables
  var playerIDWithBall = getPlayerValuePerPosition(undefined, game.playerWithBall, "playerID");

  var ballPosition = [];
  ballPosition[0] = parseFloat(getPlayerValuePerID(playerIDWithBall, "fieldPosX"));
  ballPosition[1] = parseFloat(getPlayerValuePerID(playerIDWithBall, "fieldPosY"));

  var maxDefenseMovement = 1 // Demo value 1!!

  console.log("automaticDefenseMatrix --> Entered updateAutomaticDefense playerIDWithBall: "+playerIDWithBall+" ballPosition "+ballPosition+" goalPosition "+goalPosition)

  // 2: Calculate new automaticDefenseMatrix values

  var defensivePair = [undefined, 10019, 10018, 10020, 10017, 10016, 10015] // Demo values!!

  for(i=0; i<automaticDefenseMatrix.length; i++){


    // Position 3: Current Field Position
    defPosX = parseFloat(getPlayerValuePerID(automaticDefenseMatrix[i][0], "fieldPosX"));
    defPosY = parseFloat(getPlayerValuePerID(automaticDefenseMatrix[i][0], "fieldPosY"))
    automaticDefenseMatrix[i][3]=[defPosX,defPosY];


    // Write Position 4: ID of the defensive pair
    automaticDefenseMatrix[i][4] = defensivePair[i];

    // Write Position 5: Defensive Pair Position
    var offPosX = getPlayerValuePerID(defensivePair[i], "fieldPosX");
    var offPosY = getPlayerValuePerID(defensivePair[i], "fieldPosY");
    automaticDefenseMatrix[i][5] = [offPosX, offPosY];

    // Calculate Position 6: Current WidthDepth Position

    defPosWidthDepth = field2WidthDepth(automaticDefenseMatrix[i][3][0], automaticDefenseMatrix[i][3][1])
    automaticDefenseMatrix[i][6]=[defPosWidthDepth.width,defPosWidthDepth.depth];

    // Calculate Position 8: DesiredPosition
    if(automaticDefenseMatrix[i][2]){ // If DGK
      //console.log("automaticDefenseMatrix --> DGK ballPosition[0] "+ballPosition[0]+" ballPosition[1] "+ballPosition[1]);
      var ballPositionWidthDepth = field2WidthDepth(ballPosition[0], ballPosition[1])
      //console.log("automaticDefenseMatrix --> DGK ballPositionWidthDepth.width "+ballPositionWidthDepth.width);
      var desiredPos = WidthDepth2field(ballPositionWidthDepth.width, -5);
      automaticDefenseMatrix[i][8] = [desiredPos.posX, desiredPos.posY];
      //console.log("automaticDefenseMatrix --> DGK to ["+automaticDefenseMatrix[i][8][0]+","+automaticDefenseMatrix[i][8][1]+"]");

    }
    else if(automaticDefenseMatrix[i][4] == playerIDWithBall){ // pair has ball

      var initDefensivePlayerPos = field2WidthDepth(automaticDefenseMatrix[i][1][0], automaticDefenseMatrix[i][1][1])
      var offensivePlayerPos = field2WidthDepth(automaticDefenseMatrix[i][5][0], automaticDefenseMatrix[i][5][1])

      var width = offensivePlayerPos.width;
      if(width>9.5){
        width=9.5;
      }
      else if(width<-9.5){
        width = -9.5;
      }

      if(offensivePlayerPos.depth > generalDefenseDepth+generalMinPlayersDistance+initDefensivePlayerPos.depth){
        var depth = 2*initDefensivePlayerPos.depth+2*generalDefenseDepth-offensivePlayerPos.depth;
      }
      else{
        var depth = offensivePlayerPos.depth - generalMinPlayersDistance;
      }

      if(depth < 0.5){
        depth = 0.5
      }

      var desiredPos = WidthDepth2field(width,depth);
      console.log("AutomaticDefenseAlgorithm D"+i+" Pair w/ball: width: "+width+" depth: "+depth+" initDefensivePlayerPos.depth: "+initDefensivePlayerPos.depth)
      automaticDefenseMatrix[i][8] = [desiredPos.posX, desiredPos.posY];

    }
    else{ //pair has no ball
      var initDefensivePlayerPos = field2WidthDepth(automaticDefenseMatrix[i][1][0], automaticDefenseMatrix[i][1][1])
      var offensivePairPlayerPos = field2WidthDepth(automaticDefenseMatrix[i][5][0], automaticDefenseMatrix[i][5][1])
      var ballPositionWidthDepth = field2WidthDepth(ballPosition[0], ballPosition[1])

      var width = offensivePairPlayerPos.width + (ballPositionWidthDepth.width-offensivePairPlayerPos.width)*(2*generalDefenseBasculation/22); //22 is the total width, from one wing to the other

      if(width>9.5){
        width=9.5;
      }
      else if(width<-9.5){
        width = -9.5;
      }

      depth = initDefensivePlayerPos.depth;

      console.log("AutomaticDefenseAlgorithm D"+i+" Pair wo/ball: width: "+width+" depth: "+depth+" offensivePairPlayerPos.width: "+offensivePairPlayerPos.width+" ballPositionWidthDepth.width "+ballPositionWidthDepth.width)

      var desiredPos = WidthDepth2field(width,depth);
      automaticDefenseMatrix[i][8] = [desiredPos.posX, desiredPos.posY];

    }

    console.log("automaticDefenseMatrix["+i+"]: "+automaticDefenseMatrix[i][6]+"\n")


    if(automaticDefenseMatrix[i][8] != undefined){
      // Calculate Position 7: DesiredPositionAchievable
      automaticDefenseMatrix[i][9]=[automaticDefenseMatrix[i][8][0]*maxDefenseMovement, automaticDefenseMatrix[i][8][1]*maxDefenseMovement]

      // 3: Animate to new position
      console.log("automaticDefenseMatrix --> Move player"+automaticDefenseMatrix[i][0]+" to ["+automaticDefenseMatrix[i][9][0]+","+automaticDefenseMatrix[i][9][1]+"]");
      var displayPos = fieldPos2displayPos( automaticDefenseMatrix[i][9][0], automaticDefenseMatrix[i][9][1], getCurrentLayout());
      $("#"+automaticDefenseMatrix[i][0]).animate({
        top: displayPos.top,
        left: displayPos.left
      }, 200, "swing");
    }
  }

}


function field2WidthDepth(fieldPosX, fieldPosY){

  var fieldPosXCut1 = 42.5233644859813;
  var fieldPosXCut2 = 57.47663551;
  var elipseCenter1 = 8.5046729; // Meters
  var areaHeight = 6.02; // Meters
  var areaFlatWidth = 2.9906; // Meters

  var widthPos = undefined;
  var depthPos = undefined;

  if((fieldPosX > fieldPosXCut1) && (fieldPosX<fieldPosXCut2)){
    widthPos = (fieldPosX - parseFloat(50))*20/100;
    depthPos = (fieldPosY*parseFloat(20/50))-areaHeight;
  }
  else if(fieldPosX < fieldPosXCut1){
    var fieldPosXMeters = fieldPosX*20/100;
    var fieldPosYMeters = fieldPosY*20/50;
    var angle = Math.atan((elipseCenter1-fieldPosXMeters)/fieldPosYMeters);

    widthPos = -(areaFlatWidth/2)-angle*areaHeight;
    depthPos = Math.sqrt((fieldPosYMeters*fieldPosYMeters)+((elipseCenter1-fieldPosXMeters)*(elipseCenter1-fieldPosXMeters)))-areaHeight;

  }
  else if(fieldPosX > fieldPosXCut2){
    var fieldPosXMeters = fieldPosX*20/100;
    var fieldPosYMeters = fieldPosY*20/50;
    var angle = Math.atan((fieldPosXMeters-(20-elipseCenter1))/fieldPosYMeters);

    widthPos = (areaFlatWidth/2)+angle*areaHeight;
    depthPos = Math.sqrt((fieldPosYMeters*fieldPosYMeters)+((-(20-elipseCenter1)+fieldPosXMeters)*(-(20-elipseCenter1)+fieldPosXMeters)))-areaHeight;
  }

  // console.log("automaticDefenseMatrix --> field2WidthDepth(fieldPosX, fieldPosY) "+fieldPosX+" "+fieldPosY+" converted to: "+widthPos+" "+depthPos);

  return {
        width: widthPos,
        depth: depthPos
    };
}


function WidthDepth2field(width, depth){

  var widthCut1 = -1.4959;
  var widthCut2 = 1.4959;
  var elipseCenter1 = 8.5046729; // Meters
  var areaHeight = 6.02; // Meters
  var areaFlatWidth = 2.9906; // Meters

  var fieldPosX = undefined;
  var fieldPosY = undefined;

  if((width > widthCut1) && (width<widthCut2)){

    fieldPosX = 50+width*100/20;
    fieldPosY = (depth+areaHeight)*50/20;

    console.log("WidthDepth2field A: ["+width+","+depth+"] --> ["+fieldPosX+","+fieldPosY+"]");

  }
  else if(width < widthCut1){

    var angle =(-(areaFlatWidth/2)-width)/areaHeight; // (-(areaFlatWidth/2)-width)/areaHeight
    var aux = (Math.pow(depth+areaHeight, 2)/(1+(Math.pow(Math.tan(angle),2)))); // ((G34+D5)^2)/(1+(H39^2))
    var fieldPosYMeters = Math.sqrt(aux);
    var fieldPosXMeters = elipseCenter1-Math.tan(angle)*fieldPosYMeters

    fieldPosX = fieldPosXMeters*100/20;
    fieldPosY = fieldPosYMeters*50/20;
    console.log("WidthDepth2field B: ["+width+","+depth+"] --> angle: "+angle+" aux: "+aux+"--> ["+fieldPosX+","+fieldPosY+"]");

  }
  else if(width > widthCut2){
    var angle =(-(areaFlatWidth/2)+width)/areaHeight;
    var tan = Math.tan(angle);
    var aux1 = Math.pow(parseFloat(depth)+areaHeight, 2);
    var aux2 = (1+(Math.pow(tan,2)));
    var aux = aux1 / aux2; // ((G48+D5)^2)/(1+(G54^2))
    var fieldPosYMeters = Math.sqrt(aux);
    var fieldPosXMeters = Math.tan(angle)*fieldPosYMeters-elipseCenter1+20;

    fieldPosX = fieldPosXMeters*100/20;
    fieldPosY = fieldPosYMeters*50/20;

    console.log("WidthDepth2field C: ["+width+","+depth+"] --> angle: "+angle+" tan: "+tan+" aux1: "+aux1+" aux2: "+aux2+" aux: "+aux+"--> ["+fieldPosX+","+fieldPosY+"]");
  }

  return {
        posX: fieldPosX,
        posY: fieldPosY
    };
}


// ////////////////////////////////////////////////////////////////////
// //main method for the automatic positioning of the defensive players
// function updateAutomaticDefense(){

//   //obtaining the defensive team to iterate players
//   if( game.offensiveTeam=="teamA" ){
//     offensiveTeam = "teamA"
//     defensiveTeam = "teamB"
//   }else{
//     offensiveTeam = "teamB"
//     defensiveTeam = "teamA"
//   }

//   //iterating all defensive players to update their position
//   $("."+defensiveTeam).each(function() {

//  writeConsoleLog(amp)
//  writeConsoleLog(x)
//  writeConsoleLog(y)

//     //obtaining the players position and converting into the field coordinate space
//     var posX = getPlayerValuePerID( playerId, "fieldPosX" )
//     var posY = getPlayerValuePerID( playerId, "fieldPosY")
//     //convertig coordinates into polar representation
//     var polar = cartesianToPolar( posX, posY )
//     var amp = polar.amp
//     var phase = polar.phase

//  writeConsoleLog(realAmp)


//     //anchor point logic
//     var anchorPoint = getAnchorPoint( playerPosition )
//     var anchorPolar = cartesianToPolar( anchorPoint[1], anchorPoint[0] )
//     var anchorAmp = anchorPolar.amp
//     var anchorPhase = anchorPolar.phase

//     //case when the impar has the PassTheBall, so we move towards the anchor
//     if( game.playerWithBall!=parPosition ){

//       [newAmp, newPhase] = moveBackwards(amp, phase, anchorAmp, anchorPhase)

//     }

//     //case when the par has the ball, so we move towards him
//     else{

//       [newAmp, newPhase] = moveForward( amp, phase, parAmp, parPhase )

//     }



//     //converting back to cartesian and the to display position space
//     var cartesian = polarToCartesians( newAmp, newPhase )
//     var displayPos = fieldPos2displayPos( cartesian.x, cartesian.y, getCurrentLayout());

//     setPlayerValuePerID(playerId, "fieldPosX", cartesian.x);
//     setPlayerValuePerID(playerId, "fieldPosY", cartesian.y);


//     // adding the automatic movement to the snapshot
//     if( $("#modeHidden").html() == "recording" ){
//       for( i=0; i<actualMoves.length; i++ ){
//         if( actualMoves[i].position == playerPosition  ){
//           actualMoves[i].positions.push( cartesian.x+"&&"+cartesian.y )
//         }
//       }
//     }

//     //animating the player
//     $(this).animate({
//       top: displayPos.top,
//       left: displayPos.left
//     }, 200, "swing");


//   });


// }



// ///////////////////////////////////////////////////////////////////////
// function moveBackwards(amp, phase, anchorAmp, anchorPhase){

//   //amp = applyAmplitudeCorrection( amp,phase )
//   var radians = BASCULACION*(Math.PI/180)

//   //if we are really close to the anchor point, we do not we make a small step
//   if( amp-DEPTH < anchorAmp ){
//     newAmp = anchorAmp
//   }
//   //otherwise we make the usual step
//   else{
//     newAmp = amp - DEPTH
//   }

//   if( Math.abs(phase - anchorPhase) < radians ) {
//     newPhase = anchorPhase
//   }else if( phase > anchorPhase ){
//     newPhase = phase - radians
//   }else{
//     newPhase = phase + radians
//   }

//   return [newAmp, newPhase]

// }



// ///////////////////////////////////////////////////////////////////////
// function moveForward( amp, phase, parAmp, parPhase ){

//   //amp = applyAmplitudeCorrection( amp,phase )

//   //attacker has surpassed the defender, he moves backwards
//   if( amp>parAmp ){
//     newAmp = amp - DEPTH
//   }
//   //if the defender is next to the attacker, we do not update the y coordinate
//   else if( amp+DEPTH >= (parAmp-5) ){
//     newAmp = parAmp-2
//   }
//   //otherwise the defender moves towards the attacker
//   else{
//     newAmp = amp + DEPTH
//   }

//   //moving right towards the attacker
//   radians = BASCULACION*(Math.PI/180)

//   if( parPhase > phase+radians ){
//     newPhase = phase+radians
//   }
//   //moving right towards the attacker
//   else if( parPhase < phase-radians ){
//     newPhase = phase - radians
//   }else{
//     newPhase = parPhase
//   }

//   return [newAmp, newPhase]

// }



// ///////////////////////////////////////////////////////////////////////
// function applyAmplitudeCorrection( amp, phase ){

//   cartesian = polarToCartesians( amp, phase )
//   x = cartesian.x-50
//   y = cartesian.y

//   console.log(amp)
//   console.log(x)
//   console.log(y)

//   realAmp = Math.sqrt( x*x + (0.35*y)*(0.35*y) )

//   console.log(realAmp)


//   return realAmp

// }



// ////////////////////////////////////////////////////////////////////
// function initializeAnchorPoints(){

//   anchorPoints = {}

//   if( game.offensiveTeam=="teamA" ){
//     offensiveTeam = "teamA"
//     defensiveTeam = "teamB"
//   }else{
//     offensiveTeam = "teamB"
//     defensiveTeam = "teamA"
//   }

//   //iterating all players to update their position
//   $(".players").each(function() {  //in the future we need to use game.players...

//     var playerId = $(this).attr("id")
//     var playerPosition = getPositionOfPlayerPerId( playerId )

//     if( $(this).hasClass(defensiveTeam) ){
//       pos = getPositionFieldForPosition( playerPosition )
//       anchorPoints[playerPosition] = pos
//     }else{
//       anchorPoints[playerPosition] = undefined
//     }

//   });

//   return anchorPoints

// }



// ////////////////////////////////////////////////////////////////////
// function getAnchorPoint( playerPos ){
//   return anchorPoints[playerPos]
// }



// ////////////////////////////////////////////////////////////////////
// function setAnchorPoint( playerPos, positions ){
//   anchorPoints[playerPos] = positions
// }



// /////////////////////////////////////////////////////////////////////
// //method that updates the anchor points for the players
// function updateAnchorPoints(){

//   a=0

// }




// ///////////////////////////////////////////////////////////////
// // method that converts from castesian coordinates to polar
// function cartesianToPolar( x, y ){

//     amp = Math.sqrt( (x-50)*(x-50) + y*y )
//     phase = Math.acos( (x-50)/amp )

//     return {
//       "amp":amp,
//       "phase":phase
//     }

// }



// ///////////////////////////////////////////////////////////////////
// // method that converts from polar to cartesian coordinates
// function polarToCartesians( amp, phase ){

//   x = amp*Math.cos( phase ) + 50
//   y = amp*Math.sin( phase )


//   return {
//     "x":x,
//     "y":y
//   }

// }



// //
