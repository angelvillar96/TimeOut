///////////////////////////////////////////////////////////////////////////
// GAME SETTINGS
///////////////////////////////////////////////////////////////////////////

var BASELINE_SPEED = 1000;

//game settings with default values
var GAME_SETTINGS = {      //all of these need to be strings
  display_tutorial: "true",
  playSpeed: "1000",
  lineThickness: "4",
  memoryArrows: "6",
  showTraces: "true",
  drawDefensivePairs: "false",
  automaticDefenseMovement: "false",
  beta_version_finished: "false",

  teamA: "0000",           //code of team A
  teamB: "0001",           //code of team B

  fieldColor: "0001",
  layout: "HalfVertOffense", //halfVertOffense
  orientation: "0",          //0 or 180
  halfFull: "HalfHoriz",      //can be HalfVert, HalfHoriz, Full
  bench: "Left",             //can be Left or Right
  offensiveSystem: "offensive33", //offensive33
  defensiveSystem: "defense60",   //defense60

  favPlay1: "",
  favPlay2: "",
  favPlay3: "",
  favPlay4: "",
  favPlay5: "",
};


var confirmation_required = ""


//////////////////////////////////////////////////////////////////////////
//method that removes all the local window.localStorage user settings
function deleteLocalStorage(){

  GAME_SETTINGS.playSpeed = "";
  GAME_SETTINGS.memoryArrows = "";
  GAME_SETTINGS.lineThickness= "";
  GAME_SETTINGS.display_tutorial = "";
  GAME_SETTINGS.showTraces = "";
  GAME_SETTINGS.drawDefensivePairs = "";
  GAME_SETTINGS.automaticDefenseMovement = "";
  GAME_SETTINGS.beta_version_finished = "";

  GAME_SETTINGS.teamA = "";
  GAME_SETTINGS.teamB = "";
  GAME_SETTINGS.fieldColor = "";
  GAME_SETTINGS.orientation = "";
  GAME_SETTINGS.halfFull = "";
  GAME_SETTINGS.bench = "";

  GAME_SETTINGS.favPlay1 = "";
  GAME_SETTINGS.favPlay2 = "";
  GAME_SETTINGS.favPlay3 = "";
  GAME_SETTINGS.favPlay4 = "";
  GAME_SETTINGS.favPlay5 = "";

  GAME_SETTINGS.offensiveSystem = "";
  GAME_SETTINGS.defensiveSystem = "";
  GAME_SETTINGS.layout = "";

}



///////////////////////////////////////////////////////////////////////////////////////////
//method that checks if user settins exist, and if not fills them with the default settings
function checkAndLoadData(){


  if( GAME_SETTINGS.beta_version_finished=="" ){
    GAME_SETTINGS.beta_version_finished = GAME_SETTINGS.beta_version_finished;
  }
  if( GAME_SETTINGS.memoryArrows=="" ){
    GAME_SETTINGS.memoryArrows = GAME_SETTINGS.memoryArrows;
  }
  if( GAME_SETTINGS.lineThickness=="" ){
    GAME_SETTINGS.lineThickness = GAME_SETTINGS.lineThickness;
  }
  if( GAME_SETTINGS.playSpeed=="" ){
    GAME_SETTINGS.playSpeed = GAME_SETTINGS.playSpeed;
  }
  if( GAME_SETTINGS.display_tutorial == "" ){
    GAME_SETTINGS.display_tutorial = GAME_SETTINGS.display_tutorial;
  }
  if( GAME_SETTINGS.drawDefensivePairs == "" ){
    GAME_SETTINGS.drawDefensivePairs = GAME_SETTINGS.drawDefensivePairs;
  }
  if( GAME_SETTINGS.showTraces == "" ){
    GAME_SETTINGS.showTraces = GAME_SETTINGS.showTraces;
  }
  if( GAME_SETTINGS.automaticDefenseMovement == "" ){
    GAME_SETTINGS.automaticDefenseMovement = GAME_SETTINGS.automaticDefenseMovement;
  }
  if( GAME_SETTINGS.teamA == "" ){
    GAME_SETTINGS.teamA = GAME_SETTINGS.teamA;
  }
  if( GAME_SETTINGS.teamB == "" ){
    GAME_SETTINGS.teamB = GAME_SETTINGS.teamB;
  }
  if( GAME_SETTINGS.fieldColor == "" ){
    GAME_SETTINGS.fieldColor = GAME_SETTINGS.fieldColor;
  }
  if( GAME_SETTINGS.orientation == "" ){
    GAME_SETTINGS.orientation = GAME_SETTINGS.orientation;
  }
  if( GAME_SETTINGS.bench == "" ){
    GAME_SETTINGS.bench = GAME_SETTINGS.bench;
  }
  if( GAME_SETTINGS.halfFull == "" ){
    GAME_SETTINGS.halfFull = GAME_SETTINGS.halfFull;
  }
  if( GAME_SETTINGS.favPlay1 == "" ){
    GAME_SETTINGS.favPlay1 = GAME_SETTINGS.favPlay1;
  }
  if( GAME_SETTINGS.favPlay2 == "" ){
    GAME_SETTINGS.favPlay2 = GAME_SETTINGS.favPlay2;
  }
  if( GAME_SETTINGS.favPlay3 == "" ){
    GAME_SETTINGS.favPlay3 = GAME_SETTINGS.favPlay3;
  }
  if( GAME_SETTINGS.favPlay4 == "" ){
    GAME_SETTINGS.favPlay4 = GAME_SETTINGS.favPlay4;
  }
  if( GAME_SETTINGS.favPlay5 == "" ){
    GAME_SETTINGS.favPlay5 = GAME_SETTINGS.favPlay5;
  }
  if( GAME_SETTINGS.offensiveSystem == "" ){
    GAME_SETTINGS.offensiveSystem = GAME_SETTINGS.offensiveSystem;
  }
  if( GAME_SETTINGS.defensiveSystem == "" ){
    GAME_SETTINGS.defensiveSystem = GAME_SETTINGS.defensiveSystem;
  }
  if( GAME_SETTINGS.layout == "" ){
    GAME_SETTINGS.layout = GAME_SETTINGS.layout;
  }


}



////////////////////////////////////////////////////////////////////////////////////////////
//method that loads the favorite plays in the buttons and sets the correct color to the star
function loadFavoritePlays(){

    $("#favPlay1").html(GAME_SETTINGS.favPlay1)
    $("#favPlay2").html(GAME_SETTINGS.favPlay2)
    $("#favPlay3").html(GAME_SETTINGS.favPlay3)
    $("#favPlay4").html(GAME_SETTINGS.favPlay4)
    $("#favPlay5").html(GAME_SETTINGS.favPlay5)

    if( GAME_SETTINGS.favPlay1.length == 0 ){
      $("#star1").attr("src","img/Icon_Star_Gray.svg");
    }
    if( GAME_SETTINGS.favPlay2.length == 0 ){
      $("#star2").attr("src","img/Icon_Star_Gray.svg");
    }
    if( GAME_SETTINGS.favPlay3.length == 0 ){
      $("#star3").attr("src","img/Icon_Star_Gray.svg");
    }
    if( GAME_SETTINGS.favPlay4.length == 0 ){
      $("#star4").attr("src","img/Icon_Star_Gray.svg");
    }
    if( GAME_SETTINGS.favPlay5.length == 0 ){
      $("#star5").attr("src","img/Icon_Star_Gray.svg");
    }

}



////////////////////////////////////////////////////////////////////////////
// method that converts a percentage (x0.75%,..) into a usable speed (1000 ms)
function percentToSpeed( percentage ){

  var speed = BASELINE_SPEED * ( 2 - parseFloat(percentage) );
  return speed

}



////////////////////////////////////////////////////////////////////////////
// method that converts a usable speed (1000 ms) into a percentage (x0.75%,..)
function speedToPercent( speed ){

  var percentage = 2 - speed/BASELINE_SPEED
  return percentage

}



////////////////////////////////////////////////////////////////////////////
// checking if beta end data has passed
function check_beta_finished(){
  BETA_DATA = new Date(2022, 01, 01);  // till february first 2022
  var today = new Date();
  day = today.getDate()
  mm    = today.getMonth();
  yyyy  = today.getFullYear();
  cur_date = new Date(yyyy, mm ,day)
  if( BETA_DATA > cur_date){
    GAME_SETTINGS.beta_version_finished = "false"
  }else{
    GAME_SETTINGS.beta_version_finished = "true"
    $("#beta_finished_modal").modal({backdrop: 'static', keyboard: false})
    $("#beta_finished_modal").modal("show")
  }
  return
}



///////////////////////////////////////////////////////////////
// MAIN ///////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
$(document).ready(function() {
// $(document).ready(function($) {

  //auxiliar method to delete everything (should be commented)
//  window.localStorage.clear()
  //

  //empty initializing the variables
  // if( window.localStorage == null || GAME_SETTINGS.length < 1 ){
  //   deleteLocalStorage()
  // }
  // checkAndLoadData();

  check_beta_finished()

  //writeConsoleLog( window.localStorage );

  //if the app is opened for the first time, an alert is shown
//  console.log(window.localStorage)
  if( GAME_SETTINGS.display_tutorial == "true"  ){
    writeConsoleLog("\n\nWelcome to TACbrd\n\n");
    start_tacbrd_tutorial()
  }

  //loading the favorite plays in the buttons
  loadFavoritePlays()


  /////////////////////////////////////////////////////
  //method that processes clicks on the settings button
  $("#settingsButton").click(function(){
    //play speed selector
    var speed = GAME_SETTINGS.playSpeed
    var percentage = speedToPercent( speed )
    $("#PlaySpeedSelectorSlider").val(percentage);
    $("#PlaySpeedDisplay").html( "Play Speed: x" + parseFloat(Math.round(percentage * 100)) + "%");


    //arrow memory selector
    var memoryArrows = GAME_SETTINGS.memoryArrows
    if( parseInt(memoryArrows) == 9 ){
      $("#MemoryArrowsSelector").hide()
      $("#deleteArrowsCheckbox").prop('checked', false);
    }else{
      $("#MemoryArrowsSelectorSlider").val( parseInt(memoryArrows) );
      $("#MemoryArrowsDisplay").html( "Arrow Life: " + memoryArrows);
      $("#deleteArrowsCheckbox").prop('checked', true);
    }


    // Load values
    if(GAME_SETTINGS.drawDefensivePairs == "true"){
      $("#checkboxDefensivePairs").prop('checked', true);
    }
    else{
      $("#checkboxDefensivePairs").prop('checked', false);
    }


    if(GAME_SETTINGS.showTraces == "true"){
      $("#checkboxTraceLines").prop('checked', true);

      //line Thickness selector
      var lineThickness = GAME_SETTINGS.lineThickness;
      $("#LineThicknessSelectorSlider").val(lineThickness);
      $("#LineThicknessDisplay").html( "Line Thickness: " + lineThickness/2);
//      $("#deleteArrowsCheckbox").prop('checked', true);
    }
    else{
      $("#checkboxDefensivePairs").prop('checked', false);
      $("#LineThicknessSelector").hide()
    }


    if(GAME_SETTINGS.automaticDefenseMovement == "true"){
      $("#checkboxAutomaticDefenseMovement").prop('checked', true);
    }
    else{
      $("#checkboxAutomaticDefenseMovement").prop('checked', false);
    }

    $('#openSettingsPopup').modal('show');

  });


  /////////////////////////////////////////////////////
  //method that updates the speed indicator while movign the speed slider
  $('#PlaySpeedSelectorSlider').on("input change", function() {

    //updating the slider display
    var value = $(this).val()
    $("#PlaySpeedDisplay").html( "Play Speed: x" + parseFloat(Math.round(value * 100)) + "%");

  });


  ///////////////////////////////////////////////////////
  // method that processes the memory arrow checkbox
  $("#deleteArrowsCheckbox").change(function() {
    if(this.checked) {
      $("#MemoryArrowsSelector").show("fast")
      var memoryArrows = $("#MemoryArrowsSelectorSlider").val();
      $("#MemoryArrowsDisplay").html( "Arrow Life: " + memoryArrows);
    }else{
      $("#MemoryArrowsSelector").hide("fast")
    }
  });


  ///////////////////////////////////////////////////////
  // method that processes the Trace Lines checkbox
  $("#checkboxTraceLines").change(function() {
    if(this.checked) {
      $("#LineThicknessSelector").show("fast")
      var lineThickness = $("#LineThicknessSelectorSlider").val();
      $("#LineThicknessDisplay").html( "Line Thickness: " + lineThickness/2);
    }else{
      $("#LineThicknessSelector").hide("fast")
    }
  });


  /////////////////////////////////////////////////////
  //method that updates the arrow life indicator while moving the slider
  $('#MemoryArrowsSelectorSlider').on("input change", function() {

    //updating the slider display
    var value = $(this).val()
    $("#MemoryArrowsDisplay").html( "Arrow Life: " +value);

  });


  /////////////////////////////////////////////////////
  //method that updates the arrow life indicator while moving the slider
  $('#LineThicknessSelectorSlider').on("input change", function() {

    //updating the slider display
    var value = $(this).val()
    $("#LineThicknessDisplay").html( "Line Thickness: " +value/2);

  });


  /////////////////////////////////////////////////////
  //method that applies the selected settings once the "Apply changes" button has been clicked
  $("#applyAppSettingsChanges").click(function(){

    //saving the play speed
    var speedPercentage = $("#PlaySpeedSelectorSlider").val()
    var actualPlaySpeed = percentToSpeed( speedPercentage )
    writeConsoleLog(actualPlaySpeed)
    GAME_SETTINGS.playSpeed = actualPlaySpeed

    //saving the arrows memory
    if($("#deleteArrowsCheckbox").is(":checked")){
      var memoryArrows = $("#MemoryArrowsSelectorSlider").val()
    }else{
      memoryArrows = 9  //9 is equivalent to infinity
    }
    GAME_SETTINGS.memoryArrows = memoryArrows


    //line thickness
    var lineThickness  = $("#LineThicknessSelectorSlider").val()
    GAME_SETTINGS.lineThickness = lineThickness


    // Checkbox defensive pairs
    writeConsoleLog("Settings for the checkbox: "+$("#checkboxDefensivePairs").is(":checked"));
    if($("#checkboxDefensivePairs").is(":checked")){
      GAME_SETTINGS.drawDefensivePairs = "true";
      // Init lines
      assignPairs(drawOffDefLines);
    }
    else{
      GAME_SETTINGS.drawDefensivePairs = "false";
      // If checkbox has been un-selected the lines have to be deleted
      deleteOffDefPairsLines();
    }

    // Checkbox for trace lines
    if($("#checkboxTraceLines").is(":checked")){
      GAME_SETTINGS.showTraces = "true";
      if($("#modeHidden").html() == "recording"){
        arrowsManager( actualPlay.snapshots, actualPlay.snapshots.length)
      }else{
        arrowsManager( actualPlay.snapshots, actualSnapshotIndex)
      }
    }
    else{
      GAME_SETTINGS.showTraces = "false";
      resetCanvas()
    }

    // Checkbox for automatic defense
    if($("#checkboxAutomaticDefenseMovement").is(":checked")){
      GAME_SETTINGS.automaticDefenseMovement = "true";
    }
    else{
      GAME_SETTINGS.automaticDefenseMovement = "false";
    }

  });


  ///////////////////////////////////////////////////
  // method that resets the players db to default
  $("#reset-players").click(function(){
    confirmation_required = "players"
    message = "Are you sure you want to reset the Tactics Database?<br><br>\
    You will lose all created players"
    $("#confirm_reset_database_message").html(message)
    $("#confirm_reset_database").modal("show")
  });


  ///////////////////////////////////////////////////
  // method that resets the teams db to default
  $("#reset-teams").click(function(){
    confirmation_required = "teams"
    message = "Are you sure you want to reset the Tactics Database?<br><br>\
    You will lose all created teams"
    $("#confirm_reset_database_message").html(message)
    $("#confirm_reset_database").modal("show")
  });


  ///////////////////////////////////////////////////
  // method that resets the players db to default
  $("#reset-tactics").click(function(){
    confirmation_required = "tactics"
    message = "Are you sure you want to reset the Tactics Database?<br><br>\
    You will lose all created tactics"
    $("#confirm_reset_database_message").html(message)
    $("#confirm_reset_database").modal("show")
  });


  ///////////////////////////////////////////////////
  // method that resets the given database after confirmation
  $("#confirmation-pressed").click(function(){

    for(var i=0; i<plays.length; i++){
      playName = plays[i].playName
      console.log("Removing play: " + playName)
      plays_db.deletePlayGivenName(playName)
    }
    plays = [];
    snapshots = [];
    moves = [];
    confirmation_required = ""

  });


  ///////////////////////////////////////////////////
  // displaying tutorial once again when button is pressed
  $("#start_tutorial").click(function(){
    $("#openSettingsPopup").modal("toggle")
    start_tacbrd_tutorial()
  });


});



//
