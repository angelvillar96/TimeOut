/*##############################################################
File that contains the necessary functions to manage the tactics
##############################################################*/

allPlays = [];
actualPlay = [];
actualSnapshot = [];
actualMoves = [];
actualSnapshotIndex = 1;
snapshotNumber = 0;

auxPlay = [];
auxSnapshots = [];
auxMoves = [];

lastSnapshotUndone = false;

$(document).ready(function() {
  // console.Warn("Play manager loads1")
// $(document).ready(function($) {

  ////////////////////////////////////////////
  //method that switches the field orientation
  $("#buttonRecord").click(function(){

    console.log("in")

    if( $("#modeHidden").html() == "recording" && $("#playState").html() != "edit" ){
      alert("A tactic is already being recorded");
      return
    }

    // in beta version, only 50 tactics are allowed.
    if(allPlays.length >= 50){
      $("#beta_version_modal").modal("show")
      $("#beta_version_modal_message").html("You have reached the maximum of 5 tactics.<br>Remove one before creating new ones.<br>To delete: Open Play > Long Press in one Play > Remove Play")
      return
    }

    // ball must be visible to start recording
    if($("#ball").attr("id")==undefined){
      $("#random_error_modal").modal("show")
      $("#random_error_modal_message").html("Ball must be visible to start recording")
      return
    }

    //setting an empty play to start to record on top of it //  play.id, play.playName, play.type, play.favorite, []
    actualPlay = new Play( allPlays.length + 1 , "", "", "", [], "", "")

    $("#modeHidden").html("recording");
    setState("Recording");

    movesForFirstSnapshot = []
    movesForSecondSnapshot = []
    players = 0
    playerWithBall = ""

    // Save offensive, defensive strategy and initial player with ball
    actualPlay.offensiveStrategy = game[game.offensiveTeam].offensiveTeam;
    actualPlay.defensiveStrategy = game[getDefensiveTeam()].defensiveTeam;
    //writeConsoleLog("PLAYINIT actualPlay.offensiveStrategy: "+actualPlay.offensiveStrategy+"  actualPlay.defensiveStrategy: "+actualPlay.defensiveStrategy);

    var offensivePositions = getPositionsForSystem(actualPlay.offensiveStrategy);
    var defensivePositions = getPositionsForSystem(actualPlay.defensiveStrategy);
    // var gamePositions = offensivePositions.concat(defensivePositions);
    //writeConsoleLog("PLAYINIT offensivePositions: "+offensivePositions+"defensivePositions"+defensivePositions);

    for(i=0; i<offensivePositions.length; i++){

      //we get the position and convert it to fieldPos
      var actualTop = getPlayerValuePerID(game[game.offensiveTeam].courtPlayers[offensivePositions[i]],'fieldPosY');
      var actualLeft = getPlayerValuePerID(game[game.offensiveTeam].courtPlayers[offensivePositions[i]],'fieldPosX')

      //writeConsoleLog("PLAYINIT actualTop: "+actualTop+" actualLeft: "+actualLeft);

      //creating a move for the player, which corresponds with its initial position
      player = game[game.offensiveTeam].courtPlayers[offensivePositions[i]];
      //writeConsoleLog("PLAYINIT player: "+player);

      //getting the position of the actual player (or ball)
      position = offensivePositions[i];

      playName = actualPlay.playName
      snapshotNumber = 0

      positions = []
      positions2 = []
      positions.push(actualLeft + "&&" +  actualTop)
      positions.push(actualLeft + "&&" +  actualTop)
      positions2.push(actualLeft + "&&" +  actualTop)

      //writeConsoleLog( "recorded:  " + player + "  " + team + "  " + position + "  " + pos.left + "  " + pos.top)

      lastMoveIdx = lastMoveIdx + 1
      move = new Move(lastMoveIdx, position, playName, snapshotNumber, positions )
      movesForFirstSnapshot.push( move )

      lastMoveIdx = lastMoveIdx + 1
      move2 = new Move(lastMoveIdx, position, playName, snapshotNumber+1, positions2 )
      movesForSecondSnapshot.push( move2 )

      players++

    }

    for(i=0; i<defensivePositions.length; i++){

     //we get the position and convert it to fieldPos
      var actualTop = getPlayerValuePerID(game[getDefensiveTeam()].courtPlayers[defensivePositions[i]],'fieldPosY');
      var actualLeft = getPlayerValuePerID(game[getDefensiveTeam()].courtPlayers[defensivePositions[i]],'fieldPosX')

      writeConsoleLog("PLAYINIT actualTop: "+actualTop+" actualLeft: "+actualLeft);


      //creating a move for the player, which corresponds with its initial position
      player = game[getDefensiveTeam()].courtPlayers[defensivePositions[i]];
      writeConsoleLog("PLAYINIT player: "+player);

      //getting the position of the actual player (or ball)
      position = defensivePositions[i];

      playName = actualPlay.playName
      snapshotNumber = 0

      positions = []
      positions2 = []
      positions.push(actualLeft + "&&" +  actualTop)
      positions.push(actualLeft + "&&" +  actualTop)
      positions2.push(actualLeft + "&&" +  actualTop)

      //writeConsoleLog( "recorded:  " + player + "  " + team + "  " + position + "  " + pos.left + "  " + pos.top)
      lastMoveIdx = lastMoveIdx + 1
      move = new Move(lastMoveIdx, position, playName, snapshotNumber, positions )
      movesForFirstSnapshot.push( move )

      lastMoveIdx = lastMoveIdx + 1
      move2 = new Move(lastMoveIdx, position, playName, snapshotNumber+1, positions2 )
      movesForSecondSnapshot.push( move2 )

      players++

    }

    playerWithBall = game.playerWithBall

    if( players == 0 ){
      alert( "You should have players on the field to be able to create a Tactic" )
      setState("FreeDraw");
    }

    //initializing variables to create the objects
    datetime = new Date().toLocaleString();
    //playName = "newPlay" + datetime
    playName = actualPlay.playName
    type = actualPlay.type
    favorite = "false"
    team = $("#teamA").html();
    var offensiveStrategyPlay = actualPlay.offensiveStrategy;
    var defensiveStrategyPlay = actualPlay.defensiveStrategy;

    writeConsoleLog("PLAYINIT offensiveStrategyPlay: "+offensiveStrategyPlay);

    //creating the Play and filling it with the initial parameters and with the first snapshot (initial positions)
    lastSnapshotIdx = lastSnapshotIdx + 1
    snapshot = new Snapshot(lastSnapshotIdx, snapshotNumber, playName, playerWithBall, movesForFirstSnapshot )
    actualPlay = new Play( allPlays.length + 1 , playName, type, favorite, [snapshot], offensiveStrategyPlay, defensiveStrategyPlay )
    allPlays.push(actualPlay)
    //alert("Starting to record")

    //initializing the second snapshot
    actualMoves = movesForSecondSnapshot
    lastSnapshotIdx = lastSnapshotIdx + 1
    actualSnapshot = new Snapshot(lastSnapshotIdx, snapshotNumber+1, playName, playerWithBall, movesForSecondSnapshot )

    processSnapshotDisplay()

  });



  ////////////////////////////////////////////
  //method that saves the actual tactic
  $("#buttonSave").click(function(){

    //if no play was being recorded, an error is displayed
    if( $("#modeHidden").html() != "recording" ){
      alert("No play was being recorded. Hence, nothin can be saved")
      return
    }

    //saving the last set of moves, snapshot and play
    //newSnapshot()
    actualSnapshot.moves = actualMoves;

    (actualPlay.snapshots).push(actualSnapshot)

    //if we were on FreeDrawWithBackup mode (edit mode), we save on the all plays vector
    console.log("Mode:  " + $("#playState").html() )
    if( $("#playState").html() == "edit" ){
      allPlays.push(actualPlay)
    }

    //check that things are being saved
    /*for( i=0; i<actualMoves.length; i++ ){
      writeConsoleLog( actualPlay.snapshots[1].moves[i].player )
      writeConsoleLog( actualPlay.snapshots[1].moves[i].positions )
    }*/

    //showing the popup to save the play
    $("#inputNewPlayName").val("")
    $("#infoCreateNewPlay").html("")
    $("#newPlayPopup").modal("show")


    setState("FreeDraw");

    //check of the snapshots that have been saved
    //for(q=0;q<(actualPlay.snapshots).length;q++){
    //  writeConsoleLog( q )
    //  writeConsoleLog( actualPlay.snapshots[q].playerWithBall)
    //}

  });



  /////////////////////////////////////////////
  //method that opens a previously saved tactic
  $("#buttonOpen").click(function(){

    //restarting the lists
    $("#listOfPlaysToAdd").empty();
    $("#playsAlreadySaveToBeOpened").empty();

    //if there are no tactics, display message
    if( allPlays.length == 0 ){
      $("#playsAlreadySaveToBeOpened").append("<div class='row' id='text-no-plays-device'><div class='col-11 playAttribute'>There are no tactics saved on this device</div></div> ");
    }

    displays = []

    //we iterate through all the saved plays and we display the tactics
    for( i=0 ; i<allPlays.length ; i++ ){
      name = allPlays[i].playName
      type = allPlays[i].type
      favorite = allPlays[i].favorite
      offStrategy = allPlays[i].offensiveStrategy
      defStrategy = allPlays[i].defensiveStrategy
      display = name
      idForButton = display.split(' ').join('---');
      writeConsoleLog(idForButton)
      //adding the favorite tactics at the top of the list ( .prepend appends in the first possition)
      if(favorite=="true"){
        //$(".list-group").prepend("<button type='button' id='" + idForButton + "' class='list-group-item list-group-item-action playButton' data-dismiss='modal'>" + display + "<img src='img/Icon_Favorite.svg' width='20' height='20'/></button> ");
        $("#playsAlreadySaveToBeOpened").append("<button type='button' id='" + idForButton + "' class='list-group-item list-group-item-action playButton mt-3' data-dismiss='modal'><div class='row'><div class='col-8 col-md-5 playAttribute'>" + display + "<img src='img/Icon_Favorite.svg' width='20' height='20'/></div><div class='col-4 col-md-2 align-middle playAttribute'>"+type+"</div><div class='col-2 align-middle playAttribute'>"+getTacticReadableName(offStrategy)+"</div><div class='col-2 align-middle playAttribute'>"+getTacticReadableName(defStrategy)+"</div></div></button>");
        $("#"+idForButton).addClass("favoriteButton")
      }
      //append non-favorite tactics
      else{
        $(".list-group").append("<button type='button' id='" + idForButton + "' class='list-group-item list-group-item-action playButton mt-3' data-dismiss='modal'><div class='row'><div class='col-8 col-md-5 playAttribute'>" + display + "</div><div class='col-4 col-md-2 align-middle playAttribute'>"+type+"</div><div class='col-2 align-middle openPlayPopupOff playAttribute'>"+getTacticReadableName(offStrategy)+"</div><div class='col-2 align-middle openPlayPopupDef playAttribute'>"+getTacticReadableName(defStrategy)+"</div></div></button>");
      }
      displays.push(idForButton)
    }


    for(  i=0; i<displays.length; i++){

      //creating the event listener for a normal click to open the tactic
      document.getElementById(displays[i]).addEventListener("click", function(){
        loadTactic( $(this).attr("id") );
      }, false);

      //creating the event to edit the tactic: edit name, edit type, delete tactic
      id = document.getElementById(displays[i]).id
      writeConsoleLog( $("#"+id) )
      $("#"+id).bind( "taphold", editPlayMenu);

    }

  });



  /////////////////////////////////////////////
  //method that plays the open tactic
  $("#buttonPlay").click(function(){

    //if the play is recently opened, we play from the start
    if( $("#playState").html() == "playing" || $("#playState").html() == "opened" || $("#playState").html() == "paused" || $("#playState").html() == "stopped" ){
      //we call the carryOutMoveForward in sequential mode (true) starting from snapshot 1 (snapshot 0 is just for positioning)
      $("#playState").html("playing")
      carryOutMoveForward( actualSnapshotIndex, true )
      $("#buttonPause").show();
      $("#buttonPlay").hide();
    }
    else{
      alert("There is no tactic loaded.")
    }

    console.log("playState:" + $('#playState').html());

  });



  ////////////////////////////////////////////////////////////////
  //method that pauses the execution of the play in continuous mode
  $("#buttonPause").click(function(){

    //if the play is recently opened, we play from the start
    if( $("#playState").html() == "playing" ){
      $("#playState").html("paused")
      processSnapshotDisplay()
      $("#buttonPause").hide();
      $("#buttonPlay").show();
    }
    else if( $("#playState").html() == "paused" ){
      writeConsoleLog("Execution is already paused.")
    }

    console.log("playState:" + $('#playState').html());

  });



  ////////////////////////////////////////////////////////////////
  //method that stops the execution and sends it to snapshot 1
  $("#buttonStop").click(function(){

    if($("#playState").html() == "playing" ){
      $("#playState").html("paused")
      processSnapshotDisplay()
      $("#buttonPause").hide();
      $("#buttonPlay").show();
      console.log("playState:" + $('#playState').html());
    }
    else if( $("#playState").html() != "noPlay" ){//if there is a tactic loaded, it sends back to snapshot1

      //if we are already on the first snapshot, we go to no-play mode
      if( actualSnapshotIndex<=1 ){
        $("#playState").html("noPlay")
        setState("FreeDraw");
        processSnapshotDisplay()
        resetCanvas()
        return
      }

      //changing the play state to stopped
      $("#playState").html("stopped")
      $("#buttonPause").hide();
      $("#buttonPlay").show();
      console.log("playState:" + $('#playState').html());
      actualSnapshotIndex = 1
      loadTactic( actualPlay.playName )
    }

    if( $("#modeHidden").html() == "recording" ){

      //if we were recording, we pop the cancelled play from the array
      allPlays = allPlays.splice( 0 , allPlays.length-1)

      $("#playState").html("noPlay")
      setState("FreeDraw");
      processSnapshotDisplay()
      resetCanvas()
      return
    }

  });



  ///////////////////////////////////////////////
  //method that undos the last snapshot recorded
  $("#buttonUndo").click(function(){

    //if no play is being recorded, an error is displayed
    if( $("#modeHidden").html() != "recording" ){
      alert("No play was being recorded. Hence, nothin can be saved")
      return
    }

    writeConsoleLog("Undoing last snapshot\n");

    //if we are on the first snapshot, nothing can be undone
    if( actualPlay.snapshots.length < 2 ){
      writeConsoleLog("everything possible undone")
      return;
    }

    //deleting the last snapshot from the list of snapshots
    actualPlay.snapshots = actualPlay.snapshots.slice( 0 , actualPlay.snapshots.length-1 );

    snapshotNumber -= 1;
    processSnapshotDisplay()

    //getting the last moves for the players
    var moves = actualPlay.snapshots[ actualPlay.snapshots.length - 1 ].moves;

    //getting the variables for the player with the ball and whether to undo a pass or movement
    var playerWithBallInThisSnapshot = actualPlay.snapshots[ actualPlay.snapshots.length - 1 ].playerWithBall;

    var doneRemoving = false
    var doneAdding = false

    //iteratiing over all moves in the snapshot
    for( i=0; i<moves.length; i++){

      //obtaining the player to process
      playerPosition = moves[i].position;

      var playerID = $(".position"+playerPosition).attr("id")


      if(playerPosition=="ball"){
        pos = moves[i].positions[ moves[i].positions.length-1 ]
        continue;
      }

      //getting the last position from last snapshot to set the player
      pos = moves[i].positions[ moves[i].positions.length-1 ]
      setPositionLeft = pos.split("&&")[0]
      setPositionTop = pos.split("&&")[1]

      //converting the serialized position to display coordinates
      pos = fieldPos2displayPos( setPositionLeft, setPositionTop, getCurrentLayout() )

      //adding the ball to the correct player
      if( playerPosition == game.playerWithBall && doneRemoving == false ){
        doneRemoving = true
      }
      if( playerPosition == playerWithBallInThisSnapshot && doneAdding == false ){
        doneAdding = true
        nextPlayerWithBall = playerPosition
        $( ".ball" ).animate({
          top: pos.top,
          left: pos.left
        },0);
      }

      //moving the player to the correct position
      $( ".position" + playerPosition ).animate({
        top: pos.top,
        left: pos.left
      },0);

    }


    game.playerWithBall = nextPlayerWithBall
    actualMoves = actualPlay.snapshots[actualPlay.snapshots.length-1].moves
    actualSnapshot = actualPlay.snapshots[actualPlay.snapshots.length-1]

    //redoing the arrows
    arrowsManager(actualPlay.snapshots, actualPlay.snapshots.length);

    var newData = initializeEmptySnapshot()
    actualSnapshot = newData.newSnapshot
    actualMoves = newData.newMoves


  });



  /////////////////////////////////////////////
  $("#buttonForwards").click(function() {

    //if the play is recently opened, we play from the start
    if( $("#playState").html() == "playing" || $("#playState").html() == "opened" || $("#playState").html() == "paused" || $("#playState").html() == "stopped" ){

      $("#playState").html("playing")
      $("#buttonPause").hide();
      $("#buttonPlay").show();
      snapshots = actualPlay.snapshots

      writeConsoleLog( "actual snapshot index:  " + actualSnapshotIndex )
      writeConsoleLog( "number of snapshots: " + snapshots.length )
      writeConsoleLog( "playerWithTheBall:  " +  snapshots[ actualSnapshotIndex ].playerWithBall )

      if( actualSnapshotIndex == snapshots.length-1 ){
        //alert("All snapshots have already been shown")
        return
      }

      idPlayerWithBall = game.playerWithBall
      writeConsoleLog("removing the ball from:  " + idPlayerWithBall)
      playerWithBall = snapshots[ actualSnapshotIndex ].playerWithBall
      writeConsoleLog("adding the ball to:  " + playerWithBall)
      game.playerWithBall = playerWithBall

      //carrying out the move
      //removePlayerArrows()
      carryOutMoveForward( actualSnapshotIndex )

      //actualizing the actual snapshot index
      if( actualSnapshotIndex < snapshots.length-1 ){
        actualSnapshotIndex += 1
        $('#snapshotDisplay').val( actualSnapshotIndex );
      }

    }
    else{
      alert("There is no tactic loaded.")
    }



  });



  /////////////////////////////////////////////
  $("#buttonBackwards").click(function() {

    //if the play is recently opened, we play from the start
    if( $("#playState").html() == "playing" || $("#playState").html() == "opened" || $("#playState").html() == "paused" || $("#playState").html() == "stopped" ){

      $("#playState").html("playing")
      $("#buttonPause").hide();
      $("#buttonPlay").show();
      snapshots = actualPlay.snapshots
      if( actualSnapshotIndex == 1 ){
        //alert("First snapshot is shown. Not possible to go backwards")
        return
      }

      if( actualSnapshotIndex == snapshots.length ){
        actualSnapshotIndex -= 0
      }

      //actualizing the actual snapshot index
      if( actualSnapshotIndex > 1 ){
        actualSnapshotIndex -= 1
        $('#snapshotDisplay').val( actualSnapshotIndex );
      }

      writeConsoleLog( actualSnapshotIndex )
      writeConsoleLog( snapshots.length )
      writeConsoleLog( "playerWithTheBall:  " +  snapshots[ actualSnapshotIndex ].playerWithBall )

      playerWithBall = game.playerWithBall
      writeConsoleLog("removing the ball from:  " + playerWithBall)
      playerWithBall = snapshots[ actualSnapshotIndex-1 ].playerWithBall
      writeConsoleLog("adding the ball to:  " + playerWithBall)
      game.playerWithBall = playerWithBall

      //carrying out the move
      // removePlayerArrows()
      carryOutMoveBackwards( actualSnapshotIndex )



    }
    else{
      alert("There is no tactic loaded.")
    }



  });



  ////////////////////////////////////////////////////////////////////
  //function that goes to a given snapshot when moving the range input
  // NOT IN USE ANYMORE
  $('#snapshotDisplay').on("change", function() {

    index = $('#snapshotDisplay').val()

    if( actualSnapshotIndex == index || actualPlay.length==0 ){
      return
    }

    $(".teamA").remove();
    $(".teamB").remove();
    RemoveBall();

    targetPositions =  snapshots[ index ].moves

    //load every player in the position
    for( k=0 ; k<targetPositions.length ; k++ ){
      //getting the data about the current element
      player = targetPositions[k].player
      position = targetPositions[k].position
      setPosition = targetPositions[k].positions[0]
      setPositionLeft = setPosition.split("&&")[0]
      setPositionTop = setPosition.split("&&")[1]
      writeConsoleLog("iteration: " + k + "  " + position + "  " + setPositionLeft + "  " + setPositionTop )

      //checking if it is the ball or the player
      if( position == "ball" ){
        asignBall( setPositionLeft, setPositionTop )
      }else{
        setPlayer(team, position, [setPositionTop, setPositionLeft], getCurrentLayout());
      }

      //asign the ball to the corresponding player
      if( snapshots[0].playerWithBall == getPositionOfPlayerPerId(player) ){
        game.playerWithBall = getPositionOfPlayerPerId(player)
      }

    }

    // setOrientation();

  });



  /////////////////////////////////////////////////
  //Button that cancels the new play popup
  $("#cancelNewPlayPopup").click(function() {
    $('#newPlayPopup').modal('show');
    return
  });



  /////////////////////////////////////////
  //Button set position first on the new play popup
  $("#setPosNewPlayPopup").click(function() {

    playName = $("#inputNewPlayName").val()
    if( $("#inlineRadio1").prop('checked') ){
      type = "offense"
    }else if( $("#inlineRadio2").prop('checked') ){
      type = "defense"
    }else{
      type = "all"
    }

    if( playName=="" ){
      actualPlay = new Play( allPlays.length + 1 , team, "", "", [] )
      $('#newPlayPopup').modal('hide');
      alert("ERROR: No name given to the tactic")
      return
    }

    actualPlay = new Play( allPlays.length + 1 , playName, type, [] )
    $('#newPlayPopup').modal('hide');
    return

  });



  ////////////////////////////////////////
  //Button that starts saving the new play
  $("#startNewPlayPopup").click(function() {

    playName = $("#inputNewPlayName").val()
    if( playName == "" ){
      $("#infoCreateNewPlay").html("Please introduce a play name")
      return;
    }
    //checking if a play with that name already exists
    else{
      for(i=0;i<allPlays.length;i++){
        if(playName == allPlays[i].playName){
          $("#infoCreateNewPlay").html("A play with that name already exists")
          return;
        }
      }
    }

    // accepting only alphanumeric, spaces and underscores
    if( parse_play_name(playName, "#infoCreateNewPlay")==false ){
      return true
    }


    if( $("#inlineRadio1").prop('checked') ){
      type = "offense"
    }else if( $("#inlineRadio2").prop('checked') ){
      type = "defense"
    }else{
      type = "all"
    }

    writeConsoleLog("Playname:  " + playName)
    writeConsoleLog("Type: " + type)
    actualPlay.playName = playName
    actualPlay.type = type
    actualPlay.favorite = "false"

    //saving the playName into moves and snapshots too
    for( i=0;i<actualPlay.snapshots.length;i++ ){
      actualPlay.snapshots[i].playName = playName;
      for( j=0;j<actualPlay.snapshots[i].moves.length;j++ ){
        actualPlay.snapshots[i].moves[j].playName = playName;
      }
    }

    //including the tactic in the list and in the database
    for( i=0 ; i<allPlays.length ; i++){
      if( allPlays[i].playName == actualPlay.playName ){
        allPlays[i] = actualPlay
        //alert("play was saved in file: " + actualPlay.playName + "  !!")
        break
      }
    }

    // savePlayInDatabase( actualPlay )
    plays_db.savePlayInDatabase(actualPlay)

    $('#newPlayPopup').modal('hide');
    setState("FreeDraw");
    $("#snapshotDisplay").html("Free-Draw");
    return



  });



  /////////////////////////////////////////////////////////////////////
  //button that re-saves the information of a tactic after being edited
  $("#saveEditPlayPopup").click(function() {


    //obtaining the new values for the tactic variables
    oldPlayName = $("#editPlayModalTitle").text().split(": ")[1]

    playName = $("#inputEditPlayName").val()
    if( playName == "" ){
      $("#infoEditPlay").html("Please introduce a play name")
      writeConsoleLog("Please introduce a play name")
      return;
    }
    //checking if a play with that name already exists
    else{
      for(i=0;i<allPlays.length;i++){
        if(playName == allPlays[i].playName){
          writeConsoleLog("A play with that name already exists")
          $("#infoEditPlay").html("A play with that name already exists")
          return;
        }
      }
    }

    // accepting only alphanumeric, spaces and underscores
    if( parse_play_name(playName, "#infoEditPlay")==false ){
      return true
    }

    writeConsoleLog( "playName " + playName )

    if( $("#inlineRadio1Edit").prop('checked') ){
      playType = "offense"
    }else if( $("#inlineRadio2Edit").prop('checked') ){
      playType = "defense"
    }else{
      playType = "all"
    }


    //iterating the vector with all plays and updating the correct tactic
    for( i=0; i<allPlays.length; i++ ){

      if( allPlays[i].playName == oldPlayName ){
        allPlays[i].playName = playName
        allPlays[i].type = playType

        snapshotsToChange = allPlays[i].snapshots

        //updating also the value of the variables in the snapshots and moves
        for( j=0; j<snapshotsToChange.length; j++ ){
          snapshotsToChange[j].playName = playName;
          movesToChange = snapshotsToChange[j].moves

          for( k=0; k<movesToChange.length; k++ ){
            movesToChange[k].playName = playName
          }
        }

        break;
      }
    }


    //checking the fav buttons and updating if necessary
    if( GAME_SETTINGS.favPlay1 == oldPlayName ){
      GAME_SETTINGS.favPlay1 = playName
      $("#favPlay1").html(playName)
    }
    else if( GAME_SETTINGS.favPlay2 == oldPlayName ){
      GAME_SETTINGS.favPlay2 = playName
      $("#favPlay1").html(playName)
    }
    else if( GAME_SETTINGS.favPlay3 == oldPlayName ){
      GAME_SETTINGS.favPlay3 = playName
      $("#favPlay1").html(playName)
    }
    else if( GAME_SETTINGS.favPlay4 == oldPlayName ){
      GAME_SETTINGS.favPlay4 = playName
      $("#favPlay1").html(playName)
    }
    else if( GAME_SETTINGS.favPlay5 == oldPlayName ){
      GAME_SETTINGS.favPlay5 = playName
      $("#favPlay1").html(playName)
    }


    //updating the values on the database
    var db = openDatabase('timeOutDB', '1.0', 'Database for the TimeOutApp', 10 * 1024 * 1024)
    db.transaction(function (tx) {
      tx.executeSql("UPDATE Plays SET playName='" + playName + "', type='" + playType + "'  WHERE playName='" + oldPlayName + "' ")
      tx.executeSql("UPDATE Snapshots SET playName='" + playName + "' WHERE playName='" + oldPlayName + "' ")
      tx.executeSql("UPDATE Moves SET playName='" + playName + "' WHERE playName='" + oldPlayName + "' ")
    });

    writeConsoleLog( "Tactic " + oldPlayName + " successfully updated to " + playName )

    $('#editPlayPopup').modal('hide');


  });



  /////////////////////////////////////////////////////////////////////
  //button that deletes a tactic after being edited
  $("#deleteEditPlayPopup").click(function() {

    playName = $("#editPlayModalTitle").text().split(": ")[1]

    //removing the tactic from the favortites if necessary
    if( GAME_SETTINGS.favPlay1 == playName ){
      GAME_SETTINGS.favPlay1 = ""
      $("#star1").attr("src","img/Icon_Star_Gray.svg");
      $("#favPlay1").html("")
    }
    if( GAME_SETTINGS.favPlay2 == playName ){
      GAME_SETTINGS.favPlay2 = ""
      $("#star2").attr("src","img/Icon_Star_Gray.svg");
      $("#favPlay2").html("")
    }
    if( GAME_SETTINGS.favPlay3 == playName ){
      GAME_SETTINGS.favPlay3 = ""
      $("#star3").attr("src","img/Icon_Star_Gray.svg");
      $("#favPlay3").html("")
    }
    if( GAME_SETTINGS.favPlay4 == playName ){
      GAME_SETTINGS.favPlay4 = ""
      $("#star4").attr("src","img/Icon_Star_Gray.svg");
      $("#favPlay4").html("")
    }
    if( GAME_SETTINGS.favPlay5 == playName ){
      GAME_SETTINGS.favPlay5 = ""
      $("#star5").attr("src","img/Icon_Star_Gray.svg");
      $("#favPlay5").html("")
    }

    //removing the information relevant to the given tactic from the database
    plays_db.deletePlayGivenName(playName)

    //iterating the vector with all plays and deleting the correct tactic
    for( i=0; i<allPlays.length; i++ ){
      writeConsoleLog( allPlays[i].playName + "   " + playName )
      if( allPlays[i].playName == playName ){
        allPlays.splice(i,1);
        break;
      }
    }

    // var db = openDatabase('timeOutDB', '1.0', 'Database for the TimeOutApp', 10 * 1024 * 1024)
    // db.transaction(function (tx) {
    //   tx.executeSql("DELETE FROM Plays WHERE playName='" + playName + "' ")
    //   tx.executeSql("DELETE FROM Snapshots WHERE playName='" + playName + "' ")
    //   tx.executeSql("DELETE FROM Moves WHERE playName='" + playName + "' ")
    // });

    writeConsoleLog( "Tactic " + playName + " successfully removed" )
    $('#editPlayPopup').modal('hide');

    setState("FreeDraw")
    processSnapshotDisplay()
    resetCanvas()

  });



  /////////////////////////////////////////////////////////////////////
  //OLD VERSION: NOT BEING USED
  //button that adds/removes a tactic from the favorites list
  $("#addTacticToFavorites").click(function() {

    //obtaining the new values for the tactic variables
    playName = $("#editPlayModalTitle").text().split(": ")[1]
    writeConsoleLog( "playName to add/remove to favorites:  " + playName )

    //iterating the vector with all plays and updating the correct tactic
    for( i=0; i<allPlays.length; i++ ){

      if( allPlays[i].playName == playName ){
        if( allPlays[i].favorite=="true" ){
          favorite = "false"
        }else{
          favorite = "true"
        }

        allPlays[i].favorite = favorite
        break;
      }
    }

    //updating the values on the database
    var db = openDatabase('timeOutDB', '1.0', 'Database for the TimeOutApp', 10 * 1024 * 1024)
    writeConsoleLog(favorite);
    db.transaction(function (tx) {
      tx.executeSql("UPDATE Plays SET favorite='" + favorite + "' WHERE playName='" + playName + "' ")
    });

    writeConsoleLog( "Tactic " + playName + " successfully updated")

  });



  ///////////////////////////////////////////////////////////////////////
  //method that processes clicking on the favorite tactic buttons
  $(".favButtons").click(function(){

    var buttonNumber = $(this).attr("id")[ $(this).attr("id").length - 1 ]
    $("#favModalTitle").html("Favorite Play Button " + buttonNumber)
    var playName = $("#favPlay"+buttonNumber).html()

    if( playName == "" || playName.length<1 ){
      allowUserToChooseFavTactic(buttonNumber)
      $("#favPlayPopup").modal("show")
      //alert("No Tactic Saved in this button")
      return
    }

    //processSnapshotDisplay()
    $("#playState").html("opened")
    $("#buttonPause").hide();
    $("#buttonPlay").show();
    actualSnapshotIndex = 1
    setState("Playing");

    loadTactic( playName )


  });



  ///////////////////////////////////////////////////////////////////////
  //method that processes the long clicks on the favorite tactic buttons
  $(".favButtons").on("taphold",function(){

    //getting the favorite button pressed and the (if existent) id of the tactic assigned to the button
    var buttonNumber = $(this).attr("id")[ $(this).attr("id").length - 1 ]
    var tacticLoaded = $("#favPlay"+buttonNumber).html()
    $("#favModalTitle").html("Favorite Play Button " + buttonNumber)

    //case for adding a fav play to this button
    if( tacticLoaded == "" || tacticLoaded.length==0 ){

      allowUserToChooseFavTactic(buttonNumber)

    }


    //case for deleting the play saved on the button
    else{
      $("#ButtonFavEmpty").hide()
      $("#ButtonFavFull").show()

      var playName = undefined
      for( i=0;i<allPlays.length;i++ ){
        tacticLoaded = tacticLoaded.split('---').join(' ')
        if( allPlays[i].playName == tacticLoaded ){
          playName = allPlays[i].playName;
          break
        }
      }
      //$("#favPlaySaved").html("Play Assigned to this button: " + playName)
      $("#favPlaySaved").html(playName)
    }


    $("#favPlayPopup").modal("show")


  });



  //////////////////////////////////////////////////////////////
  //method that handles removing a play from the favorite button
  $("#removeFromFavorites").click(function(){

      var playName = $("#favPlaySaved").html()
      var buttonNumber = $("#favModalTitle").html()
      buttonNumber = buttonNumber[buttonNumber.length-1]
      $("#favPlay"+buttonNumber).html("")
      if( buttonNumber=="1" ){
        GAME_SETTINGS.favPlay1 = ""
        $("#star1").attr("src","img/Icon_Star_Gray.svg");
      }
      else if( buttonNumber=="2" ){
        GAME_SETTINGS.favPlay2 = ""
        $("#star2").attr("src","img/Icon_Star_Gray.svg");
      }
      else if( buttonNumber=="3" ){
        GAME_SETTINGS.favPlay3 = ""
        $("#star3").attr("src","img/Icon_Star_Gray.svg");
      }
      else if( buttonNumber=="4" ){
        GAME_SETTINGS.favPlay4 = ""
        $("#star4").attr("src","img/Icon_Star_Gray.svg");
      }
      else if( buttonNumber=="5" ){
        GAME_SETTINGS.favPlay5 = ""
        $("#star5").attr("src","img/Icon_Star_Gray.svg");
      }

    $("#homeLogo").click()
    processSnapshotDisplay()
    resetCanvas()

  });


  //////////////////////////////////////////////////////////////
  //method that handles changes on the new_play name input
  $("#inputNewPlayName").on("input", function(){

    playName = $("#inputNewPlayName").val()
    parse_play_name(playName, "#infoCreateNewPlay")

  });


  //////////////////////////////////////////////////////////////
  //method that handles changes on the edit_play name input
  $("#inputEditPlayName").on("input", function(){

    playName = $("#inputEditPlayName").val()
    parse_play_name(playName, "#infoEditPlay")

  });


});

//
