class Play{

    constructor(id, playName, type, favorite, snapshots, offensiveStrategy, defensiveStrategy){
        this.id = id;
        this.playName = playName;
        this.type = type;
        this.favorite = favorite;
        this.snapshots = snapshots;
        this.offensiveStrategy = offensiveStrategy;
        this.defensiveStrategy = defensiveStrategy;
    }


}
