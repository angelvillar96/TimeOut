function setState(targetState){

	if(targetState == "Loading"){
		$(".nav-item").addClass("disabled");
		writeConsoleLog("Loading mode has been set");
		$("#buttonPause").hide();
    $("#buttonPlay").show();
	}
	else if(targetState == "FreeDraw"){
		$(".nav-item").removeClass("disabled");
		$(".disabled-while-free-draw-mode").addClass("disabled");
		$("#modeHidden").html("normal")
		$("#buttonPause").hide();
    $("#buttonPlay").show();
		writeConsoleLog("FreeDraw mode has been set");
		$("#snapshotDisplay").html("Free Draw")
	}
	else if(targetState == "FreeDrawWithBackup"){
		$(".nav-item").removeClass("disabled");
		$(".disabled-while-free-draw-with-backup-mode").addClass("disabled");
		$("#modeHidden").html("recording")
		$("#buttonPause").hide();
    $("#buttonPlay").show();
		writeConsoleLog("FreeDrawWithBackup mode has been set");
		$("#snapshotDisplay").html("Free Draw")
	}
	else if(targetState == "Recording"){
		$(".nav-item").removeClass("disabled");
		$(".disabled-while-recording").addClass("disabled");
		$("#modeHidden").html("recording");
		$("#buttonPause").hide();
    $("#buttonPlay").show();
		writeConsoleLog("Recording mode has been set");
	}
	else if(targetState == "Playing"){
		$(".nav-item").removeClass("disabled");
		$(".disabled-while-playing").addClass("disabled");
		writeConsoleLog("Playing mode has been set");
	}

}
