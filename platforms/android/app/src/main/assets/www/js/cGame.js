class Game{

    constructor(id, teamA, teamB, offensiveTeam, playerWithBall, offDefPairsArray){
        this.id = id;
        this.teamA = teamA;
        this.teamB = teamB;
        this.offensiveTeam = offensiveTeam;
        this.playerWithBall = playerWithBall;
        this.offDefPairsArray = offDefPairsArray
    }

    getTeamA(){
      return this.teamA
    }

    getTeamB(){
      return this.teamB
    }

}
