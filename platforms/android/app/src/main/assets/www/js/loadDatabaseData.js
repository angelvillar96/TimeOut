/*########################

########################*/

var teams = [];
var games = [];
var players = [];
var actualGame = [];

var plays = [];
var snapshots = [];
var moves = [];

var lastSnapshotIdx = -1;
var lastMoveIdx = -1;

/*###################
Method for loading the data from the GAMES ObjectStore of the database
###################*/
function load_games(db){

  console.log("IN load_games");

  req = db.transaction("Games").objectStore("Games").openCursor()
  req.onsuccess = function(event) {
    var cursor = event.target.result;
    if (cursor) {
      // console.log("LOAD_GAMES Name for SSN " + cursor.key + " is " + cursor.value.teamA);
      var id = cursor.value.teamA + "&&" + cursor.value.teamB;
      var game = new Game(id, cursor.value.teamA, cursor.value.teamB);
      games.push(game)
      cursor.continue();
    }
    else {
      console.log("LOAD_GAMES No more entries!");
    }
  };

}


/*###################
Method for loading the data from the TEAMS ObjectStore of the database
###################*/
function load_teams(db){

  console.log("IN load_teams");

  db.transaction("Teams").objectStore("Teams").openCursor().onsuccess = function(event) {
    var cursor = event.target.result;
    if (cursor) {
      var actualCourtPlayers = new CourtPlayers(cursor.value.courtPlayers);
      var actualTeam = new Team( cursor.key, cursor.value.teamName, cursor.value.image_path, cursor.value.filledColorA, cursor.value.lineColorA,
                                 cursor.value.textColorA, cursor.value.filledColorB,  cursor.value.lineColorB, cursor.value.textColorB,
                                 cursor.value.offensiveTeam, cursor.value.defensiveTeam, [], actualCourtPlayers);
      teams.push(actualTeam)      
      cursor.continue();
    }
    else {
      console.log("LOAD_TEAMS No more entries!");
    }
  };
}


/*###################
Method for loading the data from the PLAYERS ObjectStore of the database
###################*/
function load_players(db){

  console.log("IN load_players");

  db.transaction("Players").objectStore("Players").openCursor().onsuccess = function(event) {
    var cursor = event.target.result;
    if (cursor) {
      // console.log("LOAD_PLAYERS Name for SSN " + cursor.key + " is " + cursor.value.teamName);
      var actualPlayer = new Player(cursor.key, cursor.value.team, cursor.value.image_path, cursor.value.number, cursor.value.playerNameLong,
                                    cursor.value.playerNameShort, cursor.value.validPositions, cursor.value.starter)
      players.push(actualPlayer)
      cursor.continue();
    }
    else {
      console.log("LOAD_PLAYERS No more entries!");
    }
  };
}


/*###################
Method for loading the data from the PLAYS ObjectStore of the database
###################*/
function load_plays(db){

  console.log("IN load_plays");

  db.transaction("Plays").objectStore("Plays").openCursor().onsuccess = function(event) {
    var cursor = event.target.result;
    if (cursor) {
      // console.log("LOAD_PLAYERS Name for SSN " + cursor.key + " is " + cursor.value.teamName);
      var actualPlay = new Play(cursor.key, cursor.value.playName, cursor.value.type, cursor.value.favorite, [],
                                cursor.value.offensiveStrategy, cursor.value.defensiveStrategy)
      plays.push(actualPlay)
      cursor.continue();
    }
    else {
      console.log("LOAD_PLAYS No more entries!");
    }
  };
}


/*###################
Method for loading the data from the SNAPSHOTS ObjectStore of the database
###################*/
function load_snapshots(db){

  console.log("IN load_snapshots");

  db.transaction("Snapshots").objectStore("Snapshots").openCursor().onsuccess = function(event) {
    var cursor = event.target.result;
    if (cursor) {
      // console.log("LOAD_PLAYERS Name for SSN " + cursor.key + " is " + cursor.value.teamName);
      var actualSnapshot = new Snapshot(cursor.key, cursor.value.snapshotNumber, cursor.value.playName, cursor.value.playerWithBall, [])
      snapshots.push(actualSnapshot)
      if(cursor.key > lastSnapshotIdx){
        lastSnapshotIdx = cursor.key
      }
      cursor.continue();
    }
    else {
      console.log("LOAD_SNAPSHOTS No more entries!");
    }
  };
}


/*###################
Method for loading the data from the MOVES ObjectStore of the database
###################*/
function load_moves(db){

  console.log("IN load_moves");

  db.transaction("Moves").objectStore("Moves").openCursor().onsuccess = function(event) {
    var cursor = event.target.result;
    if (cursor) {
      // console.log("LOAD_PLAYERS Name for SSN " + cursor.key + " is " + cursor.value.teamName);
      // console.log(cursor.value.positions)
      // positions = cursor.value.positions.split("%%")
      positions = cursor.value.positions
      var actualMove = new Move(cursor.key, cursor.value.position, cursor.value.playName, cursor.value.snapshotNumber, positions)
      moves.push(actualMove)
      if(cursor.key > lastMoveIdx){
        lastMoveIdx = cursor.key
      }

      cursor.continue();
    }
    else {
      console.log("LOAD_MOVES No more entries!");
    }
  };
}


// $(document).ready(function($) {
$(document).ready(function() {

  setState("Loading");
  $("#playState").html("noPlay")

  console.log(games)
  console.log(teams)
  console.log(players)
  console.log(plays)
  console.log(snapshots)
  console.log(moves)


  setTimeout(function(){

    writeConsoleLog("Data to be loaded into the objects");


    ////////////////////////////////////////////////////
    // LOADING DATA CORRESPONDING TO GAMES-TEAMS-PLAYERS
    ////////////////////////////////////////////////////
    console.log("Games length: "+games.length);
    for(i=0; i<games.length; i++){

      // Select teamA and teamB for the game
      var lastTeams = [];
      // try{
        lastTeams[0]=parseInt(GAME_SETTINGS.teamA);
        lastTeams[1]=parseInt(GAME_SETTINGS.teamB);
      // }
      // catch{
      //   lastTeams[0]=0;
      //   lastTeams[1]=1;
      // }

      games[i].teamA = teams[lastTeams[0]]; //teams[4];
      games[i].teamB = teams[lastTeams[1]]; //teams[6];
      games[i].offensiveTeam = "teamA";
      games[i].offDefPairsArray = [undefined, 6, 5, 3, 2, 1, 4];

      // Assign players to the teams playing the game
      for( k=0; k<players.length; k++){
        //writeConsoleLog("About to assign player: "+players[k].playerNameShort);
        if( parseInt(players[k].team) == parseInt(games[i].teamA.teamID)){
          games[i].teamA.players.push(players[k]);
          //writeConsoleLog("Assigning TeamA player: "+ players[k].playerNameShort);
        }
        else if( parseInt(players[k].team) == parseInt(games[i].teamB.teamID)){
          games[i].teamB.players.push(players[k]);
          //writeConsoleLog("Assigning TeamB player: "+players[k].playerNameShort);
        }
      }

      // Parse Information of courtPlayers and assign it to teams A and B
      // TEAM A
      // games[i].teamA.courtPlayers = deserializeCourtPlayers( games[i].teamA.courtPlayers );
      // // TEAM B
      // if(games[i].teamA.teamID != games[i].teamB.teamID){
      //     games[i].teamB.courtPlayers = deserializeCourtPlayers( games[i].teamB.courtPlayers );
      // }

      writeConsoleLog("TeamA.teamID: " + games[i].teamA.teamID + "   TeamA.teamName: " + games[i].teamA.teamName + "   TeamA.filledColorA: " + games[i].teamA.filledColorA+ "   TeamA.players.length: " + games[i].teamA.players.length+ "   TeamA.courtPlayers.DLO: " + games[i].teamA.courtPlayers.DLO);
      writeConsoleLog("TeamB.teamID: " + games[i].teamB.teamID + "   TeamB.teamName: " + games[i].teamB.teamName + "   TeamB.filledColorA: " + games[i].teamB.filledColorA+ "   TeamB.players.length: " + games[i].teamB.players.length+ "   TeamB.courtPlayers.OCB: " + games[i].teamB.courtPlayers.OCB);
      //alert("TeamA: " + games[i].teamB.players[2].playerNameShort + " " + games[i].teamB.players[2].starter);
      //alert("Ids: "+games[i].teamA.players[0].playerID + "  "+games[i].teamA.players[1].playerID + "  "+games[i].teamA.players[2].playerID + "  "+games[i].teamA.players[3].playerID);

    }


   //////////////////////////////////////////////////////
   // LOADING DATA CORRESPONDING TO PLAYS-SNAPSHOTS-MOVES
   //////////////////////////////////////////////////////
   for(i=0; i<plays.length; i++){

     writeConsoleLog( "starting procesing the play: " + plays[i].playName + "  " + plays[i].id +" "+plays[i].offensiveStrategy)

      play = plays[i]

      actualSnapshots = []

      for( j=0; j<snapshots.length; j++ ){
        if( snapshots[j].playName == play.playName ){
          actualMoves = []
          //writeConsoleLog( "adding snapshot #" + snapshots[j].snapshotNumber + "  " + snapshots[j].playName )
          for( k=0; k<moves.length; k++ ){
            //writeConsoleLog("comparing:  " + moves[k].playName + "  with  " + snapshots[j].playName)
            //writeConsoleLog("comparing:  " + moves[k].snapshotNumber + "  with  " + snapshots[j].snapshotNumber + "\n")
            if( moves[k].playName == snapshots[j].playName &&  moves[k].snapshotNumber == snapshots[j].snapshotNumber ){
              //writeConsoleLog( "adding move to " + moves[k].playName )
              actualMoves.push( moves[k] )
              //writeConsoleLog( moves[k] )
            }
          }
          snapshots[j].moves = actualMoves
          actualSnapshots.push( snapshots[j] )
        }
      }
      writeConsoleLog( actualSnapshots )
      play.snapshots = actualSnapshots

    }


    allPlays = plays

    game = games[0];
    game.playerWithBall = "OCB"; // set Ball to the center back
    actualGame = games[0];


    //setting the last layout and
    var lastLayout = "";
    //try{
    lastLayout = GAME_SETTINGS.layout;
    //}
    // catch{
    //   lastLayout = "HalfHorizOffense";
    // }
    setLayout( lastLayout, true )
    if( lastLayout.includes("Defense")){
      game.offensiveTeam = "teamB";
    }else{
      game.offensiveTeam = "teamA";
    }
    $("#offensiveTeam").text("Attacking: "+game[game.offensiveTeam].teamName);


    //Load players to the initial positions:
    if(game.offensiveTeam == "teamA"){
      setTeam("teamB", game.teamB.defensiveTeam, getCurrentLayout(), true);
      setTeam("teamA", game.teamA.offensiveTeam, getCurrentLayout(), true);
    }
    else{
      setTeam("teamB", game.teamB.offensiveTeam, getCurrentLayout(), true);
      setTeam("teamA", game.teamA.defensiveTeam, getCurrentLayout(), true);
    }
    writeConsoleLog(game)

    //assigning the favorite plays to the buttons
    assignFavPlays()
    setState("FreeDraw");

    //  initializing data saving functions
    console.log(teams_db)
    setup_data_savers()

  }, 1000);


});


///////////////////////////////////////////////////////////////
//method that assigns the fav plays to the buttons
function assignFavPlays(){

  counter = 1

  for( i=0; i<allPlays.length; i++ ){

    if (allPlays[i].favorite == "true"){
      $("#favPlay"+counter).html(allPlays[i].id)
      writeConsoleLog("assigned: " + allPlays[i].playName + " to fav button " + counter)
      writeConsoleLog( allPlays[i].id )
      counter++;
      if(counter==6){
        return
      }
    }

  }

}




//
