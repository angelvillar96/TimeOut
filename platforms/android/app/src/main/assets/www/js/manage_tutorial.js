/*
Method for managing the controls for the first use tutorial
*/

var current_tutorial_slide = 0

$(document).ready(function() {
// $(document).ready(function($) {

  // processing clicking on the forward_button
  $("#button-forward").click(function(){
    if(current_tutorial_slide==0){
      slide_1()
    }else if(current_tutorial_slide==1){
      slide_2()
    }else if(current_tutorial_slide==2){
      slide_3()
    }else if(current_tutorial_slide==3){
      slide_4()
    }else if(current_tutorial_slide==4){
      $("#FirstUsePopup").modal("toggle")
    }
  });


  // processing clicking on the forward_button
  $("#button-back").click(function(){
    if(current_tutorial_slide==1){
      slide_0()
    }else if(current_tutorial_slide==2){
      slide_1()
    }else if(current_tutorial_slide==3){
      slide_2()
    }else if(current_tutorial_slide==4){
      slide_3()
    }
  });


  $("#showTutorial").change(function() {
    console.log(this.checked)
    if(this.checked) {
      GAME_SETTINGS.display_tutorial = "true"
    }else{
      GAME_SETTINGS.display_tutorial = "false"
    }
    console.log(GAME_SETTINGS.display_tutorial)
    return
  });


  $("#disclaimerAccepted").change(function() {
    console.log(this.checked)
    if(this.checked) {
        $("#button-forward").prop("disabled",false).css('opacity',1);
    }else{
      $("#button-forward").prop("disabled",true).css('opacity',0.5);
    }
    return
  });

});



// when we start the tutorial, we load the content of the first slide and set the buttons
function slide_0(){

  current_tutorial_slide = 0

  // displaying disclaimet
  $("#show-tutorial-again").css("display", "none")
  $("#disclaimer").css("display", "none")

  // disabling backwards butto
  $("#button-back").prop("disabled",true).css('opacity',0.5);
  $("#button-forward").prop("disabled",false).css('opacity',1);
  $("#button-forward").html("<svg id='i-arrow-right' xmlns='http://www.w3.org/2000/svg' class='arrow-tutorial' viewBox='0 0 32 32' width='32'"+
    "height='32' fill='none' stroke='currentcolor' stroke-linecap='round' stroke-linejoin='round' stroke-width='5'>"+
    "<path d='M22 6 L30 16 22 26 M30 16 L2 16'/></svg>")

  // adding title
  $("#tutorial-title").removeClass("supertitle")
  $("#tutorial-title").html("Thanks for installing TACBrd!")
  $("#tutorial-subtitle").html("")
  $(".slide.modal-title").html("1/5")

  // adding bullet points
  $("#tutorial-bullet-points").html("<ul>" +
      "<li><b>Drag & drop</b> to move players</li>" +
      "<li><b>Tap</b> to pass the ball </li>" +
      "<li>Set your imagination free!</li>" +
    "</ul>")

  // adding gif
  $("#tutorial-gif").removeClass("superlogo")
  $("#tutorial-gif").attr("src", "img/tutorial/drag_and_drop.gif")

  return
  }


// loading content of slide 1
function slide_1(){

  current_tutorial_slide = 1

  // displaying disclaimet
  $("#show-tutorial-again").css("display", "none")
  $("#disclaimer").css("display", "none")

  // disabling backwards butto
  $("#button-back").prop("disabled",false).css('opacity',1);
  $("#button-forward").prop("disabled",false).css('opacity',1);
  $("#button-forward").html("<svg id='i-arrow-right' xmlns='http://www.w3.org/2000/svg' class='arrow-tutorial' viewBox='0 0 32 32' width='32'"+
    "height='32' fill='none' stroke='currentcolor' stroke-linecap='round' stroke-linejoin='round' stroke-width='5'>"+
    "<path d='M22 6 L30 16 22 26 M30 16 L2 16'/></svg>")

  // adding title
  $("#tutorial-title").removeClass("supertitle")
  $("#tutorial-title").html("Save your favortite Tactics!")
  $("#tutorial-subtitle").html("")
  $(".slide.modal-title").html("2/5")

  // adding bullet points
  $("#tutorial-bullet-points").html("<ul>" +
      "<li>Press <img src='img/Icon_Record.svg' class='d-inline-block align-top icon-in-tutorial' alt=''> to start recording</li>" +
      "<li>Move players and pass the ball</li>" +
      "<li>Press <img src='img/Icon_Save_Tutorial.svg' class='d-inline-block align-top icon-in-tutorial' alt=''> to save your tactic</li>" +
    "</ul>")

  // adding gif
  $("#tutorial-gif").removeClass("superlogo")
  $("#tutorial-gif").attr("src", "img/tutorial/save_tactic.gif")
  return
}


// loading content of slide 2
function slide_2(){

  current_tutorial_slide = 2

  // displaying disclaimet
  $("#show-tutorial-again").css("display", "none")
  $("#disclaimer").css("display", "none")

  // disabling backwards butto
  $("#button-back").prop("disabled",false).css('opacity',1);
  $("#button-forward").prop("disabled",false).css('opacity',1);
  $("#button-forward").html("<svg id='i-arrow-right' xmlns='http://www.w3.org/2000/svg' class='arrow-tutorial' viewBox='0 0 32 32' width='32'"+
    "height='32' fill='none' stroke='currentcolor' stroke-linecap='round' stroke-linejoin='round' stroke-width='5'>"+
    "<path d='M22 6 L30 16 22 26 M30 16 L2 16'/></svg>")

  // adding title
  $("#tutorial-title").removeClass("supertitle")
  $("#tutorial-title").html("Load and play Tactics!")
  $("#tutorial-subtitle").html("")
  $(".slide.modal-title").html("3/5")

  // adding bullet points
  $("#tutorial-bullet-points").html("<ul>" +
      "<li>Press <img src='img/Icon_Open_Tutorial.svg' class='d-inline-block align-top icon-in-tutorial' alt=''> to load your tactics</li>" +
      "<li>Click <img src='img/Icon_Play_Tutorial.svg' class='d-inline-block align-top icon-in-tutorial' alt=''> to play your tactic</li>" +
      "<li>Use <img src='img/Icon_Forwards_Tutorial.svg' class='d-inline-block align-top icon-in-tutorial' alt=''>" +
      "or <img src='img/Icon_Backwards_Tutorial.svg' class='d-inline-block align-top icon-in-tutorial' alt=''> to go step by step</li>" +
    "</ul>")

  // adding gif
  $("#tutorial-gif").removeClass("superlogo")
  $("#tutorial-gif").attr("src", "img/tutorial/load_tactic.gif")
  return
}


// loading content of slide 3
function slide_3(){

  current_tutorial_slide = 3

  // displaying disclaimet
  $("#show-tutorial-again").css("display", "none")
  $("#disclaimer").css("display", "none")

  // disabling backwards butto
  $("#button-back").prop("disabled",false).css('opacity',1);
  $("#button-forward").prop("disabled",false).css('opacity',1);
  $("#button-forward").html("<svg id='i-arrow-right' xmlns='http://www.w3.org/2000/svg' class='arrow-tutorial' viewBox='0 0 32 32' width='32'"+
    "height='32' fill='none' stroke='currentcolor' stroke-linecap='round' stroke-linejoin='round' stroke-width='5'>"+
    "<path d='M22 6 L30 16 22 26 M30 16 L2 16'/></svg>")

  // adding title
  $("#tutorial-title").removeClass("supertitle")
  $("#tutorial-title").html("Set up your Favorite Layout!")
  $("#tutorial-subtitle").html("")
  $(".slide.modal-title").html("4/5")

  // adding bullet points
  $("#tutorial-bullet-points").html("<ul>" +
      "<li>Use <img src='img/Icon_OD_Tutorial.svg' class='d-inline-block align-top icon-in-tutorial' alt=''>, " +
      "<img src='img/Icon_RO_Tutorial.svg' class='d-inline-block align-top icon-in-tutorial' alt=''>, " +
      "<img src='img/Icon_HF_Tutorial.svg' class='d-inline-block align-top icon-in-tutorial' alt=''> to change the layout</li>" +
      "<li>Click <img src='img/Icon_OF_Tutorial.svg' class='d-inline-block align-top icon-in-tutorial' alt=''> to change the offense</li>" +
      "<li>Click <img src='img/Icon_DF_Tutorial.svg' class='d-inline-block align-top icon-in-tutorial' alt=''> to change the defense</li>" +
    "</ul>")

  // adding gif
  $("#tutorial-gif").removeClass("superlogo")
  $("#tutorial-gif").attr("src", "img/tutorial/layout.gif")

  return
}


// loading content of slide 4
function slide_4(){

  current_tutorial_slide = 4

  // displaying disclaimet
  $("#show-tutorial-again").css("display", "inline")
  $("#disclaimer").css("display", "inline")

  if(GAME_SETTINGS.display_tutorial == "true"){
    $("#showTutorial").attr("checked", true)
  }else{
    $("#showTutorial").attr("checked", false)
  }

  // disabling backwards button
  $("#button-back").prop("disabled",false).css('opacity',1);

  // deactivating the forward button if disclaimer checkbox is not activated
  if($("#disclaimerAccepted").prop("checked")==false){
    $("#button-forward").prop("disabled",true).css('opacity',0.5);
  }else{
    $("#button-forward").prop("disabled",false).css('opacity',1);
  }
  $("#button-forward").html("<img src='img/Icon_Handball.svg' class='d-inline-block align-top ball-icon' alt=''>")

  // adding title
  $("#tutorial-title").addClass("supertitle")
  $("#tutorial-title").html("TACBrd")
  $("#tutorial-subtitle").html("Step-up your timeout game!")
  $(".slide.modal-title").html("5/5")

  // adding bullet points
  $("#tutorial-bullet-points").html("")

  // adding gif
  $("#tutorial-gif").addClass("superlogo")
  $("#tutorial-gif").attr("src", "img/timeout_logo_black_white.svg")

  return
}


//
function start_tacbrd_tutorial(){

  if(GAME_SETTINGS.display_tutorial == "true"){
    $("#close-tutorial-modal").attr("disabled", true)
    $("#close-tutorial-modal").css("visibility", "hidden")
    $('#FirstUsePopup').modal({
      backdrop: 'static',
      keyboard: false
    })
  }else{
    $("#close-tutorial-modal").attr("disabled", false)
    $("#close-tutorial-modal").css("visibility", "visible")
  }

  slide_0()
  $("#FirstUsePopup").modal("toggle")
}



//
